// package: fcp.accounting.v1
// file: v1/auth_common/dtos.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_auth_common_common_pb from "../../v1/auth_common/common_pb";

export class User extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getStatus(): number;
  setStatus(value: number): void;

  getLogin(): string;
  setLogin(value: string): void;

  getPhoneNumber(): string;
  setPhoneNumber(value: string): void;

  getFirstName(): string;
  setFirstName(value: string): void;

  getLastName(): string;
  setLastName(value: string): void;

  getMiddleName(): string;
  setMiddleName(value: string): void;

  getEmail(): string;
  setEmail(value: string): void;

  getLatitude(): number;
  setLatitude(value: number): void;

  getLongitude(): number;
  setLongitude(value: number): void;

  getAddress(): string;
  setAddress(value: string): void;

  getCountryCode(): string;
  setCountryCode(value: string): void;

  getLanguageCode(): string;
  setLanguageCode(value: string): void;

  getRole(): string;
  setRole(value: string): void;

  getBusinessName(): string;
  setBusinessName(value: string): void;

  getBusinessAddress(): string;
  setBusinessAddress(value: string): void;

  getChangePassword(): boolean;
  setChangePassword(value: boolean): void;

  getVerified(): boolean;
  setVerified(value: boolean): void;

  getCreditApproved(): boolean;
  setCreditApproved(value: boolean): void;

  getCommoditiesAccess(): boolean;
  setCommoditiesAccess(value: boolean): void;

  getReferralUrl(): string;
  setReferralUrl(value: string): void;

  getReferralUrlIos(): string;
  setReferralUrlIos(value: string): void;

  hasDealer(): boolean;
  clearDealer(): void;
  getDealer(): Dealer | undefined;
  setDealer(value?: Dealer): void;

  clearContactsList(): void;
  getContactsList(): Array<string>;
  setContactsList(value: Array<string>): void;
  addContacts(value: string, index?: number): string;

  hasTaxInfo(): boolean;
  clearTaxInfo(): void;
  getTaxInfo(): TaxInfo | undefined;
  setTaxInfo(value?: TaxInfo): void;

  hasBusinessInfo(): boolean;
  clearBusinessInfo(): void;
  getBusinessInfo(): BusinessInfo | undefined;
  setBusinessInfo(value?: BusinessInfo): void;

  getState(): number;
  setState(value: number): void;

  getNote(): string;
  setNote(value: string): void;

  hasUpdated(): boolean;
  clearUpdated(): void;
  getUpdated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdated(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getUpdatedBy(): string;
  setUpdatedBy(value: string): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreated(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getCreatedBy(): string;
  setCreatedBy(value: string): void;

  hasSupplier(): boolean;
  clearSupplier(): void;
  getSupplier(): UserBriefInfo | undefined;
  setSupplier(value?: UserBriefInfo): void;

  hasReferralUser(): boolean;
  clearReferralUser(): void;
  getReferralUser(): UserBriefInfo | undefined;
  setReferralUser(value?: UserBriefInfo): void;

  clearContactPersonsList(): void;
  getContactPersonsList(): Array<ContactPerson>;
  setContactPersonsList(value: Array<ContactPerson>): void;
  addContactPersons(value?: ContactPerson, index?: number): ContactPerson;

  clearLocationsList(): void;
  getLocationsList(): Array<Location>;
  setLocationsList(value: Array<Location>): void;
  addLocations(value?: Location, index?: number): Location;

  clearDocumentsList(): void;
  getDocumentsList(): Array<Document>;
  setDocumentsList(value: Array<Document>): void;
  addDocuments(value?: Document, index?: number): Document;

  clearEmployeesList(): void;
  getEmployeesList(): Array<User>;
  setEmployeesList(value: Array<User>): void;
  addEmployees(value?: User, index?: number): User;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): User.AsObject;
  static toObject(includeInstance: boolean, msg: User): User.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: User, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): User;
  static deserializeBinaryFromReader(message: User, reader: jspb.BinaryReader): User;
}

export namespace User {
  export type AsObject = {
    id: string,
    status: number,
    login: string,
    phoneNumber: string,
    firstName: string,
    lastName: string,
    middleName: string,
    email: string,
    latitude: number,
    longitude: number,
    address: string,
    countryCode: string,
    languageCode: string,
    role: string,
    businessName: string,
    businessAddress: string,
    changePassword: boolean,
    verified: boolean,
    creditApproved: boolean,
    commoditiesAccess: boolean,
    referralUrl: string,
    referralUrlIos: string,
    dealer?: Dealer.AsObject,
    contactsList: Array<string>,
    taxInfo?: TaxInfo.AsObject,
    businessInfo?: BusinessInfo.AsObject,
    state: number,
    note: string,
    updated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy: string,
    created?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    createdBy: string,
    supplier?: UserBriefInfo.AsObject,
    referralUser?: UserBriefInfo.AsObject,
    contactPersonsList: Array<ContactPerson.AsObject>,
    locationsList: Array<Location.AsObject>,
    documentsList: Array<Document.AsObject>,
    employeesList: Array<User.AsObject>,
  }
}

export class UserFilter extends jspb.Message {
  hasRole(): boolean;
  clearRole(): void;
  getRole(): google_protobuf_wrappers_pb.StringValue | undefined;
  setRole(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUserId(): boolean;
  clearUserId(): void;
  getUserId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUserId(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): v1_auth_common_common_pb.TimeRange | undefined;
  setCreated(value?: v1_auth_common_common_pb.TimeRange): void;

  hasLocked(): boolean;
  clearLocked(): void;
  getLocked(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setLocked(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasLogin(): boolean;
  clearLogin(): void;
  getLogin(): google_protobuf_wrappers_pb.StringValue | undefined;
  setLogin(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasSupplierId(): boolean;
  clearSupplierId(): void;
  getSupplierId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setSupplierId(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasVerified(): boolean;
  clearVerified(): void;
  getVerified(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setVerified(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasBusinessName(): boolean;
  clearBusinessName(): void;
  getBusinessName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setBusinessName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreditApproved(): boolean;
  clearCreditApproved(): void;
  getCreditApproved(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setCreditApproved(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCommoditiesAccess(): boolean;
  clearCommoditiesAccess(): void;
  getCommoditiesAccess(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setCommoditiesAccess(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasReferralUserId(): boolean;
  clearReferralUserId(): void;
  getReferralUserId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setReferralUserId(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserFilter.AsObject;
  static toObject(includeInstance: boolean, msg: UserFilter): UserFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UserFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserFilter;
  static deserializeBinaryFromReader(message: UserFilter, reader: jspb.BinaryReader): UserFilter;
}

export namespace UserFilter {
  export type AsObject = {
    role?: google_protobuf_wrappers_pb.StringValue.AsObject,
    userId?: google_protobuf_wrappers_pb.StringValue.AsObject,
    created?: v1_auth_common_common_pb.TimeRange.AsObject,
    locked?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    login?: google_protobuf_wrappers_pb.StringValue.AsObject,
    supplierId?: google_protobuf_wrappers_pb.StringValue.AsObject,
    verified?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    businessName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    creditApproved?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    commoditiesAccess?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    referralUserId?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class Dealer extends jspb.Message {
  getIsDealer(): boolean;
  setIsDealer(value: boolean): void;

  getDiscount(): number;
  setDiscount(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Dealer.AsObject;
  static toObject(includeInstance: boolean, msg: Dealer): Dealer.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Dealer, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Dealer;
  static deserializeBinaryFromReader(message: Dealer, reader: jspb.BinaryReader): Dealer;
}

export namespace Dealer {
  export type AsObject = {
    isDealer: boolean,
    discount: number,
  }
}

export class TaxInfo extends jspb.Message {
  getEdrpou(): string;
  setEdrpou(value: string): void;

  getTaxNumber(): string;
  setTaxNumber(value: string): void;

  getTaxRegime(): string;
  setTaxRegime(value: string): void;

  getVatStatus(): boolean;
  setVatStatus(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TaxInfo.AsObject;
  static toObject(includeInstance: boolean, msg: TaxInfo): TaxInfo.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: TaxInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TaxInfo;
  static deserializeBinaryFromReader(message: TaxInfo, reader: jspb.BinaryReader): TaxInfo;
}

export namespace TaxInfo {
  export type AsObject = {
    edrpou: string,
    taxNumber: string,
    taxRegime: string,
    vatStatus: boolean,
  }
}

export class BusinessInfo extends jspb.Message {
  getBusinessProfile(): string;
  setBusinessProfile(value: string): void;

  getArableArea(): number;
  setArableArea(value: number): void;

  getWacc(): number;
  setWacc(value: number): void;

  getPromissoryNote(): number;
  setPromissoryNote(value: number): void;

  getCollateral(): number;
  setCollateral(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BusinessInfo.AsObject;
  static toObject(includeInstance: boolean, msg: BusinessInfo): BusinessInfo.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: BusinessInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BusinessInfo;
  static deserializeBinaryFromReader(message: BusinessInfo, reader: jspb.BinaryReader): BusinessInfo;
}

export namespace BusinessInfo {
  export type AsObject = {
    businessProfile: string,
    arableArea: number,
    wacc: number,
    promissoryNote: number,
    collateral: number,
  }
}

export class ContactPerson extends jspb.Message {
  getStatus(): ContactPerson.StatusMap[keyof ContactPerson.StatusMap];
  setStatus(value: ContactPerson.StatusMap[keyof ContactPerson.StatusMap]): void;

  getUserId(): string;
  setUserId(value: string): void;

  getName(): string;
  setName(value: string): void;

  getPosition(): string;
  setPosition(value: string): void;

  getPhone(): string;
  setPhone(value: string): void;

  getEmail(): string;
  setEmail(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ContactPerson.AsObject;
  static toObject(includeInstance: boolean, msg: ContactPerson): ContactPerson.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ContactPerson, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ContactPerson;
  static deserializeBinaryFromReader(message: ContactPerson, reader: jspb.BinaryReader): ContactPerson;
}

export namespace ContactPerson {
  export type AsObject = {
    status: ContactPerson.StatusMap[keyof ContactPerson.StatusMap],
    userId: string,
    name: string,
    position: string,
    phone: string,
    email: string,
  }

  export interface StatusMap {
    CONTACTPERSON: 0;
    PRIMARY: 1;
  }

  export const Status: StatusMap;
}

export class ContactPersonFilter extends jspb.Message {
  hasUserId(): boolean;
  clearUserId(): void;
  getUserId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUserId(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearStatusesList(): void;
  getStatusesList(): Array<ContactPerson.StatusMap[keyof ContactPerson.StatusMap]>;
  setStatusesList(value: Array<ContactPerson.StatusMap[keyof ContactPerson.StatusMap]>): void;
  addStatuses(value: ContactPerson.StatusMap[keyof ContactPerson.StatusMap], index?: number): ContactPerson.StatusMap[keyof ContactPerson.StatusMap];

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ContactPersonFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ContactPersonFilter): ContactPersonFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ContactPersonFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ContactPersonFilter;
  static deserializeBinaryFromReader(message: ContactPersonFilter, reader: jspb.BinaryReader): ContactPersonFilter;
}

export namespace ContactPersonFilter {
  export type AsObject = {
    userId?: google_protobuf_wrappers_pb.StringValue.AsObject,
    statusesList: Array<ContactPerson.StatusMap[keyof ContactPerson.StatusMap]>,
  }
}

export class ListContactPersonRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ContactPersonFilter | undefined;
  setFilter(value?: ContactPersonFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListContactPersonRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListContactPersonRequest): ListContactPersonRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListContactPersonRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListContactPersonRequest;
  static deserializeBinaryFromReader(message: ListContactPersonRequest, reader: jspb.BinaryReader): ListContactPersonRequest;
}

export namespace ListContactPersonRequest {
  export type AsObject = {
    filter?: ContactPersonFilter.AsObject,
  }
}

export class ListContactPersonResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ContactPerson>;
  setItemsList(value: Array<ContactPerson>): void;
  addItems(value?: ContactPerson, index?: number): ContactPerson;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListContactPersonResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListContactPersonResponse): ListContactPersonResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListContactPersonResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListContactPersonResponse;
  static deserializeBinaryFromReader(message: ListContactPersonResponse, reader: jspb.BinaryReader): ListContactPersonResponse;
}

export namespace ListContactPersonResponse {
  export type AsObject = {
    itemsList: Array<ContactPerson.AsObject>,
  }
}

export class SaveContactPersonRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ContactPerson | undefined;
  setItem(value?: ContactPerson): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveContactPersonRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveContactPersonRequest): SaveContactPersonRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveContactPersonRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveContactPersonRequest;
  static deserializeBinaryFromReader(message: SaveContactPersonRequest, reader: jspb.BinaryReader): SaveContactPersonRequest;
}

export namespace SaveContactPersonRequest {
  export type AsObject = {
    item?: ContactPerson.AsObject,
  }
}

export class SaveContactPersonResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveContactPersonResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveContactPersonResponse): SaveContactPersonResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveContactPersonResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveContactPersonResponse;
  static deserializeBinaryFromReader(message: SaveContactPersonResponse, reader: jspb.BinaryReader): SaveContactPersonResponse;
}

export namespace SaveContactPersonResponse {
  export type AsObject = {
  }
}

export class DeleteContactPersonRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ContactPerson | undefined;
  setItem(value?: ContactPerson): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteContactPersonRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteContactPersonRequest): DeleteContactPersonRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteContactPersonRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteContactPersonRequest;
  static deserializeBinaryFromReader(message: DeleteContactPersonRequest, reader: jspb.BinaryReader): DeleteContactPersonRequest;
}

export namespace DeleteContactPersonRequest {
  export type AsObject = {
    item?: ContactPerson.AsObject,
  }
}

export class DeleteContactPersonResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteContactPersonResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteContactPersonResponse): DeleteContactPersonResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteContactPersonResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteContactPersonResponse;
  static deserializeBinaryFromReader(message: DeleteContactPersonResponse, reader: jspb.BinaryReader): DeleteContactPersonResponse;
}

export namespace DeleteContactPersonResponse {
  export type AsObject = {
  }
}

export class Location extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getType(): LocationTypeMap[keyof LocationTypeMap];
  setType(value: LocationTypeMap[keyof LocationTypeMap]): void;

  getName(): string;
  setName(value: string): void;

  getRegion(): string;
  setRegion(value: string): void;

  getDistrict(): string;
  setDistrict(value: string): void;

  getCity(): string;
  setCity(value: string): void;

  getStreet(): string;
  setStreet(value: string): void;

  getBuilding(): string;
  setBuilding(value: string): void;

  getGps(): string;
  setGps(value: string): void;

  getDefault(): boolean;
  setDefault(value: boolean): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreated(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Location.AsObject;
  static toObject(includeInstance: boolean, msg: Location): Location.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Location, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Location;
  static deserializeBinaryFromReader(message: Location, reader: jspb.BinaryReader): Location;
}

export namespace Location {
  export type AsObject = {
    userId: string,
    type: LocationTypeMap[keyof LocationTypeMap],
    name: string,
    region: string,
    district: string,
    city: string,
    street: string,
    building: string,
    gps: string,
    pb_default: boolean,
    created?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class GetDefaultLocationRequest extends jspb.Message {
  hasFarmerId(): boolean;
  clearFarmerId(): void;
  getFarmerId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setFarmerId(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDefaultLocationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetDefaultLocationRequest): GetDefaultLocationRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetDefaultLocationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDefaultLocationRequest;
  static deserializeBinaryFromReader(message: GetDefaultLocationRequest, reader: jspb.BinaryReader): GetDefaultLocationRequest;
}

export namespace GetDefaultLocationRequest {
  export type AsObject = {
    farmerId?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetDefaultLocationResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Location | undefined;
  setItem(value?: Location): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDefaultLocationResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetDefaultLocationResponse): GetDefaultLocationResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetDefaultLocationResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDefaultLocationResponse;
  static deserializeBinaryFromReader(message: GetDefaultLocationResponse, reader: jspb.BinaryReader): GetDefaultLocationResponse;
}

export namespace GetDefaultLocationResponse {
  export type AsObject = {
    item?: Location.AsObject,
  }
}

export class LocationFilter extends jspb.Message {
  hasUserId(): boolean;
  clearUserId(): void;
  getUserId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUserId(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasLocationType(): boolean;
  clearLocationType(): void;
  getLocationType(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setLocationType(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDefault(): boolean;
  clearDefault(): void;
  getDefault(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDefault(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LocationFilter.AsObject;
  static toObject(includeInstance: boolean, msg: LocationFilter): LocationFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LocationFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LocationFilter;
  static deserializeBinaryFromReader(message: LocationFilter, reader: jspb.BinaryReader): LocationFilter;
}

export namespace LocationFilter {
  export type AsObject = {
    userId?: google_protobuf_wrappers_pb.StringValue.AsObject,
    locationType?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    pb_default?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class ListLocationRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): LocationFilter | undefined;
  setFilter(value?: LocationFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListLocationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListLocationRequest): ListLocationRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListLocationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListLocationRequest;
  static deserializeBinaryFromReader(message: ListLocationRequest, reader: jspb.BinaryReader): ListLocationRequest;
}

export namespace ListLocationRequest {
  export type AsObject = {
    filter?: LocationFilter.AsObject,
  }
}

export class ListLocationResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Location>;
  setItemsList(value: Array<Location>): void;
  addItems(value?: Location, index?: number): Location;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListLocationResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListLocationResponse): ListLocationResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListLocationResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListLocationResponse;
  static deserializeBinaryFromReader(message: ListLocationResponse, reader: jspb.BinaryReader): ListLocationResponse;
}

export namespace ListLocationResponse {
  export type AsObject = {
    itemsList: Array<Location.AsObject>,
  }
}

export class SaveLocationRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Location | undefined;
  setItem(value?: Location): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveLocationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveLocationRequest): SaveLocationRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveLocationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveLocationRequest;
  static deserializeBinaryFromReader(message: SaveLocationRequest, reader: jspb.BinaryReader): SaveLocationRequest;
}

export namespace SaveLocationRequest {
  export type AsObject = {
    item?: Location.AsObject,
  }
}

export class SaveLocationResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveLocationResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveLocationResponse): SaveLocationResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveLocationResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveLocationResponse;
  static deserializeBinaryFromReader(message: SaveLocationResponse, reader: jspb.BinaryReader): SaveLocationResponse;
}

export namespace SaveLocationResponse {
  export type AsObject = {
  }
}

export class DeleteLocationRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Location | undefined;
  setItem(value?: Location): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteLocationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteLocationRequest): DeleteLocationRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteLocationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteLocationRequest;
  static deserializeBinaryFromReader(message: DeleteLocationRequest, reader: jspb.BinaryReader): DeleteLocationRequest;
}

export namespace DeleteLocationRequest {
  export type AsObject = {
    item?: Location.AsObject,
  }
}

export class DeleteLocationResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteLocationResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteLocationResponse): DeleteLocationResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteLocationResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteLocationResponse;
  static deserializeBinaryFromReader(message: DeleteLocationResponse, reader: jspb.BinaryReader): DeleteLocationResponse;
}

export namespace DeleteLocationResponse {
  export type AsObject = {
  }
}

export class Document extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getType(): Document.TypeMap[keyof Document.TypeMap];
  setType(value: Document.TypeMap[keyof Document.TypeMap]): void;

  getName(): string;
  setName(value: string): void;

  getUrl(): string;
  setUrl(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Document.AsObject;
  static toObject(includeInstance: boolean, msg: Document): Document.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Document, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Document;
  static deserializeBinaryFromReader(message: Document, reader: jspb.BinaryReader): Document;
}

export namespace Document {
  export type AsObject = {
    userId: string,
    type: Document.TypeMap[keyof Document.TypeMap],
    name: string,
    url: string,
  }

  export interface TypeMap {
    FINANCIAL: 0;
    LEGAL: 1;
    CERTIFICATE: 2;
  }

  export const Type: TypeMap;
}

export class DocumentFilter extends jspb.Message {
  hasUserId(): boolean;
  clearUserId(): void;
  getUserId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUserId(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DocumentFilter.AsObject;
  static toObject(includeInstance: boolean, msg: DocumentFilter): DocumentFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DocumentFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DocumentFilter;
  static deserializeBinaryFromReader(message: DocumentFilter, reader: jspb.BinaryReader): DocumentFilter;
}

export namespace DocumentFilter {
  export type AsObject = {
    userId?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class ListDocumentRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): DocumentFilter | undefined;
  setFilter(value?: DocumentFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDocumentRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListDocumentRequest): ListDocumentRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDocumentRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDocumentRequest;
  static deserializeBinaryFromReader(message: ListDocumentRequest, reader: jspb.BinaryReader): ListDocumentRequest;
}

export namespace ListDocumentRequest {
  export type AsObject = {
    filter?: DocumentFilter.AsObject,
  }
}

export class ListDocumentResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Document>;
  setItemsList(value: Array<Document>): void;
  addItems(value?: Document, index?: number): Document;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDocumentResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListDocumentResponse): ListDocumentResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDocumentResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDocumentResponse;
  static deserializeBinaryFromReader(message: ListDocumentResponse, reader: jspb.BinaryReader): ListDocumentResponse;
}

export namespace ListDocumentResponse {
  export type AsObject = {
    itemsList: Array<Document.AsObject>,
  }
}

export class SaveDocumentRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Document | undefined;
  setItem(value?: Document): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveDocumentRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveDocumentRequest): SaveDocumentRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveDocumentRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveDocumentRequest;
  static deserializeBinaryFromReader(message: SaveDocumentRequest, reader: jspb.BinaryReader): SaveDocumentRequest;
}

export namespace SaveDocumentRequest {
  export type AsObject = {
    item?: Document.AsObject,
  }
}

export class SaveDocumentResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveDocumentResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveDocumentResponse): SaveDocumentResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveDocumentResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveDocumentResponse;
  static deserializeBinaryFromReader(message: SaveDocumentResponse, reader: jspb.BinaryReader): SaveDocumentResponse;
}

export namespace SaveDocumentResponse {
  export type AsObject = {
  }
}

export class DeleteDocumentRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Document | undefined;
  setItem(value?: Document): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteDocumentRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteDocumentRequest): DeleteDocumentRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteDocumentRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteDocumentRequest;
  static deserializeBinaryFromReader(message: DeleteDocumentRequest, reader: jspb.BinaryReader): DeleteDocumentRequest;
}

export namespace DeleteDocumentRequest {
  export type AsObject = {
    item?: Document.AsObject,
  }
}

export class DeleteDocumentResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteDocumentResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteDocumentResponse): DeleteDocumentResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteDocumentResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteDocumentResponse;
  static deserializeBinaryFromReader(message: DeleteDocumentResponse, reader: jspb.BinaryReader): DeleteDocumentResponse;
}

export namespace DeleteDocumentResponse {
  export type AsObject = {
  }
}

export class UserReferralInfo extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getStatus(): number;
  setStatus(value: number): void;

  getRole(): string;
  setRole(value: string): void;

  getPhoneNumber(): string;
  setPhoneNumber(value: string): void;

  getFirstName(): string;
  setFirstName(value: string): void;

  getLastName(): string;
  setLastName(value: string): void;

  getMiddleName(): string;
  setMiddleName(value: string): void;

  getEdrpou(): string;
  setEdrpou(value: string): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreated(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserReferralInfo.AsObject;
  static toObject(includeInstance: boolean, msg: UserReferralInfo): UserReferralInfo.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UserReferralInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserReferralInfo;
  static deserializeBinaryFromReader(message: UserReferralInfo, reader: jspb.BinaryReader): UserReferralInfo;
}

export namespace UserReferralInfo {
  export type AsObject = {
    id: string,
    status: number,
    role: string,
    phoneNumber: string,
    firstName: string,
    lastName: string,
    middleName: string,
    edrpou: string,
    created?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class UserBriefInfo extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getStatus(): number;
  setStatus(value: number): void;

  getLogin(): string;
  setLogin(value: string): void;

  getPhoneNumber(): string;
  setPhoneNumber(value: string): void;

  getFirstName(): string;
  setFirstName(value: string): void;

  getLastName(): string;
  setLastName(value: string): void;

  getMiddleName(): string;
  setMiddleName(value: string): void;

  getEmail(): string;
  setEmail(value: string): void;

  getLatitude(): number;
  setLatitude(value: number): void;

  getLongitude(): number;
  setLongitude(value: number): void;

  getAddress(): string;
  setAddress(value: string): void;

  getCountryCode(): string;
  setCountryCode(value: string): void;

  getLanguageCode(): string;
  setLanguageCode(value: string): void;

  getRole(): string;
  setRole(value: string): void;

  getBusinessName(): string;
  setBusinessName(value: string): void;

  getBusinessAddress(): string;
  setBusinessAddress(value: string): void;

  getChangePassword(): boolean;
  setChangePassword(value: boolean): void;

  getVerified(): boolean;
  setVerified(value: boolean): void;

  getCreditApproved(): boolean;
  setCreditApproved(value: boolean): void;

  getCommoditiesAccess(): boolean;
  setCommoditiesAccess(value: boolean): void;

  getReferralUrl(): string;
  setReferralUrl(value: string): void;

  hasDealer(): boolean;
  clearDealer(): void;
  getDealer(): Dealer | undefined;
  setDealer(value?: Dealer): void;

  clearContactsList(): void;
  getContactsList(): Array<string>;
  setContactsList(value: Array<string>): void;
  addContacts(value: string, index?: number): string;

  hasTaxInfo(): boolean;
  clearTaxInfo(): void;
  getTaxInfo(): TaxInfo | undefined;
  setTaxInfo(value?: TaxInfo): void;

  hasBusinessInfo(): boolean;
  clearBusinessInfo(): void;
  getBusinessInfo(): BusinessInfo | undefined;
  setBusinessInfo(value?: BusinessInfo): void;

  hasSupplier(): boolean;
  clearSupplier(): void;
  getSupplier(): UserBriefInfo | undefined;
  setSupplier(value?: UserBriefInfo): void;

  hasReferralUser(): boolean;
  clearReferralUser(): void;
  getReferralUser(): UserBriefInfo | undefined;
  setReferralUser(value?: UserBriefInfo): void;

  clearContactPersonsList(): void;
  getContactPersonsList(): Array<ContactPerson>;
  setContactPersonsList(value: Array<ContactPerson>): void;
  addContactPersons(value?: ContactPerson, index?: number): ContactPerson;

  clearLocationsList(): void;
  getLocationsList(): Array<Location>;
  setLocationsList(value: Array<Location>): void;
  addLocations(value?: Location, index?: number): Location;

  clearDocumentsList(): void;
  getDocumentsList(): Array<Document>;
  setDocumentsList(value: Array<Document>): void;
  addDocuments(value?: Document, index?: number): Document;

  clearEmployeesList(): void;
  getEmployeesList(): Array<User>;
  setEmployeesList(value: Array<User>): void;
  addEmployees(value?: User, index?: number): User;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserBriefInfo.AsObject;
  static toObject(includeInstance: boolean, msg: UserBriefInfo): UserBriefInfo.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UserBriefInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserBriefInfo;
  static deserializeBinaryFromReader(message: UserBriefInfo, reader: jspb.BinaryReader): UserBriefInfo;
}

export namespace UserBriefInfo {
  export type AsObject = {
    id: string,
    status: number,
    login: string,
    phoneNumber: string,
    firstName: string,
    lastName: string,
    middleName: string,
    email: string,
    latitude: number,
    longitude: number,
    address: string,
    countryCode: string,
    languageCode: string,
    role: string,
    businessName: string,
    businessAddress: string,
    changePassword: boolean,
    verified: boolean,
    creditApproved: boolean,
    commoditiesAccess: boolean,
    referralUrl: string,
    dealer?: Dealer.AsObject,
    contactsList: Array<string>,
    taxInfo?: TaxInfo.AsObject,
    businessInfo?: BusinessInfo.AsObject,
    supplier?: UserBriefInfo.AsObject,
    referralUser?: UserBriefInfo.AsObject,
    contactPersonsList: Array<ContactPerson.AsObject>,
    locationsList: Array<Location.AsObject>,
    documentsList: Array<Document.AsObject>,
    employeesList: Array<User.AsObject>,
  }
}

export class Role extends jspb.Message {
  getRole(): string;
  setRole(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  clearPermissionsList(): void;
  getPermissionsList(): Array<Permission>;
  setPermissionsList(value: Array<Permission>): void;
  addPermissions(value?: Permission, index?: number): Permission;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Role.AsObject;
  static toObject(includeInstance: boolean, msg: Role): Role.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Role, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Role;
  static deserializeBinaryFromReader(message: Role, reader: jspb.BinaryReader): Role;
}

export namespace Role {
  export type AsObject = {
    role: string,
    description: string,
    permissionsList: Array<Permission.AsObject>,
  }
}

export class Permission extends jspb.Message {
  getPermission(): string;
  setPermission(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Permission.AsObject;
  static toObject(includeInstance: boolean, msg: Permission): Permission.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Permission, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Permission;
  static deserializeBinaryFromReader(message: Permission, reader: jspb.BinaryReader): Permission;
}

export namespace Permission {
  export type AsObject = {
    permission: string,
    description: string,
  }
}

export class RoleFull extends jspb.Message {
  getRole(): string;
  setRole(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  clearPermissionsList(): void;
  getPermissionsList(): Array<PermissionFull>;
  setPermissionsList(value: Array<PermissionFull>): void;
  addPermissions(value?: PermissionFull, index?: number): PermissionFull;

  getState(): number;
  setState(value: number): void;

  getNote(): string;
  setNote(value: string): void;

  hasUpdated(): boolean;
  clearUpdated(): void;
  getUpdated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdated(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getUpdatedBy(): string;
  setUpdatedBy(value: string): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreated(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getCreatedBy(): string;
  setCreatedBy(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RoleFull.AsObject;
  static toObject(includeInstance: boolean, msg: RoleFull): RoleFull.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RoleFull, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RoleFull;
  static deserializeBinaryFromReader(message: RoleFull, reader: jspb.BinaryReader): RoleFull;
}

export namespace RoleFull {
  export type AsObject = {
    role: string,
    description: string,
    permissionsList: Array<PermissionFull.AsObject>,
    state: number,
    note: string,
    updated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy: string,
    created?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    createdBy: string,
  }
}

export class PermissionFull extends jspb.Message {
  getPermission(): string;
  setPermission(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  getState(): number;
  setState(value: number): void;

  getNote(): string;
  setNote(value: string): void;

  hasUpdated(): boolean;
  clearUpdated(): void;
  getUpdated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdated(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getUpdatedBy(): string;
  setUpdatedBy(value: string): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreated(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getCreatedBy(): string;
  setCreatedBy(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PermissionFull.AsObject;
  static toObject(includeInstance: boolean, msg: PermissionFull): PermissionFull.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PermissionFull, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PermissionFull;
  static deserializeBinaryFromReader(message: PermissionFull, reader: jspb.BinaryReader): PermissionFull;
}

export namespace PermissionFull {
  export type AsObject = {
    permission: string,
    description: string,
    state: number,
    note: string,
    updated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy: string,
    created?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    createdBy: string,
  }
}

export class Session extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getToken(): string;
  setToken(value: string): void;

  getUserId(): string;
  setUserId(value: string): void;

  getServiceName(): string;
  setServiceName(value: string): void;

  hasCreatedTime(): boolean;
  clearCreatedTime(): void;
  getCreatedTime(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedTime(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasExpiredTime(): boolean;
  clearExpiredTime(): void;
  getExpiredTime(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setExpiredTime(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Session.AsObject;
  static toObject(includeInstance: boolean, msg: Session): Session.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Session, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Session;
  static deserializeBinaryFromReader(message: Session, reader: jspb.BinaryReader): Session;
}

export namespace Session {
  export type AsObject = {
    id: number,
    token: string,
    userId: string,
    serviceName: string,
    createdTime?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    expiredTime?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class Counterparty extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getName(): string;
  setName(value: string): void;

  getEdrpou(): string;
  setEdrpou(value: string): void;

  getUserId(): string;
  setUserId(value: string): void;

  clearLocationsList(): void;
  getLocationsList(): Array<CounterpartyLocation>;
  setLocationsList(value: Array<CounterpartyLocation>): void;
  addLocations(value?: CounterpartyLocation, index?: number): CounterpartyLocation;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreated(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getCreatedBy(): string;
  setCreatedBy(value: string): void;

  hasUpdated(): boolean;
  clearUpdated(): void;
  getUpdated(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdated(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getUpdatedBy(): string;
  setUpdatedBy(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Counterparty.AsObject;
  static toObject(includeInstance: boolean, msg: Counterparty): Counterparty.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Counterparty, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Counterparty;
  static deserializeBinaryFromReader(message: Counterparty, reader: jspb.BinaryReader): Counterparty;
}

export namespace Counterparty {
  export type AsObject = {
    id: string,
    name: string,
    edrpou: string,
    userId: string,
    locationsList: Array<CounterpartyLocation.AsObject>,
    created?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    createdBy: string,
    updated?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy: string,
  }
}

export class CounterpartyFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setId(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasEdrpou(): boolean;
  clearEdrpou(): void;
  getEdrpou(): google_protobuf_wrappers_pb.StringValue | undefined;
  setEdrpou(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUserId(): boolean;
  clearUserId(): void;
  getUserId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUserId(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreated(): boolean;
  clearCreated(): void;
  getCreated(): v1_auth_common_common_pb.TimeRange | undefined;
  setCreated(value?: v1_auth_common_common_pb.TimeRange): void;

  hasUpdated(): boolean;
  clearUpdated(): void;
  getUpdated(): v1_auth_common_common_pb.TimeRange | undefined;
  setUpdated(value?: v1_auth_common_common_pb.TimeRange): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CounterpartyFilter.AsObject;
  static toObject(includeInstance: boolean, msg: CounterpartyFilter): CounterpartyFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CounterpartyFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CounterpartyFilter;
  static deserializeBinaryFromReader(message: CounterpartyFilter, reader: jspb.BinaryReader): CounterpartyFilter;
}

export namespace CounterpartyFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.StringValue.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    edrpou?: google_protobuf_wrappers_pb.StringValue.AsObject,
    userId?: google_protobuf_wrappers_pb.StringValue.AsObject,
    created?: v1_auth_common_common_pb.TimeRange.AsObject,
    updated?: v1_auth_common_common_pb.TimeRange.AsObject,
  }
}

export class GetCounterpartyRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCounterpartyRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetCounterpartyRequest): GetCounterpartyRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCounterpartyRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCounterpartyRequest;
  static deserializeBinaryFromReader(message: GetCounterpartyRequest, reader: jspb.BinaryReader): GetCounterpartyRequest;
}

export namespace GetCounterpartyRequest {
  export type AsObject = {
    id: string,
  }
}

export class GetCounterpartyResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Counterparty | undefined;
  setItem(value?: Counterparty): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCounterpartyResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetCounterpartyResponse): GetCounterpartyResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCounterpartyResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCounterpartyResponse;
  static deserializeBinaryFromReader(message: GetCounterpartyResponse, reader: jspb.BinaryReader): GetCounterpartyResponse;
}

export namespace GetCounterpartyResponse {
  export type AsObject = {
    item?: Counterparty.AsObject,
  }
}

export class ListCounterpartyRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): CounterpartyFilter | undefined;
  setFilter(value?: CounterpartyFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCounterpartyRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListCounterpartyRequest): ListCounterpartyRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCounterpartyRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCounterpartyRequest;
  static deserializeBinaryFromReader(message: ListCounterpartyRequest, reader: jspb.BinaryReader): ListCounterpartyRequest;
}

export namespace ListCounterpartyRequest {
  export type AsObject = {
    filter?: CounterpartyFilter.AsObject,
  }
}

export class ListCounterpartyResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Counterparty>;
  setItemsList(value: Array<Counterparty>): void;
  addItems(value?: Counterparty, index?: number): Counterparty;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCounterpartyResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListCounterpartyResponse): ListCounterpartyResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCounterpartyResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCounterpartyResponse;
  static deserializeBinaryFromReader(message: ListCounterpartyResponse, reader: jspb.BinaryReader): ListCounterpartyResponse;
}

export namespace ListCounterpartyResponse {
  export type AsObject = {
    itemsList: Array<Counterparty.AsObject>,
  }
}

export class SaveCounterpartyRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Counterparty | undefined;
  setItem(value?: Counterparty): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveCounterpartyRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveCounterpartyRequest): SaveCounterpartyRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveCounterpartyRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveCounterpartyRequest;
  static deserializeBinaryFromReader(message: SaveCounterpartyRequest, reader: jspb.BinaryReader): SaveCounterpartyRequest;
}

export namespace SaveCounterpartyRequest {
  export type AsObject = {
    item?: Counterparty.AsObject,
  }
}

export class SaveCounterpartyResponse extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveCounterpartyResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveCounterpartyResponse): SaveCounterpartyResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveCounterpartyResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveCounterpartyResponse;
  static deserializeBinaryFromReader(message: SaveCounterpartyResponse, reader: jspb.BinaryReader): SaveCounterpartyResponse;
}

export namespace SaveCounterpartyResponse {
  export type AsObject = {
    id: string,
  }
}

export class DeleteCounterpartyRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteCounterpartyRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteCounterpartyRequest): DeleteCounterpartyRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteCounterpartyRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteCounterpartyRequest;
  static deserializeBinaryFromReader(message: DeleteCounterpartyRequest, reader: jspb.BinaryReader): DeleteCounterpartyRequest;
}

export namespace DeleteCounterpartyRequest {
  export type AsObject = {
    id: string,
  }
}

export class DeleteCounterpartyResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteCounterpartyResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteCounterpartyResponse): DeleteCounterpartyResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteCounterpartyResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteCounterpartyResponse;
  static deserializeBinaryFromReader(message: DeleteCounterpartyResponse, reader: jspb.BinaryReader): DeleteCounterpartyResponse;
}

export namespace DeleteCounterpartyResponse {
  export type AsObject = {
  }
}

export class CounterpartyLocationTypeValue extends jspb.Message {
  getValue(): CounterpartyLocationTypeMap[keyof CounterpartyLocationTypeMap];
  setValue(value: CounterpartyLocationTypeMap[keyof CounterpartyLocationTypeMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CounterpartyLocationTypeValue.AsObject;
  static toObject(includeInstance: boolean, msg: CounterpartyLocationTypeValue): CounterpartyLocationTypeValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CounterpartyLocationTypeValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CounterpartyLocationTypeValue;
  static deserializeBinaryFromReader(message: CounterpartyLocationTypeValue, reader: jspb.BinaryReader): CounterpartyLocationTypeValue;
}

export namespace CounterpartyLocationTypeValue {
  export type AsObject = {
    value: CounterpartyLocationTypeMap[keyof CounterpartyLocationTypeMap],
  }
}

export class CounterpartyLocation extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getCounterpartyId(): string;
  setCounterpartyId(value: string): void;

  getType(): CounterpartyLocationTypeMap[keyof CounterpartyLocationTypeMap];
  setType(value: CounterpartyLocationTypeMap[keyof CounterpartyLocationTypeMap]): void;

  getName(): string;
  setName(value: string): void;

  getRegion(): string;
  setRegion(value: string): void;

  getDistrict(): string;
  setDistrict(value: string): void;

  getCity(): string;
  setCity(value: string): void;

  getStreet(): string;
  setStreet(value: string): void;

  getBuilding(): string;
  setBuilding(value: string): void;

  getGps(): string;
  setGps(value: string): void;

  getReceiver(): string;
  setReceiver(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CounterpartyLocation.AsObject;
  static toObject(includeInstance: boolean, msg: CounterpartyLocation): CounterpartyLocation.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CounterpartyLocation, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CounterpartyLocation;
  static deserializeBinaryFromReader(message: CounterpartyLocation, reader: jspb.BinaryReader): CounterpartyLocation;
}

export namespace CounterpartyLocation {
  export type AsObject = {
    id: number,
    counterpartyId: string,
    type: CounterpartyLocationTypeMap[keyof CounterpartyLocationTypeMap],
    name: string,
    region: string,
    district: string,
    city: string,
    street: string,
    building: string,
    gps: string,
    receiver: string,
  }
}

export class CounterpartyLocationFilter extends jspb.Message {
  hasCounterpartyId(): boolean;
  clearCounterpartyId(): void;
  getCounterpartyId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCounterpartyId(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasType(): boolean;
  clearType(): void;
  getType(): CounterpartyLocationTypeValue | undefined;
  setType(value?: CounterpartyLocationTypeValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CounterpartyLocationFilter.AsObject;
  static toObject(includeInstance: boolean, msg: CounterpartyLocationFilter): CounterpartyLocationFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CounterpartyLocationFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CounterpartyLocationFilter;
  static deserializeBinaryFromReader(message: CounterpartyLocationFilter, reader: jspb.BinaryReader): CounterpartyLocationFilter;
}

export namespace CounterpartyLocationFilter {
  export type AsObject = {
    counterpartyId?: google_protobuf_wrappers_pb.StringValue.AsObject,
    type?: CounterpartyLocationTypeValue.AsObject,
  }
}

export class GetCounterpartyLocationRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCounterpartyLocationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetCounterpartyLocationRequest): GetCounterpartyLocationRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCounterpartyLocationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCounterpartyLocationRequest;
  static deserializeBinaryFromReader(message: GetCounterpartyLocationRequest, reader: jspb.BinaryReader): GetCounterpartyLocationRequest;
}

export namespace GetCounterpartyLocationRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetCounterpartyLocationResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): CounterpartyLocation | undefined;
  setItem(value?: CounterpartyLocation): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCounterpartyLocationResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetCounterpartyLocationResponse): GetCounterpartyLocationResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCounterpartyLocationResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCounterpartyLocationResponse;
  static deserializeBinaryFromReader(message: GetCounterpartyLocationResponse, reader: jspb.BinaryReader): GetCounterpartyLocationResponse;
}

export namespace GetCounterpartyLocationResponse {
  export type AsObject = {
    item?: CounterpartyLocation.AsObject,
  }
}

export class ListCounterpartyLocationRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): CounterpartyLocationFilter | undefined;
  setFilter(value?: CounterpartyLocationFilter): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCounterpartyLocationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListCounterpartyLocationRequest): ListCounterpartyLocationRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCounterpartyLocationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCounterpartyLocationRequest;
  static deserializeBinaryFromReader(message: ListCounterpartyLocationRequest, reader: jspb.BinaryReader): ListCounterpartyLocationRequest;
}

export namespace ListCounterpartyLocationRequest {
  export type AsObject = {
    filter?: CounterpartyLocationFilter.AsObject,
  }
}

export class ListCounterpartyLocationResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<CounterpartyLocation>;
  setItemsList(value: Array<CounterpartyLocation>): void;
  addItems(value?: CounterpartyLocation, index?: number): CounterpartyLocation;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCounterpartyLocationResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListCounterpartyLocationResponse): ListCounterpartyLocationResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCounterpartyLocationResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCounterpartyLocationResponse;
  static deserializeBinaryFromReader(message: ListCounterpartyLocationResponse, reader: jspb.BinaryReader): ListCounterpartyLocationResponse;
}

export namespace ListCounterpartyLocationResponse {
  export type AsObject = {
    itemsList: Array<CounterpartyLocation.AsObject>,
  }
}

export class SaveCounterpartyLocationRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): CounterpartyLocation | undefined;
  setItem(value?: CounterpartyLocation): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveCounterpartyLocationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveCounterpartyLocationRequest): SaveCounterpartyLocationRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveCounterpartyLocationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveCounterpartyLocationRequest;
  static deserializeBinaryFromReader(message: SaveCounterpartyLocationRequest, reader: jspb.BinaryReader): SaveCounterpartyLocationRequest;
}

export namespace SaveCounterpartyLocationRequest {
  export type AsObject = {
    item?: CounterpartyLocation.AsObject,
  }
}

export class SaveCounterpartyLocationResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveCounterpartyLocationResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveCounterpartyLocationResponse): SaveCounterpartyLocationResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveCounterpartyLocationResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveCounterpartyLocationResponse;
  static deserializeBinaryFromReader(message: SaveCounterpartyLocationResponse, reader: jspb.BinaryReader): SaveCounterpartyLocationResponse;
}

export namespace SaveCounterpartyLocationResponse {
  export type AsObject = {
    id: number,
  }
}

export class DeleteCounterpartyLocationRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteCounterpartyLocationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteCounterpartyLocationRequest): DeleteCounterpartyLocationRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteCounterpartyLocationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteCounterpartyLocationRequest;
  static deserializeBinaryFromReader(message: DeleteCounterpartyLocationRequest, reader: jspb.BinaryReader): DeleteCounterpartyLocationRequest;
}

export namespace DeleteCounterpartyLocationRequest {
  export type AsObject = {
    id: number,
  }
}

export class DeleteCounterpartyLocationResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteCounterpartyLocationResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteCounterpartyLocationResponse): DeleteCounterpartyLocationResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteCounterpartyLocationResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteCounterpartyLocationResponse;
  static deserializeBinaryFromReader(message: DeleteCounterpartyLocationResponse, reader: jspb.BinaryReader): DeleteCounterpartyLocationResponse;
}

export namespace DeleteCounterpartyLocationResponse {
  export type AsObject = {
  }
}

export interface LocationTypeMap {
  LEGAL: 0;
  ACTUAL: 1;
  DELIVERY: 2;
}

export const LocationType: LocationTypeMap;

export interface RoleTypeMap {
  ADMIN: 0;
  MANAGER: 1;
  FARMER: 2;
  AGENT: 3;
  SUPPLIER: 4;
  MERCHANDISER: 5;
  DESTRIBUTER: 6;
  PROVIDER: 7;
  TRADER: 8;
  PROCESSOR: 9;
  RETAILER: 10;
  BANK: 11;
  INSURER: 12;
  SUPPORTER: 13;
}

export const RoleType: RoleTypeMap;

export interface StatusMap {
  NEW: 0;
  AUTHORIZED: 1;
  BLOCKED: 2;
  FIRED: 3;
}

export const Status: StatusMap;

export interface CounterpartyLocationTypeMap {
  COUNTERPARTYLOCATIONTYPE_UNSPECIFIED: 0;
  COUNTERPARTYLOCATIONTYPE_DELIVERY: 1;
}

export const CounterpartyLocationType: CounterpartyLocationTypeMap;

