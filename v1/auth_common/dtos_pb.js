// source: v1/auth_common/dtos.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var google_protobuf_wrappers_pb = require('google-protobuf/google/protobuf/wrappers_pb.js');
goog.object.extend(proto, google_protobuf_wrappers_pb);
var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js');
goog.object.extend(proto, google_protobuf_timestamp_pb);
var v1_auth_common_common_pb = require('../../v1/auth_common/common_pb.js');
goog.object.extend(proto, v1_auth_common_common_pb);
goog.exportSymbol('proto.fcp.accounting.v1.BusinessInfo', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.ContactPerson', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.ContactPerson.Status', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.ContactPersonFilter', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.Counterparty', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.CounterpartyFilter', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.CounterpartyLocation', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.CounterpartyLocationFilter', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.CounterpartyLocationType', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.CounterpartyLocationTypeValue', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.Dealer', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.DeleteContactPersonRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.DeleteContactPersonResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.DeleteCounterpartyRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.DeleteCounterpartyResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.DeleteDocumentRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.DeleteDocumentResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.DeleteLocationRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.DeleteLocationResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.Document', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.Document.Type', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.DocumentFilter', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.GetCounterpartyLocationRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.GetCounterpartyLocationResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.GetCounterpartyRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.GetCounterpartyResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.GetDefaultLocationRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.GetDefaultLocationResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.ListContactPersonRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.ListContactPersonResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.ListCounterpartyLocationRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.ListCounterpartyLocationResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.ListCounterpartyRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.ListCounterpartyResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.ListDocumentRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.ListDocumentResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.ListLocationRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.ListLocationResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.Location', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.LocationFilter', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.LocationType', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.Permission', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.PermissionFull', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.Role', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.RoleFull', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.RoleType', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.SaveContactPersonRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.SaveContactPersonResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.SaveCounterpartyLocationRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.SaveCounterpartyLocationResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.SaveCounterpartyRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.SaveCounterpartyResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.SaveDocumentRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.SaveDocumentResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.SaveLocationRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.SaveLocationResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.Session', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.Status', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.TaxInfo', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.User', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.UserBriefInfo', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.UserFilter', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.UserReferralInfo', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.User = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.accounting.v1.User.repeatedFields_, null);
};
goog.inherits(proto.fcp.accounting.v1.User, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.User.displayName = 'proto.fcp.accounting.v1.User';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.UserFilter = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.UserFilter, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.UserFilter.displayName = 'proto.fcp.accounting.v1.UserFilter';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.Dealer = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.Dealer, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.Dealer.displayName = 'proto.fcp.accounting.v1.Dealer';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.TaxInfo = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.TaxInfo, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.TaxInfo.displayName = 'proto.fcp.accounting.v1.TaxInfo';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.BusinessInfo = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.BusinessInfo, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.BusinessInfo.displayName = 'proto.fcp.accounting.v1.BusinessInfo';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.ContactPerson = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.ContactPerson, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.ContactPerson.displayName = 'proto.fcp.accounting.v1.ContactPerson';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.ContactPersonFilter = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.accounting.v1.ContactPersonFilter.repeatedFields_, null);
};
goog.inherits(proto.fcp.accounting.v1.ContactPersonFilter, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.ContactPersonFilter.displayName = 'proto.fcp.accounting.v1.ContactPersonFilter';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.ListContactPersonRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.ListContactPersonRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.ListContactPersonRequest.displayName = 'proto.fcp.accounting.v1.ListContactPersonRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.ListContactPersonResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.accounting.v1.ListContactPersonResponse.repeatedFields_, null);
};
goog.inherits(proto.fcp.accounting.v1.ListContactPersonResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.ListContactPersonResponse.displayName = 'proto.fcp.accounting.v1.ListContactPersonResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.SaveContactPersonRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.SaveContactPersonRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.SaveContactPersonRequest.displayName = 'proto.fcp.accounting.v1.SaveContactPersonRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.SaveContactPersonResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.SaveContactPersonResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.SaveContactPersonResponse.displayName = 'proto.fcp.accounting.v1.SaveContactPersonResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.DeleteContactPersonRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.DeleteContactPersonRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.DeleteContactPersonRequest.displayName = 'proto.fcp.accounting.v1.DeleteContactPersonRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.DeleteContactPersonResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.DeleteContactPersonResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.DeleteContactPersonResponse.displayName = 'proto.fcp.accounting.v1.DeleteContactPersonResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.Location = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.Location, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.Location.displayName = 'proto.fcp.accounting.v1.Location';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.GetDefaultLocationRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.GetDefaultLocationRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.GetDefaultLocationRequest.displayName = 'proto.fcp.accounting.v1.GetDefaultLocationRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.GetDefaultLocationResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.GetDefaultLocationResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.GetDefaultLocationResponse.displayName = 'proto.fcp.accounting.v1.GetDefaultLocationResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.LocationFilter = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.LocationFilter, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.LocationFilter.displayName = 'proto.fcp.accounting.v1.LocationFilter';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.ListLocationRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.ListLocationRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.ListLocationRequest.displayName = 'proto.fcp.accounting.v1.ListLocationRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.ListLocationResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.accounting.v1.ListLocationResponse.repeatedFields_, null);
};
goog.inherits(proto.fcp.accounting.v1.ListLocationResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.ListLocationResponse.displayName = 'proto.fcp.accounting.v1.ListLocationResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.SaveLocationRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.SaveLocationRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.SaveLocationRequest.displayName = 'proto.fcp.accounting.v1.SaveLocationRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.SaveLocationResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.SaveLocationResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.SaveLocationResponse.displayName = 'proto.fcp.accounting.v1.SaveLocationResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.DeleteLocationRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.DeleteLocationRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.DeleteLocationRequest.displayName = 'proto.fcp.accounting.v1.DeleteLocationRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.DeleteLocationResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.DeleteLocationResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.DeleteLocationResponse.displayName = 'proto.fcp.accounting.v1.DeleteLocationResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.Document = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.Document, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.Document.displayName = 'proto.fcp.accounting.v1.Document';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.DocumentFilter = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.DocumentFilter, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.DocumentFilter.displayName = 'proto.fcp.accounting.v1.DocumentFilter';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.ListDocumentRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.ListDocumentRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.ListDocumentRequest.displayName = 'proto.fcp.accounting.v1.ListDocumentRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.ListDocumentResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.accounting.v1.ListDocumentResponse.repeatedFields_, null);
};
goog.inherits(proto.fcp.accounting.v1.ListDocumentResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.ListDocumentResponse.displayName = 'proto.fcp.accounting.v1.ListDocumentResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.SaveDocumentRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.SaveDocumentRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.SaveDocumentRequest.displayName = 'proto.fcp.accounting.v1.SaveDocumentRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.SaveDocumentResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.SaveDocumentResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.SaveDocumentResponse.displayName = 'proto.fcp.accounting.v1.SaveDocumentResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.DeleteDocumentRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.DeleteDocumentRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.DeleteDocumentRequest.displayName = 'proto.fcp.accounting.v1.DeleteDocumentRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.DeleteDocumentResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.DeleteDocumentResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.DeleteDocumentResponse.displayName = 'proto.fcp.accounting.v1.DeleteDocumentResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.UserReferralInfo = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.UserReferralInfo, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.UserReferralInfo.displayName = 'proto.fcp.accounting.v1.UserReferralInfo';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.UserBriefInfo = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.accounting.v1.UserBriefInfo.repeatedFields_, null);
};
goog.inherits(proto.fcp.accounting.v1.UserBriefInfo, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.UserBriefInfo.displayName = 'proto.fcp.accounting.v1.UserBriefInfo';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.Role = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.accounting.v1.Role.repeatedFields_, null);
};
goog.inherits(proto.fcp.accounting.v1.Role, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.Role.displayName = 'proto.fcp.accounting.v1.Role';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.Permission = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.Permission, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.Permission.displayName = 'proto.fcp.accounting.v1.Permission';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.RoleFull = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.accounting.v1.RoleFull.repeatedFields_, null);
};
goog.inherits(proto.fcp.accounting.v1.RoleFull, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.RoleFull.displayName = 'proto.fcp.accounting.v1.RoleFull';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.PermissionFull = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.PermissionFull, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.PermissionFull.displayName = 'proto.fcp.accounting.v1.PermissionFull';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.Session = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.Session, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.Session.displayName = 'proto.fcp.accounting.v1.Session';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.Counterparty = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.accounting.v1.Counterparty.repeatedFields_, null);
};
goog.inherits(proto.fcp.accounting.v1.Counterparty, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.Counterparty.displayName = 'proto.fcp.accounting.v1.Counterparty';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.CounterpartyFilter = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.CounterpartyFilter, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.CounterpartyFilter.displayName = 'proto.fcp.accounting.v1.CounterpartyFilter';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.GetCounterpartyRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.GetCounterpartyRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.GetCounterpartyRequest.displayName = 'proto.fcp.accounting.v1.GetCounterpartyRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.GetCounterpartyResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.GetCounterpartyResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.GetCounterpartyResponse.displayName = 'proto.fcp.accounting.v1.GetCounterpartyResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.ListCounterpartyRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.ListCounterpartyRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.ListCounterpartyRequest.displayName = 'proto.fcp.accounting.v1.ListCounterpartyRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.ListCounterpartyResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.accounting.v1.ListCounterpartyResponse.repeatedFields_, null);
};
goog.inherits(proto.fcp.accounting.v1.ListCounterpartyResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.ListCounterpartyResponse.displayName = 'proto.fcp.accounting.v1.ListCounterpartyResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.SaveCounterpartyRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.SaveCounterpartyRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.SaveCounterpartyRequest.displayName = 'proto.fcp.accounting.v1.SaveCounterpartyRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.SaveCounterpartyResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.SaveCounterpartyResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.SaveCounterpartyResponse.displayName = 'proto.fcp.accounting.v1.SaveCounterpartyResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.DeleteCounterpartyRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.DeleteCounterpartyRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.DeleteCounterpartyRequest.displayName = 'proto.fcp.accounting.v1.DeleteCounterpartyRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.DeleteCounterpartyResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.DeleteCounterpartyResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.DeleteCounterpartyResponse.displayName = 'proto.fcp.accounting.v1.DeleteCounterpartyResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.CounterpartyLocationTypeValue = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.CounterpartyLocationTypeValue, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.CounterpartyLocationTypeValue.displayName = 'proto.fcp.accounting.v1.CounterpartyLocationTypeValue';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.CounterpartyLocation = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.CounterpartyLocation, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.CounterpartyLocation.displayName = 'proto.fcp.accounting.v1.CounterpartyLocation';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.CounterpartyLocationFilter = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.CounterpartyLocationFilter, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.CounterpartyLocationFilter.displayName = 'proto.fcp.accounting.v1.CounterpartyLocationFilter';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.GetCounterpartyLocationRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.GetCounterpartyLocationRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.GetCounterpartyLocationRequest.displayName = 'proto.fcp.accounting.v1.GetCounterpartyLocationRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.GetCounterpartyLocationResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.GetCounterpartyLocationResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.GetCounterpartyLocationResponse.displayName = 'proto.fcp.accounting.v1.GetCounterpartyLocationResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.ListCounterpartyLocationRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.ListCounterpartyLocationRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.ListCounterpartyLocationRequest.displayName = 'proto.fcp.accounting.v1.ListCounterpartyLocationRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.ListCounterpartyLocationResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.accounting.v1.ListCounterpartyLocationResponse.repeatedFields_, null);
};
goog.inherits(proto.fcp.accounting.v1.ListCounterpartyLocationResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.ListCounterpartyLocationResponse.displayName = 'proto.fcp.accounting.v1.ListCounterpartyLocationResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.SaveCounterpartyLocationRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.SaveCounterpartyLocationRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.SaveCounterpartyLocationRequest.displayName = 'proto.fcp.accounting.v1.SaveCounterpartyLocationRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.SaveCounterpartyLocationResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.SaveCounterpartyLocationResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.SaveCounterpartyLocationResponse.displayName = 'proto.fcp.accounting.v1.SaveCounterpartyLocationResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest.displayName = 'proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse.displayName = 'proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse';
}

/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.accounting.v1.User.repeatedFields_ = [18,36,37,38,39];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.User.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.User.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.User} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.User.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, ""),
    status: jspb.Message.getFieldWithDefault(msg, 2, 0),
    login: jspb.Message.getFieldWithDefault(msg, 3, ""),
    phoneNumber: jspb.Message.getFieldWithDefault(msg, 4, ""),
    firstName: jspb.Message.getFieldWithDefault(msg, 5, ""),
    lastName: jspb.Message.getFieldWithDefault(msg, 6, ""),
    middleName: jspb.Message.getFieldWithDefault(msg, 7, ""),
    email: jspb.Message.getFieldWithDefault(msg, 8, ""),
    latitude: jspb.Message.getFloatingPointFieldWithDefault(msg, 9, 0.0),
    longitude: jspb.Message.getFloatingPointFieldWithDefault(msg, 10, 0.0),
    address: jspb.Message.getFieldWithDefault(msg, 11, ""),
    countryCode: jspb.Message.getFieldWithDefault(msg, 12, ""),
    languageCode: jspb.Message.getFieldWithDefault(msg, 13, ""),
    role: jspb.Message.getFieldWithDefault(msg, 14, ""),
    businessName: jspb.Message.getFieldWithDefault(msg, 15, ""),
    businessAddress: jspb.Message.getFieldWithDefault(msg, 16, ""),
    changePassword: jspb.Message.getBooleanFieldWithDefault(msg, 17, false),
    verified: jspb.Message.getBooleanFieldWithDefault(msg, 27, false),
    creditApproved: jspb.Message.getBooleanFieldWithDefault(msg, 28, false),
    commoditiesAccess: jspb.Message.getBooleanFieldWithDefault(msg, 29, false),
    referralUrl: jspb.Message.getFieldWithDefault(msg, 30, ""),
    referralUrlIos: jspb.Message.getFieldWithDefault(msg, 40, ""),
    dealer: (f = msg.getDealer()) && proto.fcp.accounting.v1.Dealer.toObject(includeInstance, f),
    contactsList: (f = jspb.Message.getRepeatedField(msg, 18)) == null ? undefined : f,
    taxInfo: (f = msg.getTaxInfo()) && proto.fcp.accounting.v1.TaxInfo.toObject(includeInstance, f),
    businessInfo: (f = msg.getBusinessInfo()) && proto.fcp.accounting.v1.BusinessInfo.toObject(includeInstance, f),
    state: jspb.Message.getFieldWithDefault(msg, 21, 0),
    note: jspb.Message.getFieldWithDefault(msg, 22, ""),
    updated: (f = msg.getUpdated()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    updatedBy: jspb.Message.getFieldWithDefault(msg, 24, ""),
    created: (f = msg.getCreated()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    createdBy: jspb.Message.getFieldWithDefault(msg, 26, ""),
    supplier: (f = msg.getSupplier()) && proto.fcp.accounting.v1.UserBriefInfo.toObject(includeInstance, f),
    referralUser: (f = msg.getReferralUser()) && proto.fcp.accounting.v1.UserBriefInfo.toObject(includeInstance, f),
    contactPersonsList: jspb.Message.toObjectList(msg.getContactPersonsList(),
    proto.fcp.accounting.v1.ContactPerson.toObject, includeInstance),
    locationsList: jspb.Message.toObjectList(msg.getLocationsList(),
    proto.fcp.accounting.v1.Location.toObject, includeInstance),
    documentsList: jspb.Message.toObjectList(msg.getDocumentsList(),
    proto.fcp.accounting.v1.Document.toObject, includeInstance),
    employeesList: jspb.Message.toObjectList(msg.getEmployeesList(),
    proto.fcp.accounting.v1.User.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.User}
 */
proto.fcp.accounting.v1.User.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.User;
  return proto.fcp.accounting.v1.User.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.User} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.User}
 */
proto.fcp.accounting.v1.User.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readUint32());
      msg.setStatus(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setLogin(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setPhoneNumber(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setFirstName(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setLastName(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setMiddleName(value);
      break;
    case 8:
      var value = /** @type {string} */ (reader.readString());
      msg.setEmail(value);
      break;
    case 9:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setLatitude(value);
      break;
    case 10:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setLongitude(value);
      break;
    case 11:
      var value = /** @type {string} */ (reader.readString());
      msg.setAddress(value);
      break;
    case 12:
      var value = /** @type {string} */ (reader.readString());
      msg.setCountryCode(value);
      break;
    case 13:
      var value = /** @type {string} */ (reader.readString());
      msg.setLanguageCode(value);
      break;
    case 14:
      var value = /** @type {string} */ (reader.readString());
      msg.setRole(value);
      break;
    case 15:
      var value = /** @type {string} */ (reader.readString());
      msg.setBusinessName(value);
      break;
    case 16:
      var value = /** @type {string} */ (reader.readString());
      msg.setBusinessAddress(value);
      break;
    case 17:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setChangePassword(value);
      break;
    case 27:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setVerified(value);
      break;
    case 28:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setCreditApproved(value);
      break;
    case 29:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setCommoditiesAccess(value);
      break;
    case 30:
      var value = /** @type {string} */ (reader.readString());
      msg.setReferralUrl(value);
      break;
    case 40:
      var value = /** @type {string} */ (reader.readString());
      msg.setReferralUrlIos(value);
      break;
    case 33:
      var value = new proto.fcp.accounting.v1.Dealer;
      reader.readMessage(value,proto.fcp.accounting.v1.Dealer.deserializeBinaryFromReader);
      msg.setDealer(value);
      break;
    case 18:
      var value = /** @type {string} */ (reader.readString());
      msg.addContacts(value);
      break;
    case 19:
      var value = new proto.fcp.accounting.v1.TaxInfo;
      reader.readMessage(value,proto.fcp.accounting.v1.TaxInfo.deserializeBinaryFromReader);
      msg.setTaxInfo(value);
      break;
    case 20:
      var value = new proto.fcp.accounting.v1.BusinessInfo;
      reader.readMessage(value,proto.fcp.accounting.v1.BusinessInfo.deserializeBinaryFromReader);
      msg.setBusinessInfo(value);
      break;
    case 21:
      var value = /** @type {number} */ (reader.readUint32());
      msg.setState(value);
      break;
    case 22:
      var value = /** @type {string} */ (reader.readString());
      msg.setNote(value);
      break;
    case 23:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setUpdated(value);
      break;
    case 24:
      var value = /** @type {string} */ (reader.readString());
      msg.setUpdatedBy(value);
      break;
    case 25:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setCreated(value);
      break;
    case 26:
      var value = /** @type {string} */ (reader.readString());
      msg.setCreatedBy(value);
      break;
    case 35:
      var value = new proto.fcp.accounting.v1.UserBriefInfo;
      reader.readMessage(value,proto.fcp.accounting.v1.UserBriefInfo.deserializeBinaryFromReader);
      msg.setSupplier(value);
      break;
    case 42:
      var value = new proto.fcp.accounting.v1.UserBriefInfo;
      reader.readMessage(value,proto.fcp.accounting.v1.UserBriefInfo.deserializeBinaryFromReader);
      msg.setReferralUser(value);
      break;
    case 36:
      var value = new proto.fcp.accounting.v1.ContactPerson;
      reader.readMessage(value,proto.fcp.accounting.v1.ContactPerson.deserializeBinaryFromReader);
      msg.addContactPersons(value);
      break;
    case 37:
      var value = new proto.fcp.accounting.v1.Location;
      reader.readMessage(value,proto.fcp.accounting.v1.Location.deserializeBinaryFromReader);
      msg.addLocations(value);
      break;
    case 38:
      var value = new proto.fcp.accounting.v1.Document;
      reader.readMessage(value,proto.fcp.accounting.v1.Document.deserializeBinaryFromReader);
      msg.addDocuments(value);
      break;
    case 39:
      var value = new proto.fcp.accounting.v1.User;
      reader.readMessage(value,proto.fcp.accounting.v1.User.deserializeBinaryFromReader);
      msg.addEmployees(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.User.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.User.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.User} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.User.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getStatus();
  if (f !== 0) {
    writer.writeUint32(
      2,
      f
    );
  }
  f = message.getLogin();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getPhoneNumber();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getFirstName();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getLastName();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getMiddleName();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getEmail();
  if (f.length > 0) {
    writer.writeString(
      8,
      f
    );
  }
  f = message.getLatitude();
  if (f !== 0.0) {
    writer.writeFloat(
      9,
      f
    );
  }
  f = message.getLongitude();
  if (f !== 0.0) {
    writer.writeFloat(
      10,
      f
    );
  }
  f = message.getAddress();
  if (f.length > 0) {
    writer.writeString(
      11,
      f
    );
  }
  f = message.getCountryCode();
  if (f.length > 0) {
    writer.writeString(
      12,
      f
    );
  }
  f = message.getLanguageCode();
  if (f.length > 0) {
    writer.writeString(
      13,
      f
    );
  }
  f = message.getRole();
  if (f.length > 0) {
    writer.writeString(
      14,
      f
    );
  }
  f = message.getBusinessName();
  if (f.length > 0) {
    writer.writeString(
      15,
      f
    );
  }
  f = message.getBusinessAddress();
  if (f.length > 0) {
    writer.writeString(
      16,
      f
    );
  }
  f = message.getChangePassword();
  if (f) {
    writer.writeBool(
      17,
      f
    );
  }
  f = message.getVerified();
  if (f) {
    writer.writeBool(
      27,
      f
    );
  }
  f = message.getCreditApproved();
  if (f) {
    writer.writeBool(
      28,
      f
    );
  }
  f = message.getCommoditiesAccess();
  if (f) {
    writer.writeBool(
      29,
      f
    );
  }
  f = message.getReferralUrl();
  if (f.length > 0) {
    writer.writeString(
      30,
      f
    );
  }
  f = message.getReferralUrlIos();
  if (f.length > 0) {
    writer.writeString(
      40,
      f
    );
  }
  f = message.getDealer();
  if (f != null) {
    writer.writeMessage(
      33,
      f,
      proto.fcp.accounting.v1.Dealer.serializeBinaryToWriter
    );
  }
  f = message.getContactsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      18,
      f
    );
  }
  f = message.getTaxInfo();
  if (f != null) {
    writer.writeMessage(
      19,
      f,
      proto.fcp.accounting.v1.TaxInfo.serializeBinaryToWriter
    );
  }
  f = message.getBusinessInfo();
  if (f != null) {
    writer.writeMessage(
      20,
      f,
      proto.fcp.accounting.v1.BusinessInfo.serializeBinaryToWriter
    );
  }
  f = message.getState();
  if (f !== 0) {
    writer.writeUint32(
      21,
      f
    );
  }
  f = message.getNote();
  if (f.length > 0) {
    writer.writeString(
      22,
      f
    );
  }
  f = message.getUpdated();
  if (f != null) {
    writer.writeMessage(
      23,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getUpdatedBy();
  if (f.length > 0) {
    writer.writeString(
      24,
      f
    );
  }
  f = message.getCreated();
  if (f != null) {
    writer.writeMessage(
      25,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getCreatedBy();
  if (f.length > 0) {
    writer.writeString(
      26,
      f
    );
  }
  f = message.getSupplier();
  if (f != null) {
    writer.writeMessage(
      35,
      f,
      proto.fcp.accounting.v1.UserBriefInfo.serializeBinaryToWriter
    );
  }
  f = message.getReferralUser();
  if (f != null) {
    writer.writeMessage(
      42,
      f,
      proto.fcp.accounting.v1.UserBriefInfo.serializeBinaryToWriter
    );
  }
  f = message.getContactPersonsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      36,
      f,
      proto.fcp.accounting.v1.ContactPerson.serializeBinaryToWriter
    );
  }
  f = message.getLocationsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      37,
      f,
      proto.fcp.accounting.v1.Location.serializeBinaryToWriter
    );
  }
  f = message.getDocumentsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      38,
      f,
      proto.fcp.accounting.v1.Document.serializeBinaryToWriter
    );
  }
  f = message.getEmployeesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      39,
      f,
      proto.fcp.accounting.v1.User.serializeBinaryToWriter
    );
  }
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.User.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional uint32 status = 2;
 * @return {number}
 */
proto.fcp.accounting.v1.User.prototype.getStatus = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setStatus = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional string login = 3;
 * @return {string}
 */
proto.fcp.accounting.v1.User.prototype.getLogin = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setLogin = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string phone_number = 4;
 * @return {string}
 */
proto.fcp.accounting.v1.User.prototype.getPhoneNumber = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setPhoneNumber = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string first_name = 5;
 * @return {string}
 */
proto.fcp.accounting.v1.User.prototype.getFirstName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setFirstName = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional string last_name = 6;
 * @return {string}
 */
proto.fcp.accounting.v1.User.prototype.getLastName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setLastName = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional string middle_name = 7;
 * @return {string}
 */
proto.fcp.accounting.v1.User.prototype.getMiddleName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setMiddleName = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * optional string email = 8;
 * @return {string}
 */
proto.fcp.accounting.v1.User.prototype.getEmail = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 8, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setEmail = function(value) {
  return jspb.Message.setProto3StringField(this, 8, value);
};


/**
 * optional float latitude = 9;
 * @return {number}
 */
proto.fcp.accounting.v1.User.prototype.getLatitude = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 9, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setLatitude = function(value) {
  return jspb.Message.setProto3FloatField(this, 9, value);
};


/**
 * optional float longitude = 10;
 * @return {number}
 */
proto.fcp.accounting.v1.User.prototype.getLongitude = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 10, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setLongitude = function(value) {
  return jspb.Message.setProto3FloatField(this, 10, value);
};


/**
 * optional string address = 11;
 * @return {string}
 */
proto.fcp.accounting.v1.User.prototype.getAddress = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 11, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setAddress = function(value) {
  return jspb.Message.setProto3StringField(this, 11, value);
};


/**
 * optional string country_code = 12;
 * @return {string}
 */
proto.fcp.accounting.v1.User.prototype.getCountryCode = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 12, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setCountryCode = function(value) {
  return jspb.Message.setProto3StringField(this, 12, value);
};


/**
 * optional string language_code = 13;
 * @return {string}
 */
proto.fcp.accounting.v1.User.prototype.getLanguageCode = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 13, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setLanguageCode = function(value) {
  return jspb.Message.setProto3StringField(this, 13, value);
};


/**
 * optional string role = 14;
 * @return {string}
 */
proto.fcp.accounting.v1.User.prototype.getRole = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 14, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setRole = function(value) {
  return jspb.Message.setProto3StringField(this, 14, value);
};


/**
 * optional string business_name = 15;
 * @return {string}
 */
proto.fcp.accounting.v1.User.prototype.getBusinessName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 15, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setBusinessName = function(value) {
  return jspb.Message.setProto3StringField(this, 15, value);
};


/**
 * optional string business_address = 16;
 * @return {string}
 */
proto.fcp.accounting.v1.User.prototype.getBusinessAddress = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 16, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setBusinessAddress = function(value) {
  return jspb.Message.setProto3StringField(this, 16, value);
};


/**
 * optional bool change_password = 17;
 * @return {boolean}
 */
proto.fcp.accounting.v1.User.prototype.getChangePassword = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 17, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setChangePassword = function(value) {
  return jspb.Message.setProto3BooleanField(this, 17, value);
};


/**
 * optional bool verified = 27;
 * @return {boolean}
 */
proto.fcp.accounting.v1.User.prototype.getVerified = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 27, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setVerified = function(value) {
  return jspb.Message.setProto3BooleanField(this, 27, value);
};


/**
 * optional bool credit_approved = 28;
 * @return {boolean}
 */
proto.fcp.accounting.v1.User.prototype.getCreditApproved = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 28, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setCreditApproved = function(value) {
  return jspb.Message.setProto3BooleanField(this, 28, value);
};


/**
 * optional bool commodities_access = 29;
 * @return {boolean}
 */
proto.fcp.accounting.v1.User.prototype.getCommoditiesAccess = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 29, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setCommoditiesAccess = function(value) {
  return jspb.Message.setProto3BooleanField(this, 29, value);
};


/**
 * optional string referral_url = 30;
 * @return {string}
 */
proto.fcp.accounting.v1.User.prototype.getReferralUrl = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 30, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setReferralUrl = function(value) {
  return jspb.Message.setProto3StringField(this, 30, value);
};


/**
 * optional string referral_url_ios = 40;
 * @return {string}
 */
proto.fcp.accounting.v1.User.prototype.getReferralUrlIos = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 40, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setReferralUrlIos = function(value) {
  return jspb.Message.setProto3StringField(this, 40, value);
};


/**
 * optional Dealer dealer = 33;
 * @return {?proto.fcp.accounting.v1.Dealer}
 */
proto.fcp.accounting.v1.User.prototype.getDealer = function() {
  return /** @type{?proto.fcp.accounting.v1.Dealer} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.Dealer, 33));
};


/**
 * @param {?proto.fcp.accounting.v1.Dealer|undefined} value
 * @return {!proto.fcp.accounting.v1.User} returns this
*/
proto.fcp.accounting.v1.User.prototype.setDealer = function(value) {
  return jspb.Message.setWrapperField(this, 33, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.clearDealer = function() {
  return this.setDealer(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.User.prototype.hasDealer = function() {
  return jspb.Message.getField(this, 33) != null;
};


/**
 * repeated string contacts = 18;
 * @return {!Array<string>}
 */
proto.fcp.accounting.v1.User.prototype.getContactsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 18));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setContactsList = function(value) {
  return jspb.Message.setField(this, 18, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.addContacts = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 18, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.clearContactsList = function() {
  return this.setContactsList([]);
};


/**
 * optional TaxInfo tax_info = 19;
 * @return {?proto.fcp.accounting.v1.TaxInfo}
 */
proto.fcp.accounting.v1.User.prototype.getTaxInfo = function() {
  return /** @type{?proto.fcp.accounting.v1.TaxInfo} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.TaxInfo, 19));
};


/**
 * @param {?proto.fcp.accounting.v1.TaxInfo|undefined} value
 * @return {!proto.fcp.accounting.v1.User} returns this
*/
proto.fcp.accounting.v1.User.prototype.setTaxInfo = function(value) {
  return jspb.Message.setWrapperField(this, 19, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.clearTaxInfo = function() {
  return this.setTaxInfo(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.User.prototype.hasTaxInfo = function() {
  return jspb.Message.getField(this, 19) != null;
};


/**
 * optional BusinessInfo business_info = 20;
 * @return {?proto.fcp.accounting.v1.BusinessInfo}
 */
proto.fcp.accounting.v1.User.prototype.getBusinessInfo = function() {
  return /** @type{?proto.fcp.accounting.v1.BusinessInfo} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.BusinessInfo, 20));
};


/**
 * @param {?proto.fcp.accounting.v1.BusinessInfo|undefined} value
 * @return {!proto.fcp.accounting.v1.User} returns this
*/
proto.fcp.accounting.v1.User.prototype.setBusinessInfo = function(value) {
  return jspb.Message.setWrapperField(this, 20, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.clearBusinessInfo = function() {
  return this.setBusinessInfo(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.User.prototype.hasBusinessInfo = function() {
  return jspb.Message.getField(this, 20) != null;
};


/**
 * optional uint32 state = 21;
 * @return {number}
 */
proto.fcp.accounting.v1.User.prototype.getState = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 21, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setState = function(value) {
  return jspb.Message.setProto3IntField(this, 21, value);
};


/**
 * optional string note = 22;
 * @return {string}
 */
proto.fcp.accounting.v1.User.prototype.getNote = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 22, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setNote = function(value) {
  return jspb.Message.setProto3StringField(this, 22, value);
};


/**
 * optional google.protobuf.Timestamp updated = 23;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.fcp.accounting.v1.User.prototype.getUpdated = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 23));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.fcp.accounting.v1.User} returns this
*/
proto.fcp.accounting.v1.User.prototype.setUpdated = function(value) {
  return jspb.Message.setWrapperField(this, 23, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.clearUpdated = function() {
  return this.setUpdated(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.User.prototype.hasUpdated = function() {
  return jspb.Message.getField(this, 23) != null;
};


/**
 * optional string updated_by = 24;
 * @return {string}
 */
proto.fcp.accounting.v1.User.prototype.getUpdatedBy = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 24, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setUpdatedBy = function(value) {
  return jspb.Message.setProto3StringField(this, 24, value);
};


/**
 * optional google.protobuf.Timestamp created = 25;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.fcp.accounting.v1.User.prototype.getCreated = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 25));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.fcp.accounting.v1.User} returns this
*/
proto.fcp.accounting.v1.User.prototype.setCreated = function(value) {
  return jspb.Message.setWrapperField(this, 25, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.clearCreated = function() {
  return this.setCreated(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.User.prototype.hasCreated = function() {
  return jspb.Message.getField(this, 25) != null;
};


/**
 * optional string created_by = 26;
 * @return {string}
 */
proto.fcp.accounting.v1.User.prototype.getCreatedBy = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 26, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.setCreatedBy = function(value) {
  return jspb.Message.setProto3StringField(this, 26, value);
};


/**
 * optional UserBriefInfo supplier = 35;
 * @return {?proto.fcp.accounting.v1.UserBriefInfo}
 */
proto.fcp.accounting.v1.User.prototype.getSupplier = function() {
  return /** @type{?proto.fcp.accounting.v1.UserBriefInfo} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.UserBriefInfo, 35));
};


/**
 * @param {?proto.fcp.accounting.v1.UserBriefInfo|undefined} value
 * @return {!proto.fcp.accounting.v1.User} returns this
*/
proto.fcp.accounting.v1.User.prototype.setSupplier = function(value) {
  return jspb.Message.setWrapperField(this, 35, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.clearSupplier = function() {
  return this.setSupplier(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.User.prototype.hasSupplier = function() {
  return jspb.Message.getField(this, 35) != null;
};


/**
 * optional UserBriefInfo referral_user = 42;
 * @return {?proto.fcp.accounting.v1.UserBriefInfo}
 */
proto.fcp.accounting.v1.User.prototype.getReferralUser = function() {
  return /** @type{?proto.fcp.accounting.v1.UserBriefInfo} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.UserBriefInfo, 42));
};


/**
 * @param {?proto.fcp.accounting.v1.UserBriefInfo|undefined} value
 * @return {!proto.fcp.accounting.v1.User} returns this
*/
proto.fcp.accounting.v1.User.prototype.setReferralUser = function(value) {
  return jspb.Message.setWrapperField(this, 42, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.clearReferralUser = function() {
  return this.setReferralUser(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.User.prototype.hasReferralUser = function() {
  return jspb.Message.getField(this, 42) != null;
};


/**
 * repeated ContactPerson contact_persons = 36;
 * @return {!Array<!proto.fcp.accounting.v1.ContactPerson>}
 */
proto.fcp.accounting.v1.User.prototype.getContactPersonsList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.ContactPerson>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.accounting.v1.ContactPerson, 36));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.ContactPerson>} value
 * @return {!proto.fcp.accounting.v1.User} returns this
*/
proto.fcp.accounting.v1.User.prototype.setContactPersonsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 36, value);
};


/**
 * @param {!proto.fcp.accounting.v1.ContactPerson=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.ContactPerson}
 */
proto.fcp.accounting.v1.User.prototype.addContactPersons = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 36, opt_value, proto.fcp.accounting.v1.ContactPerson, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.clearContactPersonsList = function() {
  return this.setContactPersonsList([]);
};


/**
 * repeated Location locations = 37;
 * @return {!Array<!proto.fcp.accounting.v1.Location>}
 */
proto.fcp.accounting.v1.User.prototype.getLocationsList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.Location>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.accounting.v1.Location, 37));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.Location>} value
 * @return {!proto.fcp.accounting.v1.User} returns this
*/
proto.fcp.accounting.v1.User.prototype.setLocationsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 37, value);
};


/**
 * @param {!proto.fcp.accounting.v1.Location=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.Location}
 */
proto.fcp.accounting.v1.User.prototype.addLocations = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 37, opt_value, proto.fcp.accounting.v1.Location, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.clearLocationsList = function() {
  return this.setLocationsList([]);
};


/**
 * repeated Document documents = 38;
 * @return {!Array<!proto.fcp.accounting.v1.Document>}
 */
proto.fcp.accounting.v1.User.prototype.getDocumentsList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.Document>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.accounting.v1.Document, 38));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.Document>} value
 * @return {!proto.fcp.accounting.v1.User} returns this
*/
proto.fcp.accounting.v1.User.prototype.setDocumentsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 38, value);
};


/**
 * @param {!proto.fcp.accounting.v1.Document=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.Document}
 */
proto.fcp.accounting.v1.User.prototype.addDocuments = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 38, opt_value, proto.fcp.accounting.v1.Document, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.clearDocumentsList = function() {
  return this.setDocumentsList([]);
};


/**
 * repeated User employees = 39;
 * @return {!Array<!proto.fcp.accounting.v1.User>}
 */
proto.fcp.accounting.v1.User.prototype.getEmployeesList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.User>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.accounting.v1.User, 39));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.User>} value
 * @return {!proto.fcp.accounting.v1.User} returns this
*/
proto.fcp.accounting.v1.User.prototype.setEmployeesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 39, value);
};


/**
 * @param {!proto.fcp.accounting.v1.User=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.User}
 */
proto.fcp.accounting.v1.User.prototype.addEmployees = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 39, opt_value, proto.fcp.accounting.v1.User, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.User} returns this
 */
proto.fcp.accounting.v1.User.prototype.clearEmployeesList = function() {
  return this.setEmployeesList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.UserFilter.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.UserFilter.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.UserFilter} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.UserFilter.toObject = function(includeInstance, msg) {
  var f, obj = {
    role: (f = msg.getRole()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    userId: (f = msg.getUserId()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    created: (f = msg.getCreated()) && v1_auth_common_common_pb.TimeRange.toObject(includeInstance, f),
    locked: (f = msg.getLocked()) && google_protobuf_wrappers_pb.BoolValue.toObject(includeInstance, f),
    login: (f = msg.getLogin()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    supplierId: (f = msg.getSupplierId()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    verified: (f = msg.getVerified()) && google_protobuf_wrappers_pb.BoolValue.toObject(includeInstance, f),
    businessName: (f = msg.getBusinessName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    creditApproved: (f = msg.getCreditApproved()) && google_protobuf_wrappers_pb.BoolValue.toObject(includeInstance, f),
    commoditiesAccess: (f = msg.getCommoditiesAccess()) && google_protobuf_wrappers_pb.BoolValue.toObject(includeInstance, f),
    referralUserId: (f = msg.getReferralUserId()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.UserFilter}
 */
proto.fcp.accounting.v1.UserFilter.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.UserFilter;
  return proto.fcp.accounting.v1.UserFilter.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.UserFilter} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.UserFilter}
 */
proto.fcp.accounting.v1.UserFilter.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setRole(value);
      break;
    case 2:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setUserId(value);
      break;
    case 3:
      var value = new v1_auth_common_common_pb.TimeRange;
      reader.readMessage(value,v1_auth_common_common_pb.TimeRange.deserializeBinaryFromReader);
      msg.setCreated(value);
      break;
    case 4:
      var value = new google_protobuf_wrappers_pb.BoolValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.BoolValue.deserializeBinaryFromReader);
      msg.setLocked(value);
      break;
    case 5:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setLogin(value);
      break;
    case 6:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setSupplierId(value);
      break;
    case 7:
      var value = new google_protobuf_wrappers_pb.BoolValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.BoolValue.deserializeBinaryFromReader);
      msg.setVerified(value);
      break;
    case 8:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setBusinessName(value);
      break;
    case 9:
      var value = new google_protobuf_wrappers_pb.BoolValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.BoolValue.deserializeBinaryFromReader);
      msg.setCreditApproved(value);
      break;
    case 10:
      var value = new google_protobuf_wrappers_pb.BoolValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.BoolValue.deserializeBinaryFromReader);
      msg.setCommoditiesAccess(value);
      break;
    case 11:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setReferralUserId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.UserFilter.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.UserFilter.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.UserFilter} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.UserFilter.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getRole();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getUserId();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getCreated();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      v1_auth_common_common_pb.TimeRange.serializeBinaryToWriter
    );
  }
  f = message.getLocked();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      google_protobuf_wrappers_pb.BoolValue.serializeBinaryToWriter
    );
  }
  f = message.getLogin();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getSupplierId();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getVerified();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      google_protobuf_wrappers_pb.BoolValue.serializeBinaryToWriter
    );
  }
  f = message.getBusinessName();
  if (f != null) {
    writer.writeMessage(
      8,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getCreditApproved();
  if (f != null) {
    writer.writeMessage(
      9,
      f,
      google_protobuf_wrappers_pb.BoolValue.serializeBinaryToWriter
    );
  }
  f = message.getCommoditiesAccess();
  if (f != null) {
    writer.writeMessage(
      10,
      f,
      google_protobuf_wrappers_pb.BoolValue.serializeBinaryToWriter
    );
  }
  f = message.getReferralUserId();
  if (f != null) {
    writer.writeMessage(
      11,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
};


/**
 * optional google.protobuf.StringValue role = 1;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.UserFilter.prototype.getRole = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 1));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
*/
proto.fcp.accounting.v1.UserFilter.prototype.setRole = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
 */
proto.fcp.accounting.v1.UserFilter.prototype.clearRole = function() {
  return this.setRole(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserFilter.prototype.hasRole = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional google.protobuf.StringValue user_id = 2;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.UserFilter.prototype.getUserId = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 2));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
*/
proto.fcp.accounting.v1.UserFilter.prototype.setUserId = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
 */
proto.fcp.accounting.v1.UserFilter.prototype.clearUserId = function() {
  return this.setUserId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserFilter.prototype.hasUserId = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional TimeRange created = 3;
 * @return {?proto.fcp.accounting.v1.TimeRange}
 */
proto.fcp.accounting.v1.UserFilter.prototype.getCreated = function() {
  return /** @type{?proto.fcp.accounting.v1.TimeRange} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_common_pb.TimeRange, 3));
};


/**
 * @param {?proto.fcp.accounting.v1.TimeRange|undefined} value
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
*/
proto.fcp.accounting.v1.UserFilter.prototype.setCreated = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
 */
proto.fcp.accounting.v1.UserFilter.prototype.clearCreated = function() {
  return this.setCreated(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserFilter.prototype.hasCreated = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional google.protobuf.BoolValue locked = 4;
 * @return {?proto.google.protobuf.BoolValue}
 */
proto.fcp.accounting.v1.UserFilter.prototype.getLocked = function() {
  return /** @type{?proto.google.protobuf.BoolValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.BoolValue, 4));
};


/**
 * @param {?proto.google.protobuf.BoolValue|undefined} value
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
*/
proto.fcp.accounting.v1.UserFilter.prototype.setLocked = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
 */
proto.fcp.accounting.v1.UserFilter.prototype.clearLocked = function() {
  return this.setLocked(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserFilter.prototype.hasLocked = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional google.protobuf.StringValue login = 5;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.UserFilter.prototype.getLogin = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 5));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
*/
proto.fcp.accounting.v1.UserFilter.prototype.setLogin = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
 */
proto.fcp.accounting.v1.UserFilter.prototype.clearLogin = function() {
  return this.setLogin(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserFilter.prototype.hasLogin = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional google.protobuf.StringValue supplier_id = 6;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.UserFilter.prototype.getSupplierId = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 6));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
*/
proto.fcp.accounting.v1.UserFilter.prototype.setSupplierId = function(value) {
  return jspb.Message.setWrapperField(this, 6, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
 */
proto.fcp.accounting.v1.UserFilter.prototype.clearSupplierId = function() {
  return this.setSupplierId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserFilter.prototype.hasSupplierId = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional google.protobuf.BoolValue verified = 7;
 * @return {?proto.google.protobuf.BoolValue}
 */
proto.fcp.accounting.v1.UserFilter.prototype.getVerified = function() {
  return /** @type{?proto.google.protobuf.BoolValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.BoolValue, 7));
};


/**
 * @param {?proto.google.protobuf.BoolValue|undefined} value
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
*/
proto.fcp.accounting.v1.UserFilter.prototype.setVerified = function(value) {
  return jspb.Message.setWrapperField(this, 7, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
 */
proto.fcp.accounting.v1.UserFilter.prototype.clearVerified = function() {
  return this.setVerified(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserFilter.prototype.hasVerified = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional google.protobuf.StringValue business_name = 8;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.UserFilter.prototype.getBusinessName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 8));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
*/
proto.fcp.accounting.v1.UserFilter.prototype.setBusinessName = function(value) {
  return jspb.Message.setWrapperField(this, 8, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
 */
proto.fcp.accounting.v1.UserFilter.prototype.clearBusinessName = function() {
  return this.setBusinessName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserFilter.prototype.hasBusinessName = function() {
  return jspb.Message.getField(this, 8) != null;
};


/**
 * optional google.protobuf.BoolValue credit_approved = 9;
 * @return {?proto.google.protobuf.BoolValue}
 */
proto.fcp.accounting.v1.UserFilter.prototype.getCreditApproved = function() {
  return /** @type{?proto.google.protobuf.BoolValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.BoolValue, 9));
};


/**
 * @param {?proto.google.protobuf.BoolValue|undefined} value
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
*/
proto.fcp.accounting.v1.UserFilter.prototype.setCreditApproved = function(value) {
  return jspb.Message.setWrapperField(this, 9, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
 */
proto.fcp.accounting.v1.UserFilter.prototype.clearCreditApproved = function() {
  return this.setCreditApproved(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserFilter.prototype.hasCreditApproved = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * optional google.protobuf.BoolValue commodities_access = 10;
 * @return {?proto.google.protobuf.BoolValue}
 */
proto.fcp.accounting.v1.UserFilter.prototype.getCommoditiesAccess = function() {
  return /** @type{?proto.google.protobuf.BoolValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.BoolValue, 10));
};


/**
 * @param {?proto.google.protobuf.BoolValue|undefined} value
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
*/
proto.fcp.accounting.v1.UserFilter.prototype.setCommoditiesAccess = function(value) {
  return jspb.Message.setWrapperField(this, 10, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
 */
proto.fcp.accounting.v1.UserFilter.prototype.clearCommoditiesAccess = function() {
  return this.setCommoditiesAccess(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserFilter.prototype.hasCommoditiesAccess = function() {
  return jspb.Message.getField(this, 10) != null;
};


/**
 * optional google.protobuf.StringValue referral_user_id = 11;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.UserFilter.prototype.getReferralUserId = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 11));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
*/
proto.fcp.accounting.v1.UserFilter.prototype.setReferralUserId = function(value) {
  return jspb.Message.setWrapperField(this, 11, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.UserFilter} returns this
 */
proto.fcp.accounting.v1.UserFilter.prototype.clearReferralUserId = function() {
  return this.setReferralUserId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserFilter.prototype.hasReferralUserId = function() {
  return jspb.Message.getField(this, 11) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.Dealer.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.Dealer.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.Dealer} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.Dealer.toObject = function(includeInstance, msg) {
  var f, obj = {
    isDealer: jspb.Message.getBooleanFieldWithDefault(msg, 1, false),
    discount: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.Dealer}
 */
proto.fcp.accounting.v1.Dealer.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.Dealer;
  return proto.fcp.accounting.v1.Dealer.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.Dealer} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.Dealer}
 */
proto.fcp.accounting.v1.Dealer.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setIsDealer(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setDiscount(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.Dealer.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.Dealer.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.Dealer} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.Dealer.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getIsDealer();
  if (f) {
    writer.writeBool(
      1,
      f
    );
  }
  f = message.getDiscount();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
};


/**
 * optional bool is_dealer = 1;
 * @return {boolean}
 */
proto.fcp.accounting.v1.Dealer.prototype.getIsDealer = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.accounting.v1.Dealer} returns this
 */
proto.fcp.accounting.v1.Dealer.prototype.setIsDealer = function(value) {
  return jspb.Message.setProto3BooleanField(this, 1, value);
};


/**
 * optional int64 discount = 2;
 * @return {number}
 */
proto.fcp.accounting.v1.Dealer.prototype.getDiscount = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.Dealer} returns this
 */
proto.fcp.accounting.v1.Dealer.prototype.setDiscount = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.TaxInfo.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.TaxInfo.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.TaxInfo} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.TaxInfo.toObject = function(includeInstance, msg) {
  var f, obj = {
    edrpou: jspb.Message.getFieldWithDefault(msg, 1, ""),
    taxNumber: jspb.Message.getFieldWithDefault(msg, 2, ""),
    taxRegime: jspb.Message.getFieldWithDefault(msg, 3, ""),
    vatStatus: jspb.Message.getBooleanFieldWithDefault(msg, 4, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.TaxInfo}
 */
proto.fcp.accounting.v1.TaxInfo.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.TaxInfo;
  return proto.fcp.accounting.v1.TaxInfo.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.TaxInfo} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.TaxInfo}
 */
proto.fcp.accounting.v1.TaxInfo.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setEdrpou(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setTaxNumber(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setTaxRegime(value);
      break;
    case 4:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setVatStatus(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.TaxInfo.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.TaxInfo.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.TaxInfo} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.TaxInfo.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEdrpou();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getTaxNumber();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getTaxRegime();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getVatStatus();
  if (f) {
    writer.writeBool(
      4,
      f
    );
  }
};


/**
 * optional string edrpou = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.TaxInfo.prototype.getEdrpou = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.TaxInfo} returns this
 */
proto.fcp.accounting.v1.TaxInfo.prototype.setEdrpou = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string tax_number = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.TaxInfo.prototype.getTaxNumber = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.TaxInfo} returns this
 */
proto.fcp.accounting.v1.TaxInfo.prototype.setTaxNumber = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string tax_regime = 3;
 * @return {string}
 */
proto.fcp.accounting.v1.TaxInfo.prototype.getTaxRegime = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.TaxInfo} returns this
 */
proto.fcp.accounting.v1.TaxInfo.prototype.setTaxRegime = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional bool vat_status = 4;
 * @return {boolean}
 */
proto.fcp.accounting.v1.TaxInfo.prototype.getVatStatus = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 4, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.accounting.v1.TaxInfo} returns this
 */
proto.fcp.accounting.v1.TaxInfo.prototype.setVatStatus = function(value) {
  return jspb.Message.setProto3BooleanField(this, 4, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.BusinessInfo.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.BusinessInfo.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.BusinessInfo} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.BusinessInfo.toObject = function(includeInstance, msg) {
  var f, obj = {
    businessProfile: jspb.Message.getFieldWithDefault(msg, 1, ""),
    arableArea: jspb.Message.getFieldWithDefault(msg, 2, 0),
    wacc: jspb.Message.getFieldWithDefault(msg, 3, 0),
    promissoryNote: jspb.Message.getFieldWithDefault(msg, 4, 0),
    collateral: jspb.Message.getFieldWithDefault(msg, 5, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.BusinessInfo}
 */
proto.fcp.accounting.v1.BusinessInfo.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.BusinessInfo;
  return proto.fcp.accounting.v1.BusinessInfo.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.BusinessInfo} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.BusinessInfo}
 */
proto.fcp.accounting.v1.BusinessInfo.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setBusinessProfile(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setArableArea(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setWacc(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setPromissoryNote(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setCollateral(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.BusinessInfo.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.BusinessInfo.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.BusinessInfo} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.BusinessInfo.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getBusinessProfile();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getArableArea();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
  f = message.getWacc();
  if (f !== 0) {
    writer.writeInt64(
      3,
      f
    );
  }
  f = message.getPromissoryNote();
  if (f !== 0) {
    writer.writeInt64(
      4,
      f
    );
  }
  f = message.getCollateral();
  if (f !== 0) {
    writer.writeInt64(
      5,
      f
    );
  }
};


/**
 * optional string business_profile = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.BusinessInfo.prototype.getBusinessProfile = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.BusinessInfo} returns this
 */
proto.fcp.accounting.v1.BusinessInfo.prototype.setBusinessProfile = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional int64 arable_area = 2;
 * @return {number}
 */
proto.fcp.accounting.v1.BusinessInfo.prototype.getArableArea = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.BusinessInfo} returns this
 */
proto.fcp.accounting.v1.BusinessInfo.prototype.setArableArea = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional int64 wacc = 3;
 * @return {number}
 */
proto.fcp.accounting.v1.BusinessInfo.prototype.getWacc = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.BusinessInfo} returns this
 */
proto.fcp.accounting.v1.BusinessInfo.prototype.setWacc = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * optional int64 promissory_note = 4;
 * @return {number}
 */
proto.fcp.accounting.v1.BusinessInfo.prototype.getPromissoryNote = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.BusinessInfo} returns this
 */
proto.fcp.accounting.v1.BusinessInfo.prototype.setPromissoryNote = function(value) {
  return jspb.Message.setProto3IntField(this, 4, value);
};


/**
 * optional int64 collateral = 5;
 * @return {number}
 */
proto.fcp.accounting.v1.BusinessInfo.prototype.getCollateral = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.BusinessInfo} returns this
 */
proto.fcp.accounting.v1.BusinessInfo.prototype.setCollateral = function(value) {
  return jspb.Message.setProto3IntField(this, 5, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.ContactPerson.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.ContactPerson.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.ContactPerson} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ContactPerson.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: jspb.Message.getFieldWithDefault(msg, 1, 0),
    userId: jspb.Message.getFieldWithDefault(msg, 2, ""),
    name: jspb.Message.getFieldWithDefault(msg, 3, ""),
    position: jspb.Message.getFieldWithDefault(msg, 4, ""),
    phone: jspb.Message.getFieldWithDefault(msg, 5, ""),
    email: jspb.Message.getFieldWithDefault(msg, 6, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.ContactPerson}
 */
proto.fcp.accounting.v1.ContactPerson.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.ContactPerson;
  return proto.fcp.accounting.v1.ContactPerson.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.ContactPerson} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.ContactPerson}
 */
proto.fcp.accounting.v1.ContactPerson.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.fcp.accounting.v1.ContactPerson.Status} */ (reader.readEnum());
      msg.setStatus(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setPosition(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setPhone(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setEmail(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.ContactPerson.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.ContactPerson.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.ContactPerson} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ContactPerson.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getStatus();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getPosition();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getPhone();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getEmail();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.fcp.accounting.v1.ContactPerson.Status = {
  CONTACTPERSON: 0,
  PRIMARY: 1
};

/**
 * optional Status status = 1;
 * @return {!proto.fcp.accounting.v1.ContactPerson.Status}
 */
proto.fcp.accounting.v1.ContactPerson.prototype.getStatus = function() {
  return /** @type {!proto.fcp.accounting.v1.ContactPerson.Status} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.fcp.accounting.v1.ContactPerson.Status} value
 * @return {!proto.fcp.accounting.v1.ContactPerson} returns this
 */
proto.fcp.accounting.v1.ContactPerson.prototype.setStatus = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional string user_id = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.ContactPerson.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.ContactPerson} returns this
 */
proto.fcp.accounting.v1.ContactPerson.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string name = 3;
 * @return {string}
 */
proto.fcp.accounting.v1.ContactPerson.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.ContactPerson} returns this
 */
proto.fcp.accounting.v1.ContactPerson.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string position = 4;
 * @return {string}
 */
proto.fcp.accounting.v1.ContactPerson.prototype.getPosition = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.ContactPerson} returns this
 */
proto.fcp.accounting.v1.ContactPerson.prototype.setPosition = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string phone = 5;
 * @return {string}
 */
proto.fcp.accounting.v1.ContactPerson.prototype.getPhone = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.ContactPerson} returns this
 */
proto.fcp.accounting.v1.ContactPerson.prototype.setPhone = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional string email = 6;
 * @return {string}
 */
proto.fcp.accounting.v1.ContactPerson.prototype.getEmail = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.ContactPerson} returns this
 */
proto.fcp.accounting.v1.ContactPerson.prototype.setEmail = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.accounting.v1.ContactPersonFilter.repeatedFields_ = [2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.ContactPersonFilter.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.ContactPersonFilter.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.ContactPersonFilter} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ContactPersonFilter.toObject = function(includeInstance, msg) {
  var f, obj = {
    userId: (f = msg.getUserId()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    statusesList: (f = jspb.Message.getRepeatedField(msg, 2)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.ContactPersonFilter}
 */
proto.fcp.accounting.v1.ContactPersonFilter.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.ContactPersonFilter;
  return proto.fcp.accounting.v1.ContactPersonFilter.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.ContactPersonFilter} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.ContactPersonFilter}
 */
proto.fcp.accounting.v1.ContactPersonFilter.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setUserId(value);
      break;
    case 2:
      var value = /** @type {!Array<!proto.fcp.accounting.v1.ContactPerson.Status>} */ (reader.readPackedEnum());
      msg.setStatusesList(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.ContactPersonFilter.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.ContactPersonFilter.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.ContactPersonFilter} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ContactPersonFilter.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUserId();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getStatusesList();
  if (f.length > 0) {
    writer.writePackedEnum(
      2,
      f
    );
  }
};


/**
 * optional google.protobuf.StringValue user_id = 1;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.ContactPersonFilter.prototype.getUserId = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 1));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.ContactPersonFilter} returns this
*/
proto.fcp.accounting.v1.ContactPersonFilter.prototype.setUserId = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.ContactPersonFilter} returns this
 */
proto.fcp.accounting.v1.ContactPersonFilter.prototype.clearUserId = function() {
  return this.setUserId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.ContactPersonFilter.prototype.hasUserId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * repeated ContactPerson.Status statuses = 2;
 * @return {!Array<!proto.fcp.accounting.v1.ContactPerson.Status>}
 */
proto.fcp.accounting.v1.ContactPersonFilter.prototype.getStatusesList = function() {
  return /** @type {!Array<!proto.fcp.accounting.v1.ContactPerson.Status>} */ (jspb.Message.getRepeatedField(this, 2));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.ContactPerson.Status>} value
 * @return {!proto.fcp.accounting.v1.ContactPersonFilter} returns this
 */
proto.fcp.accounting.v1.ContactPersonFilter.prototype.setStatusesList = function(value) {
  return jspb.Message.setField(this, 2, value || []);
};


/**
 * @param {!proto.fcp.accounting.v1.ContactPerson.Status} value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.ContactPersonFilter} returns this
 */
proto.fcp.accounting.v1.ContactPersonFilter.prototype.addStatuses = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 2, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.ContactPersonFilter} returns this
 */
proto.fcp.accounting.v1.ContactPersonFilter.prototype.clearStatusesList = function() {
  return this.setStatusesList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.ListContactPersonRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.ListContactPersonRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.ListContactPersonRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListContactPersonRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.accounting.v1.ContactPersonFilter.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.ListContactPersonRequest}
 */
proto.fcp.accounting.v1.ListContactPersonRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.ListContactPersonRequest;
  return proto.fcp.accounting.v1.ListContactPersonRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.ListContactPersonRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.ListContactPersonRequest}
 */
proto.fcp.accounting.v1.ListContactPersonRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.ContactPersonFilter;
      reader.readMessage(value,proto.fcp.accounting.v1.ContactPersonFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.ListContactPersonRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.ListContactPersonRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.ListContactPersonRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListContactPersonRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.accounting.v1.ContactPersonFilter.serializeBinaryToWriter
    );
  }
};


/**
 * optional ContactPersonFilter filter = 1;
 * @return {?proto.fcp.accounting.v1.ContactPersonFilter}
 */
proto.fcp.accounting.v1.ListContactPersonRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.accounting.v1.ContactPersonFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.ContactPersonFilter, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.ContactPersonFilter|undefined} value
 * @return {!proto.fcp.accounting.v1.ListContactPersonRequest} returns this
*/
proto.fcp.accounting.v1.ListContactPersonRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.ListContactPersonRequest} returns this
 */
proto.fcp.accounting.v1.ListContactPersonRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.ListContactPersonRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 1) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.accounting.v1.ListContactPersonResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.ListContactPersonResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.ListContactPersonResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.ListContactPersonResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListContactPersonResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    proto.fcp.accounting.v1.ContactPerson.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.ListContactPersonResponse}
 */
proto.fcp.accounting.v1.ListContactPersonResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.ListContactPersonResponse;
  return proto.fcp.accounting.v1.ListContactPersonResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.ListContactPersonResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.ListContactPersonResponse}
 */
proto.fcp.accounting.v1.ListContactPersonResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.ContactPerson;
      reader.readMessage(value,proto.fcp.accounting.v1.ContactPerson.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.ListContactPersonResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.ListContactPersonResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.ListContactPersonResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListContactPersonResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.fcp.accounting.v1.ContactPerson.serializeBinaryToWriter
    );
  }
};


/**
 * repeated ContactPerson items = 1;
 * @return {!Array<!proto.fcp.accounting.v1.ContactPerson>}
 */
proto.fcp.accounting.v1.ListContactPersonResponse.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.ContactPerson>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.accounting.v1.ContactPerson, 1));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.ContactPerson>} value
 * @return {!proto.fcp.accounting.v1.ListContactPersonResponse} returns this
*/
proto.fcp.accounting.v1.ListContactPersonResponse.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.fcp.accounting.v1.ContactPerson=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.ContactPerson}
 */
proto.fcp.accounting.v1.ListContactPersonResponse.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.fcp.accounting.v1.ContactPerson, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.ListContactPersonResponse} returns this
 */
proto.fcp.accounting.v1.ListContactPersonResponse.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.SaveContactPersonRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.SaveContactPersonRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.SaveContactPersonRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveContactPersonRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    item: (f = msg.getItem()) && proto.fcp.accounting.v1.ContactPerson.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.SaveContactPersonRequest}
 */
proto.fcp.accounting.v1.SaveContactPersonRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.SaveContactPersonRequest;
  return proto.fcp.accounting.v1.SaveContactPersonRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.SaveContactPersonRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.SaveContactPersonRequest}
 */
proto.fcp.accounting.v1.SaveContactPersonRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.ContactPerson;
      reader.readMessage(value,proto.fcp.accounting.v1.ContactPerson.deserializeBinaryFromReader);
      msg.setItem(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.SaveContactPersonRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.SaveContactPersonRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.SaveContactPersonRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveContactPersonRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItem();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.accounting.v1.ContactPerson.serializeBinaryToWriter
    );
  }
};


/**
 * optional ContactPerson item = 1;
 * @return {?proto.fcp.accounting.v1.ContactPerson}
 */
proto.fcp.accounting.v1.SaveContactPersonRequest.prototype.getItem = function() {
  return /** @type{?proto.fcp.accounting.v1.ContactPerson} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.ContactPerson, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.ContactPerson|undefined} value
 * @return {!proto.fcp.accounting.v1.SaveContactPersonRequest} returns this
*/
proto.fcp.accounting.v1.SaveContactPersonRequest.prototype.setItem = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.SaveContactPersonRequest} returns this
 */
proto.fcp.accounting.v1.SaveContactPersonRequest.prototype.clearItem = function() {
  return this.setItem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.SaveContactPersonRequest.prototype.hasItem = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.SaveContactPersonResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.SaveContactPersonResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.SaveContactPersonResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveContactPersonResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.SaveContactPersonResponse}
 */
proto.fcp.accounting.v1.SaveContactPersonResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.SaveContactPersonResponse;
  return proto.fcp.accounting.v1.SaveContactPersonResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.SaveContactPersonResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.SaveContactPersonResponse}
 */
proto.fcp.accounting.v1.SaveContactPersonResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.SaveContactPersonResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.SaveContactPersonResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.SaveContactPersonResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveContactPersonResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.DeleteContactPersonRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.DeleteContactPersonRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.DeleteContactPersonRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteContactPersonRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    item: (f = msg.getItem()) && proto.fcp.accounting.v1.ContactPerson.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.DeleteContactPersonRequest}
 */
proto.fcp.accounting.v1.DeleteContactPersonRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.DeleteContactPersonRequest;
  return proto.fcp.accounting.v1.DeleteContactPersonRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.DeleteContactPersonRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.DeleteContactPersonRequest}
 */
proto.fcp.accounting.v1.DeleteContactPersonRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.ContactPerson;
      reader.readMessage(value,proto.fcp.accounting.v1.ContactPerson.deserializeBinaryFromReader);
      msg.setItem(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.DeleteContactPersonRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.DeleteContactPersonRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.DeleteContactPersonRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteContactPersonRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItem();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.accounting.v1.ContactPerson.serializeBinaryToWriter
    );
  }
};


/**
 * optional ContactPerson item = 1;
 * @return {?proto.fcp.accounting.v1.ContactPerson}
 */
proto.fcp.accounting.v1.DeleteContactPersonRequest.prototype.getItem = function() {
  return /** @type{?proto.fcp.accounting.v1.ContactPerson} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.ContactPerson, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.ContactPerson|undefined} value
 * @return {!proto.fcp.accounting.v1.DeleteContactPersonRequest} returns this
*/
proto.fcp.accounting.v1.DeleteContactPersonRequest.prototype.setItem = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.DeleteContactPersonRequest} returns this
 */
proto.fcp.accounting.v1.DeleteContactPersonRequest.prototype.clearItem = function() {
  return this.setItem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.DeleteContactPersonRequest.prototype.hasItem = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.DeleteContactPersonResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.DeleteContactPersonResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.DeleteContactPersonResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteContactPersonResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.DeleteContactPersonResponse}
 */
proto.fcp.accounting.v1.DeleteContactPersonResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.DeleteContactPersonResponse;
  return proto.fcp.accounting.v1.DeleteContactPersonResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.DeleteContactPersonResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.DeleteContactPersonResponse}
 */
proto.fcp.accounting.v1.DeleteContactPersonResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.DeleteContactPersonResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.DeleteContactPersonResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.DeleteContactPersonResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteContactPersonResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.Location.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.Location.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.Location} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.Location.toObject = function(includeInstance, msg) {
  var f, obj = {
    userId: jspb.Message.getFieldWithDefault(msg, 2, ""),
    type: jspb.Message.getFieldWithDefault(msg, 3, 0),
    name: jspb.Message.getFieldWithDefault(msg, 4, ""),
    region: jspb.Message.getFieldWithDefault(msg, 5, ""),
    district: jspb.Message.getFieldWithDefault(msg, 6, ""),
    city: jspb.Message.getFieldWithDefault(msg, 7, ""),
    street: jspb.Message.getFieldWithDefault(msg, 8, ""),
    building: jspb.Message.getFieldWithDefault(msg, 9, ""),
    gps: jspb.Message.getFieldWithDefault(msg, 10, ""),
    pb_default: jspb.Message.getBooleanFieldWithDefault(msg, 11, false),
    created: (f = msg.getCreated()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.Location}
 */
proto.fcp.accounting.v1.Location.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.Location;
  return proto.fcp.accounting.v1.Location.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.Location} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.Location}
 */
proto.fcp.accounting.v1.Location.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    case 3:
      var value = /** @type {!proto.fcp.accounting.v1.LocationType} */ (reader.readEnum());
      msg.setType(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setRegion(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setDistrict(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setCity(value);
      break;
    case 8:
      var value = /** @type {string} */ (reader.readString());
      msg.setStreet(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.setBuilding(value);
      break;
    case 10:
      var value = /** @type {string} */ (reader.readString());
      msg.setGps(value);
      break;
    case 11:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setDefault(value);
      break;
    case 12:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setCreated(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.Location.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.Location.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.Location} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.Location.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getType();
  if (f !== 0.0) {
    writer.writeEnum(
      3,
      f
    );
  }
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getRegion();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getDistrict();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getCity();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getStreet();
  if (f.length > 0) {
    writer.writeString(
      8,
      f
    );
  }
  f = message.getBuilding();
  if (f.length > 0) {
    writer.writeString(
      9,
      f
    );
  }
  f = message.getGps();
  if (f.length > 0) {
    writer.writeString(
      10,
      f
    );
  }
  f = message.getDefault();
  if (f) {
    writer.writeBool(
      11,
      f
    );
  }
  f = message.getCreated();
  if (f != null) {
    writer.writeMessage(
      12,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
};


/**
 * optional string user_id = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.Location.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Location} returns this
 */
proto.fcp.accounting.v1.Location.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional LocationType type = 3;
 * @return {!proto.fcp.accounting.v1.LocationType}
 */
proto.fcp.accounting.v1.Location.prototype.getType = function() {
  return /** @type {!proto.fcp.accounting.v1.LocationType} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {!proto.fcp.accounting.v1.LocationType} value
 * @return {!proto.fcp.accounting.v1.Location} returns this
 */
proto.fcp.accounting.v1.Location.prototype.setType = function(value) {
  return jspb.Message.setProto3EnumField(this, 3, value);
};


/**
 * optional string name = 4;
 * @return {string}
 */
proto.fcp.accounting.v1.Location.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Location} returns this
 */
proto.fcp.accounting.v1.Location.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string region = 5;
 * @return {string}
 */
proto.fcp.accounting.v1.Location.prototype.getRegion = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Location} returns this
 */
proto.fcp.accounting.v1.Location.prototype.setRegion = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional string district = 6;
 * @return {string}
 */
proto.fcp.accounting.v1.Location.prototype.getDistrict = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Location} returns this
 */
proto.fcp.accounting.v1.Location.prototype.setDistrict = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional string city = 7;
 * @return {string}
 */
proto.fcp.accounting.v1.Location.prototype.getCity = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Location} returns this
 */
proto.fcp.accounting.v1.Location.prototype.setCity = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * optional string street = 8;
 * @return {string}
 */
proto.fcp.accounting.v1.Location.prototype.getStreet = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 8, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Location} returns this
 */
proto.fcp.accounting.v1.Location.prototype.setStreet = function(value) {
  return jspb.Message.setProto3StringField(this, 8, value);
};


/**
 * optional string building = 9;
 * @return {string}
 */
proto.fcp.accounting.v1.Location.prototype.getBuilding = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 9, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Location} returns this
 */
proto.fcp.accounting.v1.Location.prototype.setBuilding = function(value) {
  return jspb.Message.setProto3StringField(this, 9, value);
};


/**
 * optional string gps = 10;
 * @return {string}
 */
proto.fcp.accounting.v1.Location.prototype.getGps = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 10, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Location} returns this
 */
proto.fcp.accounting.v1.Location.prototype.setGps = function(value) {
  return jspb.Message.setProto3StringField(this, 10, value);
};


/**
 * optional bool default = 11;
 * @return {boolean}
 */
proto.fcp.accounting.v1.Location.prototype.getDefault = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 11, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.accounting.v1.Location} returns this
 */
proto.fcp.accounting.v1.Location.prototype.setDefault = function(value) {
  return jspb.Message.setProto3BooleanField(this, 11, value);
};


/**
 * optional google.protobuf.Timestamp created = 12;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.fcp.accounting.v1.Location.prototype.getCreated = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 12));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.fcp.accounting.v1.Location} returns this
*/
proto.fcp.accounting.v1.Location.prototype.setCreated = function(value) {
  return jspb.Message.setWrapperField(this, 12, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.Location} returns this
 */
proto.fcp.accounting.v1.Location.prototype.clearCreated = function() {
  return this.setCreated(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.Location.prototype.hasCreated = function() {
  return jspb.Message.getField(this, 12) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.GetDefaultLocationRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.GetDefaultLocationRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.GetDefaultLocationRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.GetDefaultLocationRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    farmerId: (f = msg.getFarmerId()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.GetDefaultLocationRequest}
 */
proto.fcp.accounting.v1.GetDefaultLocationRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.GetDefaultLocationRequest;
  return proto.fcp.accounting.v1.GetDefaultLocationRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.GetDefaultLocationRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.GetDefaultLocationRequest}
 */
proto.fcp.accounting.v1.GetDefaultLocationRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setFarmerId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.GetDefaultLocationRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.GetDefaultLocationRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.GetDefaultLocationRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.GetDefaultLocationRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFarmerId();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
};


/**
 * optional google.protobuf.StringValue farmer_id = 1;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.GetDefaultLocationRequest.prototype.getFarmerId = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 1));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.GetDefaultLocationRequest} returns this
*/
proto.fcp.accounting.v1.GetDefaultLocationRequest.prototype.setFarmerId = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.GetDefaultLocationRequest} returns this
 */
proto.fcp.accounting.v1.GetDefaultLocationRequest.prototype.clearFarmerId = function() {
  return this.setFarmerId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.GetDefaultLocationRequest.prototype.hasFarmerId = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.GetDefaultLocationResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.GetDefaultLocationResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.GetDefaultLocationResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.GetDefaultLocationResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    item: (f = msg.getItem()) && proto.fcp.accounting.v1.Location.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.GetDefaultLocationResponse}
 */
proto.fcp.accounting.v1.GetDefaultLocationResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.GetDefaultLocationResponse;
  return proto.fcp.accounting.v1.GetDefaultLocationResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.GetDefaultLocationResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.GetDefaultLocationResponse}
 */
proto.fcp.accounting.v1.GetDefaultLocationResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.Location;
      reader.readMessage(value,proto.fcp.accounting.v1.Location.deserializeBinaryFromReader);
      msg.setItem(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.GetDefaultLocationResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.GetDefaultLocationResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.GetDefaultLocationResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.GetDefaultLocationResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItem();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.accounting.v1.Location.serializeBinaryToWriter
    );
  }
};


/**
 * optional Location item = 1;
 * @return {?proto.fcp.accounting.v1.Location}
 */
proto.fcp.accounting.v1.GetDefaultLocationResponse.prototype.getItem = function() {
  return /** @type{?proto.fcp.accounting.v1.Location} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.Location, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.Location|undefined} value
 * @return {!proto.fcp.accounting.v1.GetDefaultLocationResponse} returns this
*/
proto.fcp.accounting.v1.GetDefaultLocationResponse.prototype.setItem = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.GetDefaultLocationResponse} returns this
 */
proto.fcp.accounting.v1.GetDefaultLocationResponse.prototype.clearItem = function() {
  return this.setItem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.GetDefaultLocationResponse.prototype.hasItem = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.LocationFilter.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.LocationFilter.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.LocationFilter} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.LocationFilter.toObject = function(includeInstance, msg) {
  var f, obj = {
    userId: (f = msg.getUserId()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    locationType: (f = msg.getLocationType()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    pb_default: (f = msg.getDefault()) && google_protobuf_wrappers_pb.BoolValue.toObject(includeInstance, f),
    name: (f = msg.getName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.LocationFilter}
 */
proto.fcp.accounting.v1.LocationFilter.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.LocationFilter;
  return proto.fcp.accounting.v1.LocationFilter.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.LocationFilter} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.LocationFilter}
 */
proto.fcp.accounting.v1.LocationFilter.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setUserId(value);
      break;
    case 2:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setLocationType(value);
      break;
    case 3:
      var value = new google_protobuf_wrappers_pb.BoolValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.BoolValue.deserializeBinaryFromReader);
      msg.setDefault(value);
      break;
    case 4:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.LocationFilter.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.LocationFilter.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.LocationFilter} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.LocationFilter.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUserId();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getLocationType();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getDefault();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      google_protobuf_wrappers_pb.BoolValue.serializeBinaryToWriter
    );
  }
  f = message.getName();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
};


/**
 * optional google.protobuf.StringValue user_id = 1;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.LocationFilter.prototype.getUserId = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 1));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.LocationFilter} returns this
*/
proto.fcp.accounting.v1.LocationFilter.prototype.setUserId = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.LocationFilter} returns this
 */
proto.fcp.accounting.v1.LocationFilter.prototype.clearUserId = function() {
  return this.setUserId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.LocationFilter.prototype.hasUserId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional google.protobuf.Int64Value location_type = 2;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.accounting.v1.LocationFilter.prototype.getLocationType = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 2));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.accounting.v1.LocationFilter} returns this
*/
proto.fcp.accounting.v1.LocationFilter.prototype.setLocationType = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.LocationFilter} returns this
 */
proto.fcp.accounting.v1.LocationFilter.prototype.clearLocationType = function() {
  return this.setLocationType(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.LocationFilter.prototype.hasLocationType = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional google.protobuf.BoolValue default = 3;
 * @return {?proto.google.protobuf.BoolValue}
 */
proto.fcp.accounting.v1.LocationFilter.prototype.getDefault = function() {
  return /** @type{?proto.google.protobuf.BoolValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.BoolValue, 3));
};


/**
 * @param {?proto.google.protobuf.BoolValue|undefined} value
 * @return {!proto.fcp.accounting.v1.LocationFilter} returns this
*/
proto.fcp.accounting.v1.LocationFilter.prototype.setDefault = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.LocationFilter} returns this
 */
proto.fcp.accounting.v1.LocationFilter.prototype.clearDefault = function() {
  return this.setDefault(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.LocationFilter.prototype.hasDefault = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional google.protobuf.StringValue name = 4;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.LocationFilter.prototype.getName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 4));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.LocationFilter} returns this
*/
proto.fcp.accounting.v1.LocationFilter.prototype.setName = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.LocationFilter} returns this
 */
proto.fcp.accounting.v1.LocationFilter.prototype.clearName = function() {
  return this.setName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.LocationFilter.prototype.hasName = function() {
  return jspb.Message.getField(this, 4) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.ListLocationRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.ListLocationRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.ListLocationRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListLocationRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.accounting.v1.LocationFilter.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.ListLocationRequest}
 */
proto.fcp.accounting.v1.ListLocationRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.ListLocationRequest;
  return proto.fcp.accounting.v1.ListLocationRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.ListLocationRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.ListLocationRequest}
 */
proto.fcp.accounting.v1.ListLocationRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.LocationFilter;
      reader.readMessage(value,proto.fcp.accounting.v1.LocationFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.ListLocationRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.ListLocationRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.ListLocationRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListLocationRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.accounting.v1.LocationFilter.serializeBinaryToWriter
    );
  }
};


/**
 * optional LocationFilter filter = 1;
 * @return {?proto.fcp.accounting.v1.LocationFilter}
 */
proto.fcp.accounting.v1.ListLocationRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.accounting.v1.LocationFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.LocationFilter, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.LocationFilter|undefined} value
 * @return {!proto.fcp.accounting.v1.ListLocationRequest} returns this
*/
proto.fcp.accounting.v1.ListLocationRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.ListLocationRequest} returns this
 */
proto.fcp.accounting.v1.ListLocationRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.ListLocationRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 1) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.accounting.v1.ListLocationResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.ListLocationResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.ListLocationResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.ListLocationResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListLocationResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    proto.fcp.accounting.v1.Location.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.ListLocationResponse}
 */
proto.fcp.accounting.v1.ListLocationResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.ListLocationResponse;
  return proto.fcp.accounting.v1.ListLocationResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.ListLocationResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.ListLocationResponse}
 */
proto.fcp.accounting.v1.ListLocationResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.Location;
      reader.readMessage(value,proto.fcp.accounting.v1.Location.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.ListLocationResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.ListLocationResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.ListLocationResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListLocationResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.fcp.accounting.v1.Location.serializeBinaryToWriter
    );
  }
};


/**
 * repeated Location items = 1;
 * @return {!Array<!proto.fcp.accounting.v1.Location>}
 */
proto.fcp.accounting.v1.ListLocationResponse.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.Location>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.accounting.v1.Location, 1));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.Location>} value
 * @return {!proto.fcp.accounting.v1.ListLocationResponse} returns this
*/
proto.fcp.accounting.v1.ListLocationResponse.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.fcp.accounting.v1.Location=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.Location}
 */
proto.fcp.accounting.v1.ListLocationResponse.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.fcp.accounting.v1.Location, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.ListLocationResponse} returns this
 */
proto.fcp.accounting.v1.ListLocationResponse.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.SaveLocationRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.SaveLocationRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.SaveLocationRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveLocationRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    item: (f = msg.getItem()) && proto.fcp.accounting.v1.Location.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.SaveLocationRequest}
 */
proto.fcp.accounting.v1.SaveLocationRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.SaveLocationRequest;
  return proto.fcp.accounting.v1.SaveLocationRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.SaveLocationRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.SaveLocationRequest}
 */
proto.fcp.accounting.v1.SaveLocationRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.Location;
      reader.readMessage(value,proto.fcp.accounting.v1.Location.deserializeBinaryFromReader);
      msg.setItem(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.SaveLocationRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.SaveLocationRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.SaveLocationRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveLocationRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItem();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.accounting.v1.Location.serializeBinaryToWriter
    );
  }
};


/**
 * optional Location item = 1;
 * @return {?proto.fcp.accounting.v1.Location}
 */
proto.fcp.accounting.v1.SaveLocationRequest.prototype.getItem = function() {
  return /** @type{?proto.fcp.accounting.v1.Location} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.Location, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.Location|undefined} value
 * @return {!proto.fcp.accounting.v1.SaveLocationRequest} returns this
*/
proto.fcp.accounting.v1.SaveLocationRequest.prototype.setItem = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.SaveLocationRequest} returns this
 */
proto.fcp.accounting.v1.SaveLocationRequest.prototype.clearItem = function() {
  return this.setItem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.SaveLocationRequest.prototype.hasItem = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.SaveLocationResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.SaveLocationResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.SaveLocationResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveLocationResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.SaveLocationResponse}
 */
proto.fcp.accounting.v1.SaveLocationResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.SaveLocationResponse;
  return proto.fcp.accounting.v1.SaveLocationResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.SaveLocationResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.SaveLocationResponse}
 */
proto.fcp.accounting.v1.SaveLocationResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.SaveLocationResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.SaveLocationResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.SaveLocationResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveLocationResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.DeleteLocationRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.DeleteLocationRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.DeleteLocationRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteLocationRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    item: (f = msg.getItem()) && proto.fcp.accounting.v1.Location.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.DeleteLocationRequest}
 */
proto.fcp.accounting.v1.DeleteLocationRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.DeleteLocationRequest;
  return proto.fcp.accounting.v1.DeleteLocationRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.DeleteLocationRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.DeleteLocationRequest}
 */
proto.fcp.accounting.v1.DeleteLocationRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.Location;
      reader.readMessage(value,proto.fcp.accounting.v1.Location.deserializeBinaryFromReader);
      msg.setItem(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.DeleteLocationRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.DeleteLocationRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.DeleteLocationRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteLocationRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItem();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.accounting.v1.Location.serializeBinaryToWriter
    );
  }
};


/**
 * optional Location item = 1;
 * @return {?proto.fcp.accounting.v1.Location}
 */
proto.fcp.accounting.v1.DeleteLocationRequest.prototype.getItem = function() {
  return /** @type{?proto.fcp.accounting.v1.Location} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.Location, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.Location|undefined} value
 * @return {!proto.fcp.accounting.v1.DeleteLocationRequest} returns this
*/
proto.fcp.accounting.v1.DeleteLocationRequest.prototype.setItem = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.DeleteLocationRequest} returns this
 */
proto.fcp.accounting.v1.DeleteLocationRequest.prototype.clearItem = function() {
  return this.setItem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.DeleteLocationRequest.prototype.hasItem = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.DeleteLocationResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.DeleteLocationResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.DeleteLocationResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteLocationResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.DeleteLocationResponse}
 */
proto.fcp.accounting.v1.DeleteLocationResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.DeleteLocationResponse;
  return proto.fcp.accounting.v1.DeleteLocationResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.DeleteLocationResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.DeleteLocationResponse}
 */
proto.fcp.accounting.v1.DeleteLocationResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.DeleteLocationResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.DeleteLocationResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.DeleteLocationResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteLocationResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.Document.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.Document.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.Document} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.Document.toObject = function(includeInstance, msg) {
  var f, obj = {
    userId: jspb.Message.getFieldWithDefault(msg, 2, ""),
    type: jspb.Message.getFieldWithDefault(msg, 3, 0),
    name: jspb.Message.getFieldWithDefault(msg, 4, ""),
    url: jspb.Message.getFieldWithDefault(msg, 5, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.Document}
 */
proto.fcp.accounting.v1.Document.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.Document;
  return proto.fcp.accounting.v1.Document.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.Document} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.Document}
 */
proto.fcp.accounting.v1.Document.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    case 3:
      var value = /** @type {!proto.fcp.accounting.v1.Document.Type} */ (reader.readEnum());
      msg.setType(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setUrl(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.Document.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.Document.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.Document} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.Document.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getType();
  if (f !== 0.0) {
    writer.writeEnum(
      3,
      f
    );
  }
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getUrl();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
};


/**
 * @enum {number}
 */
proto.fcp.accounting.v1.Document.Type = {
  FINANCIAL: 0,
  LEGAL: 1,
  CERTIFICATE: 2
};

/**
 * optional string user_id = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.Document.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Document} returns this
 */
proto.fcp.accounting.v1.Document.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional Type type = 3;
 * @return {!proto.fcp.accounting.v1.Document.Type}
 */
proto.fcp.accounting.v1.Document.prototype.getType = function() {
  return /** @type {!proto.fcp.accounting.v1.Document.Type} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {!proto.fcp.accounting.v1.Document.Type} value
 * @return {!proto.fcp.accounting.v1.Document} returns this
 */
proto.fcp.accounting.v1.Document.prototype.setType = function(value) {
  return jspb.Message.setProto3EnumField(this, 3, value);
};


/**
 * optional string name = 4;
 * @return {string}
 */
proto.fcp.accounting.v1.Document.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Document} returns this
 */
proto.fcp.accounting.v1.Document.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string url = 5;
 * @return {string}
 */
proto.fcp.accounting.v1.Document.prototype.getUrl = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Document} returns this
 */
proto.fcp.accounting.v1.Document.prototype.setUrl = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.DocumentFilter.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.DocumentFilter.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.DocumentFilter} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DocumentFilter.toObject = function(includeInstance, msg) {
  var f, obj = {
    userId: (f = msg.getUserId()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.DocumentFilter}
 */
proto.fcp.accounting.v1.DocumentFilter.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.DocumentFilter;
  return proto.fcp.accounting.v1.DocumentFilter.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.DocumentFilter} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.DocumentFilter}
 */
proto.fcp.accounting.v1.DocumentFilter.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setUserId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.DocumentFilter.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.DocumentFilter.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.DocumentFilter} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DocumentFilter.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUserId();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
};


/**
 * optional google.protobuf.StringValue user_id = 1;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.DocumentFilter.prototype.getUserId = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 1));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.DocumentFilter} returns this
*/
proto.fcp.accounting.v1.DocumentFilter.prototype.setUserId = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.DocumentFilter} returns this
 */
proto.fcp.accounting.v1.DocumentFilter.prototype.clearUserId = function() {
  return this.setUserId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.DocumentFilter.prototype.hasUserId = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.ListDocumentRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.ListDocumentRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.ListDocumentRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListDocumentRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.accounting.v1.DocumentFilter.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.ListDocumentRequest}
 */
proto.fcp.accounting.v1.ListDocumentRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.ListDocumentRequest;
  return proto.fcp.accounting.v1.ListDocumentRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.ListDocumentRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.ListDocumentRequest}
 */
proto.fcp.accounting.v1.ListDocumentRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.DocumentFilter;
      reader.readMessage(value,proto.fcp.accounting.v1.DocumentFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.ListDocumentRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.ListDocumentRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.ListDocumentRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListDocumentRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.accounting.v1.DocumentFilter.serializeBinaryToWriter
    );
  }
};


/**
 * optional DocumentFilter filter = 1;
 * @return {?proto.fcp.accounting.v1.DocumentFilter}
 */
proto.fcp.accounting.v1.ListDocumentRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.accounting.v1.DocumentFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.DocumentFilter, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.DocumentFilter|undefined} value
 * @return {!proto.fcp.accounting.v1.ListDocumentRequest} returns this
*/
proto.fcp.accounting.v1.ListDocumentRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.ListDocumentRequest} returns this
 */
proto.fcp.accounting.v1.ListDocumentRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.ListDocumentRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 1) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.accounting.v1.ListDocumentResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.ListDocumentResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.ListDocumentResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.ListDocumentResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListDocumentResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    proto.fcp.accounting.v1.Document.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.ListDocumentResponse}
 */
proto.fcp.accounting.v1.ListDocumentResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.ListDocumentResponse;
  return proto.fcp.accounting.v1.ListDocumentResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.ListDocumentResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.ListDocumentResponse}
 */
proto.fcp.accounting.v1.ListDocumentResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.Document;
      reader.readMessage(value,proto.fcp.accounting.v1.Document.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.ListDocumentResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.ListDocumentResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.ListDocumentResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListDocumentResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.fcp.accounting.v1.Document.serializeBinaryToWriter
    );
  }
};


/**
 * repeated Document items = 1;
 * @return {!Array<!proto.fcp.accounting.v1.Document>}
 */
proto.fcp.accounting.v1.ListDocumentResponse.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.Document>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.accounting.v1.Document, 1));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.Document>} value
 * @return {!proto.fcp.accounting.v1.ListDocumentResponse} returns this
*/
proto.fcp.accounting.v1.ListDocumentResponse.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.fcp.accounting.v1.Document=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.Document}
 */
proto.fcp.accounting.v1.ListDocumentResponse.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.fcp.accounting.v1.Document, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.ListDocumentResponse} returns this
 */
proto.fcp.accounting.v1.ListDocumentResponse.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.SaveDocumentRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.SaveDocumentRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.SaveDocumentRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveDocumentRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    item: (f = msg.getItem()) && proto.fcp.accounting.v1.Document.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.SaveDocumentRequest}
 */
proto.fcp.accounting.v1.SaveDocumentRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.SaveDocumentRequest;
  return proto.fcp.accounting.v1.SaveDocumentRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.SaveDocumentRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.SaveDocumentRequest}
 */
proto.fcp.accounting.v1.SaveDocumentRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.Document;
      reader.readMessage(value,proto.fcp.accounting.v1.Document.deserializeBinaryFromReader);
      msg.setItem(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.SaveDocumentRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.SaveDocumentRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.SaveDocumentRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveDocumentRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItem();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.accounting.v1.Document.serializeBinaryToWriter
    );
  }
};


/**
 * optional Document item = 1;
 * @return {?proto.fcp.accounting.v1.Document}
 */
proto.fcp.accounting.v1.SaveDocumentRequest.prototype.getItem = function() {
  return /** @type{?proto.fcp.accounting.v1.Document} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.Document, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.Document|undefined} value
 * @return {!proto.fcp.accounting.v1.SaveDocumentRequest} returns this
*/
proto.fcp.accounting.v1.SaveDocumentRequest.prototype.setItem = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.SaveDocumentRequest} returns this
 */
proto.fcp.accounting.v1.SaveDocumentRequest.prototype.clearItem = function() {
  return this.setItem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.SaveDocumentRequest.prototype.hasItem = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.SaveDocumentResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.SaveDocumentResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.SaveDocumentResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveDocumentResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.SaveDocumentResponse}
 */
proto.fcp.accounting.v1.SaveDocumentResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.SaveDocumentResponse;
  return proto.fcp.accounting.v1.SaveDocumentResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.SaveDocumentResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.SaveDocumentResponse}
 */
proto.fcp.accounting.v1.SaveDocumentResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.SaveDocumentResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.SaveDocumentResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.SaveDocumentResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveDocumentResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.DeleteDocumentRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.DeleteDocumentRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.DeleteDocumentRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteDocumentRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    item: (f = msg.getItem()) && proto.fcp.accounting.v1.Document.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.DeleteDocumentRequest}
 */
proto.fcp.accounting.v1.DeleteDocumentRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.DeleteDocumentRequest;
  return proto.fcp.accounting.v1.DeleteDocumentRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.DeleteDocumentRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.DeleteDocumentRequest}
 */
proto.fcp.accounting.v1.DeleteDocumentRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.Document;
      reader.readMessage(value,proto.fcp.accounting.v1.Document.deserializeBinaryFromReader);
      msg.setItem(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.DeleteDocumentRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.DeleteDocumentRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.DeleteDocumentRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteDocumentRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItem();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.accounting.v1.Document.serializeBinaryToWriter
    );
  }
};


/**
 * optional Document item = 1;
 * @return {?proto.fcp.accounting.v1.Document}
 */
proto.fcp.accounting.v1.DeleteDocumentRequest.prototype.getItem = function() {
  return /** @type{?proto.fcp.accounting.v1.Document} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.Document, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.Document|undefined} value
 * @return {!proto.fcp.accounting.v1.DeleteDocumentRequest} returns this
*/
proto.fcp.accounting.v1.DeleteDocumentRequest.prototype.setItem = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.DeleteDocumentRequest} returns this
 */
proto.fcp.accounting.v1.DeleteDocumentRequest.prototype.clearItem = function() {
  return this.setItem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.DeleteDocumentRequest.prototype.hasItem = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.DeleteDocumentResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.DeleteDocumentResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.DeleteDocumentResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteDocumentResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.DeleteDocumentResponse}
 */
proto.fcp.accounting.v1.DeleteDocumentResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.DeleteDocumentResponse;
  return proto.fcp.accounting.v1.DeleteDocumentResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.DeleteDocumentResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.DeleteDocumentResponse}
 */
proto.fcp.accounting.v1.DeleteDocumentResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.DeleteDocumentResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.DeleteDocumentResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.DeleteDocumentResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteDocumentResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.UserReferralInfo.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.UserReferralInfo} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.UserReferralInfo.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, ""),
    status: jspb.Message.getFieldWithDefault(msg, 2, 0),
    role: jspb.Message.getFieldWithDefault(msg, 3, ""),
    phoneNumber: jspb.Message.getFieldWithDefault(msg, 4, ""),
    firstName: jspb.Message.getFieldWithDefault(msg, 5, ""),
    lastName: jspb.Message.getFieldWithDefault(msg, 6, ""),
    middleName: jspb.Message.getFieldWithDefault(msg, 7, ""),
    edrpou: jspb.Message.getFieldWithDefault(msg, 8, ""),
    created: (f = msg.getCreated()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.UserReferralInfo}
 */
proto.fcp.accounting.v1.UserReferralInfo.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.UserReferralInfo;
  return proto.fcp.accounting.v1.UserReferralInfo.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.UserReferralInfo} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.UserReferralInfo}
 */
proto.fcp.accounting.v1.UserReferralInfo.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readUint32());
      msg.setStatus(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setRole(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setPhoneNumber(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setFirstName(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setLastName(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setMiddleName(value);
      break;
    case 8:
      var value = /** @type {string} */ (reader.readString());
      msg.setEdrpou(value);
      break;
    case 24:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setCreated(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.UserReferralInfo.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.UserReferralInfo} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.UserReferralInfo.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getStatus();
  if (f !== 0) {
    writer.writeUint32(
      2,
      f
    );
  }
  f = message.getRole();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getPhoneNumber();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getFirstName();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getLastName();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getMiddleName();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getEdrpou();
  if (f.length > 0) {
    writer.writeString(
      8,
      f
    );
  }
  f = message.getCreated();
  if (f != null) {
    writer.writeMessage(
      24,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserReferralInfo} returns this
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.setId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional uint32 status = 2;
 * @return {number}
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.getStatus = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.UserReferralInfo} returns this
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.setStatus = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional string role = 3;
 * @return {string}
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.getRole = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserReferralInfo} returns this
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.setRole = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string phone_number = 4;
 * @return {string}
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.getPhoneNumber = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserReferralInfo} returns this
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.setPhoneNumber = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string first_name = 5;
 * @return {string}
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.getFirstName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserReferralInfo} returns this
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.setFirstName = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional string last_name = 6;
 * @return {string}
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.getLastName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserReferralInfo} returns this
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.setLastName = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional string middle_name = 7;
 * @return {string}
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.getMiddleName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserReferralInfo} returns this
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.setMiddleName = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * optional string edrpou = 8;
 * @return {string}
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.getEdrpou = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 8, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserReferralInfo} returns this
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.setEdrpou = function(value) {
  return jspb.Message.setProto3StringField(this, 8, value);
};


/**
 * optional google.protobuf.Timestamp created = 24;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.getCreated = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 24));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.fcp.accounting.v1.UserReferralInfo} returns this
*/
proto.fcp.accounting.v1.UserReferralInfo.prototype.setCreated = function(value) {
  return jspb.Message.setWrapperField(this, 24, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.UserReferralInfo} returns this
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.clearCreated = function() {
  return this.setCreated(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserReferralInfo.prototype.hasCreated = function() {
  return jspb.Message.getField(this, 24) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.accounting.v1.UserBriefInfo.repeatedFields_ = [18,36,37,38,39];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.UserBriefInfo.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.UserBriefInfo} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.UserBriefInfo.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, ""),
    status: jspb.Message.getFieldWithDefault(msg, 2, 0),
    login: jspb.Message.getFieldWithDefault(msg, 3, ""),
    phoneNumber: jspb.Message.getFieldWithDefault(msg, 4, ""),
    firstName: jspb.Message.getFieldWithDefault(msg, 5, ""),
    lastName: jspb.Message.getFieldWithDefault(msg, 6, ""),
    middleName: jspb.Message.getFieldWithDefault(msg, 7, ""),
    email: jspb.Message.getFieldWithDefault(msg, 8, ""),
    latitude: jspb.Message.getFloatingPointFieldWithDefault(msg, 9, 0.0),
    longitude: jspb.Message.getFloatingPointFieldWithDefault(msg, 10, 0.0),
    address: jspb.Message.getFieldWithDefault(msg, 11, ""),
    countryCode: jspb.Message.getFieldWithDefault(msg, 12, ""),
    languageCode: jspb.Message.getFieldWithDefault(msg, 13, ""),
    role: jspb.Message.getFieldWithDefault(msg, 14, ""),
    businessName: jspb.Message.getFieldWithDefault(msg, 15, ""),
    businessAddress: jspb.Message.getFieldWithDefault(msg, 16, ""),
    changePassword: jspb.Message.getBooleanFieldWithDefault(msg, 17, false),
    verified: jspb.Message.getBooleanFieldWithDefault(msg, 40, false),
    creditApproved: jspb.Message.getBooleanFieldWithDefault(msg, 41, false),
    commoditiesAccess: jspb.Message.getBooleanFieldWithDefault(msg, 21, false),
    referralUrl: jspb.Message.getFieldWithDefault(msg, 22, ""),
    dealer: (f = msg.getDealer()) && proto.fcp.accounting.v1.Dealer.toObject(includeInstance, f),
    contactsList: (f = jspb.Message.getRepeatedField(msg, 18)) == null ? undefined : f,
    taxInfo: (f = msg.getTaxInfo()) && proto.fcp.accounting.v1.TaxInfo.toObject(includeInstance, f),
    businessInfo: (f = msg.getBusinessInfo()) && proto.fcp.accounting.v1.BusinessInfo.toObject(includeInstance, f),
    supplier: (f = msg.getSupplier()) && proto.fcp.accounting.v1.UserBriefInfo.toObject(includeInstance, f),
    referralUser: (f = msg.getReferralUser()) && proto.fcp.accounting.v1.UserBriefInfo.toObject(includeInstance, f),
    contactPersonsList: jspb.Message.toObjectList(msg.getContactPersonsList(),
    proto.fcp.accounting.v1.ContactPerson.toObject, includeInstance),
    locationsList: jspb.Message.toObjectList(msg.getLocationsList(),
    proto.fcp.accounting.v1.Location.toObject, includeInstance),
    documentsList: jspb.Message.toObjectList(msg.getDocumentsList(),
    proto.fcp.accounting.v1.Document.toObject, includeInstance),
    employeesList: jspb.Message.toObjectList(msg.getEmployeesList(),
    proto.fcp.accounting.v1.User.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.UserBriefInfo}
 */
proto.fcp.accounting.v1.UserBriefInfo.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.UserBriefInfo;
  return proto.fcp.accounting.v1.UserBriefInfo.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.UserBriefInfo} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.UserBriefInfo}
 */
proto.fcp.accounting.v1.UserBriefInfo.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readUint32());
      msg.setStatus(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setLogin(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setPhoneNumber(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setFirstName(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setLastName(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setMiddleName(value);
      break;
    case 8:
      var value = /** @type {string} */ (reader.readString());
      msg.setEmail(value);
      break;
    case 9:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setLatitude(value);
      break;
    case 10:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setLongitude(value);
      break;
    case 11:
      var value = /** @type {string} */ (reader.readString());
      msg.setAddress(value);
      break;
    case 12:
      var value = /** @type {string} */ (reader.readString());
      msg.setCountryCode(value);
      break;
    case 13:
      var value = /** @type {string} */ (reader.readString());
      msg.setLanguageCode(value);
      break;
    case 14:
      var value = /** @type {string} */ (reader.readString());
      msg.setRole(value);
      break;
    case 15:
      var value = /** @type {string} */ (reader.readString());
      msg.setBusinessName(value);
      break;
    case 16:
      var value = /** @type {string} */ (reader.readString());
      msg.setBusinessAddress(value);
      break;
    case 17:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setChangePassword(value);
      break;
    case 40:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setVerified(value);
      break;
    case 41:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setCreditApproved(value);
      break;
    case 21:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setCommoditiesAccess(value);
      break;
    case 22:
      var value = /** @type {string} */ (reader.readString());
      msg.setReferralUrl(value);
      break;
    case 25:
      var value = new proto.fcp.accounting.v1.Dealer;
      reader.readMessage(value,proto.fcp.accounting.v1.Dealer.deserializeBinaryFromReader);
      msg.setDealer(value);
      break;
    case 18:
      var value = /** @type {string} */ (reader.readString());
      msg.addContacts(value);
      break;
    case 19:
      var value = new proto.fcp.accounting.v1.TaxInfo;
      reader.readMessage(value,proto.fcp.accounting.v1.TaxInfo.deserializeBinaryFromReader);
      msg.setTaxInfo(value);
      break;
    case 20:
      var value = new proto.fcp.accounting.v1.BusinessInfo;
      reader.readMessage(value,proto.fcp.accounting.v1.BusinessInfo.deserializeBinaryFromReader);
      msg.setBusinessInfo(value);
      break;
    case 35:
      var value = new proto.fcp.accounting.v1.UserBriefInfo;
      reader.readMessage(value,proto.fcp.accounting.v1.UserBriefInfo.deserializeBinaryFromReader);
      msg.setSupplier(value);
      break;
    case 42:
      var value = new proto.fcp.accounting.v1.UserBriefInfo;
      reader.readMessage(value,proto.fcp.accounting.v1.UserBriefInfo.deserializeBinaryFromReader);
      msg.setReferralUser(value);
      break;
    case 36:
      var value = new proto.fcp.accounting.v1.ContactPerson;
      reader.readMessage(value,proto.fcp.accounting.v1.ContactPerson.deserializeBinaryFromReader);
      msg.addContactPersons(value);
      break;
    case 37:
      var value = new proto.fcp.accounting.v1.Location;
      reader.readMessage(value,proto.fcp.accounting.v1.Location.deserializeBinaryFromReader);
      msg.addLocations(value);
      break;
    case 38:
      var value = new proto.fcp.accounting.v1.Document;
      reader.readMessage(value,proto.fcp.accounting.v1.Document.deserializeBinaryFromReader);
      msg.addDocuments(value);
      break;
    case 39:
      var value = new proto.fcp.accounting.v1.User;
      reader.readMessage(value,proto.fcp.accounting.v1.User.deserializeBinaryFromReader);
      msg.addEmployees(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.UserBriefInfo.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.UserBriefInfo} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.UserBriefInfo.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getStatus();
  if (f !== 0) {
    writer.writeUint32(
      2,
      f
    );
  }
  f = message.getLogin();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getPhoneNumber();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getFirstName();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getLastName();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getMiddleName();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getEmail();
  if (f.length > 0) {
    writer.writeString(
      8,
      f
    );
  }
  f = message.getLatitude();
  if (f !== 0.0) {
    writer.writeFloat(
      9,
      f
    );
  }
  f = message.getLongitude();
  if (f !== 0.0) {
    writer.writeFloat(
      10,
      f
    );
  }
  f = message.getAddress();
  if (f.length > 0) {
    writer.writeString(
      11,
      f
    );
  }
  f = message.getCountryCode();
  if (f.length > 0) {
    writer.writeString(
      12,
      f
    );
  }
  f = message.getLanguageCode();
  if (f.length > 0) {
    writer.writeString(
      13,
      f
    );
  }
  f = message.getRole();
  if (f.length > 0) {
    writer.writeString(
      14,
      f
    );
  }
  f = message.getBusinessName();
  if (f.length > 0) {
    writer.writeString(
      15,
      f
    );
  }
  f = message.getBusinessAddress();
  if (f.length > 0) {
    writer.writeString(
      16,
      f
    );
  }
  f = message.getChangePassword();
  if (f) {
    writer.writeBool(
      17,
      f
    );
  }
  f = message.getVerified();
  if (f) {
    writer.writeBool(
      40,
      f
    );
  }
  f = message.getCreditApproved();
  if (f) {
    writer.writeBool(
      41,
      f
    );
  }
  f = message.getCommoditiesAccess();
  if (f) {
    writer.writeBool(
      21,
      f
    );
  }
  f = message.getReferralUrl();
  if (f.length > 0) {
    writer.writeString(
      22,
      f
    );
  }
  f = message.getDealer();
  if (f != null) {
    writer.writeMessage(
      25,
      f,
      proto.fcp.accounting.v1.Dealer.serializeBinaryToWriter
    );
  }
  f = message.getContactsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      18,
      f
    );
  }
  f = message.getTaxInfo();
  if (f != null) {
    writer.writeMessage(
      19,
      f,
      proto.fcp.accounting.v1.TaxInfo.serializeBinaryToWriter
    );
  }
  f = message.getBusinessInfo();
  if (f != null) {
    writer.writeMessage(
      20,
      f,
      proto.fcp.accounting.v1.BusinessInfo.serializeBinaryToWriter
    );
  }
  f = message.getSupplier();
  if (f != null) {
    writer.writeMessage(
      35,
      f,
      proto.fcp.accounting.v1.UserBriefInfo.serializeBinaryToWriter
    );
  }
  f = message.getReferralUser();
  if (f != null) {
    writer.writeMessage(
      42,
      f,
      proto.fcp.accounting.v1.UserBriefInfo.serializeBinaryToWriter
    );
  }
  f = message.getContactPersonsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      36,
      f,
      proto.fcp.accounting.v1.ContactPerson.serializeBinaryToWriter
    );
  }
  f = message.getLocationsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      37,
      f,
      proto.fcp.accounting.v1.Location.serializeBinaryToWriter
    );
  }
  f = message.getDocumentsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      38,
      f,
      proto.fcp.accounting.v1.Document.serializeBinaryToWriter
    );
  }
  f = message.getEmployeesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      39,
      f,
      proto.fcp.accounting.v1.User.serializeBinaryToWriter
    );
  }
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional uint32 status = 2;
 * @return {number}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getStatus = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setStatus = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional string login = 3;
 * @return {string}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getLogin = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setLogin = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string phone_number = 4;
 * @return {string}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getPhoneNumber = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setPhoneNumber = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string first_name = 5;
 * @return {string}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getFirstName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setFirstName = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional string last_name = 6;
 * @return {string}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getLastName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setLastName = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional string middle_name = 7;
 * @return {string}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getMiddleName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setMiddleName = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * optional string email = 8;
 * @return {string}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getEmail = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 8, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setEmail = function(value) {
  return jspb.Message.setProto3StringField(this, 8, value);
};


/**
 * optional float latitude = 9;
 * @return {number}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getLatitude = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 9, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setLatitude = function(value) {
  return jspb.Message.setProto3FloatField(this, 9, value);
};


/**
 * optional float longitude = 10;
 * @return {number}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getLongitude = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 10, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setLongitude = function(value) {
  return jspb.Message.setProto3FloatField(this, 10, value);
};


/**
 * optional string address = 11;
 * @return {string}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getAddress = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 11, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setAddress = function(value) {
  return jspb.Message.setProto3StringField(this, 11, value);
};


/**
 * optional string country_code = 12;
 * @return {string}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getCountryCode = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 12, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setCountryCode = function(value) {
  return jspb.Message.setProto3StringField(this, 12, value);
};


/**
 * optional string language_code = 13;
 * @return {string}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getLanguageCode = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 13, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setLanguageCode = function(value) {
  return jspb.Message.setProto3StringField(this, 13, value);
};


/**
 * optional string role = 14;
 * @return {string}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getRole = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 14, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setRole = function(value) {
  return jspb.Message.setProto3StringField(this, 14, value);
};


/**
 * optional string business_name = 15;
 * @return {string}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getBusinessName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 15, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setBusinessName = function(value) {
  return jspb.Message.setProto3StringField(this, 15, value);
};


/**
 * optional string business_address = 16;
 * @return {string}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getBusinessAddress = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 16, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setBusinessAddress = function(value) {
  return jspb.Message.setProto3StringField(this, 16, value);
};


/**
 * optional bool change_password = 17;
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getChangePassword = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 17, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setChangePassword = function(value) {
  return jspb.Message.setProto3BooleanField(this, 17, value);
};


/**
 * optional bool verified = 40;
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getVerified = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 40, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setVerified = function(value) {
  return jspb.Message.setProto3BooleanField(this, 40, value);
};


/**
 * optional bool credit_approved = 41;
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getCreditApproved = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 41, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setCreditApproved = function(value) {
  return jspb.Message.setProto3BooleanField(this, 41, value);
};


/**
 * optional bool commodities_access = 21;
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getCommoditiesAccess = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 21, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setCommoditiesAccess = function(value) {
  return jspb.Message.setProto3BooleanField(this, 21, value);
};


/**
 * optional string referral_url = 22;
 * @return {string}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getReferralUrl = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 22, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setReferralUrl = function(value) {
  return jspb.Message.setProto3StringField(this, 22, value);
};


/**
 * optional Dealer dealer = 25;
 * @return {?proto.fcp.accounting.v1.Dealer}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getDealer = function() {
  return /** @type{?proto.fcp.accounting.v1.Dealer} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.Dealer, 25));
};


/**
 * @param {?proto.fcp.accounting.v1.Dealer|undefined} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
*/
proto.fcp.accounting.v1.UserBriefInfo.prototype.setDealer = function(value) {
  return jspb.Message.setWrapperField(this, 25, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.clearDealer = function() {
  return this.setDealer(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.hasDealer = function() {
  return jspb.Message.getField(this, 25) != null;
};


/**
 * repeated string contacts = 18;
 * @return {!Array<string>}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getContactsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 18));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.setContactsList = function(value) {
  return jspb.Message.setField(this, 18, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.addContacts = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 18, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.clearContactsList = function() {
  return this.setContactsList([]);
};


/**
 * optional TaxInfo tax_info = 19;
 * @return {?proto.fcp.accounting.v1.TaxInfo}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getTaxInfo = function() {
  return /** @type{?proto.fcp.accounting.v1.TaxInfo} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.TaxInfo, 19));
};


/**
 * @param {?proto.fcp.accounting.v1.TaxInfo|undefined} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
*/
proto.fcp.accounting.v1.UserBriefInfo.prototype.setTaxInfo = function(value) {
  return jspb.Message.setWrapperField(this, 19, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.clearTaxInfo = function() {
  return this.setTaxInfo(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.hasTaxInfo = function() {
  return jspb.Message.getField(this, 19) != null;
};


/**
 * optional BusinessInfo business_info = 20;
 * @return {?proto.fcp.accounting.v1.BusinessInfo}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getBusinessInfo = function() {
  return /** @type{?proto.fcp.accounting.v1.BusinessInfo} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.BusinessInfo, 20));
};


/**
 * @param {?proto.fcp.accounting.v1.BusinessInfo|undefined} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
*/
proto.fcp.accounting.v1.UserBriefInfo.prototype.setBusinessInfo = function(value) {
  return jspb.Message.setWrapperField(this, 20, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.clearBusinessInfo = function() {
  return this.setBusinessInfo(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.hasBusinessInfo = function() {
  return jspb.Message.getField(this, 20) != null;
};


/**
 * optional UserBriefInfo supplier = 35;
 * @return {?proto.fcp.accounting.v1.UserBriefInfo}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getSupplier = function() {
  return /** @type{?proto.fcp.accounting.v1.UserBriefInfo} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.UserBriefInfo, 35));
};


/**
 * @param {?proto.fcp.accounting.v1.UserBriefInfo|undefined} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
*/
proto.fcp.accounting.v1.UserBriefInfo.prototype.setSupplier = function(value) {
  return jspb.Message.setWrapperField(this, 35, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.clearSupplier = function() {
  return this.setSupplier(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.hasSupplier = function() {
  return jspb.Message.getField(this, 35) != null;
};


/**
 * optional UserBriefInfo referral_user = 42;
 * @return {?proto.fcp.accounting.v1.UserBriefInfo}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getReferralUser = function() {
  return /** @type{?proto.fcp.accounting.v1.UserBriefInfo} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.UserBriefInfo, 42));
};


/**
 * @param {?proto.fcp.accounting.v1.UserBriefInfo|undefined} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
*/
proto.fcp.accounting.v1.UserBriefInfo.prototype.setReferralUser = function(value) {
  return jspb.Message.setWrapperField(this, 42, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.clearReferralUser = function() {
  return this.setReferralUser(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.hasReferralUser = function() {
  return jspb.Message.getField(this, 42) != null;
};


/**
 * repeated ContactPerson contact_persons = 36;
 * @return {!Array<!proto.fcp.accounting.v1.ContactPerson>}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getContactPersonsList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.ContactPerson>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.accounting.v1.ContactPerson, 36));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.ContactPerson>} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
*/
proto.fcp.accounting.v1.UserBriefInfo.prototype.setContactPersonsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 36, value);
};


/**
 * @param {!proto.fcp.accounting.v1.ContactPerson=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.ContactPerson}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.addContactPersons = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 36, opt_value, proto.fcp.accounting.v1.ContactPerson, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.clearContactPersonsList = function() {
  return this.setContactPersonsList([]);
};


/**
 * repeated Location locations = 37;
 * @return {!Array<!proto.fcp.accounting.v1.Location>}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getLocationsList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.Location>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.accounting.v1.Location, 37));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.Location>} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
*/
proto.fcp.accounting.v1.UserBriefInfo.prototype.setLocationsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 37, value);
};


/**
 * @param {!proto.fcp.accounting.v1.Location=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.Location}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.addLocations = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 37, opt_value, proto.fcp.accounting.v1.Location, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.clearLocationsList = function() {
  return this.setLocationsList([]);
};


/**
 * repeated Document documents = 38;
 * @return {!Array<!proto.fcp.accounting.v1.Document>}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getDocumentsList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.Document>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.accounting.v1.Document, 38));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.Document>} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
*/
proto.fcp.accounting.v1.UserBriefInfo.prototype.setDocumentsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 38, value);
};


/**
 * @param {!proto.fcp.accounting.v1.Document=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.Document}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.addDocuments = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 38, opt_value, proto.fcp.accounting.v1.Document, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.clearDocumentsList = function() {
  return this.setDocumentsList([]);
};


/**
 * repeated User employees = 39;
 * @return {!Array<!proto.fcp.accounting.v1.User>}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.getEmployeesList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.User>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.accounting.v1.User, 39));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.User>} value
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
*/
proto.fcp.accounting.v1.UserBriefInfo.prototype.setEmployeesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 39, value);
};


/**
 * @param {!proto.fcp.accounting.v1.User=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.User}
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.addEmployees = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 39, opt_value, proto.fcp.accounting.v1.User, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.UserBriefInfo} returns this
 */
proto.fcp.accounting.v1.UserBriefInfo.prototype.clearEmployeesList = function() {
  return this.setEmployeesList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.accounting.v1.Role.repeatedFields_ = [3];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.Role.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.Role.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.Role} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.Role.toObject = function(includeInstance, msg) {
  var f, obj = {
    role: jspb.Message.getFieldWithDefault(msg, 1, ""),
    description: jspb.Message.getFieldWithDefault(msg, 2, ""),
    permissionsList: jspb.Message.toObjectList(msg.getPermissionsList(),
    proto.fcp.accounting.v1.Permission.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.Role}
 */
proto.fcp.accounting.v1.Role.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.Role;
  return proto.fcp.accounting.v1.Role.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.Role} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.Role}
 */
proto.fcp.accounting.v1.Role.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setRole(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setDescription(value);
      break;
    case 3:
      var value = new proto.fcp.accounting.v1.Permission;
      reader.readMessage(value,proto.fcp.accounting.v1.Permission.deserializeBinaryFromReader);
      msg.addPermissions(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.Role.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.Role.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.Role} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.Role.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getRole();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getDescription();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getPermissionsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      3,
      f,
      proto.fcp.accounting.v1.Permission.serializeBinaryToWriter
    );
  }
};


/**
 * optional string role = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.Role.prototype.getRole = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Role} returns this
 */
proto.fcp.accounting.v1.Role.prototype.setRole = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string description = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.Role.prototype.getDescription = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Role} returns this
 */
proto.fcp.accounting.v1.Role.prototype.setDescription = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * repeated Permission permissions = 3;
 * @return {!Array<!proto.fcp.accounting.v1.Permission>}
 */
proto.fcp.accounting.v1.Role.prototype.getPermissionsList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.Permission>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.accounting.v1.Permission, 3));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.Permission>} value
 * @return {!proto.fcp.accounting.v1.Role} returns this
*/
proto.fcp.accounting.v1.Role.prototype.setPermissionsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 3, value);
};


/**
 * @param {!proto.fcp.accounting.v1.Permission=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.Permission}
 */
proto.fcp.accounting.v1.Role.prototype.addPermissions = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 3, opt_value, proto.fcp.accounting.v1.Permission, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.Role} returns this
 */
proto.fcp.accounting.v1.Role.prototype.clearPermissionsList = function() {
  return this.setPermissionsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.Permission.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.Permission.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.Permission} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.Permission.toObject = function(includeInstance, msg) {
  var f, obj = {
    permission: jspb.Message.getFieldWithDefault(msg, 1, ""),
    description: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.Permission}
 */
proto.fcp.accounting.v1.Permission.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.Permission;
  return proto.fcp.accounting.v1.Permission.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.Permission} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.Permission}
 */
proto.fcp.accounting.v1.Permission.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setPermission(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setDescription(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.Permission.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.Permission.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.Permission} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.Permission.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPermission();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getDescription();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string permission = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.Permission.prototype.getPermission = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Permission} returns this
 */
proto.fcp.accounting.v1.Permission.prototype.setPermission = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string description = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.Permission.prototype.getDescription = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Permission} returns this
 */
proto.fcp.accounting.v1.Permission.prototype.setDescription = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.accounting.v1.RoleFull.repeatedFields_ = [3];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.RoleFull.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.RoleFull.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.RoleFull} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.RoleFull.toObject = function(includeInstance, msg) {
  var f, obj = {
    role: jspb.Message.getFieldWithDefault(msg, 1, ""),
    description: jspb.Message.getFieldWithDefault(msg, 2, ""),
    permissionsList: jspb.Message.toObjectList(msg.getPermissionsList(),
    proto.fcp.accounting.v1.PermissionFull.toObject, includeInstance),
    state: jspb.Message.getFieldWithDefault(msg, 4, 0),
    note: jspb.Message.getFieldWithDefault(msg, 5, ""),
    updated: (f = msg.getUpdated()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    updatedBy: jspb.Message.getFieldWithDefault(msg, 7, ""),
    created: (f = msg.getCreated()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    createdBy: jspb.Message.getFieldWithDefault(msg, 9, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.RoleFull}
 */
proto.fcp.accounting.v1.RoleFull.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.RoleFull;
  return proto.fcp.accounting.v1.RoleFull.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.RoleFull} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.RoleFull}
 */
proto.fcp.accounting.v1.RoleFull.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setRole(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setDescription(value);
      break;
    case 3:
      var value = new proto.fcp.accounting.v1.PermissionFull;
      reader.readMessage(value,proto.fcp.accounting.v1.PermissionFull.deserializeBinaryFromReader);
      msg.addPermissions(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readUint32());
      msg.setState(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setNote(value);
      break;
    case 6:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setUpdated(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setUpdatedBy(value);
      break;
    case 8:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setCreated(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.setCreatedBy(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.RoleFull.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.RoleFull.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.RoleFull} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.RoleFull.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getRole();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getDescription();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getPermissionsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      3,
      f,
      proto.fcp.accounting.v1.PermissionFull.serializeBinaryToWriter
    );
  }
  f = message.getState();
  if (f !== 0) {
    writer.writeUint32(
      4,
      f
    );
  }
  f = message.getNote();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getUpdated();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getUpdatedBy();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getCreated();
  if (f != null) {
    writer.writeMessage(
      8,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getCreatedBy();
  if (f.length > 0) {
    writer.writeString(
      9,
      f
    );
  }
};


/**
 * optional string role = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.RoleFull.prototype.getRole = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.RoleFull} returns this
 */
proto.fcp.accounting.v1.RoleFull.prototype.setRole = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string description = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.RoleFull.prototype.getDescription = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.RoleFull} returns this
 */
proto.fcp.accounting.v1.RoleFull.prototype.setDescription = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * repeated PermissionFull permissions = 3;
 * @return {!Array<!proto.fcp.accounting.v1.PermissionFull>}
 */
proto.fcp.accounting.v1.RoleFull.prototype.getPermissionsList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.PermissionFull>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.accounting.v1.PermissionFull, 3));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.PermissionFull>} value
 * @return {!proto.fcp.accounting.v1.RoleFull} returns this
*/
proto.fcp.accounting.v1.RoleFull.prototype.setPermissionsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 3, value);
};


/**
 * @param {!proto.fcp.accounting.v1.PermissionFull=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.PermissionFull}
 */
proto.fcp.accounting.v1.RoleFull.prototype.addPermissions = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 3, opt_value, proto.fcp.accounting.v1.PermissionFull, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.RoleFull} returns this
 */
proto.fcp.accounting.v1.RoleFull.prototype.clearPermissionsList = function() {
  return this.setPermissionsList([]);
};


/**
 * optional uint32 state = 4;
 * @return {number}
 */
proto.fcp.accounting.v1.RoleFull.prototype.getState = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.RoleFull} returns this
 */
proto.fcp.accounting.v1.RoleFull.prototype.setState = function(value) {
  return jspb.Message.setProto3IntField(this, 4, value);
};


/**
 * optional string note = 5;
 * @return {string}
 */
proto.fcp.accounting.v1.RoleFull.prototype.getNote = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.RoleFull} returns this
 */
proto.fcp.accounting.v1.RoleFull.prototype.setNote = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional google.protobuf.Timestamp updated = 6;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.fcp.accounting.v1.RoleFull.prototype.getUpdated = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 6));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.fcp.accounting.v1.RoleFull} returns this
*/
proto.fcp.accounting.v1.RoleFull.prototype.setUpdated = function(value) {
  return jspb.Message.setWrapperField(this, 6, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.RoleFull} returns this
 */
proto.fcp.accounting.v1.RoleFull.prototype.clearUpdated = function() {
  return this.setUpdated(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.RoleFull.prototype.hasUpdated = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional string updated_by = 7;
 * @return {string}
 */
proto.fcp.accounting.v1.RoleFull.prototype.getUpdatedBy = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.RoleFull} returns this
 */
proto.fcp.accounting.v1.RoleFull.prototype.setUpdatedBy = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * optional google.protobuf.Timestamp created = 8;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.fcp.accounting.v1.RoleFull.prototype.getCreated = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 8));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.fcp.accounting.v1.RoleFull} returns this
*/
proto.fcp.accounting.v1.RoleFull.prototype.setCreated = function(value) {
  return jspb.Message.setWrapperField(this, 8, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.RoleFull} returns this
 */
proto.fcp.accounting.v1.RoleFull.prototype.clearCreated = function() {
  return this.setCreated(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.RoleFull.prototype.hasCreated = function() {
  return jspb.Message.getField(this, 8) != null;
};


/**
 * optional string created_by = 9;
 * @return {string}
 */
proto.fcp.accounting.v1.RoleFull.prototype.getCreatedBy = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 9, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.RoleFull} returns this
 */
proto.fcp.accounting.v1.RoleFull.prototype.setCreatedBy = function(value) {
  return jspb.Message.setProto3StringField(this, 9, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.PermissionFull.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.PermissionFull.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.PermissionFull} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.PermissionFull.toObject = function(includeInstance, msg) {
  var f, obj = {
    permission: jspb.Message.getFieldWithDefault(msg, 1, ""),
    description: jspb.Message.getFieldWithDefault(msg, 2, ""),
    state: jspb.Message.getFieldWithDefault(msg, 3, 0),
    note: jspb.Message.getFieldWithDefault(msg, 4, ""),
    updated: (f = msg.getUpdated()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    updatedBy: jspb.Message.getFieldWithDefault(msg, 6, ""),
    created: (f = msg.getCreated()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    createdBy: jspb.Message.getFieldWithDefault(msg, 8, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.PermissionFull}
 */
proto.fcp.accounting.v1.PermissionFull.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.PermissionFull;
  return proto.fcp.accounting.v1.PermissionFull.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.PermissionFull} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.PermissionFull}
 */
proto.fcp.accounting.v1.PermissionFull.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setPermission(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setDescription(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readUint32());
      msg.setState(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setNote(value);
      break;
    case 5:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setUpdated(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setUpdatedBy(value);
      break;
    case 7:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setCreated(value);
      break;
    case 8:
      var value = /** @type {string} */ (reader.readString());
      msg.setCreatedBy(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.PermissionFull.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.PermissionFull.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.PermissionFull} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.PermissionFull.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPermission();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getDescription();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getState();
  if (f !== 0) {
    writer.writeUint32(
      3,
      f
    );
  }
  f = message.getNote();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getUpdated();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getUpdatedBy();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getCreated();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getCreatedBy();
  if (f.length > 0) {
    writer.writeString(
      8,
      f
    );
  }
};


/**
 * optional string permission = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.PermissionFull.prototype.getPermission = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.PermissionFull} returns this
 */
proto.fcp.accounting.v1.PermissionFull.prototype.setPermission = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string description = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.PermissionFull.prototype.getDescription = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.PermissionFull} returns this
 */
proto.fcp.accounting.v1.PermissionFull.prototype.setDescription = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional uint32 state = 3;
 * @return {number}
 */
proto.fcp.accounting.v1.PermissionFull.prototype.getState = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.PermissionFull} returns this
 */
proto.fcp.accounting.v1.PermissionFull.prototype.setState = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * optional string note = 4;
 * @return {string}
 */
proto.fcp.accounting.v1.PermissionFull.prototype.getNote = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.PermissionFull} returns this
 */
proto.fcp.accounting.v1.PermissionFull.prototype.setNote = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional google.protobuf.Timestamp updated = 5;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.fcp.accounting.v1.PermissionFull.prototype.getUpdated = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 5));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.fcp.accounting.v1.PermissionFull} returns this
*/
proto.fcp.accounting.v1.PermissionFull.prototype.setUpdated = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.PermissionFull} returns this
 */
proto.fcp.accounting.v1.PermissionFull.prototype.clearUpdated = function() {
  return this.setUpdated(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.PermissionFull.prototype.hasUpdated = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional string updated_by = 6;
 * @return {string}
 */
proto.fcp.accounting.v1.PermissionFull.prototype.getUpdatedBy = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.PermissionFull} returns this
 */
proto.fcp.accounting.v1.PermissionFull.prototype.setUpdatedBy = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional google.protobuf.Timestamp created = 7;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.fcp.accounting.v1.PermissionFull.prototype.getCreated = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 7));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.fcp.accounting.v1.PermissionFull} returns this
*/
proto.fcp.accounting.v1.PermissionFull.prototype.setCreated = function(value) {
  return jspb.Message.setWrapperField(this, 7, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.PermissionFull} returns this
 */
proto.fcp.accounting.v1.PermissionFull.prototype.clearCreated = function() {
  return this.setCreated(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.PermissionFull.prototype.hasCreated = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional string created_by = 8;
 * @return {string}
 */
proto.fcp.accounting.v1.PermissionFull.prototype.getCreatedBy = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 8, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.PermissionFull} returns this
 */
proto.fcp.accounting.v1.PermissionFull.prototype.setCreatedBy = function(value) {
  return jspb.Message.setProto3StringField(this, 8, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.Session.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.Session.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.Session} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.Session.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0),
    token: jspb.Message.getFieldWithDefault(msg, 2, ""),
    userId: jspb.Message.getFieldWithDefault(msg, 3, ""),
    serviceName: jspb.Message.getFieldWithDefault(msg, 4, ""),
    createdTime: (f = msg.getCreatedTime()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    expiredTime: (f = msg.getExpiredTime()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.Session}
 */
proto.fcp.accounting.v1.Session.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.Session;
  return proto.fcp.accounting.v1.Session.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.Session} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.Session}
 */
proto.fcp.accounting.v1.Session.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setToken(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setServiceName(value);
      break;
    case 5:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setCreatedTime(value);
      break;
    case 6:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setExpiredTime(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.Session.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.Session.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.Session} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.Session.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getToken();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getServiceName();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getCreatedTime();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getExpiredTime();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.accounting.v1.Session.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.Session} returns this
 */
proto.fcp.accounting.v1.Session.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string token = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.Session.prototype.getToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Session} returns this
 */
proto.fcp.accounting.v1.Session.prototype.setToken = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string user_id = 3;
 * @return {string}
 */
proto.fcp.accounting.v1.Session.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Session} returns this
 */
proto.fcp.accounting.v1.Session.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string service_name = 4;
 * @return {string}
 */
proto.fcp.accounting.v1.Session.prototype.getServiceName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Session} returns this
 */
proto.fcp.accounting.v1.Session.prototype.setServiceName = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional google.protobuf.Timestamp created_time = 5;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.fcp.accounting.v1.Session.prototype.getCreatedTime = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 5));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.fcp.accounting.v1.Session} returns this
*/
proto.fcp.accounting.v1.Session.prototype.setCreatedTime = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.Session} returns this
 */
proto.fcp.accounting.v1.Session.prototype.clearCreatedTime = function() {
  return this.setCreatedTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.Session.prototype.hasCreatedTime = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional google.protobuf.Timestamp expired_time = 6;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.fcp.accounting.v1.Session.prototype.getExpiredTime = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 6));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.fcp.accounting.v1.Session} returns this
*/
proto.fcp.accounting.v1.Session.prototype.setExpiredTime = function(value) {
  return jspb.Message.setWrapperField(this, 6, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.Session} returns this
 */
proto.fcp.accounting.v1.Session.prototype.clearExpiredTime = function() {
  return this.setExpiredTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.Session.prototype.hasExpiredTime = function() {
  return jspb.Message.getField(this, 6) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.accounting.v1.Counterparty.repeatedFields_ = [5];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.Counterparty.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.Counterparty.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.Counterparty} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.Counterparty.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, ""),
    name: jspb.Message.getFieldWithDefault(msg, 2, ""),
    edrpou: jspb.Message.getFieldWithDefault(msg, 3, ""),
    userId: jspb.Message.getFieldWithDefault(msg, 4, ""),
    locationsList: jspb.Message.toObjectList(msg.getLocationsList(),
    proto.fcp.accounting.v1.CounterpartyLocation.toObject, includeInstance),
    created: (f = msg.getCreated()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    createdBy: jspb.Message.getFieldWithDefault(msg, 12, ""),
    updated: (f = msg.getUpdated()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    updatedBy: jspb.Message.getFieldWithDefault(msg, 14, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.Counterparty}
 */
proto.fcp.accounting.v1.Counterparty.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.Counterparty;
  return proto.fcp.accounting.v1.Counterparty.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.Counterparty} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.Counterparty}
 */
proto.fcp.accounting.v1.Counterparty.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setEdrpou(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    case 5:
      var value = new proto.fcp.accounting.v1.CounterpartyLocation;
      reader.readMessage(value,proto.fcp.accounting.v1.CounterpartyLocation.deserializeBinaryFromReader);
      msg.addLocations(value);
      break;
    case 11:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setCreated(value);
      break;
    case 12:
      var value = /** @type {string} */ (reader.readString());
      msg.setCreatedBy(value);
      break;
    case 13:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setUpdated(value);
      break;
    case 14:
      var value = /** @type {string} */ (reader.readString());
      msg.setUpdatedBy(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.Counterparty.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.Counterparty.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.Counterparty} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.Counterparty.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getEdrpou();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getLocationsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      5,
      f,
      proto.fcp.accounting.v1.CounterpartyLocation.serializeBinaryToWriter
    );
  }
  f = message.getCreated();
  if (f != null) {
    writer.writeMessage(
      11,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getCreatedBy();
  if (f.length > 0) {
    writer.writeString(
      12,
      f
    );
  }
  f = message.getUpdated();
  if (f != null) {
    writer.writeMessage(
      13,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getUpdatedBy();
  if (f.length > 0) {
    writer.writeString(
      14,
      f
    );
  }
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.Counterparty.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Counterparty} returns this
 */
proto.fcp.accounting.v1.Counterparty.prototype.setId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string name = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.Counterparty.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Counterparty} returns this
 */
proto.fcp.accounting.v1.Counterparty.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string edrpou = 3;
 * @return {string}
 */
proto.fcp.accounting.v1.Counterparty.prototype.getEdrpou = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Counterparty} returns this
 */
proto.fcp.accounting.v1.Counterparty.prototype.setEdrpou = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string user_id = 4;
 * @return {string}
 */
proto.fcp.accounting.v1.Counterparty.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Counterparty} returns this
 */
proto.fcp.accounting.v1.Counterparty.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * repeated CounterpartyLocation locations = 5;
 * @return {!Array<!proto.fcp.accounting.v1.CounterpartyLocation>}
 */
proto.fcp.accounting.v1.Counterparty.prototype.getLocationsList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.CounterpartyLocation>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.accounting.v1.CounterpartyLocation, 5));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.CounterpartyLocation>} value
 * @return {!proto.fcp.accounting.v1.Counterparty} returns this
*/
proto.fcp.accounting.v1.Counterparty.prototype.setLocationsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 5, value);
};


/**
 * @param {!proto.fcp.accounting.v1.CounterpartyLocation=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.CounterpartyLocation}
 */
proto.fcp.accounting.v1.Counterparty.prototype.addLocations = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 5, opt_value, proto.fcp.accounting.v1.CounterpartyLocation, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.Counterparty} returns this
 */
proto.fcp.accounting.v1.Counterparty.prototype.clearLocationsList = function() {
  return this.setLocationsList([]);
};


/**
 * optional google.protobuf.Timestamp created = 11;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.fcp.accounting.v1.Counterparty.prototype.getCreated = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 11));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.fcp.accounting.v1.Counterparty} returns this
*/
proto.fcp.accounting.v1.Counterparty.prototype.setCreated = function(value) {
  return jspb.Message.setWrapperField(this, 11, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.Counterparty} returns this
 */
proto.fcp.accounting.v1.Counterparty.prototype.clearCreated = function() {
  return this.setCreated(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.Counterparty.prototype.hasCreated = function() {
  return jspb.Message.getField(this, 11) != null;
};


/**
 * optional string created_by = 12;
 * @return {string}
 */
proto.fcp.accounting.v1.Counterparty.prototype.getCreatedBy = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 12, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Counterparty} returns this
 */
proto.fcp.accounting.v1.Counterparty.prototype.setCreatedBy = function(value) {
  return jspb.Message.setProto3StringField(this, 12, value);
};


/**
 * optional google.protobuf.Timestamp updated = 13;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.fcp.accounting.v1.Counterparty.prototype.getUpdated = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 13));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.fcp.accounting.v1.Counterparty} returns this
*/
proto.fcp.accounting.v1.Counterparty.prototype.setUpdated = function(value) {
  return jspb.Message.setWrapperField(this, 13, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.Counterparty} returns this
 */
proto.fcp.accounting.v1.Counterparty.prototype.clearUpdated = function() {
  return this.setUpdated(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.Counterparty.prototype.hasUpdated = function() {
  return jspb.Message.getField(this, 13) != null;
};


/**
 * optional string updated_by = 14;
 * @return {string}
 */
proto.fcp.accounting.v1.Counterparty.prototype.getUpdatedBy = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 14, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.Counterparty} returns this
 */
proto.fcp.accounting.v1.Counterparty.prototype.setUpdatedBy = function(value) {
  return jspb.Message.setProto3StringField(this, 14, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.CounterpartyFilter.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.CounterpartyFilter} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.CounterpartyFilter.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: (f = msg.getId()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    name: (f = msg.getName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    edrpou: (f = msg.getEdrpou()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    userId: (f = msg.getUserId()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    created: (f = msg.getCreated()) && v1_auth_common_common_pb.TimeRange.toObject(includeInstance, f),
    updated: (f = msg.getUpdated()) && v1_auth_common_common_pb.TimeRange.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.CounterpartyFilter}
 */
proto.fcp.accounting.v1.CounterpartyFilter.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.CounterpartyFilter;
  return proto.fcp.accounting.v1.CounterpartyFilter.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.CounterpartyFilter} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.CounterpartyFilter}
 */
proto.fcp.accounting.v1.CounterpartyFilter.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setId(value);
      break;
    case 2:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setName(value);
      break;
    case 3:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setEdrpou(value);
      break;
    case 4:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setUserId(value);
      break;
    case 11:
      var value = new v1_auth_common_common_pb.TimeRange;
      reader.readMessage(value,v1_auth_common_common_pb.TimeRange.deserializeBinaryFromReader);
      msg.setCreated(value);
      break;
    case 13:
      var value = new v1_auth_common_common_pb.TimeRange;
      reader.readMessage(value,v1_auth_common_common_pb.TimeRange.deserializeBinaryFromReader);
      msg.setUpdated(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.CounterpartyFilter.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.CounterpartyFilter} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.CounterpartyFilter.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getName();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getEdrpou();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getUserId();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getCreated();
  if (f != null) {
    writer.writeMessage(
      11,
      f,
      v1_auth_common_common_pb.TimeRange.serializeBinaryToWriter
    );
  }
  f = message.getUpdated();
  if (f != null) {
    writer.writeMessage(
      13,
      f,
      v1_auth_common_common_pb.TimeRange.serializeBinaryToWriter
    );
  }
};


/**
 * optional google.protobuf.StringValue id = 1;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.getId = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 1));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.CounterpartyFilter} returns this
*/
proto.fcp.accounting.v1.CounterpartyFilter.prototype.setId = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.CounterpartyFilter} returns this
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.clearId = function() {
  return this.setId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.hasId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional google.protobuf.StringValue name = 2;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.getName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 2));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.CounterpartyFilter} returns this
*/
proto.fcp.accounting.v1.CounterpartyFilter.prototype.setName = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.CounterpartyFilter} returns this
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.clearName = function() {
  return this.setName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.hasName = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional google.protobuf.StringValue edrpou = 3;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.getEdrpou = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 3));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.CounterpartyFilter} returns this
*/
proto.fcp.accounting.v1.CounterpartyFilter.prototype.setEdrpou = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.CounterpartyFilter} returns this
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.clearEdrpou = function() {
  return this.setEdrpou(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.hasEdrpou = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional google.protobuf.StringValue user_id = 4;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.getUserId = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 4));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.CounterpartyFilter} returns this
*/
proto.fcp.accounting.v1.CounterpartyFilter.prototype.setUserId = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.CounterpartyFilter} returns this
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.clearUserId = function() {
  return this.setUserId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.hasUserId = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional TimeRange created = 11;
 * @return {?proto.fcp.accounting.v1.TimeRange}
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.getCreated = function() {
  return /** @type{?proto.fcp.accounting.v1.TimeRange} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_common_pb.TimeRange, 11));
};


/**
 * @param {?proto.fcp.accounting.v1.TimeRange|undefined} value
 * @return {!proto.fcp.accounting.v1.CounterpartyFilter} returns this
*/
proto.fcp.accounting.v1.CounterpartyFilter.prototype.setCreated = function(value) {
  return jspb.Message.setWrapperField(this, 11, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.CounterpartyFilter} returns this
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.clearCreated = function() {
  return this.setCreated(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.hasCreated = function() {
  return jspb.Message.getField(this, 11) != null;
};


/**
 * optional TimeRange updated = 13;
 * @return {?proto.fcp.accounting.v1.TimeRange}
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.getUpdated = function() {
  return /** @type{?proto.fcp.accounting.v1.TimeRange} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_common_pb.TimeRange, 13));
};


/**
 * @param {?proto.fcp.accounting.v1.TimeRange|undefined} value
 * @return {!proto.fcp.accounting.v1.CounterpartyFilter} returns this
*/
proto.fcp.accounting.v1.CounterpartyFilter.prototype.setUpdated = function(value) {
  return jspb.Message.setWrapperField(this, 13, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.CounterpartyFilter} returns this
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.clearUpdated = function() {
  return this.setUpdated(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.CounterpartyFilter.prototype.hasUpdated = function() {
  return jspb.Message.getField(this, 13) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.GetCounterpartyRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.GetCounterpartyRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.GetCounterpartyRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.GetCounterpartyRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.GetCounterpartyRequest}
 */
proto.fcp.accounting.v1.GetCounterpartyRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.GetCounterpartyRequest;
  return proto.fcp.accounting.v1.GetCounterpartyRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.GetCounterpartyRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.GetCounterpartyRequest}
 */
proto.fcp.accounting.v1.GetCounterpartyRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.GetCounterpartyRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.GetCounterpartyRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.GetCounterpartyRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.GetCounterpartyRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.GetCounterpartyRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.GetCounterpartyRequest} returns this
 */
proto.fcp.accounting.v1.GetCounterpartyRequest.prototype.setId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.GetCounterpartyResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.GetCounterpartyResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.GetCounterpartyResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.GetCounterpartyResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    item: (f = msg.getItem()) && proto.fcp.accounting.v1.Counterparty.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.GetCounterpartyResponse}
 */
proto.fcp.accounting.v1.GetCounterpartyResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.GetCounterpartyResponse;
  return proto.fcp.accounting.v1.GetCounterpartyResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.GetCounterpartyResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.GetCounterpartyResponse}
 */
proto.fcp.accounting.v1.GetCounterpartyResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.Counterparty;
      reader.readMessage(value,proto.fcp.accounting.v1.Counterparty.deserializeBinaryFromReader);
      msg.setItem(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.GetCounterpartyResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.GetCounterpartyResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.GetCounterpartyResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.GetCounterpartyResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItem();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.accounting.v1.Counterparty.serializeBinaryToWriter
    );
  }
};


/**
 * optional Counterparty item = 1;
 * @return {?proto.fcp.accounting.v1.Counterparty}
 */
proto.fcp.accounting.v1.GetCounterpartyResponse.prototype.getItem = function() {
  return /** @type{?proto.fcp.accounting.v1.Counterparty} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.Counterparty, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.Counterparty|undefined} value
 * @return {!proto.fcp.accounting.v1.GetCounterpartyResponse} returns this
*/
proto.fcp.accounting.v1.GetCounterpartyResponse.prototype.setItem = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.GetCounterpartyResponse} returns this
 */
proto.fcp.accounting.v1.GetCounterpartyResponse.prototype.clearItem = function() {
  return this.setItem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.GetCounterpartyResponse.prototype.hasItem = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.ListCounterpartyRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.ListCounterpartyRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.ListCounterpartyRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListCounterpartyRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.accounting.v1.CounterpartyFilter.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.ListCounterpartyRequest}
 */
proto.fcp.accounting.v1.ListCounterpartyRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.ListCounterpartyRequest;
  return proto.fcp.accounting.v1.ListCounterpartyRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.ListCounterpartyRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.ListCounterpartyRequest}
 */
proto.fcp.accounting.v1.ListCounterpartyRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.CounterpartyFilter;
      reader.readMessage(value,proto.fcp.accounting.v1.CounterpartyFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.ListCounterpartyRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.ListCounterpartyRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.ListCounterpartyRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListCounterpartyRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.accounting.v1.CounterpartyFilter.serializeBinaryToWriter
    );
  }
};


/**
 * optional CounterpartyFilter filter = 1;
 * @return {?proto.fcp.accounting.v1.CounterpartyFilter}
 */
proto.fcp.accounting.v1.ListCounterpartyRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.accounting.v1.CounterpartyFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.CounterpartyFilter, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.CounterpartyFilter|undefined} value
 * @return {!proto.fcp.accounting.v1.ListCounterpartyRequest} returns this
*/
proto.fcp.accounting.v1.ListCounterpartyRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.ListCounterpartyRequest} returns this
 */
proto.fcp.accounting.v1.ListCounterpartyRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.ListCounterpartyRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 1) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.accounting.v1.ListCounterpartyResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.ListCounterpartyResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.ListCounterpartyResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.ListCounterpartyResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListCounterpartyResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    proto.fcp.accounting.v1.Counterparty.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.ListCounterpartyResponse}
 */
proto.fcp.accounting.v1.ListCounterpartyResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.ListCounterpartyResponse;
  return proto.fcp.accounting.v1.ListCounterpartyResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.ListCounterpartyResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.ListCounterpartyResponse}
 */
proto.fcp.accounting.v1.ListCounterpartyResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.Counterparty;
      reader.readMessage(value,proto.fcp.accounting.v1.Counterparty.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.ListCounterpartyResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.ListCounterpartyResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.ListCounterpartyResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListCounterpartyResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.fcp.accounting.v1.Counterparty.serializeBinaryToWriter
    );
  }
};


/**
 * repeated Counterparty items = 1;
 * @return {!Array<!proto.fcp.accounting.v1.Counterparty>}
 */
proto.fcp.accounting.v1.ListCounterpartyResponse.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.Counterparty>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.accounting.v1.Counterparty, 1));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.Counterparty>} value
 * @return {!proto.fcp.accounting.v1.ListCounterpartyResponse} returns this
*/
proto.fcp.accounting.v1.ListCounterpartyResponse.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.fcp.accounting.v1.Counterparty=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.Counterparty}
 */
proto.fcp.accounting.v1.ListCounterpartyResponse.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.fcp.accounting.v1.Counterparty, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.ListCounterpartyResponse} returns this
 */
proto.fcp.accounting.v1.ListCounterpartyResponse.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.SaveCounterpartyRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.SaveCounterpartyRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.SaveCounterpartyRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveCounterpartyRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    item: (f = msg.getItem()) && proto.fcp.accounting.v1.Counterparty.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.SaveCounterpartyRequest}
 */
proto.fcp.accounting.v1.SaveCounterpartyRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.SaveCounterpartyRequest;
  return proto.fcp.accounting.v1.SaveCounterpartyRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.SaveCounterpartyRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.SaveCounterpartyRequest}
 */
proto.fcp.accounting.v1.SaveCounterpartyRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.Counterparty;
      reader.readMessage(value,proto.fcp.accounting.v1.Counterparty.deserializeBinaryFromReader);
      msg.setItem(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.SaveCounterpartyRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.SaveCounterpartyRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.SaveCounterpartyRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveCounterpartyRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItem();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.accounting.v1.Counterparty.serializeBinaryToWriter
    );
  }
};


/**
 * optional Counterparty item = 1;
 * @return {?proto.fcp.accounting.v1.Counterparty}
 */
proto.fcp.accounting.v1.SaveCounterpartyRequest.prototype.getItem = function() {
  return /** @type{?proto.fcp.accounting.v1.Counterparty} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.Counterparty, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.Counterparty|undefined} value
 * @return {!proto.fcp.accounting.v1.SaveCounterpartyRequest} returns this
*/
proto.fcp.accounting.v1.SaveCounterpartyRequest.prototype.setItem = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.SaveCounterpartyRequest} returns this
 */
proto.fcp.accounting.v1.SaveCounterpartyRequest.prototype.clearItem = function() {
  return this.setItem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.SaveCounterpartyRequest.prototype.hasItem = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.SaveCounterpartyResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.SaveCounterpartyResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.SaveCounterpartyResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveCounterpartyResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.SaveCounterpartyResponse}
 */
proto.fcp.accounting.v1.SaveCounterpartyResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.SaveCounterpartyResponse;
  return proto.fcp.accounting.v1.SaveCounterpartyResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.SaveCounterpartyResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.SaveCounterpartyResponse}
 */
proto.fcp.accounting.v1.SaveCounterpartyResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.SaveCounterpartyResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.SaveCounterpartyResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.SaveCounterpartyResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveCounterpartyResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.SaveCounterpartyResponse.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.SaveCounterpartyResponse} returns this
 */
proto.fcp.accounting.v1.SaveCounterpartyResponse.prototype.setId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.DeleteCounterpartyRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.DeleteCounterpartyRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.DeleteCounterpartyRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteCounterpartyRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.DeleteCounterpartyRequest}
 */
proto.fcp.accounting.v1.DeleteCounterpartyRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.DeleteCounterpartyRequest;
  return proto.fcp.accounting.v1.DeleteCounterpartyRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.DeleteCounterpartyRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.DeleteCounterpartyRequest}
 */
proto.fcp.accounting.v1.DeleteCounterpartyRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.DeleteCounterpartyRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.DeleteCounterpartyRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.DeleteCounterpartyRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteCounterpartyRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.DeleteCounterpartyRequest.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.DeleteCounterpartyRequest} returns this
 */
proto.fcp.accounting.v1.DeleteCounterpartyRequest.prototype.setId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.DeleteCounterpartyResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.DeleteCounterpartyResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.DeleteCounterpartyResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteCounterpartyResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.DeleteCounterpartyResponse}
 */
proto.fcp.accounting.v1.DeleteCounterpartyResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.DeleteCounterpartyResponse;
  return proto.fcp.accounting.v1.DeleteCounterpartyResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.DeleteCounterpartyResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.DeleteCounterpartyResponse}
 */
proto.fcp.accounting.v1.DeleteCounterpartyResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.DeleteCounterpartyResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.DeleteCounterpartyResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.DeleteCounterpartyResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteCounterpartyResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.CounterpartyLocationTypeValue.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.CounterpartyLocationTypeValue.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.CounterpartyLocationTypeValue} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.CounterpartyLocationTypeValue.toObject = function(includeInstance, msg) {
  var f, obj = {
    value: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.CounterpartyLocationTypeValue}
 */
proto.fcp.accounting.v1.CounterpartyLocationTypeValue.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.CounterpartyLocationTypeValue;
  return proto.fcp.accounting.v1.CounterpartyLocationTypeValue.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.CounterpartyLocationTypeValue} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.CounterpartyLocationTypeValue}
 */
proto.fcp.accounting.v1.CounterpartyLocationTypeValue.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.fcp.accounting.v1.CounterpartyLocationType} */ (reader.readEnum());
      msg.setValue(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.CounterpartyLocationTypeValue.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.CounterpartyLocationTypeValue.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.CounterpartyLocationTypeValue} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.CounterpartyLocationTypeValue.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getValue();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
};


/**
 * optional CounterpartyLocationType value = 1;
 * @return {!proto.fcp.accounting.v1.CounterpartyLocationType}
 */
proto.fcp.accounting.v1.CounterpartyLocationTypeValue.prototype.getValue = function() {
  return /** @type {!proto.fcp.accounting.v1.CounterpartyLocationType} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.fcp.accounting.v1.CounterpartyLocationType} value
 * @return {!proto.fcp.accounting.v1.CounterpartyLocationTypeValue} returns this
 */
proto.fcp.accounting.v1.CounterpartyLocationTypeValue.prototype.setValue = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.CounterpartyLocation.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.CounterpartyLocation} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.CounterpartyLocation.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0),
    counterpartyId: jspb.Message.getFieldWithDefault(msg, 2, ""),
    type: jspb.Message.getFieldWithDefault(msg, 3, 0),
    name: jspb.Message.getFieldWithDefault(msg, 4, ""),
    region: jspb.Message.getFieldWithDefault(msg, 5, ""),
    district: jspb.Message.getFieldWithDefault(msg, 6, ""),
    city: jspb.Message.getFieldWithDefault(msg, 7, ""),
    street: jspb.Message.getFieldWithDefault(msg, 8, ""),
    building: jspb.Message.getFieldWithDefault(msg, 9, ""),
    gps: jspb.Message.getFieldWithDefault(msg, 10, ""),
    receiver: jspb.Message.getFieldWithDefault(msg, 11, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.CounterpartyLocation}
 */
proto.fcp.accounting.v1.CounterpartyLocation.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.CounterpartyLocation;
  return proto.fcp.accounting.v1.CounterpartyLocation.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.CounterpartyLocation} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.CounterpartyLocation}
 */
proto.fcp.accounting.v1.CounterpartyLocation.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setCounterpartyId(value);
      break;
    case 3:
      var value = /** @type {!proto.fcp.accounting.v1.CounterpartyLocationType} */ (reader.readEnum());
      msg.setType(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setRegion(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setDistrict(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setCity(value);
      break;
    case 8:
      var value = /** @type {string} */ (reader.readString());
      msg.setStreet(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.setBuilding(value);
      break;
    case 10:
      var value = /** @type {string} */ (reader.readString());
      msg.setGps(value);
      break;
    case 11:
      var value = /** @type {string} */ (reader.readString());
      msg.setReceiver(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.CounterpartyLocation.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.CounterpartyLocation} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.CounterpartyLocation.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getCounterpartyId();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getType();
  if (f !== 0.0) {
    writer.writeEnum(
      3,
      f
    );
  }
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getRegion();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getDistrict();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getCity();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getStreet();
  if (f.length > 0) {
    writer.writeString(
      8,
      f
    );
  }
  f = message.getBuilding();
  if (f.length > 0) {
    writer.writeString(
      9,
      f
    );
  }
  f = message.getGps();
  if (f.length > 0) {
    writer.writeString(
      10,
      f
    );
  }
  f = message.getReceiver();
  if (f.length > 0) {
    writer.writeString(
      11,
      f
    );
  }
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.CounterpartyLocation} returns this
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string counterparty_id = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.getCounterpartyId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.CounterpartyLocation} returns this
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.setCounterpartyId = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional CounterpartyLocationType type = 3;
 * @return {!proto.fcp.accounting.v1.CounterpartyLocationType}
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.getType = function() {
  return /** @type {!proto.fcp.accounting.v1.CounterpartyLocationType} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {!proto.fcp.accounting.v1.CounterpartyLocationType} value
 * @return {!proto.fcp.accounting.v1.CounterpartyLocation} returns this
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.setType = function(value) {
  return jspb.Message.setProto3EnumField(this, 3, value);
};


/**
 * optional string name = 4;
 * @return {string}
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.CounterpartyLocation} returns this
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string region = 5;
 * @return {string}
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.getRegion = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.CounterpartyLocation} returns this
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.setRegion = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional string district = 6;
 * @return {string}
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.getDistrict = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.CounterpartyLocation} returns this
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.setDistrict = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional string city = 7;
 * @return {string}
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.getCity = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.CounterpartyLocation} returns this
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.setCity = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * optional string street = 8;
 * @return {string}
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.getStreet = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 8, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.CounterpartyLocation} returns this
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.setStreet = function(value) {
  return jspb.Message.setProto3StringField(this, 8, value);
};


/**
 * optional string building = 9;
 * @return {string}
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.getBuilding = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 9, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.CounterpartyLocation} returns this
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.setBuilding = function(value) {
  return jspb.Message.setProto3StringField(this, 9, value);
};


/**
 * optional string gps = 10;
 * @return {string}
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.getGps = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 10, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.CounterpartyLocation} returns this
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.setGps = function(value) {
  return jspb.Message.setProto3StringField(this, 10, value);
};


/**
 * optional string receiver = 11;
 * @return {string}
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.getReceiver = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 11, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.CounterpartyLocation} returns this
 */
proto.fcp.accounting.v1.CounterpartyLocation.prototype.setReceiver = function(value) {
  return jspb.Message.setProto3StringField(this, 11, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.CounterpartyLocationFilter.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.CounterpartyLocationFilter.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.CounterpartyLocationFilter} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.CounterpartyLocationFilter.toObject = function(includeInstance, msg) {
  var f, obj = {
    counterpartyId: (f = msg.getCounterpartyId()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    type: (f = msg.getType()) && proto.fcp.accounting.v1.CounterpartyLocationTypeValue.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.CounterpartyLocationFilter}
 */
proto.fcp.accounting.v1.CounterpartyLocationFilter.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.CounterpartyLocationFilter;
  return proto.fcp.accounting.v1.CounterpartyLocationFilter.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.CounterpartyLocationFilter} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.CounterpartyLocationFilter}
 */
proto.fcp.accounting.v1.CounterpartyLocationFilter.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setCounterpartyId(value);
      break;
    case 2:
      var value = new proto.fcp.accounting.v1.CounterpartyLocationTypeValue;
      reader.readMessage(value,proto.fcp.accounting.v1.CounterpartyLocationTypeValue.deserializeBinaryFromReader);
      msg.setType(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.CounterpartyLocationFilter.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.CounterpartyLocationFilter.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.CounterpartyLocationFilter} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.CounterpartyLocationFilter.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCounterpartyId();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getType();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.fcp.accounting.v1.CounterpartyLocationTypeValue.serializeBinaryToWriter
    );
  }
};


/**
 * optional google.protobuf.StringValue counterparty_id = 1;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.CounterpartyLocationFilter.prototype.getCounterpartyId = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 1));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.CounterpartyLocationFilter} returns this
*/
proto.fcp.accounting.v1.CounterpartyLocationFilter.prototype.setCounterpartyId = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.CounterpartyLocationFilter} returns this
 */
proto.fcp.accounting.v1.CounterpartyLocationFilter.prototype.clearCounterpartyId = function() {
  return this.setCounterpartyId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.CounterpartyLocationFilter.prototype.hasCounterpartyId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional CounterpartyLocationTypeValue type = 2;
 * @return {?proto.fcp.accounting.v1.CounterpartyLocationTypeValue}
 */
proto.fcp.accounting.v1.CounterpartyLocationFilter.prototype.getType = function() {
  return /** @type{?proto.fcp.accounting.v1.CounterpartyLocationTypeValue} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.CounterpartyLocationTypeValue, 2));
};


/**
 * @param {?proto.fcp.accounting.v1.CounterpartyLocationTypeValue|undefined} value
 * @return {!proto.fcp.accounting.v1.CounterpartyLocationFilter} returns this
*/
proto.fcp.accounting.v1.CounterpartyLocationFilter.prototype.setType = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.CounterpartyLocationFilter} returns this
 */
proto.fcp.accounting.v1.CounterpartyLocationFilter.prototype.clearType = function() {
  return this.setType(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.CounterpartyLocationFilter.prototype.hasType = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.GetCounterpartyLocationRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.GetCounterpartyLocationRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.GetCounterpartyLocationRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.GetCounterpartyLocationRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.GetCounterpartyLocationRequest}
 */
proto.fcp.accounting.v1.GetCounterpartyLocationRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.GetCounterpartyLocationRequest;
  return proto.fcp.accounting.v1.GetCounterpartyLocationRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.GetCounterpartyLocationRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.GetCounterpartyLocationRequest}
 */
proto.fcp.accounting.v1.GetCounterpartyLocationRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.GetCounterpartyLocationRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.GetCounterpartyLocationRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.GetCounterpartyLocationRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.GetCounterpartyLocationRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.accounting.v1.GetCounterpartyLocationRequest.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.GetCounterpartyLocationRequest} returns this
 */
proto.fcp.accounting.v1.GetCounterpartyLocationRequest.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.GetCounterpartyLocationResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.GetCounterpartyLocationResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.GetCounterpartyLocationResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.GetCounterpartyLocationResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    item: (f = msg.getItem()) && proto.fcp.accounting.v1.CounterpartyLocation.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.GetCounterpartyLocationResponse}
 */
proto.fcp.accounting.v1.GetCounterpartyLocationResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.GetCounterpartyLocationResponse;
  return proto.fcp.accounting.v1.GetCounterpartyLocationResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.GetCounterpartyLocationResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.GetCounterpartyLocationResponse}
 */
proto.fcp.accounting.v1.GetCounterpartyLocationResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.CounterpartyLocation;
      reader.readMessage(value,proto.fcp.accounting.v1.CounterpartyLocation.deserializeBinaryFromReader);
      msg.setItem(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.GetCounterpartyLocationResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.GetCounterpartyLocationResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.GetCounterpartyLocationResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.GetCounterpartyLocationResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItem();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.accounting.v1.CounterpartyLocation.serializeBinaryToWriter
    );
  }
};


/**
 * optional CounterpartyLocation item = 1;
 * @return {?proto.fcp.accounting.v1.CounterpartyLocation}
 */
proto.fcp.accounting.v1.GetCounterpartyLocationResponse.prototype.getItem = function() {
  return /** @type{?proto.fcp.accounting.v1.CounterpartyLocation} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.CounterpartyLocation, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.CounterpartyLocation|undefined} value
 * @return {!proto.fcp.accounting.v1.GetCounterpartyLocationResponse} returns this
*/
proto.fcp.accounting.v1.GetCounterpartyLocationResponse.prototype.setItem = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.GetCounterpartyLocationResponse} returns this
 */
proto.fcp.accounting.v1.GetCounterpartyLocationResponse.prototype.clearItem = function() {
  return this.setItem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.GetCounterpartyLocationResponse.prototype.hasItem = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.ListCounterpartyLocationRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.ListCounterpartyLocationRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.ListCounterpartyLocationRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListCounterpartyLocationRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.accounting.v1.CounterpartyLocationFilter.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.ListCounterpartyLocationRequest}
 */
proto.fcp.accounting.v1.ListCounterpartyLocationRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.ListCounterpartyLocationRequest;
  return proto.fcp.accounting.v1.ListCounterpartyLocationRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.ListCounterpartyLocationRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.ListCounterpartyLocationRequest}
 */
proto.fcp.accounting.v1.ListCounterpartyLocationRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.CounterpartyLocationFilter;
      reader.readMessage(value,proto.fcp.accounting.v1.CounterpartyLocationFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.ListCounterpartyLocationRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.ListCounterpartyLocationRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.ListCounterpartyLocationRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListCounterpartyLocationRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.accounting.v1.CounterpartyLocationFilter.serializeBinaryToWriter
    );
  }
};


/**
 * optional CounterpartyLocationFilter filter = 1;
 * @return {?proto.fcp.accounting.v1.CounterpartyLocationFilter}
 */
proto.fcp.accounting.v1.ListCounterpartyLocationRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.accounting.v1.CounterpartyLocationFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.CounterpartyLocationFilter, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.CounterpartyLocationFilter|undefined} value
 * @return {!proto.fcp.accounting.v1.ListCounterpartyLocationRequest} returns this
*/
proto.fcp.accounting.v1.ListCounterpartyLocationRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.ListCounterpartyLocationRequest} returns this
 */
proto.fcp.accounting.v1.ListCounterpartyLocationRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.ListCounterpartyLocationRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 1) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.accounting.v1.ListCounterpartyLocationResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.ListCounterpartyLocationResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.ListCounterpartyLocationResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.ListCounterpartyLocationResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListCounterpartyLocationResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    proto.fcp.accounting.v1.CounterpartyLocation.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.ListCounterpartyLocationResponse}
 */
proto.fcp.accounting.v1.ListCounterpartyLocationResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.ListCounterpartyLocationResponse;
  return proto.fcp.accounting.v1.ListCounterpartyLocationResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.ListCounterpartyLocationResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.ListCounterpartyLocationResponse}
 */
proto.fcp.accounting.v1.ListCounterpartyLocationResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.CounterpartyLocation;
      reader.readMessage(value,proto.fcp.accounting.v1.CounterpartyLocation.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.ListCounterpartyLocationResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.ListCounterpartyLocationResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.ListCounterpartyLocationResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.ListCounterpartyLocationResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.fcp.accounting.v1.CounterpartyLocation.serializeBinaryToWriter
    );
  }
};


/**
 * repeated CounterpartyLocation items = 1;
 * @return {!Array<!proto.fcp.accounting.v1.CounterpartyLocation>}
 */
proto.fcp.accounting.v1.ListCounterpartyLocationResponse.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.CounterpartyLocation>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.accounting.v1.CounterpartyLocation, 1));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.CounterpartyLocation>} value
 * @return {!proto.fcp.accounting.v1.ListCounterpartyLocationResponse} returns this
*/
proto.fcp.accounting.v1.ListCounterpartyLocationResponse.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.fcp.accounting.v1.CounterpartyLocation=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.CounterpartyLocation}
 */
proto.fcp.accounting.v1.ListCounterpartyLocationResponse.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.fcp.accounting.v1.CounterpartyLocation, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.ListCounterpartyLocationResponse} returns this
 */
proto.fcp.accounting.v1.ListCounterpartyLocationResponse.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.SaveCounterpartyLocationRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.SaveCounterpartyLocationRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.SaveCounterpartyLocationRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveCounterpartyLocationRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    item: (f = msg.getItem()) && proto.fcp.accounting.v1.CounterpartyLocation.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.SaveCounterpartyLocationRequest}
 */
proto.fcp.accounting.v1.SaveCounterpartyLocationRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.SaveCounterpartyLocationRequest;
  return proto.fcp.accounting.v1.SaveCounterpartyLocationRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.SaveCounterpartyLocationRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.SaveCounterpartyLocationRequest}
 */
proto.fcp.accounting.v1.SaveCounterpartyLocationRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.CounterpartyLocation;
      reader.readMessage(value,proto.fcp.accounting.v1.CounterpartyLocation.deserializeBinaryFromReader);
      msg.setItem(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.SaveCounterpartyLocationRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.SaveCounterpartyLocationRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.SaveCounterpartyLocationRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveCounterpartyLocationRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItem();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.accounting.v1.CounterpartyLocation.serializeBinaryToWriter
    );
  }
};


/**
 * optional CounterpartyLocation item = 1;
 * @return {?proto.fcp.accounting.v1.CounterpartyLocation}
 */
proto.fcp.accounting.v1.SaveCounterpartyLocationRequest.prototype.getItem = function() {
  return /** @type{?proto.fcp.accounting.v1.CounterpartyLocation} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.CounterpartyLocation, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.CounterpartyLocation|undefined} value
 * @return {!proto.fcp.accounting.v1.SaveCounterpartyLocationRequest} returns this
*/
proto.fcp.accounting.v1.SaveCounterpartyLocationRequest.prototype.setItem = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.SaveCounterpartyLocationRequest} returns this
 */
proto.fcp.accounting.v1.SaveCounterpartyLocationRequest.prototype.clearItem = function() {
  return this.setItem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.SaveCounterpartyLocationRequest.prototype.hasItem = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.SaveCounterpartyLocationResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.SaveCounterpartyLocationResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.SaveCounterpartyLocationResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveCounterpartyLocationResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.SaveCounterpartyLocationResponse}
 */
proto.fcp.accounting.v1.SaveCounterpartyLocationResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.SaveCounterpartyLocationResponse;
  return proto.fcp.accounting.v1.SaveCounterpartyLocationResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.SaveCounterpartyLocationResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.SaveCounterpartyLocationResponse}
 */
proto.fcp.accounting.v1.SaveCounterpartyLocationResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.SaveCounterpartyLocationResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.SaveCounterpartyLocationResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.SaveCounterpartyLocationResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.SaveCounterpartyLocationResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.accounting.v1.SaveCounterpartyLocationResponse.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.SaveCounterpartyLocationResponse} returns this
 */
proto.fcp.accounting.v1.SaveCounterpartyLocationResponse.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest}
 */
proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest;
  return proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest}
 */
proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest} returns this
 */
proto.fcp.accounting.v1.DeleteCounterpartyLocationRequest.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse}
 */
proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse;
  return proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse}
 */
proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.DeleteCounterpartyLocationResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};


/**
 * @enum {number}
 */
proto.fcp.accounting.v1.LocationType = {
  LEGAL: 0,
  ACTUAL: 1,
  DELIVERY: 2
};

/**
 * @enum {number}
 */
proto.fcp.accounting.v1.RoleType = {
  ADMIN: 0,
  MANAGER: 1,
  FARMER: 2,
  AGENT: 3,
  SUPPLIER: 4,
  MERCHANDISER: 5,
  DESTRIBUTER: 6,
  PROVIDER: 7,
  TRADER: 8,
  PROCESSOR: 9,
  RETAILER: 10,
  BANK: 11,
  INSURER: 12,
  SUPPORTER: 13
};

/**
 * @enum {number}
 */
proto.fcp.accounting.v1.Status = {
  NEW: 0,
  AUTHORIZED: 1,
  BLOCKED: 2,
  FIRED: 3
};

/**
 * @enum {number}
 */
proto.fcp.accounting.v1.CounterpartyLocationType = {
  COUNTERPARTYLOCATIONTYPE_UNSPECIFIED: 0,
  COUNTERPARTYLOCATIONTYPE_DELIVERY: 1
};

goog.object.extend(exports, proto.fcp.accounting.v1);
