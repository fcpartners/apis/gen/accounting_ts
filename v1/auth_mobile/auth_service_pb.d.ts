// package: fcp.accounting.v1.auth_mobile
// file: v1/auth_mobile/auth_service.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import * as v1_auth_common_dtos_pb from "../../v1/auth_common/dtos_pb";

export class SendCodeRequest extends jspb.Message {
  getPhoneNumber(): string;
  setPhoneNumber(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SendCodeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SendCodeRequest): SendCodeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SendCodeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SendCodeRequest;
  static deserializeBinaryFromReader(message: SendCodeRequest, reader: jspb.BinaryReader): SendCodeRequest;
}

export namespace SendCodeRequest {
  export type AsObject = {
    phoneNumber: string,
  }
}

export class CheckCodeRequest extends jspb.Message {
  getPhoneNumber(): string;
  setPhoneNumber(value: string): void;

  getCode(): string;
  setCode(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CheckCodeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CheckCodeRequest): CheckCodeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CheckCodeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CheckCodeRequest;
  static deserializeBinaryFromReader(message: CheckCodeRequest, reader: jspb.BinaryReader): CheckCodeRequest;
}

export namespace CheckCodeRequest {
  export type AsObject = {
    phoneNumber: string,
    code: string,
  }
}

export class CheckCodeResponse extends jspb.Message {
  getValid(): boolean;
  setValid(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CheckCodeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CheckCodeResponse): CheckCodeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CheckCodeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CheckCodeResponse;
  static deserializeBinaryFromReader(message: CheckCodeResponse, reader: jspb.BinaryReader): CheckCodeResponse;
}

export namespace CheckCodeResponse {
  export type AsObject = {
    valid: boolean,
  }
}

export class GetUserRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetUserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetUserRequest): GetUserRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetUserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetUserRequest;
  static deserializeBinaryFromReader(message: GetUserRequest, reader: jspb.BinaryReader): GetUserRequest;
}

export namespace GetUserRequest {
  export type AsObject = {
    userId: string,
  }
}

export class RegisterUserRequest extends jspb.Message {
  hasUser(): boolean;
  clearUser(): void;
  getUser(): v1_auth_common_dtos_pb.UserBriefInfo | undefined;
  setUser(value?: v1_auth_common_dtos_pb.UserBriefInfo): void;

  getReferrerHash(): string;
  setReferrerHash(value: string): void;

  getDeviceToken(): string;
  setDeviceToken(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegisterUserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RegisterUserRequest): RegisterUserRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RegisterUserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegisterUserRequest;
  static deserializeBinaryFromReader(message: RegisterUserRequest, reader: jspb.BinaryReader): RegisterUserRequest;
}

export namespace RegisterUserRequest {
  export type AsObject = {
    user?: v1_auth_common_dtos_pb.UserBriefInfo.AsObject,
    referrerHash: string,
    deviceToken: string,
  }
}

export class RegisterUserResponse extends jspb.Message {
  hasUser(): boolean;
  clearUser(): void;
  getUser(): v1_auth_common_dtos_pb.UserBriefInfo | undefined;
  setUser(value?: v1_auth_common_dtos_pb.UserBriefInfo): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegisterUserResponse.AsObject;
  static toObject(includeInstance: boolean, msg: RegisterUserResponse): RegisterUserResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RegisterUserResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegisterUserResponse;
  static deserializeBinaryFromReader(message: RegisterUserResponse, reader: jspb.BinaryReader): RegisterUserResponse;
}

export namespace RegisterUserResponse {
  export type AsObject = {
    user?: v1_auth_common_dtos_pb.UserBriefInfo.AsObject,
  }
}

export class UpdateUserRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getFirstName(): string;
  setFirstName(value: string): void;

  getLastName(): string;
  setLastName(value: string): void;

  getMiddleName(): string;
  setMiddleName(value: string): void;

  getEmail(): string;
  setEmail(value: string): void;

  getLatitude(): number;
  setLatitude(value: number): void;

  getLongitude(): number;
  setLongitude(value: number): void;

  getAddress(): string;
  setAddress(value: string): void;

  getCountryCode(): string;
  setCountryCode(value: string): void;

  getLanguageCode(): string;
  setLanguageCode(value: string): void;

  getBusinessName(): string;
  setBusinessName(value: string): void;

  getBusinessAddress(): string;
  setBusinessAddress(value: string): void;

  hasTaxInfo(): boolean;
  clearTaxInfo(): void;
  getTaxInfo(): v1_auth_common_dtos_pb.TaxInfo | undefined;
  setTaxInfo(value?: v1_auth_common_dtos_pb.TaxInfo): void;

  hasBusinessInfo(): boolean;
  clearBusinessInfo(): void;
  getBusinessInfo(): v1_auth_common_dtos_pb.BusinessInfo | undefined;
  setBusinessInfo(value?: v1_auth_common_dtos_pb.BusinessInfo): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateUserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateUserRequest): UpdateUserRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateUserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateUserRequest;
  static deserializeBinaryFromReader(message: UpdateUserRequest, reader: jspb.BinaryReader): UpdateUserRequest;
}

export namespace UpdateUserRequest {
  export type AsObject = {
    userId: string,
    firstName: string,
    lastName: string,
    middleName: string,
    email: string,
    latitude: number,
    longitude: number,
    address: string,
    countryCode: string,
    languageCode: string,
    businessName: string,
    businessAddress: string,
    taxInfo?: v1_auth_common_dtos_pb.TaxInfo.AsObject,
    businessInfo?: v1_auth_common_dtos_pb.BusinessInfo.AsObject,
  }
}

export class ValidateSessionRequest extends jspb.Message {
  getSessionToken(): string;
  setSessionToken(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValidateSessionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ValidateSessionRequest): ValidateSessionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ValidateSessionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValidateSessionRequest;
  static deserializeBinaryFromReader(message: ValidateSessionRequest, reader: jspb.BinaryReader): ValidateSessionRequest;
}

export namespace ValidateSessionRequest {
  export type AsObject = {
    sessionToken: string,
  }
}

export class ValidateSessionResponse extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValidateSessionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ValidateSessionResponse): ValidateSessionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ValidateSessionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValidateSessionResponse;
  static deserializeBinaryFromReader(message: ValidateSessionResponse, reader: jspb.BinaryReader): ValidateSessionResponse;
}

export namespace ValidateSessionResponse {
  export type AsObject = {
    userId: string,
  }
}

export class DeleteUserRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteUserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteUserRequest): DeleteUserRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteUserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteUserRequest;
  static deserializeBinaryFromReader(message: DeleteUserRequest, reader: jspb.BinaryReader): DeleteUserRequest;
}

export namespace DeleteUserRequest {
  export type AsObject = {
    userId: string,
  }
}

export class SetDeviceTokenRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getDeviceToken(): string;
  setDeviceToken(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetDeviceTokenRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SetDeviceTokenRequest): SetDeviceTokenRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetDeviceTokenRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetDeviceTokenRequest;
  static deserializeBinaryFromReader(message: SetDeviceTokenRequest, reader: jspb.BinaryReader): SetDeviceTokenRequest;
}

export namespace SetDeviceTokenRequest {
  export type AsObject = {
    userId: string,
    deviceToken: string,
  }
}

export class DeleteDeviceTokenRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getDeviceToken(): string;
  setDeviceToken(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteDeviceTokenRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteDeviceTokenRequest): DeleteDeviceTokenRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteDeviceTokenRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteDeviceTokenRequest;
  static deserializeBinaryFromReader(message: DeleteDeviceTokenRequest, reader: jspb.BinaryReader): DeleteDeviceTokenRequest;
}

export namespace DeleteDeviceTokenRequest {
  export type AsObject = {
    userId: string,
    deviceToken: string,
  }
}

export class DeleteAllDeviceTokenRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteAllDeviceTokenRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteAllDeviceTokenRequest): DeleteAllDeviceTokenRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteAllDeviceTokenRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteAllDeviceTokenRequest;
  static deserializeBinaryFromReader(message: DeleteAllDeviceTokenRequest, reader: jspb.BinaryReader): DeleteAllDeviceTokenRequest;
}

export namespace DeleteAllDeviceTokenRequest {
  export type AsObject = {
    userId: string,
  }
}

