// package: fcp.accounting.v1.auth_mobile
// file: v1/auth_mobile/auth_service.proto

import * as v1_auth_mobile_auth_service_pb from "../../v1/auth_mobile/auth_service_pb";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import * as v1_auth_common_dtos_pb from "../../v1/auth_common/dtos_pb";
import {grpc} from "@improbable-eng/grpc-web";

type MobileAuthServiceSendCode = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_mobile_auth_service_pb.SendCodeRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type MobileAuthServiceCheckCode = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_mobile_auth_service_pb.CheckCodeRequest;
  readonly responseType: typeof v1_auth_mobile_auth_service_pb.CheckCodeResponse;
};

type MobileAuthServiceGetUser = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_mobile_auth_service_pb.GetUserRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.UserBriefInfo;
};

type MobileAuthServiceRegisterUser = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_mobile_auth_service_pb.RegisterUserRequest;
  readonly responseType: typeof v1_auth_mobile_auth_service_pb.RegisterUserResponse;
};

type MobileAuthServiceUpdateUser = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_mobile_auth_service_pb.UpdateUserRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.UserBriefInfo;
};

type MobileAuthServiceValidateSession = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_mobile_auth_service_pb.ValidateSessionRequest;
  readonly responseType: typeof v1_auth_mobile_auth_service_pb.ValidateSessionResponse;
};

type MobileAuthServiceDeleteUser = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_mobile_auth_service_pb.DeleteUserRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type MobileAuthServiceSetDeviceToken = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_mobile_auth_service_pb.SetDeviceTokenRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type MobileAuthServiceDeleteDeviceToken = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_mobile_auth_service_pb.DeleteDeviceTokenRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type MobileAuthServiceDeleteAllDeviceToken = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_mobile_auth_service_pb.DeleteAllDeviceTokenRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type MobileAuthServiceListContactPerson = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.ListContactPersonRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.ListContactPersonResponse;
};

type MobileAuthServiceSaveContactPerson = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.SaveContactPersonRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.SaveContactPersonResponse;
};

type MobileAuthServiceDeleteContactPerson = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.DeleteContactPersonRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.DeleteContactPersonResponse;
};

type MobileAuthServiceListLocation = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.ListLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.ListLocationResponse;
};

type MobileAuthServiceSaveLocation = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.SaveLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.SaveLocationResponse;
};

type MobileAuthServiceDeleteLocation = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.DeleteLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.DeleteLocationResponse;
};

type MobileAuthServiceListDocument = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.ListDocumentRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.ListDocumentResponse;
};

type MobileAuthServiceSaveDocument = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.SaveDocumentRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.SaveDocumentResponse;
};

type MobileAuthServiceDeleteDocument = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.DeleteDocumentRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.DeleteDocumentResponse;
};

type MobileAuthServiceGetCounterparty = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.GetCounterpartyRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.GetCounterpartyResponse;
};

type MobileAuthServiceListCounterparty = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.ListCounterpartyRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.ListCounterpartyResponse;
};

type MobileAuthServiceSaveCounterparty = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.SaveCounterpartyRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.SaveCounterpartyResponse;
};

type MobileAuthServiceGetCounterpartyLocation = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.GetCounterpartyLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.GetCounterpartyLocationResponse;
};

type MobileAuthServiceListCounterpartyLocation = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.ListCounterpartyLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.ListCounterpartyLocationResponse;
};

type MobileAuthServiceSaveCounterpartyLocation = {
  readonly methodName: string;
  readonly service: typeof MobileAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.SaveCounterpartyLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.SaveCounterpartyLocationResponse;
};

export class MobileAuthService {
  static readonly serviceName: string;
  static readonly SendCode: MobileAuthServiceSendCode;
  static readonly CheckCode: MobileAuthServiceCheckCode;
  static readonly GetUser: MobileAuthServiceGetUser;
  static readonly RegisterUser: MobileAuthServiceRegisterUser;
  static readonly UpdateUser: MobileAuthServiceUpdateUser;
  static readonly ValidateSession: MobileAuthServiceValidateSession;
  static readonly DeleteUser: MobileAuthServiceDeleteUser;
  static readonly SetDeviceToken: MobileAuthServiceSetDeviceToken;
  static readonly DeleteDeviceToken: MobileAuthServiceDeleteDeviceToken;
  static readonly DeleteAllDeviceToken: MobileAuthServiceDeleteAllDeviceToken;
  static readonly ListContactPerson: MobileAuthServiceListContactPerson;
  static readonly SaveContactPerson: MobileAuthServiceSaveContactPerson;
  static readonly DeleteContactPerson: MobileAuthServiceDeleteContactPerson;
  static readonly ListLocation: MobileAuthServiceListLocation;
  static readonly SaveLocation: MobileAuthServiceSaveLocation;
  static readonly DeleteLocation: MobileAuthServiceDeleteLocation;
  static readonly ListDocument: MobileAuthServiceListDocument;
  static readonly SaveDocument: MobileAuthServiceSaveDocument;
  static readonly DeleteDocument: MobileAuthServiceDeleteDocument;
  static readonly GetCounterparty: MobileAuthServiceGetCounterparty;
  static readonly ListCounterparty: MobileAuthServiceListCounterparty;
  static readonly SaveCounterparty: MobileAuthServiceSaveCounterparty;
  static readonly GetCounterpartyLocation: MobileAuthServiceGetCounterpartyLocation;
  static readonly ListCounterpartyLocation: MobileAuthServiceListCounterpartyLocation;
  static readonly SaveCounterpartyLocation: MobileAuthServiceSaveCounterpartyLocation;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class MobileAuthServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  sendCode(
    requestMessage: v1_auth_mobile_auth_service_pb.SendCodeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  sendCode(
    requestMessage: v1_auth_mobile_auth_service_pb.SendCodeRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  checkCode(
    requestMessage: v1_auth_mobile_auth_service_pb.CheckCodeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_mobile_auth_service_pb.CheckCodeResponse|null) => void
  ): UnaryResponse;
  checkCode(
    requestMessage: v1_auth_mobile_auth_service_pb.CheckCodeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_mobile_auth_service_pb.CheckCodeResponse|null) => void
  ): UnaryResponse;
  getUser(
    requestMessage: v1_auth_mobile_auth_service_pb.GetUserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.UserBriefInfo|null) => void
  ): UnaryResponse;
  getUser(
    requestMessage: v1_auth_mobile_auth_service_pb.GetUserRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.UserBriefInfo|null) => void
  ): UnaryResponse;
  registerUser(
    requestMessage: v1_auth_mobile_auth_service_pb.RegisterUserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_mobile_auth_service_pb.RegisterUserResponse|null) => void
  ): UnaryResponse;
  registerUser(
    requestMessage: v1_auth_mobile_auth_service_pb.RegisterUserRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_mobile_auth_service_pb.RegisterUserResponse|null) => void
  ): UnaryResponse;
  updateUser(
    requestMessage: v1_auth_mobile_auth_service_pb.UpdateUserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.UserBriefInfo|null) => void
  ): UnaryResponse;
  updateUser(
    requestMessage: v1_auth_mobile_auth_service_pb.UpdateUserRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.UserBriefInfo|null) => void
  ): UnaryResponse;
  validateSession(
    requestMessage: v1_auth_mobile_auth_service_pb.ValidateSessionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_mobile_auth_service_pb.ValidateSessionResponse|null) => void
  ): UnaryResponse;
  validateSession(
    requestMessage: v1_auth_mobile_auth_service_pb.ValidateSessionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_mobile_auth_service_pb.ValidateSessionResponse|null) => void
  ): UnaryResponse;
  deleteUser(
    requestMessage: v1_auth_mobile_auth_service_pb.DeleteUserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  deleteUser(
    requestMessage: v1_auth_mobile_auth_service_pb.DeleteUserRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  setDeviceToken(
    requestMessage: v1_auth_mobile_auth_service_pb.SetDeviceTokenRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  setDeviceToken(
    requestMessage: v1_auth_mobile_auth_service_pb.SetDeviceTokenRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  deleteDeviceToken(
    requestMessage: v1_auth_mobile_auth_service_pb.DeleteDeviceTokenRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  deleteDeviceToken(
    requestMessage: v1_auth_mobile_auth_service_pb.DeleteDeviceTokenRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  deleteAllDeviceToken(
    requestMessage: v1_auth_mobile_auth_service_pb.DeleteAllDeviceTokenRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  deleteAllDeviceToken(
    requestMessage: v1_auth_mobile_auth_service_pb.DeleteAllDeviceTokenRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  listContactPerson(
    requestMessage: v1_auth_common_dtos_pb.ListContactPersonRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListContactPersonResponse|null) => void
  ): UnaryResponse;
  listContactPerson(
    requestMessage: v1_auth_common_dtos_pb.ListContactPersonRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListContactPersonResponse|null) => void
  ): UnaryResponse;
  saveContactPerson(
    requestMessage: v1_auth_common_dtos_pb.SaveContactPersonRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveContactPersonResponse|null) => void
  ): UnaryResponse;
  saveContactPerson(
    requestMessage: v1_auth_common_dtos_pb.SaveContactPersonRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveContactPersonResponse|null) => void
  ): UnaryResponse;
  deleteContactPerson(
    requestMessage: v1_auth_common_dtos_pb.DeleteContactPersonRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteContactPersonResponse|null) => void
  ): UnaryResponse;
  deleteContactPerson(
    requestMessage: v1_auth_common_dtos_pb.DeleteContactPersonRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteContactPersonResponse|null) => void
  ): UnaryResponse;
  listLocation(
    requestMessage: v1_auth_common_dtos_pb.ListLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListLocationResponse|null) => void
  ): UnaryResponse;
  listLocation(
    requestMessage: v1_auth_common_dtos_pb.ListLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListLocationResponse|null) => void
  ): UnaryResponse;
  saveLocation(
    requestMessage: v1_auth_common_dtos_pb.SaveLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveLocationResponse|null) => void
  ): UnaryResponse;
  saveLocation(
    requestMessage: v1_auth_common_dtos_pb.SaveLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveLocationResponse|null) => void
  ): UnaryResponse;
  deleteLocation(
    requestMessage: v1_auth_common_dtos_pb.DeleteLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteLocationResponse|null) => void
  ): UnaryResponse;
  deleteLocation(
    requestMessage: v1_auth_common_dtos_pb.DeleteLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteLocationResponse|null) => void
  ): UnaryResponse;
  listDocument(
    requestMessage: v1_auth_common_dtos_pb.ListDocumentRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListDocumentResponse|null) => void
  ): UnaryResponse;
  listDocument(
    requestMessage: v1_auth_common_dtos_pb.ListDocumentRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListDocumentResponse|null) => void
  ): UnaryResponse;
  saveDocument(
    requestMessage: v1_auth_common_dtos_pb.SaveDocumentRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveDocumentResponse|null) => void
  ): UnaryResponse;
  saveDocument(
    requestMessage: v1_auth_common_dtos_pb.SaveDocumentRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveDocumentResponse|null) => void
  ): UnaryResponse;
  deleteDocument(
    requestMessage: v1_auth_common_dtos_pb.DeleteDocumentRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteDocumentResponse|null) => void
  ): UnaryResponse;
  deleteDocument(
    requestMessage: v1_auth_common_dtos_pb.DeleteDocumentRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteDocumentResponse|null) => void
  ): UnaryResponse;
  getCounterparty(
    requestMessage: v1_auth_common_dtos_pb.GetCounterpartyRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.GetCounterpartyResponse|null) => void
  ): UnaryResponse;
  getCounterparty(
    requestMessage: v1_auth_common_dtos_pb.GetCounterpartyRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.GetCounterpartyResponse|null) => void
  ): UnaryResponse;
  listCounterparty(
    requestMessage: v1_auth_common_dtos_pb.ListCounterpartyRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListCounterpartyResponse|null) => void
  ): UnaryResponse;
  listCounterparty(
    requestMessage: v1_auth_common_dtos_pb.ListCounterpartyRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListCounterpartyResponse|null) => void
  ): UnaryResponse;
  saveCounterparty(
    requestMessage: v1_auth_common_dtos_pb.SaveCounterpartyRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveCounterpartyResponse|null) => void
  ): UnaryResponse;
  saveCounterparty(
    requestMessage: v1_auth_common_dtos_pb.SaveCounterpartyRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveCounterpartyResponse|null) => void
  ): UnaryResponse;
  getCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.GetCounterpartyLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.GetCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  getCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.GetCounterpartyLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.GetCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  listCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.ListCounterpartyLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  listCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.ListCounterpartyLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  saveCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.SaveCounterpartyLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  saveCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.SaveCounterpartyLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
}

