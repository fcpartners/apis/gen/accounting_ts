// package: fcp.accounting.v1.auth_mobile
// file: v1/auth_mobile/auth_service.proto

var v1_auth_mobile_auth_service_pb = require("../../v1/auth_mobile/auth_service_pb");
var google_protobuf_empty_pb = require("google-protobuf/google/protobuf/empty_pb");
var v1_auth_common_dtos_pb = require("../../v1/auth_common/dtos_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var MobileAuthService = (function () {
  function MobileAuthService() {}
  MobileAuthService.serviceName = "fcp.accounting.v1.auth_mobile.MobileAuthService";
  return MobileAuthService;
}());

MobileAuthService.SendCode = {
  methodName: "SendCode",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_mobile_auth_service_pb.SendCodeRequest,
  responseType: google_protobuf_empty_pb.Empty
};

MobileAuthService.CheckCode = {
  methodName: "CheckCode",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_mobile_auth_service_pb.CheckCodeRequest,
  responseType: v1_auth_mobile_auth_service_pb.CheckCodeResponse
};

MobileAuthService.GetUser = {
  methodName: "GetUser",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_mobile_auth_service_pb.GetUserRequest,
  responseType: v1_auth_common_dtos_pb.UserBriefInfo
};

MobileAuthService.RegisterUser = {
  methodName: "RegisterUser",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_mobile_auth_service_pb.RegisterUserRequest,
  responseType: v1_auth_mobile_auth_service_pb.RegisterUserResponse
};

MobileAuthService.UpdateUser = {
  methodName: "UpdateUser",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_mobile_auth_service_pb.UpdateUserRequest,
  responseType: v1_auth_common_dtos_pb.UserBriefInfo
};

MobileAuthService.ValidateSession = {
  methodName: "ValidateSession",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_mobile_auth_service_pb.ValidateSessionRequest,
  responseType: v1_auth_mobile_auth_service_pb.ValidateSessionResponse
};

MobileAuthService.DeleteUser = {
  methodName: "DeleteUser",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_mobile_auth_service_pb.DeleteUserRequest,
  responseType: google_protobuf_empty_pb.Empty
};

MobileAuthService.SetDeviceToken = {
  methodName: "SetDeviceToken",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_mobile_auth_service_pb.SetDeviceTokenRequest,
  responseType: google_protobuf_empty_pb.Empty
};

MobileAuthService.DeleteDeviceToken = {
  methodName: "DeleteDeviceToken",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_mobile_auth_service_pb.DeleteDeviceTokenRequest,
  responseType: google_protobuf_empty_pb.Empty
};

MobileAuthService.DeleteAllDeviceToken = {
  methodName: "DeleteAllDeviceToken",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_mobile_auth_service_pb.DeleteAllDeviceTokenRequest,
  responseType: google_protobuf_empty_pb.Empty
};

MobileAuthService.ListContactPerson = {
  methodName: "ListContactPerson",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.ListContactPersonRequest,
  responseType: v1_auth_common_dtos_pb.ListContactPersonResponse
};

MobileAuthService.SaveContactPerson = {
  methodName: "SaveContactPerson",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.SaveContactPersonRequest,
  responseType: v1_auth_common_dtos_pb.SaveContactPersonResponse
};

MobileAuthService.DeleteContactPerson = {
  methodName: "DeleteContactPerson",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.DeleteContactPersonRequest,
  responseType: v1_auth_common_dtos_pb.DeleteContactPersonResponse
};

MobileAuthService.ListLocation = {
  methodName: "ListLocation",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.ListLocationRequest,
  responseType: v1_auth_common_dtos_pb.ListLocationResponse
};

MobileAuthService.SaveLocation = {
  methodName: "SaveLocation",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.SaveLocationRequest,
  responseType: v1_auth_common_dtos_pb.SaveLocationResponse
};

MobileAuthService.DeleteLocation = {
  methodName: "DeleteLocation",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.DeleteLocationRequest,
  responseType: v1_auth_common_dtos_pb.DeleteLocationResponse
};

MobileAuthService.ListDocument = {
  methodName: "ListDocument",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.ListDocumentRequest,
  responseType: v1_auth_common_dtos_pb.ListDocumentResponse
};

MobileAuthService.SaveDocument = {
  methodName: "SaveDocument",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.SaveDocumentRequest,
  responseType: v1_auth_common_dtos_pb.SaveDocumentResponse
};

MobileAuthService.DeleteDocument = {
  methodName: "DeleteDocument",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.DeleteDocumentRequest,
  responseType: v1_auth_common_dtos_pb.DeleteDocumentResponse
};

MobileAuthService.GetCounterparty = {
  methodName: "GetCounterparty",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.GetCounterpartyRequest,
  responseType: v1_auth_common_dtos_pb.GetCounterpartyResponse
};

MobileAuthService.ListCounterparty = {
  methodName: "ListCounterparty",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.ListCounterpartyRequest,
  responseType: v1_auth_common_dtos_pb.ListCounterpartyResponse
};

MobileAuthService.SaveCounterparty = {
  methodName: "SaveCounterparty",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.SaveCounterpartyRequest,
  responseType: v1_auth_common_dtos_pb.SaveCounterpartyResponse
};

MobileAuthService.GetCounterpartyLocation = {
  methodName: "GetCounterpartyLocation",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.GetCounterpartyLocationRequest,
  responseType: v1_auth_common_dtos_pb.GetCounterpartyLocationResponse
};

MobileAuthService.ListCounterpartyLocation = {
  methodName: "ListCounterpartyLocation",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.ListCounterpartyLocationRequest,
  responseType: v1_auth_common_dtos_pb.ListCounterpartyLocationResponse
};

MobileAuthService.SaveCounterpartyLocation = {
  methodName: "SaveCounterpartyLocation",
  service: MobileAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.SaveCounterpartyLocationRequest,
  responseType: v1_auth_common_dtos_pb.SaveCounterpartyLocationResponse
};

exports.MobileAuthService = MobileAuthService;

function MobileAuthServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

MobileAuthServiceClient.prototype.sendCode = function sendCode(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.SendCode, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.checkCode = function checkCode(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.CheckCode, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.getUser = function getUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.GetUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.registerUser = function registerUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.RegisterUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.updateUser = function updateUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.UpdateUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.validateSession = function validateSession(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.ValidateSession, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.deleteUser = function deleteUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.DeleteUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.setDeviceToken = function setDeviceToken(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.SetDeviceToken, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.deleteDeviceToken = function deleteDeviceToken(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.DeleteDeviceToken, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.deleteAllDeviceToken = function deleteAllDeviceToken(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.DeleteAllDeviceToken, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.listContactPerson = function listContactPerson(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.ListContactPerson, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.saveContactPerson = function saveContactPerson(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.SaveContactPerson, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.deleteContactPerson = function deleteContactPerson(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.DeleteContactPerson, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.listLocation = function listLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.ListLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.saveLocation = function saveLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.SaveLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.deleteLocation = function deleteLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.DeleteLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.listDocument = function listDocument(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.ListDocument, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.saveDocument = function saveDocument(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.SaveDocument, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.deleteDocument = function deleteDocument(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.DeleteDocument, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.getCounterparty = function getCounterparty(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.GetCounterparty, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.listCounterparty = function listCounterparty(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.ListCounterparty, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.saveCounterparty = function saveCounterparty(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.SaveCounterparty, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.getCounterpartyLocation = function getCounterpartyLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.GetCounterpartyLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.listCounterpartyLocation = function listCounterpartyLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.ListCounterpartyLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileAuthServiceClient.prototype.saveCounterpartyLocation = function saveCounterpartyLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileAuthService.SaveCounterpartyLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.MobileAuthServiceClient = MobileAuthServiceClient;

