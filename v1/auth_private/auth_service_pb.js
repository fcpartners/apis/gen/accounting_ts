// source: v1/auth_private/auth_service.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js');
goog.object.extend(proto, google_protobuf_empty_pb);
var google_protobuf_wrappers_pb = require('google-protobuf/google/protobuf/wrappers_pb.js');
goog.object.extend(proto, google_protobuf_wrappers_pb);
var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js');
goog.object.extend(proto, google_protobuf_timestamp_pb);
var v1_auth_common_common_pb = require('../../v1/auth_common/common_pb.js');
goog.object.extend(proto, v1_auth_common_common_pb);
var v1_auth_common_dtos_pb = require('../../v1/auth_common/dtos_pb.js');
goog.object.extend(proto, v1_auth_common_dtos_pb);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ChangePasswordRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.CheckCodeRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.CheckCodeResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.CheckPasswordRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.CheckPasswordResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.CreatePermissionRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.CreatePermissionResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.CreateRoleRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.CreateRoleResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.CreateUserRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.DeletePermissionRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.DeletePermissionResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.GetPermissionRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.GetPermissionResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.GetRoleRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.GetRoleResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.GetUserRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ListPermissionsRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ListPermissionsResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ListRolesRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ListRolesResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ListSessionsRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ListSessionsResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ListUsersRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ListUsersResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.LockUserRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.LoginRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.LoginResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.LogoutRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.PermissionFilter', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ResetPasswordRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ResetSessionRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.RoleFilter', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.SendCodeRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.SendPushRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.SendPushRequestToRole', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.SendPushResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.SessionFilter', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.UnlockUserRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.UpdateRoleRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.UpdateRoleResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.UpdateUserRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.UserBriefForMessage', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ValidateSessionRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ValidateSessionResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest', null, global);
goog.exportSymbol('proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.UserBriefForMessage = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.UserBriefForMessage, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.UserBriefForMessage.displayName = 'proto.fcp.accounting.v1.auth_private.UserBriefForMessage';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest.repeatedFields_, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest.displayName = 'proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse.repeatedFields_, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse.displayName = 'proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest.displayName = 'proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse.repeatedFields_, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse.displayName = 'proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.SendCodeRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.SendCodeRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.SendCodeRequest.displayName = 'proto.fcp.accounting.v1.auth_private.SendCodeRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.CheckCodeRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.CheckCodeRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.CheckCodeRequest.displayName = 'proto.fcp.accounting.v1.auth_private.CheckCodeRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.CheckCodeResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.CheckCodeResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.CheckCodeResponse.displayName = 'proto.fcp.accounting.v1.auth_private.CheckCodeResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest.displayName = 'proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse.displayName = 'proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.GetUserRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.GetUserRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.GetUserRequest.displayName = 'proto.fcp.accounting.v1.auth_private.GetUserRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ListUsersRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ListUsersRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ListUsersRequest.displayName = 'proto.fcp.accounting.v1.auth_private.ListUsersRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ListUsersResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.accounting.v1.auth_private.ListUsersResponse.repeatedFields_, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ListUsersResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ListUsersResponse.displayName = 'proto.fcp.accounting.v1.auth_private.ListUsersResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.CreateUserRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.CreateUserRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.CreateUserRequest.displayName = 'proto.fcp.accounting.v1.auth_private.CreateUserRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.UpdateUserRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.UpdateUserRequest.displayName = 'proto.fcp.accounting.v1.auth_private.UpdateUserRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.CheckPasswordRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.CheckPasswordRequest.displayName = 'proto.fcp.accounting.v1.auth_private.CheckPasswordRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.CheckPasswordResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.CheckPasswordResponse.displayName = 'proto.fcp.accounting.v1.auth_private.CheckPasswordResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ChangePasswordRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ChangePasswordRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ChangePasswordRequest.displayName = 'proto.fcp.accounting.v1.auth_private.ChangePasswordRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.LockUserRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.LockUserRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.LockUserRequest.displayName = 'proto.fcp.accounting.v1.auth_private.LockUserRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.UnlockUserRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.UnlockUserRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.UnlockUserRequest.displayName = 'proto.fcp.accounting.v1.auth_private.UnlockUserRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest.displayName = 'proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest.displayName = 'proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse.displayName = 'proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ResetPasswordRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ResetPasswordRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ResetPasswordRequest.displayName = 'proto.fcp.accounting.v1.auth_private.ResetPasswordRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest.displayName = 'proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse.displayName = 'proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.SendPushRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.SendPushRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.SendPushRequest.displayName = 'proto.fcp.accounting.v1.auth_private.SendPushRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.SendPushRequestToRole = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.SendPushRequestToRole, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.displayName = 'proto.fcp.accounting.v1.auth_private.SendPushRequestToRole';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.SendPushResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.SendPushResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.SendPushResponse.displayName = 'proto.fcp.accounting.v1.auth_private.SendPushResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.GetRoleRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.GetRoleRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.GetRoleRequest.displayName = 'proto.fcp.accounting.v1.auth_private.GetRoleRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.GetRoleResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.GetRoleResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.GetRoleResponse.displayName = 'proto.fcp.accounting.v1.auth_private.GetRoleResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.CreateRoleRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.CreateRoleRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.CreateRoleRequest.displayName = 'proto.fcp.accounting.v1.auth_private.CreateRoleRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.CreateRoleResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.CreateRoleResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.CreateRoleResponse.displayName = 'proto.fcp.accounting.v1.auth_private.CreateRoleResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.UpdateRoleRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.UpdateRoleRequest.displayName = 'proto.fcp.accounting.v1.auth_private.UpdateRoleRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.UpdateRoleResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.UpdateRoleResponse.displayName = 'proto.fcp.accounting.v1.auth_private.UpdateRoleResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ListRolesRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ListRolesRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ListRolesRequest.displayName = 'proto.fcp.accounting.v1.auth_private.ListRolesRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.RoleFilter = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.RoleFilter, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.RoleFilter.displayName = 'proto.fcp.accounting.v1.auth_private.RoleFilter';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ListRolesResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.accounting.v1.auth_private.ListRolesResponse.repeatedFields_, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ListRolesResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ListRolesResponse.displayName = 'proto.fcp.accounting.v1.auth_private.ListRolesResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.GetPermissionRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.GetPermissionRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.GetPermissionRequest.displayName = 'proto.fcp.accounting.v1.auth_private.GetPermissionRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.GetPermissionResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.GetPermissionResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.GetPermissionResponse.displayName = 'proto.fcp.accounting.v1.auth_private.GetPermissionResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ListPermissionsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.displayName = 'proto.fcp.accounting.v1.auth_private.ListPermissionsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.PermissionFilter = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.PermissionFilter, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.PermissionFilter.displayName = 'proto.fcp.accounting.v1.auth_private.PermissionFilter';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.repeatedFields_, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ListPermissionsResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.displayName = 'proto.fcp.accounting.v1.auth_private.ListPermissionsResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest.displayName = 'proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse.displayName = 'proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.CreatePermissionRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.CreatePermissionRequest.displayName = 'proto.fcp.accounting.v1.auth_private.CreatePermissionRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.CreatePermissionResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.CreatePermissionResponse.displayName = 'proto.fcp.accounting.v1.auth_private.CreatePermissionResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.DeletePermissionRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.DeletePermissionRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.DeletePermissionRequest.displayName = 'proto.fcp.accounting.v1.auth_private.DeletePermissionRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.DeletePermissionResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.DeletePermissionResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.DeletePermissionResponse.displayName = 'proto.fcp.accounting.v1.auth_private.DeletePermissionResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.LoginRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.LoginRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.LoginRequest.displayName = 'proto.fcp.accounting.v1.auth_private.LoginRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.LoginResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.LoginResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.LoginResponse.displayName = 'proto.fcp.accounting.v1.auth_private.LoginResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.LogoutRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.LogoutRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.LogoutRequest.displayName = 'proto.fcp.accounting.v1.auth_private.LogoutRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ValidateSessionRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ValidateSessionRequest.displayName = 'proto.fcp.accounting.v1.auth_private.ValidateSessionRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ValidateSessionResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.displayName = 'proto.fcp.accounting.v1.auth_private.ValidateSessionResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest.displayName = 'proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse.displayName = 'proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest.displayName = 'proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse.displayName = 'proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ResetSessionRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ResetSessionRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ResetSessionRequest.displayName = 'proto.fcp.accounting.v1.auth_private.ResetSessionRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ListSessionsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ListSessionsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ListSessionsRequest.displayName = 'proto.fcp.accounting.v1.auth_private.ListSessionsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.SessionFilter = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.SessionFilter, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.SessionFilter.displayName = 'proto.fcp.accounting.v1.auth_private.SessionFilter';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ListSessionsResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.accounting.v1.auth_private.ListSessionsResponse.repeatedFields_, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ListSessionsResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ListSessionsResponse.displayName = 'proto.fcp.accounting.v1.auth_private.ListSessionsResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest.displayName = 'proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.repeatedFields_, null);
};
goog.inherits(proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.displayName = 'proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse';
}



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.UserBriefForMessage.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.UserBriefForMessage.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.UserBriefForMessage} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.UserBriefForMessage.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, ""),
    login: jspb.Message.getFieldWithDefault(msg, 2, ""),
    phoneNumber: jspb.Message.getFieldWithDefault(msg, 3, ""),
    firstName: jspb.Message.getFieldWithDefault(msg, 4, ""),
    lastName: jspb.Message.getFieldWithDefault(msg, 5, ""),
    email: jspb.Message.getFieldWithDefault(msg, 6, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.UserBriefForMessage}
 */
proto.fcp.accounting.v1.auth_private.UserBriefForMessage.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.UserBriefForMessage;
  return proto.fcp.accounting.v1.auth_private.UserBriefForMessage.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.UserBriefForMessage} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.UserBriefForMessage}
 */
proto.fcp.accounting.v1.auth_private.UserBriefForMessage.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setLogin(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setPhoneNumber(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setFirstName(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setLastName(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setEmail(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.UserBriefForMessage.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.UserBriefForMessage.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.UserBriefForMessage} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.UserBriefForMessage.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getLogin();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getPhoneNumber();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getFirstName();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getLastName();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getEmail();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UserBriefForMessage.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UserBriefForMessage} returns this
 */
proto.fcp.accounting.v1.auth_private.UserBriefForMessage.prototype.setId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string login = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UserBriefForMessage.prototype.getLogin = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UserBriefForMessage} returns this
 */
proto.fcp.accounting.v1.auth_private.UserBriefForMessage.prototype.setLogin = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string phone_number = 3;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UserBriefForMessage.prototype.getPhoneNumber = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UserBriefForMessage} returns this
 */
proto.fcp.accounting.v1.auth_private.UserBriefForMessage.prototype.setPhoneNumber = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string first_name = 4;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UserBriefForMessage.prototype.getFirstName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UserBriefForMessage} returns this
 */
proto.fcp.accounting.v1.auth_private.UserBriefForMessage.prototype.setFirstName = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string last_name = 5;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UserBriefForMessage.prototype.getLastName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UserBriefForMessage} returns this
 */
proto.fcp.accounting.v1.auth_private.UserBriefForMessage.prototype.setLastName = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional string email = 6;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UserBriefForMessage.prototype.getEmail = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UserBriefForMessage} returns this
 */
proto.fcp.accounting.v1.auth_private.UserBriefForMessage.prototype.setEmail = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    idsList: (f = jspb.Message.getRepeatedField(msg, 1)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest;
  return proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.addIds(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getIdsList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      1,
      f
    );
  }
};


/**
 * repeated string ids = 1;
 * @return {!Array<string>}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest.prototype.getIdsList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 1));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest.prototype.setIdsList = function(value) {
  return jspb.Message.setField(this, 1, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest.prototype.addIds = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 1, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsRequest.prototype.clearIdsList = function() {
  return this.setIdsList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    usersList: jspb.Message.toObjectList(msg.getUsersList(),
    proto.fcp.accounting.v1.auth_private.UserBriefForMessage.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse;
  return proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.auth_private.UserBriefForMessage;
      reader.readMessage(value,proto.fcp.accounting.v1.auth_private.UserBriefForMessage.deserializeBinaryFromReader);
      msg.addUsers(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUsersList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.fcp.accounting.v1.auth_private.UserBriefForMessage.serializeBinaryToWriter
    );
  }
};


/**
 * repeated UserBriefForMessage users = 1;
 * @return {!Array<!proto.fcp.accounting.v1.auth_private.UserBriefForMessage>}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse.prototype.getUsersList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.auth_private.UserBriefForMessage>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.accounting.v1.auth_private.UserBriefForMessage, 1));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.auth_private.UserBriefForMessage>} value
 * @return {!proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse} returns this
*/
proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse.prototype.setUsersList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.fcp.accounting.v1.auth_private.UserBriefForMessage=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.auth_private.UserBriefForMessage}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse.prototype.addUsers = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.fcp.accounting.v1.auth_private.UserBriefForMessage, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.GetUsersByIdsResponse.prototype.clearUsersList = function() {
  return this.setUsersList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    pattern: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest;
  return proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setPattern(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPattern();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string pattern = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest.prototype.getPattern = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageRequest.prototype.setPattern = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    usersList: jspb.Message.toObjectList(msg.getUsersList(),
    proto.fcp.accounting.v1.auth_private.UserBriefForMessage.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse;
  return proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.accounting.v1.auth_private.UserBriefForMessage;
      reader.readMessage(value,proto.fcp.accounting.v1.auth_private.UserBriefForMessage.deserializeBinaryFromReader);
      msg.addUsers(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUsersList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.fcp.accounting.v1.auth_private.UserBriefForMessage.serializeBinaryToWriter
    );
  }
};


/**
 * repeated UserBriefForMessage users = 1;
 * @return {!Array<!proto.fcp.accounting.v1.auth_private.UserBriefForMessage>}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse.prototype.getUsersList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.auth_private.UserBriefForMessage>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.accounting.v1.auth_private.UserBriefForMessage, 1));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.auth_private.UserBriefForMessage>} value
 * @return {!proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse} returns this
*/
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse.prototype.setUsersList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.fcp.accounting.v1.auth_private.UserBriefForMessage=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.auth_private.UserBriefForMessage}
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse.prototype.addUsers = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.fcp.accounting.v1.auth_private.UserBriefForMessage, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.GetUsersByPatternForMessageResponse.prototype.clearUsersList = function() {
  return this.setUsersList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.SendCodeRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.SendCodeRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.SendCodeRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.SendCodeRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    phoneNumber: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.SendCodeRequest}
 */
proto.fcp.accounting.v1.auth_private.SendCodeRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.SendCodeRequest;
  return proto.fcp.accounting.v1.auth_private.SendCodeRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.SendCodeRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.SendCodeRequest}
 */
proto.fcp.accounting.v1.auth_private.SendCodeRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setPhoneNumber(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.SendCodeRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.SendCodeRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.SendCodeRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.SendCodeRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPhoneNumber();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string phone_number = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.SendCodeRequest.prototype.getPhoneNumber = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.SendCodeRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.SendCodeRequest.prototype.setPhoneNumber = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.CheckCodeRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.CheckCodeRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.CheckCodeRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CheckCodeRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    phoneNumber: jspb.Message.getFieldWithDefault(msg, 1, ""),
    code: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.CheckCodeRequest}
 */
proto.fcp.accounting.v1.auth_private.CheckCodeRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.CheckCodeRequest;
  return proto.fcp.accounting.v1.auth_private.CheckCodeRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.CheckCodeRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.CheckCodeRequest}
 */
proto.fcp.accounting.v1.auth_private.CheckCodeRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setPhoneNumber(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.CheckCodeRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.CheckCodeRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.CheckCodeRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CheckCodeRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPhoneNumber();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getCode();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string phone_number = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.CheckCodeRequest.prototype.getPhoneNumber = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.CheckCodeRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.CheckCodeRequest.prototype.setPhoneNumber = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string code = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.CheckCodeRequest.prototype.getCode = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.CheckCodeRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.CheckCodeRequest.prototype.setCode = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.CheckCodeResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.CheckCodeResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.CheckCodeResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CheckCodeResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    valid: jspb.Message.getBooleanFieldWithDefault(msg, 1, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.CheckCodeResponse}
 */
proto.fcp.accounting.v1.auth_private.CheckCodeResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.CheckCodeResponse;
  return proto.fcp.accounting.v1.auth_private.CheckCodeResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.CheckCodeResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.CheckCodeResponse}
 */
proto.fcp.accounting.v1.auth_private.CheckCodeResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setValid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.CheckCodeResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.CheckCodeResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.CheckCodeResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CheckCodeResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getValid();
  if (f) {
    writer.writeBool(
      1,
      f
    );
  }
};


/**
 * optional bool valid = 1;
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.CheckCodeResponse.prototype.getValid = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.accounting.v1.auth_private.CheckCodeResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.CheckCodeResponse.prototype.setValid = function(value) {
  return jspb.Message.setProto3BooleanField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    login: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest}
 */
proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest;
  return proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest}
 */
proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setLogin(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getLogin();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string login = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest.prototype.getLogin = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.CheckUserByLoginRequest.prototype.setLogin = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    valid: jspb.Message.getBooleanFieldWithDefault(msg, 1, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse}
 */
proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse;
  return proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse}
 */
proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setValid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getValid();
  if (f) {
    writer.writeBool(
      1,
      f
    );
  }
};


/**
 * optional bool valid = 1;
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse.prototype.getValid = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.CheckUserByLoginResponse.prototype.setValid = function(value) {
  return jspb.Message.setProto3BooleanField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.GetUserRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.GetUserRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.GetUserRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetUserRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    userId: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.GetUserRequest}
 */
proto.fcp.accounting.v1.auth_private.GetUserRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.GetUserRequest;
  return proto.fcp.accounting.v1.auth_private.GetUserRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.GetUserRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.GetUserRequest}
 */
proto.fcp.accounting.v1.auth_private.GetUserRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.GetUserRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.GetUserRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.GetUserRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetUserRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string user_id = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.GetUserRequest.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.GetUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.GetUserRequest.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ListUsersRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ListUsersRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ListUsersRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListUsersRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    pagination: (f = msg.getPagination()) && v1_auth_common_common_pb.PaginationRequest.toObject(includeInstance, f),
    filter: (f = msg.getFilter()) && v1_auth_common_dtos_pb.UserFilter.toObject(includeInstance, f),
    sort: (f = msg.getSort()) && v1_auth_common_common_pb.Sort.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ListUsersRequest}
 */
proto.fcp.accounting.v1.auth_private.ListUsersRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ListUsersRequest;
  return proto.fcp.accounting.v1.auth_private.ListUsersRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ListUsersRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ListUsersRequest}
 */
proto.fcp.accounting.v1.auth_private.ListUsersRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_auth_common_common_pb.PaginationRequest;
      reader.readMessage(value,v1_auth_common_common_pb.PaginationRequest.deserializeBinaryFromReader);
      msg.setPagination(value);
      break;
    case 2:
      var value = new v1_auth_common_dtos_pb.UserFilter;
      reader.readMessage(value,v1_auth_common_dtos_pb.UserFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    case 3:
      var value = new v1_auth_common_common_pb.Sort;
      reader.readMessage(value,v1_auth_common_common_pb.Sort.deserializeBinaryFromReader);
      msg.setSort(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ListUsersRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ListUsersRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ListUsersRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListUsersRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPagination();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_auth_common_common_pb.PaginationRequest.serializeBinaryToWriter
    );
  }
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      v1_auth_common_dtos_pb.UserFilter.serializeBinaryToWriter
    );
  }
  f = message.getSort();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      v1_auth_common_common_pb.Sort.serializeBinaryToWriter
    );
  }
};


/**
 * optional fcp.accounting.v1.PaginationRequest pagination = 1;
 * @return {?proto.fcp.accounting.v1.PaginationRequest}
 */
proto.fcp.accounting.v1.auth_private.ListUsersRequest.prototype.getPagination = function() {
  return /** @type{?proto.fcp.accounting.v1.PaginationRequest} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_common_pb.PaginationRequest, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.PaginationRequest|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListUsersRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.ListUsersRequest.prototype.setPagination = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.ListUsersRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ListUsersRequest.prototype.clearPagination = function() {
  return this.setPagination(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.ListUsersRequest.prototype.hasPagination = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional fcp.accounting.v1.UserFilter filter = 2;
 * @return {?proto.fcp.accounting.v1.UserFilter}
 */
proto.fcp.accounting.v1.auth_private.ListUsersRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.accounting.v1.UserFilter} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_dtos_pb.UserFilter, 2));
};


/**
 * @param {?proto.fcp.accounting.v1.UserFilter|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListUsersRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.ListUsersRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.ListUsersRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ListUsersRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.ListUsersRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional fcp.accounting.v1.Sort sort = 3;
 * @return {?proto.fcp.accounting.v1.Sort}
 */
proto.fcp.accounting.v1.auth_private.ListUsersRequest.prototype.getSort = function() {
  return /** @type{?proto.fcp.accounting.v1.Sort} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_common_pb.Sort, 3));
};


/**
 * @param {?proto.fcp.accounting.v1.Sort|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListUsersRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.ListUsersRequest.prototype.setSort = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.ListUsersRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ListUsersRequest.prototype.clearSort = function() {
  return this.setSort(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.ListUsersRequest.prototype.hasSort = function() {
  return jspb.Message.getField(this, 3) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.accounting.v1.auth_private.ListUsersResponse.repeatedFields_ = [2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ListUsersResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ListUsersResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ListUsersResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListUsersResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    pagination: (f = msg.getPagination()) && v1_auth_common_common_pb.PaginationResponse.toObject(includeInstance, f),
    usersList: jspb.Message.toObjectList(msg.getUsersList(),
    v1_auth_common_dtos_pb.User.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ListUsersResponse}
 */
proto.fcp.accounting.v1.auth_private.ListUsersResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ListUsersResponse;
  return proto.fcp.accounting.v1.auth_private.ListUsersResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ListUsersResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ListUsersResponse}
 */
proto.fcp.accounting.v1.auth_private.ListUsersResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_auth_common_common_pb.PaginationResponse;
      reader.readMessage(value,v1_auth_common_common_pb.PaginationResponse.deserializeBinaryFromReader);
      msg.setPagination(value);
      break;
    case 2:
      var value = new v1_auth_common_dtos_pb.User;
      reader.readMessage(value,v1_auth_common_dtos_pb.User.deserializeBinaryFromReader);
      msg.addUsers(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ListUsersResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ListUsersResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ListUsersResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListUsersResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPagination();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_auth_common_common_pb.PaginationResponse.serializeBinaryToWriter
    );
  }
  f = message.getUsersList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      v1_auth_common_dtos_pb.User.serializeBinaryToWriter
    );
  }
};


/**
 * optional fcp.accounting.v1.PaginationResponse pagination = 1;
 * @return {?proto.fcp.accounting.v1.PaginationResponse}
 */
proto.fcp.accounting.v1.auth_private.ListUsersResponse.prototype.getPagination = function() {
  return /** @type{?proto.fcp.accounting.v1.PaginationResponse} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_common_pb.PaginationResponse, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.PaginationResponse|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListUsersResponse} returns this
*/
proto.fcp.accounting.v1.auth_private.ListUsersResponse.prototype.setPagination = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.ListUsersResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.ListUsersResponse.prototype.clearPagination = function() {
  return this.setPagination(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.ListUsersResponse.prototype.hasPagination = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * repeated fcp.accounting.v1.User users = 2;
 * @return {!Array<!proto.fcp.accounting.v1.User>}
 */
proto.fcp.accounting.v1.auth_private.ListUsersResponse.prototype.getUsersList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.User>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_auth_common_dtos_pb.User, 2));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.User>} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListUsersResponse} returns this
*/
proto.fcp.accounting.v1.auth_private.ListUsersResponse.prototype.setUsersList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.fcp.accounting.v1.User=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.User}
 */
proto.fcp.accounting.v1.auth_private.ListUsersResponse.prototype.addUsers = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.fcp.accounting.v1.User, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.auth_private.ListUsersResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.ListUsersResponse.prototype.clearUsersList = function() {
  return this.setUsersList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.CreateUserRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.CreateUserRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.CreateUserRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CreateUserRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    user: (f = msg.getUser()) && v1_auth_common_dtos_pb.UserBriefInfo.toObject(includeInstance, f),
    rawPassword: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.CreateUserRequest}
 */
proto.fcp.accounting.v1.auth_private.CreateUserRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.CreateUserRequest;
  return proto.fcp.accounting.v1.auth_private.CreateUserRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.CreateUserRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.CreateUserRequest}
 */
proto.fcp.accounting.v1.auth_private.CreateUserRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_auth_common_dtos_pb.UserBriefInfo;
      reader.readMessage(value,v1_auth_common_dtos_pb.UserBriefInfo.deserializeBinaryFromReader);
      msg.setUser(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setRawPassword(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.CreateUserRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.CreateUserRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.CreateUserRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CreateUserRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUser();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_auth_common_dtos_pb.UserBriefInfo.serializeBinaryToWriter
    );
  }
  f = message.getRawPassword();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional fcp.accounting.v1.UserBriefInfo user = 1;
 * @return {?proto.fcp.accounting.v1.UserBriefInfo}
 */
proto.fcp.accounting.v1.auth_private.CreateUserRequest.prototype.getUser = function() {
  return /** @type{?proto.fcp.accounting.v1.UserBriefInfo} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_dtos_pb.UserBriefInfo, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.UserBriefInfo|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.CreateUserRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.CreateUserRequest.prototype.setUser = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.CreateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.CreateUserRequest.prototype.clearUser = function() {
  return this.setUser(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.CreateUserRequest.prototype.hasUser = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional string raw_password = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.CreateUserRequest.prototype.getRawPassword = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.CreateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.CreateUserRequest.prototype.setRawPassword = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.UpdateUserRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    userId: jspb.Message.getFieldWithDefault(msg, 1, ""),
    phoneNumber: jspb.Message.getFieldWithDefault(msg, 2, ""),
    firstName: jspb.Message.getFieldWithDefault(msg, 3, ""),
    lastName: jspb.Message.getFieldWithDefault(msg, 4, ""),
    middleName: jspb.Message.getFieldWithDefault(msg, 5, ""),
    email: jspb.Message.getFieldWithDefault(msg, 6, ""),
    latitude: jspb.Message.getFloatingPointFieldWithDefault(msg, 7, 0.0),
    longitude: jspb.Message.getFloatingPointFieldWithDefault(msg, 8, 0.0),
    address: jspb.Message.getFieldWithDefault(msg, 9, ""),
    countryCode: jspb.Message.getFieldWithDefault(msg, 10, ""),
    languageCode: jspb.Message.getFieldWithDefault(msg, 11, ""),
    role: jspb.Message.getFieldWithDefault(msg, 12, ""),
    businessName: jspb.Message.getFieldWithDefault(msg, 13, ""),
    businessAddress: jspb.Message.getFieldWithDefault(msg, 14, ""),
    state: jspb.Message.getFieldWithDefault(msg, 15, 0),
    note: jspb.Message.getFieldWithDefault(msg, 16, ""),
    verified: jspb.Message.getBooleanFieldWithDefault(msg, 17, false),
    creditApproved: jspb.Message.getBooleanFieldWithDefault(msg, 18, false),
    commoditiesAccess: jspb.Message.getBooleanFieldWithDefault(msg, 21, false),
    referralUrl: jspb.Message.getFieldWithDefault(msg, 22, ""),
    taxInfo: (f = msg.getTaxInfo()) && v1_auth_common_dtos_pb.TaxInfo.toObject(includeInstance, f),
    businessInfo: (f = msg.getBusinessInfo()) && v1_auth_common_dtos_pb.BusinessInfo.toObject(includeInstance, f),
    supplier: (f = msg.getSupplier()) && v1_auth_common_dtos_pb.UserBriefInfo.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.UpdateUserRequest;
  return proto.fcp.accounting.v1.auth_private.UpdateUserRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPhoneNumber(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setFirstName(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setLastName(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setMiddleName(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setEmail(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setLatitude(value);
      break;
    case 8:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setLongitude(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.setAddress(value);
      break;
    case 10:
      var value = /** @type {string} */ (reader.readString());
      msg.setCountryCode(value);
      break;
    case 11:
      var value = /** @type {string} */ (reader.readString());
      msg.setLanguageCode(value);
      break;
    case 12:
      var value = /** @type {string} */ (reader.readString());
      msg.setRole(value);
      break;
    case 13:
      var value = /** @type {string} */ (reader.readString());
      msg.setBusinessName(value);
      break;
    case 14:
      var value = /** @type {string} */ (reader.readString());
      msg.setBusinessAddress(value);
      break;
    case 15:
      var value = /** @type {number} */ (reader.readUint32());
      msg.setState(value);
      break;
    case 16:
      var value = /** @type {string} */ (reader.readString());
      msg.setNote(value);
      break;
    case 17:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setVerified(value);
      break;
    case 18:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setCreditApproved(value);
      break;
    case 21:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setCommoditiesAccess(value);
      break;
    case 22:
      var value = /** @type {string} */ (reader.readString());
      msg.setReferralUrl(value);
      break;
    case 19:
      var value = new v1_auth_common_dtos_pb.TaxInfo;
      reader.readMessage(value,v1_auth_common_dtos_pb.TaxInfo.deserializeBinaryFromReader);
      msg.setTaxInfo(value);
      break;
    case 20:
      var value = new v1_auth_common_dtos_pb.BusinessInfo;
      reader.readMessage(value,v1_auth_common_dtos_pb.BusinessInfo.deserializeBinaryFromReader);
      msg.setBusinessInfo(value);
      break;
    case 35:
      var value = new v1_auth_common_dtos_pb.UserBriefInfo;
      reader.readMessage(value,v1_auth_common_dtos_pb.UserBriefInfo.deserializeBinaryFromReader);
      msg.setSupplier(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.UpdateUserRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getPhoneNumber();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getFirstName();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getLastName();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getMiddleName();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getEmail();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getLatitude();
  if (f !== 0.0) {
    writer.writeFloat(
      7,
      f
    );
  }
  f = message.getLongitude();
  if (f !== 0.0) {
    writer.writeFloat(
      8,
      f
    );
  }
  f = message.getAddress();
  if (f.length > 0) {
    writer.writeString(
      9,
      f
    );
  }
  f = message.getCountryCode();
  if (f.length > 0) {
    writer.writeString(
      10,
      f
    );
  }
  f = message.getLanguageCode();
  if (f.length > 0) {
    writer.writeString(
      11,
      f
    );
  }
  f = message.getRole();
  if (f.length > 0) {
    writer.writeString(
      12,
      f
    );
  }
  f = message.getBusinessName();
  if (f.length > 0) {
    writer.writeString(
      13,
      f
    );
  }
  f = message.getBusinessAddress();
  if (f.length > 0) {
    writer.writeString(
      14,
      f
    );
  }
  f = message.getState();
  if (f !== 0) {
    writer.writeUint32(
      15,
      f
    );
  }
  f = message.getNote();
  if (f.length > 0) {
    writer.writeString(
      16,
      f
    );
  }
  f = message.getVerified();
  if (f) {
    writer.writeBool(
      17,
      f
    );
  }
  f = message.getCreditApproved();
  if (f) {
    writer.writeBool(
      18,
      f
    );
  }
  f = message.getCommoditiesAccess();
  if (f) {
    writer.writeBool(
      21,
      f
    );
  }
  f = message.getReferralUrl();
  if (f.length > 0) {
    writer.writeString(
      22,
      f
    );
  }
  f = message.getTaxInfo();
  if (f != null) {
    writer.writeMessage(
      19,
      f,
      v1_auth_common_dtos_pb.TaxInfo.serializeBinaryToWriter
    );
  }
  f = message.getBusinessInfo();
  if (f != null) {
    writer.writeMessage(
      20,
      f,
      v1_auth_common_dtos_pb.BusinessInfo.serializeBinaryToWriter
    );
  }
  f = message.getSupplier();
  if (f != null) {
    writer.writeMessage(
      35,
      f,
      v1_auth_common_dtos_pb.UserBriefInfo.serializeBinaryToWriter
    );
  }
};


/**
 * optional string user_id = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string phone_number = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getPhoneNumber = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setPhoneNumber = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string first_name = 3;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getFirstName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setFirstName = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string last_name = 4;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getLastName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setLastName = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string middle_name = 5;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getMiddleName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setMiddleName = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional string email = 6;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getEmail = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setEmail = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional float latitude = 7;
 * @return {number}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getLatitude = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 7, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setLatitude = function(value) {
  return jspb.Message.setProto3FloatField(this, 7, value);
};


/**
 * optional float longitude = 8;
 * @return {number}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getLongitude = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 8, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setLongitude = function(value) {
  return jspb.Message.setProto3FloatField(this, 8, value);
};


/**
 * optional string address = 9;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getAddress = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 9, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setAddress = function(value) {
  return jspb.Message.setProto3StringField(this, 9, value);
};


/**
 * optional string country_code = 10;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getCountryCode = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 10, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setCountryCode = function(value) {
  return jspb.Message.setProto3StringField(this, 10, value);
};


/**
 * optional string language_code = 11;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getLanguageCode = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 11, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setLanguageCode = function(value) {
  return jspb.Message.setProto3StringField(this, 11, value);
};


/**
 * optional string role = 12;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getRole = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 12, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setRole = function(value) {
  return jspb.Message.setProto3StringField(this, 12, value);
};


/**
 * optional string business_name = 13;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getBusinessName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 13, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setBusinessName = function(value) {
  return jspb.Message.setProto3StringField(this, 13, value);
};


/**
 * optional string business_address = 14;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getBusinessAddress = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 14, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setBusinessAddress = function(value) {
  return jspb.Message.setProto3StringField(this, 14, value);
};


/**
 * optional uint32 state = 15;
 * @return {number}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getState = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 15, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setState = function(value) {
  return jspb.Message.setProto3IntField(this, 15, value);
};


/**
 * optional string note = 16;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getNote = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 16, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setNote = function(value) {
  return jspb.Message.setProto3StringField(this, 16, value);
};


/**
 * optional bool verified = 17;
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getVerified = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 17, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setVerified = function(value) {
  return jspb.Message.setProto3BooleanField(this, 17, value);
};


/**
 * optional bool credit_approved = 18;
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getCreditApproved = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 18, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setCreditApproved = function(value) {
  return jspb.Message.setProto3BooleanField(this, 18, value);
};


/**
 * optional bool commodities_access = 21;
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getCommoditiesAccess = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 21, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setCommoditiesAccess = function(value) {
  return jspb.Message.setProto3BooleanField(this, 21, value);
};


/**
 * optional string referral_url = 22;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getReferralUrl = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 22, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setReferralUrl = function(value) {
  return jspb.Message.setProto3StringField(this, 22, value);
};


/**
 * optional fcp.accounting.v1.TaxInfo tax_info = 19;
 * @return {?proto.fcp.accounting.v1.TaxInfo}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getTaxInfo = function() {
  return /** @type{?proto.fcp.accounting.v1.TaxInfo} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_dtos_pb.TaxInfo, 19));
};


/**
 * @param {?proto.fcp.accounting.v1.TaxInfo|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setTaxInfo = function(value) {
  return jspb.Message.setWrapperField(this, 19, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.clearTaxInfo = function() {
  return this.setTaxInfo(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.hasTaxInfo = function() {
  return jspb.Message.getField(this, 19) != null;
};


/**
 * optional fcp.accounting.v1.BusinessInfo business_info = 20;
 * @return {?proto.fcp.accounting.v1.BusinessInfo}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getBusinessInfo = function() {
  return /** @type{?proto.fcp.accounting.v1.BusinessInfo} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_dtos_pb.BusinessInfo, 20));
};


/**
 * @param {?proto.fcp.accounting.v1.BusinessInfo|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setBusinessInfo = function(value) {
  return jspb.Message.setWrapperField(this, 20, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.clearBusinessInfo = function() {
  return this.setBusinessInfo(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.hasBusinessInfo = function() {
  return jspb.Message.getField(this, 20) != null;
};


/**
 * optional fcp.accounting.v1.UserBriefInfo supplier = 35;
 * @return {?proto.fcp.accounting.v1.UserBriefInfo}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.getSupplier = function() {
  return /** @type{?proto.fcp.accounting.v1.UserBriefInfo} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_dtos_pb.UserBriefInfo, 35));
};


/**
 * @param {?proto.fcp.accounting.v1.UserBriefInfo|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.setSupplier = function(value) {
  return jspb.Message.setWrapperField(this, 35, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.clearSupplier = function() {
  return this.setSupplier(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.UpdateUserRequest.prototype.hasSupplier = function() {
  return jspb.Message.getField(this, 35) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.CheckPasswordRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.CheckPasswordRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    userId: jspb.Message.getFieldWithDefault(msg, 1, ""),
    password: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.CheckPasswordRequest}
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.CheckPasswordRequest;
  return proto.fcp.accounting.v1.auth_private.CheckPasswordRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.CheckPasswordRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.CheckPasswordRequest}
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPassword(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.CheckPasswordRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.CheckPasswordRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getPassword();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string user_id = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordRequest.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.CheckPasswordRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordRequest.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string password = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordRequest.prototype.getPassword = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.CheckPasswordRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordRequest.prototype.setPassword = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.CheckPasswordResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.CheckPasswordResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    valid: jspb.Message.getBooleanFieldWithDefault(msg, 1, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.CheckPasswordResponse}
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.CheckPasswordResponse;
  return proto.fcp.accounting.v1.auth_private.CheckPasswordResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.CheckPasswordResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.CheckPasswordResponse}
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setValid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.CheckPasswordResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.CheckPasswordResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getValid();
  if (f) {
    writer.writeBool(
      1,
      f
    );
  }
};


/**
 * optional bool valid = 1;
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordResponse.prototype.getValid = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.accounting.v1.auth_private.CheckPasswordResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.CheckPasswordResponse.prototype.setValid = function(value) {
  return jspb.Message.setProto3BooleanField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ChangePasswordRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ChangePasswordRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ChangePasswordRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ChangePasswordRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    userId: jspb.Message.getFieldWithDefault(msg, 1, ""),
    oldPassword: jspb.Message.getFieldWithDefault(msg, 2, ""),
    newPassword: jspb.Message.getFieldWithDefault(msg, 3, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ChangePasswordRequest}
 */
proto.fcp.accounting.v1.auth_private.ChangePasswordRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ChangePasswordRequest;
  return proto.fcp.accounting.v1.auth_private.ChangePasswordRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ChangePasswordRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ChangePasswordRequest}
 */
proto.fcp.accounting.v1.auth_private.ChangePasswordRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setOldPassword(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setNewPassword(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ChangePasswordRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ChangePasswordRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ChangePasswordRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ChangePasswordRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getOldPassword();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getNewPassword();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
};


/**
 * optional string user_id = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.ChangePasswordRequest.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.ChangePasswordRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ChangePasswordRequest.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string old_password = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.ChangePasswordRequest.prototype.getOldPassword = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.ChangePasswordRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ChangePasswordRequest.prototype.setOldPassword = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string new_password = 3;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.ChangePasswordRequest.prototype.getNewPassword = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.ChangePasswordRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ChangePasswordRequest.prototype.setNewPassword = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.LockUserRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.LockUserRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.LockUserRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.LockUserRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    userId: jspb.Message.getFieldWithDefault(msg, 1, ""),
    lockReason: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.LockUserRequest}
 */
proto.fcp.accounting.v1.auth_private.LockUserRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.LockUserRequest;
  return proto.fcp.accounting.v1.auth_private.LockUserRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.LockUserRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.LockUserRequest}
 */
proto.fcp.accounting.v1.auth_private.LockUserRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setLockReason(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.LockUserRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.LockUserRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.LockUserRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.LockUserRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getLockReason();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string user_id = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.LockUserRequest.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.LockUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.LockUserRequest.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string lock_reason = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.LockUserRequest.prototype.getLockReason = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.LockUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.LockUserRequest.prototype.setLockReason = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.UnlockUserRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.UnlockUserRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.UnlockUserRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.UnlockUserRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    userId: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.UnlockUserRequest}
 */
proto.fcp.accounting.v1.auth_private.UnlockUserRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.UnlockUserRequest;
  return proto.fcp.accounting.v1.auth_private.UnlockUserRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.UnlockUserRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.UnlockUserRequest}
 */
proto.fcp.accounting.v1.auth_private.UnlockUserRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.UnlockUserRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.UnlockUserRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.UnlockUserRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.UnlockUserRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string user_id = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UnlockUserRequest.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UnlockUserRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UnlockUserRequest.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    login: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest}
 */
proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest;
  return proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest}
 */
proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setLogin(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getLogin();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string login = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest.prototype.getLogin = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.InitPasswordResetRequest.prototype.setLogin = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    resetToken: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest}
 */
proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest;
  return proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest}
 */
proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setResetToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getResetToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string reset_token = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest.prototype.getResetToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenRequest.prototype.setResetToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    valid: jspb.Message.getBooleanFieldWithDefault(msg, 1, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse}
 */
proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse;
  return proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse}
 */
proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setValid(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getValid();
  if (f) {
    writer.writeBool(
      1,
      f
    );
  }
};


/**
 * optional bool valid = 1;
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse.prototype.getValid = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.ValidatePasswordResetTokenResponse.prototype.setValid = function(value) {
  return jspb.Message.setProto3BooleanField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ResetPasswordRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ResetPasswordRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ResetPasswordRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ResetPasswordRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    resetToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    newPassword: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ResetPasswordRequest}
 */
proto.fcp.accounting.v1.auth_private.ResetPasswordRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ResetPasswordRequest;
  return proto.fcp.accounting.v1.auth_private.ResetPasswordRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ResetPasswordRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ResetPasswordRequest}
 */
proto.fcp.accounting.v1.auth_private.ResetPasswordRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setResetToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setNewPassword(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ResetPasswordRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ResetPasswordRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ResetPasswordRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ResetPasswordRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getResetToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getNewPassword();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string reset_token = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.ResetPasswordRequest.prototype.getResetToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.ResetPasswordRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ResetPasswordRequest.prototype.setResetToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string new_password = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.ResetPasswordRequest.prototype.getNewPassword = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.ResetPasswordRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ResetPasswordRequest.prototype.setNewPassword = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    userId: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest}
 */
proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest;
  return proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest}
 */
proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string user_id = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.GetUserResetTokenRequest.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    code: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse}
 */
proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse;
  return proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse}
 */
proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setCode(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getCode();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string code = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse.prototype.getCode = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.GetUserResetTokenResponse.prototype.setCode = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.SendPushRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.SendPushRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.SendPushRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    userId: jspb.Message.getFieldWithDefault(msg, 1, ""),
    type: jspb.Message.getFieldWithDefault(msg, 2, ""),
    title: jspb.Message.getFieldWithDefault(msg, 3, ""),
    message: jspb.Message.getFieldWithDefault(msg, 4, ""),
    image: jspb.Message.getFieldWithDefault(msg, 5, ""),
    dataMap: (f = msg.getDataMap()) ? f.toObject(includeInstance, undefined) : []
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushRequest}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.SendPushRequest;
  return proto.fcp.accounting.v1.auth_private.SendPushRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.SendPushRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushRequest}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setType(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setTitle(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setMessage(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setImage(value);
      break;
    case 6:
      var value = msg.getDataMap();
      reader.readMessage(value, function(message, reader) {
        jspb.Map.deserializeBinary(message, reader, jspb.BinaryReader.prototype.readString, jspb.BinaryReader.prototype.readString, null, "", "");
         });
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.SendPushRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.SendPushRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.SendPushRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getType();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getTitle();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getMessage();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getImage();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getDataMap(true);
  if (f && f.getLength() > 0) {
    f.serializeBinary(6, writer, jspb.BinaryWriter.prototype.writeString, jspb.BinaryWriter.prototype.writeString);
  }
};


/**
 * optional string user_id = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequest.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.SendPushRequest.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string type = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequest.prototype.getType = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.SendPushRequest.prototype.setType = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string title = 3;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequest.prototype.getTitle = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.SendPushRequest.prototype.setTitle = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string message = 4;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequest.prototype.getMessage = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.SendPushRequest.prototype.setMessage = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string image = 5;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequest.prototype.getImage = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.SendPushRequest.prototype.setImage = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * map<string, string> data = 6;
 * @param {boolean=} opt_noLazyCreate Do not create the map if
 * empty, instead returning `undefined`
 * @return {!jspb.Map<string,string>}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequest.prototype.getDataMap = function(opt_noLazyCreate) {
  return /** @type {!jspb.Map<string,string>} */ (
      jspb.Message.getMapField(this, 6, opt_noLazyCreate,
      null));
};


/**
 * Clears values from the map. The map will be non-null.
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.SendPushRequest.prototype.clearDataMap = function() {
  this.getDataMap().clear();
  return this;};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.SendPushRequestToRole} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.toObject = function(includeInstance, msg) {
  var f, obj = {
    role: jspb.Message.getFieldWithDefault(msg, 1, ""),
    type: jspb.Message.getFieldWithDefault(msg, 2, ""),
    title: jspb.Message.getFieldWithDefault(msg, 3, ""),
    message: jspb.Message.getFieldWithDefault(msg, 4, ""),
    image: jspb.Message.getFieldWithDefault(msg, 5, ""),
    dataMap: (f = msg.getDataMap()) ? f.toObject(includeInstance, undefined) : []
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushRequestToRole}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.SendPushRequestToRole;
  return proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.SendPushRequestToRole} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushRequestToRole}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setRole(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setType(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setTitle(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setMessage(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setImage(value);
      break;
    case 6:
      var value = msg.getDataMap();
      reader.readMessage(value, function(message, reader) {
        jspb.Map.deserializeBinary(message, reader, jspb.BinaryReader.prototype.readString, jspb.BinaryReader.prototype.readString, null, "", "");
         });
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.SendPushRequestToRole} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getRole();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getType();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getTitle();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getMessage();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getImage();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getDataMap(true);
  if (f && f.getLength() > 0) {
    f.serializeBinary(6, writer, jspb.BinaryWriter.prototype.writeString, jspb.BinaryWriter.prototype.writeString);
  }
};


/**
 * optional string role = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.prototype.getRole = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushRequestToRole} returns this
 */
proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.prototype.setRole = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string type = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.prototype.getType = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushRequestToRole} returns this
 */
proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.prototype.setType = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string title = 3;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.prototype.getTitle = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushRequestToRole} returns this
 */
proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.prototype.setTitle = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string message = 4;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.prototype.getMessage = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushRequestToRole} returns this
 */
proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.prototype.setMessage = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string image = 5;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.prototype.getImage = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushRequestToRole} returns this
 */
proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.prototype.setImage = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * map<string, string> data = 6;
 * @param {boolean=} opt_noLazyCreate Do not create the map if
 * empty, instead returning `undefined`
 * @return {!jspb.Map<string,string>}
 */
proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.prototype.getDataMap = function(opt_noLazyCreate) {
  return /** @type {!jspb.Map<string,string>} */ (
      jspb.Message.getMapField(this, 6, opt_noLazyCreate,
      null));
};


/**
 * Clears values from the map. The map will be non-null.
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushRequestToRole} returns this
 */
proto.fcp.accounting.v1.auth_private.SendPushRequestToRole.prototype.clearDataMap = function() {
  this.getDataMap().clear();
  return this;};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.SendPushResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.SendPushResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.SendPushResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.SendPushResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    message: jspb.Message.getFieldWithDefault(msg, 1, ""),
    error: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushResponse}
 */
proto.fcp.accounting.v1.auth_private.SendPushResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.SendPushResponse;
  return proto.fcp.accounting.v1.auth_private.SendPushResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.SendPushResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushResponse}
 */
proto.fcp.accounting.v1.auth_private.SendPushResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setMessage(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setError(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.SendPushResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.SendPushResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.SendPushResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.SendPushResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getMessage();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getError();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string message = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.SendPushResponse.prototype.getMessage = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.SendPushResponse.prototype.setMessage = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string error = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.SendPushResponse.prototype.getError = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.SendPushResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.SendPushResponse.prototype.setError = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.GetRoleRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.GetRoleRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.GetRoleRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetRoleRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    role: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.GetRoleRequest}
 */
proto.fcp.accounting.v1.auth_private.GetRoleRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.GetRoleRequest;
  return proto.fcp.accounting.v1.auth_private.GetRoleRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.GetRoleRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.GetRoleRequest}
 */
proto.fcp.accounting.v1.auth_private.GetRoleRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setRole(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.GetRoleRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.GetRoleRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.GetRoleRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetRoleRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getRole();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string role = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.GetRoleRequest.prototype.getRole = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.GetRoleRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.GetRoleRequest.prototype.setRole = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.GetRoleResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.GetRoleResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.GetRoleResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetRoleResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    role: (f = msg.getRole()) && v1_auth_common_dtos_pb.RoleFull.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.GetRoleResponse}
 */
proto.fcp.accounting.v1.auth_private.GetRoleResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.GetRoleResponse;
  return proto.fcp.accounting.v1.auth_private.GetRoleResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.GetRoleResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.GetRoleResponse}
 */
proto.fcp.accounting.v1.auth_private.GetRoleResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_auth_common_dtos_pb.RoleFull;
      reader.readMessage(value,v1_auth_common_dtos_pb.RoleFull.deserializeBinaryFromReader);
      msg.setRole(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.GetRoleResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.GetRoleResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.GetRoleResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetRoleResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getRole();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_auth_common_dtos_pb.RoleFull.serializeBinaryToWriter
    );
  }
};


/**
 * optional fcp.accounting.v1.RoleFull role = 1;
 * @return {?proto.fcp.accounting.v1.RoleFull}
 */
proto.fcp.accounting.v1.auth_private.GetRoleResponse.prototype.getRole = function() {
  return /** @type{?proto.fcp.accounting.v1.RoleFull} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_dtos_pb.RoleFull, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.RoleFull|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.GetRoleResponse} returns this
*/
proto.fcp.accounting.v1.auth_private.GetRoleResponse.prototype.setRole = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.GetRoleResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.GetRoleResponse.prototype.clearRole = function() {
  return this.setRole(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.GetRoleResponse.prototype.hasRole = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.CreateRoleRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.CreateRoleRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.CreateRoleRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CreateRoleRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    role: (f = msg.getRole()) && v1_auth_common_dtos_pb.Role.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.CreateRoleRequest}
 */
proto.fcp.accounting.v1.auth_private.CreateRoleRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.CreateRoleRequest;
  return proto.fcp.accounting.v1.auth_private.CreateRoleRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.CreateRoleRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.CreateRoleRequest}
 */
proto.fcp.accounting.v1.auth_private.CreateRoleRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_auth_common_dtos_pb.Role;
      reader.readMessage(value,v1_auth_common_dtos_pb.Role.deserializeBinaryFromReader);
      msg.setRole(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.CreateRoleRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.CreateRoleRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.CreateRoleRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CreateRoleRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getRole();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_auth_common_dtos_pb.Role.serializeBinaryToWriter
    );
  }
};


/**
 * optional fcp.accounting.v1.Role role = 1;
 * @return {?proto.fcp.accounting.v1.Role}
 */
proto.fcp.accounting.v1.auth_private.CreateRoleRequest.prototype.getRole = function() {
  return /** @type{?proto.fcp.accounting.v1.Role} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_dtos_pb.Role, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.Role|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.CreateRoleRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.CreateRoleRequest.prototype.setRole = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.CreateRoleRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.CreateRoleRequest.prototype.clearRole = function() {
  return this.setRole(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.CreateRoleRequest.prototype.hasRole = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.CreateRoleResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.CreateRoleResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.CreateRoleResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CreateRoleResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    role: (f = msg.getRole()) && v1_auth_common_dtos_pb.RoleFull.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.CreateRoleResponse}
 */
proto.fcp.accounting.v1.auth_private.CreateRoleResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.CreateRoleResponse;
  return proto.fcp.accounting.v1.auth_private.CreateRoleResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.CreateRoleResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.CreateRoleResponse}
 */
proto.fcp.accounting.v1.auth_private.CreateRoleResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_auth_common_dtos_pb.RoleFull;
      reader.readMessage(value,v1_auth_common_dtos_pb.RoleFull.deserializeBinaryFromReader);
      msg.setRole(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.CreateRoleResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.CreateRoleResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.CreateRoleResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CreateRoleResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getRole();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_auth_common_dtos_pb.RoleFull.serializeBinaryToWriter
    );
  }
};


/**
 * optional fcp.accounting.v1.RoleFull role = 1;
 * @return {?proto.fcp.accounting.v1.RoleFull}
 */
proto.fcp.accounting.v1.auth_private.CreateRoleResponse.prototype.getRole = function() {
  return /** @type{?proto.fcp.accounting.v1.RoleFull} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_dtos_pb.RoleFull, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.RoleFull|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.CreateRoleResponse} returns this
*/
proto.fcp.accounting.v1.auth_private.CreateRoleResponse.prototype.setRole = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.CreateRoleResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.CreateRoleResponse.prototype.clearRole = function() {
  return this.setRole(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.CreateRoleResponse.prototype.hasRole = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.UpdateRoleRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.UpdateRoleRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    role: (f = msg.getRole()) && v1_auth_common_dtos_pb.Role.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateRoleRequest}
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.UpdateRoleRequest;
  return proto.fcp.accounting.v1.auth_private.UpdateRoleRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.UpdateRoleRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateRoleRequest}
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_auth_common_dtos_pb.Role;
      reader.readMessage(value,v1_auth_common_dtos_pb.Role.deserializeBinaryFromReader);
      msg.setRole(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.UpdateRoleRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.UpdateRoleRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getRole();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_auth_common_dtos_pb.Role.serializeBinaryToWriter
    );
  }
};


/**
 * optional fcp.accounting.v1.Role role = 1;
 * @return {?proto.fcp.accounting.v1.Role}
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleRequest.prototype.getRole = function() {
  return /** @type{?proto.fcp.accounting.v1.Role} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_dtos_pb.Role, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.Role|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateRoleRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.UpdateRoleRequest.prototype.setRole = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateRoleRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleRequest.prototype.clearRole = function() {
  return this.setRole(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleRequest.prototype.hasRole = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.UpdateRoleResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.UpdateRoleResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    role: (f = msg.getRole()) && v1_auth_common_dtos_pb.RoleFull.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateRoleResponse}
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.UpdateRoleResponse;
  return proto.fcp.accounting.v1.auth_private.UpdateRoleResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.UpdateRoleResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateRoleResponse}
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_auth_common_dtos_pb.RoleFull;
      reader.readMessage(value,v1_auth_common_dtos_pb.RoleFull.deserializeBinaryFromReader);
      msg.setRole(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.UpdateRoleResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.UpdateRoleResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getRole();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_auth_common_dtos_pb.RoleFull.serializeBinaryToWriter
    );
  }
};


/**
 * optional fcp.accounting.v1.RoleFull role = 1;
 * @return {?proto.fcp.accounting.v1.RoleFull}
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleResponse.prototype.getRole = function() {
  return /** @type{?proto.fcp.accounting.v1.RoleFull} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_dtos_pb.RoleFull, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.RoleFull|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateRoleResponse} returns this
*/
proto.fcp.accounting.v1.auth_private.UpdateRoleResponse.prototype.setRole = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.UpdateRoleResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleResponse.prototype.clearRole = function() {
  return this.setRole(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.UpdateRoleResponse.prototype.hasRole = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ListRolesRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ListRolesRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ListRolesRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListRolesRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.accounting.v1.auth_private.RoleFilter.toObject(includeInstance, f),
    sort: (f = msg.getSort()) && v1_auth_common_common_pb.Sort.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ListRolesRequest}
 */
proto.fcp.accounting.v1.auth_private.ListRolesRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ListRolesRequest;
  return proto.fcp.accounting.v1.auth_private.ListRolesRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ListRolesRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ListRolesRequest}
 */
proto.fcp.accounting.v1.auth_private.ListRolesRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 2:
      var value = new proto.fcp.accounting.v1.auth_private.RoleFilter;
      reader.readMessage(value,proto.fcp.accounting.v1.auth_private.RoleFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    case 3:
      var value = new v1_auth_common_common_pb.Sort;
      reader.readMessage(value,v1_auth_common_common_pb.Sort.deserializeBinaryFromReader);
      msg.setSort(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ListRolesRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ListRolesRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ListRolesRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListRolesRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.fcp.accounting.v1.auth_private.RoleFilter.serializeBinaryToWriter
    );
  }
  f = message.getSort();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      v1_auth_common_common_pb.Sort.serializeBinaryToWriter
    );
  }
};


/**
 * optional RoleFilter filter = 2;
 * @return {?proto.fcp.accounting.v1.auth_private.RoleFilter}
 */
proto.fcp.accounting.v1.auth_private.ListRolesRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.accounting.v1.auth_private.RoleFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.auth_private.RoleFilter, 2));
};


/**
 * @param {?proto.fcp.accounting.v1.auth_private.RoleFilter|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListRolesRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.ListRolesRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.ListRolesRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ListRolesRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.ListRolesRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional fcp.accounting.v1.Sort sort = 3;
 * @return {?proto.fcp.accounting.v1.Sort}
 */
proto.fcp.accounting.v1.auth_private.ListRolesRequest.prototype.getSort = function() {
  return /** @type{?proto.fcp.accounting.v1.Sort} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_common_pb.Sort, 3));
};


/**
 * @param {?proto.fcp.accounting.v1.Sort|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListRolesRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.ListRolesRequest.prototype.setSort = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.ListRolesRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ListRolesRequest.prototype.clearSort = function() {
  return this.setSort(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.ListRolesRequest.prototype.hasSort = function() {
  return jspb.Message.getField(this, 3) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.RoleFilter.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.RoleFilter.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.RoleFilter} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.RoleFilter.toObject = function(includeInstance, msg) {
  var f, obj = {
    role: (f = msg.getRole()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    permission: (f = msg.getPermission()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.RoleFilter}
 */
proto.fcp.accounting.v1.auth_private.RoleFilter.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.RoleFilter;
  return proto.fcp.accounting.v1.auth_private.RoleFilter.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.RoleFilter} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.RoleFilter}
 */
proto.fcp.accounting.v1.auth_private.RoleFilter.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setRole(value);
      break;
    case 2:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setPermission(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.RoleFilter.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.RoleFilter.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.RoleFilter} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.RoleFilter.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getRole();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getPermission();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
};


/**
 * optional google.protobuf.StringValue role = 1;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.auth_private.RoleFilter.prototype.getRole = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 1));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.RoleFilter} returns this
*/
proto.fcp.accounting.v1.auth_private.RoleFilter.prototype.setRole = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.RoleFilter} returns this
 */
proto.fcp.accounting.v1.auth_private.RoleFilter.prototype.clearRole = function() {
  return this.setRole(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.RoleFilter.prototype.hasRole = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional google.protobuf.StringValue permission = 2;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.auth_private.RoleFilter.prototype.getPermission = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 2));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.RoleFilter} returns this
*/
proto.fcp.accounting.v1.auth_private.RoleFilter.prototype.setPermission = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.RoleFilter} returns this
 */
proto.fcp.accounting.v1.auth_private.RoleFilter.prototype.clearPermission = function() {
  return this.setPermission(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.RoleFilter.prototype.hasPermission = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.accounting.v1.auth_private.ListRolesResponse.repeatedFields_ = [3];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ListRolesResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ListRolesResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ListRolesResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListRolesResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    rolesList: jspb.Message.toObjectList(msg.getRolesList(),
    v1_auth_common_dtos_pb.RoleFull.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ListRolesResponse}
 */
proto.fcp.accounting.v1.auth_private.ListRolesResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ListRolesResponse;
  return proto.fcp.accounting.v1.auth_private.ListRolesResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ListRolesResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ListRolesResponse}
 */
proto.fcp.accounting.v1.auth_private.ListRolesResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 3:
      var value = new v1_auth_common_dtos_pb.RoleFull;
      reader.readMessage(value,v1_auth_common_dtos_pb.RoleFull.deserializeBinaryFromReader);
      msg.addRoles(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ListRolesResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ListRolesResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ListRolesResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListRolesResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getRolesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      3,
      f,
      v1_auth_common_dtos_pb.RoleFull.serializeBinaryToWriter
    );
  }
};


/**
 * repeated fcp.accounting.v1.RoleFull roles = 3;
 * @return {!Array<!proto.fcp.accounting.v1.RoleFull>}
 */
proto.fcp.accounting.v1.auth_private.ListRolesResponse.prototype.getRolesList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.RoleFull>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_auth_common_dtos_pb.RoleFull, 3));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.RoleFull>} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListRolesResponse} returns this
*/
proto.fcp.accounting.v1.auth_private.ListRolesResponse.prototype.setRolesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 3, value);
};


/**
 * @param {!proto.fcp.accounting.v1.RoleFull=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.RoleFull}
 */
proto.fcp.accounting.v1.auth_private.ListRolesResponse.prototype.addRoles = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 3, opt_value, proto.fcp.accounting.v1.RoleFull, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.auth_private.ListRolesResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.ListRolesResponse.prototype.clearRolesList = function() {
  return this.setRolesList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.GetPermissionRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.GetPermissionRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.GetPermissionRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetPermissionRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    permission: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.GetPermissionRequest}
 */
proto.fcp.accounting.v1.auth_private.GetPermissionRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.GetPermissionRequest;
  return proto.fcp.accounting.v1.auth_private.GetPermissionRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.GetPermissionRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.GetPermissionRequest}
 */
proto.fcp.accounting.v1.auth_private.GetPermissionRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setPermission(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.GetPermissionRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.GetPermissionRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.GetPermissionRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetPermissionRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPermission();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string permission = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.GetPermissionRequest.prototype.getPermission = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.GetPermissionRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.GetPermissionRequest.prototype.setPermission = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.GetPermissionResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.GetPermissionResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.GetPermissionResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetPermissionResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    permission: (f = msg.getPermission()) && v1_auth_common_dtos_pb.PermissionFull.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.GetPermissionResponse}
 */
proto.fcp.accounting.v1.auth_private.GetPermissionResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.GetPermissionResponse;
  return proto.fcp.accounting.v1.auth_private.GetPermissionResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.GetPermissionResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.GetPermissionResponse}
 */
proto.fcp.accounting.v1.auth_private.GetPermissionResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_auth_common_dtos_pb.PermissionFull;
      reader.readMessage(value,v1_auth_common_dtos_pb.PermissionFull.deserializeBinaryFromReader);
      msg.setPermission(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.GetPermissionResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.GetPermissionResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.GetPermissionResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.GetPermissionResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPermission();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_auth_common_dtos_pb.PermissionFull.serializeBinaryToWriter
    );
  }
};


/**
 * optional fcp.accounting.v1.PermissionFull permission = 1;
 * @return {?proto.fcp.accounting.v1.PermissionFull}
 */
proto.fcp.accounting.v1.auth_private.GetPermissionResponse.prototype.getPermission = function() {
  return /** @type{?proto.fcp.accounting.v1.PermissionFull} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_dtos_pb.PermissionFull, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.PermissionFull|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.GetPermissionResponse} returns this
*/
proto.fcp.accounting.v1.auth_private.GetPermissionResponse.prototype.setPermission = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.GetPermissionResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.GetPermissionResponse.prototype.clearPermission = function() {
  return this.setPermission(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.GetPermissionResponse.prototype.hasPermission = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ListPermissionsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    pagination: (f = msg.getPagination()) && v1_auth_common_common_pb.PaginationRequest.toObject(includeInstance, f),
    filter: (f = msg.getFilter()) && proto.fcp.accounting.v1.auth_private.PermissionFilter.toObject(includeInstance, f),
    sort: (f = msg.getSort()) && v1_auth_common_common_pb.Sort.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ListPermissionsRequest}
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ListPermissionsRequest;
  return proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ListPermissionsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ListPermissionsRequest}
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_auth_common_common_pb.PaginationRequest;
      reader.readMessage(value,v1_auth_common_common_pb.PaginationRequest.deserializeBinaryFromReader);
      msg.setPagination(value);
      break;
    case 2:
      var value = new proto.fcp.accounting.v1.auth_private.PermissionFilter;
      reader.readMessage(value,proto.fcp.accounting.v1.auth_private.PermissionFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    case 3:
      var value = new v1_auth_common_common_pb.Sort;
      reader.readMessage(value,v1_auth_common_common_pb.Sort.deserializeBinaryFromReader);
      msg.setSort(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ListPermissionsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPagination();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_auth_common_common_pb.PaginationRequest.serializeBinaryToWriter
    );
  }
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.fcp.accounting.v1.auth_private.PermissionFilter.serializeBinaryToWriter
    );
  }
  f = message.getSort();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      v1_auth_common_common_pb.Sort.serializeBinaryToWriter
    );
  }
};


/**
 * optional fcp.accounting.v1.PaginationRequest pagination = 1;
 * @return {?proto.fcp.accounting.v1.PaginationRequest}
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.prototype.getPagination = function() {
  return /** @type{?proto.fcp.accounting.v1.PaginationRequest} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_common_pb.PaginationRequest, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.PaginationRequest|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListPermissionsRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.prototype.setPagination = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.ListPermissionsRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.prototype.clearPagination = function() {
  return this.setPagination(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.prototype.hasPagination = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional PermissionFilter filter = 2;
 * @return {?proto.fcp.accounting.v1.auth_private.PermissionFilter}
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.accounting.v1.auth_private.PermissionFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.auth_private.PermissionFilter, 2));
};


/**
 * @param {?proto.fcp.accounting.v1.auth_private.PermissionFilter|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListPermissionsRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.ListPermissionsRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional fcp.accounting.v1.Sort sort = 3;
 * @return {?proto.fcp.accounting.v1.Sort}
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.prototype.getSort = function() {
  return /** @type{?proto.fcp.accounting.v1.Sort} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_common_pb.Sort, 3));
};


/**
 * @param {?proto.fcp.accounting.v1.Sort|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListPermissionsRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.prototype.setSort = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.ListPermissionsRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.prototype.clearSort = function() {
  return this.setSort(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsRequest.prototype.hasSort = function() {
  return jspb.Message.getField(this, 3) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.PermissionFilter.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.PermissionFilter.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.PermissionFilter} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.PermissionFilter.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: (f = msg.getName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.PermissionFilter}
 */
proto.fcp.accounting.v1.auth_private.PermissionFilter.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.PermissionFilter;
  return proto.fcp.accounting.v1.auth_private.PermissionFilter.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.PermissionFilter} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.PermissionFilter}
 */
proto.fcp.accounting.v1.auth_private.PermissionFilter.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.PermissionFilter.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.PermissionFilter.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.PermissionFilter} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.PermissionFilter.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
};


/**
 * optional google.protobuf.StringValue name = 1;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.auth_private.PermissionFilter.prototype.getName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 1));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.PermissionFilter} returns this
*/
proto.fcp.accounting.v1.auth_private.PermissionFilter.prototype.setName = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.PermissionFilter} returns this
 */
proto.fcp.accounting.v1.auth_private.PermissionFilter.prototype.clearName = function() {
  return this.setName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.PermissionFilter.prototype.hasName = function() {
  return jspb.Message.getField(this, 1) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.repeatedFields_ = [2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ListPermissionsResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    pagination: (f = msg.getPagination()) && v1_auth_common_common_pb.PaginationResponse.toObject(includeInstance, f),
    permissionsList: jspb.Message.toObjectList(msg.getPermissionsList(),
    v1_auth_common_dtos_pb.PermissionFull.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ListPermissionsResponse}
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ListPermissionsResponse;
  return proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ListPermissionsResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ListPermissionsResponse}
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_auth_common_common_pb.PaginationResponse;
      reader.readMessage(value,v1_auth_common_common_pb.PaginationResponse.deserializeBinaryFromReader);
      msg.setPagination(value);
      break;
    case 2:
      var value = new v1_auth_common_dtos_pb.PermissionFull;
      reader.readMessage(value,v1_auth_common_dtos_pb.PermissionFull.deserializeBinaryFromReader);
      msg.addPermissions(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ListPermissionsResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPagination();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_auth_common_common_pb.PaginationResponse.serializeBinaryToWriter
    );
  }
  f = message.getPermissionsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      v1_auth_common_dtos_pb.PermissionFull.serializeBinaryToWriter
    );
  }
};


/**
 * optional fcp.accounting.v1.PaginationResponse pagination = 1;
 * @return {?proto.fcp.accounting.v1.PaginationResponse}
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.prototype.getPagination = function() {
  return /** @type{?proto.fcp.accounting.v1.PaginationResponse} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_common_pb.PaginationResponse, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.PaginationResponse|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListPermissionsResponse} returns this
*/
proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.prototype.setPagination = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.ListPermissionsResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.prototype.clearPagination = function() {
  return this.setPagination(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.prototype.hasPagination = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * repeated fcp.accounting.v1.PermissionFull permissions = 2;
 * @return {!Array<!proto.fcp.accounting.v1.PermissionFull>}
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.prototype.getPermissionsList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.PermissionFull>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_auth_common_dtos_pb.PermissionFull, 2));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.PermissionFull>} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListPermissionsResponse} returns this
*/
proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.prototype.setPermissionsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.fcp.accounting.v1.PermissionFull=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.PermissionFull}
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.prototype.addPermissions = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.fcp.accounting.v1.PermissionFull, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.auth_private.ListPermissionsResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.ListPermissionsResponse.prototype.clearPermissionsList = function() {
  return this.setPermissionsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    permission: jspb.Message.getFieldWithDefault(msg, 1, ""),
    description: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest}
 */
proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest;
  return proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest}
 */
proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setPermission(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setDescription(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPermission();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getDescription();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string permission = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest.prototype.getPermission = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest.prototype.setPermission = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string description = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest.prototype.getDescription = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.UpdatePermissionRequest.prototype.setDescription = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse}
 */
proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse;
  return proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse}
 */
proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.UpdatePermissionResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.CreatePermissionRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.CreatePermissionRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    permission: (f = msg.getPermission()) && v1_auth_common_dtos_pb.Permission.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.CreatePermissionRequest}
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.CreatePermissionRequest;
  return proto.fcp.accounting.v1.auth_private.CreatePermissionRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.CreatePermissionRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.CreatePermissionRequest}
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_auth_common_dtos_pb.Permission;
      reader.readMessage(value,v1_auth_common_dtos_pb.Permission.deserializeBinaryFromReader);
      msg.setPermission(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.CreatePermissionRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.CreatePermissionRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPermission();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_auth_common_dtos_pb.Permission.serializeBinaryToWriter
    );
  }
};


/**
 * optional fcp.accounting.v1.Permission permission = 1;
 * @return {?proto.fcp.accounting.v1.Permission}
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionRequest.prototype.getPermission = function() {
  return /** @type{?proto.fcp.accounting.v1.Permission} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_dtos_pb.Permission, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.Permission|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.CreatePermissionRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.CreatePermissionRequest.prototype.setPermission = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.CreatePermissionRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionRequest.prototype.clearPermission = function() {
  return this.setPermission(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionRequest.prototype.hasPermission = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.CreatePermissionResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.CreatePermissionResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    permission: (f = msg.getPermission()) && v1_auth_common_dtos_pb.PermissionFull.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.CreatePermissionResponse}
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.CreatePermissionResponse;
  return proto.fcp.accounting.v1.auth_private.CreatePermissionResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.CreatePermissionResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.CreatePermissionResponse}
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_auth_common_dtos_pb.PermissionFull;
      reader.readMessage(value,v1_auth_common_dtos_pb.PermissionFull.deserializeBinaryFromReader);
      msg.setPermission(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.CreatePermissionResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.CreatePermissionResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPermission();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_auth_common_dtos_pb.PermissionFull.serializeBinaryToWriter
    );
  }
};


/**
 * optional fcp.accounting.v1.PermissionFull permission = 1;
 * @return {?proto.fcp.accounting.v1.PermissionFull}
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionResponse.prototype.getPermission = function() {
  return /** @type{?proto.fcp.accounting.v1.PermissionFull} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_dtos_pb.PermissionFull, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.PermissionFull|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.CreatePermissionResponse} returns this
*/
proto.fcp.accounting.v1.auth_private.CreatePermissionResponse.prototype.setPermission = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.CreatePermissionResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionResponse.prototype.clearPermission = function() {
  return this.setPermission(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.CreatePermissionResponse.prototype.hasPermission = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.DeletePermissionRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.DeletePermissionRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.DeletePermissionRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.DeletePermissionRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    permission: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.DeletePermissionRequest}
 */
proto.fcp.accounting.v1.auth_private.DeletePermissionRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.DeletePermissionRequest;
  return proto.fcp.accounting.v1.auth_private.DeletePermissionRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.DeletePermissionRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.DeletePermissionRequest}
 */
proto.fcp.accounting.v1.auth_private.DeletePermissionRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setPermission(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.DeletePermissionRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.DeletePermissionRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.DeletePermissionRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.DeletePermissionRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPermission();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string permission = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.DeletePermissionRequest.prototype.getPermission = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.DeletePermissionRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.DeletePermissionRequest.prototype.setPermission = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.DeletePermissionResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.DeletePermissionResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.DeletePermissionResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.DeletePermissionResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.DeletePermissionResponse}
 */
proto.fcp.accounting.v1.auth_private.DeletePermissionResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.DeletePermissionResponse;
  return proto.fcp.accounting.v1.auth_private.DeletePermissionResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.DeletePermissionResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.DeletePermissionResponse}
 */
proto.fcp.accounting.v1.auth_private.DeletePermissionResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.DeletePermissionResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.DeletePermissionResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.DeletePermissionResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.DeletePermissionResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.LoginRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.LoginRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.LoginRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.LoginRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    login: jspb.Message.getFieldWithDefault(msg, 1, ""),
    password: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.LoginRequest}
 */
proto.fcp.accounting.v1.auth_private.LoginRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.LoginRequest;
  return proto.fcp.accounting.v1.auth_private.LoginRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.LoginRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.LoginRequest}
 */
proto.fcp.accounting.v1.auth_private.LoginRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setLogin(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPassword(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.LoginRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.LoginRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.LoginRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.LoginRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getLogin();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getPassword();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string login = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.LoginRequest.prototype.getLogin = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.LoginRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.LoginRequest.prototype.setLogin = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string password = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.LoginRequest.prototype.getPassword = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.LoginRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.LoginRequest.prototype.setPassword = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.LoginResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.LoginResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.LoginResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.LoginResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    sessionToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    role: (f = msg.getRole()) && v1_auth_common_dtos_pb.Role.toObject(includeInstance, f),
    needPasswordChange: jspb.Message.getBooleanFieldWithDefault(msg, 3, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.LoginResponse}
 */
proto.fcp.accounting.v1.auth_private.LoginResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.LoginResponse;
  return proto.fcp.accounting.v1.auth_private.LoginResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.LoginResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.LoginResponse}
 */
proto.fcp.accounting.v1.auth_private.LoginResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setSessionToken(value);
      break;
    case 4:
      var value = new v1_auth_common_dtos_pb.Role;
      reader.readMessage(value,v1_auth_common_dtos_pb.Role.deserializeBinaryFromReader);
      msg.setRole(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setNeedPasswordChange(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.LoginResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.LoginResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.LoginResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.LoginResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSessionToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getRole();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      v1_auth_common_dtos_pb.Role.serializeBinaryToWriter
    );
  }
  f = message.getNeedPasswordChange();
  if (f) {
    writer.writeBool(
      3,
      f
    );
  }
};


/**
 * optional string session_token = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.LoginResponse.prototype.getSessionToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.LoginResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.LoginResponse.prototype.setSessionToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional fcp.accounting.v1.Role role = 4;
 * @return {?proto.fcp.accounting.v1.Role}
 */
proto.fcp.accounting.v1.auth_private.LoginResponse.prototype.getRole = function() {
  return /** @type{?proto.fcp.accounting.v1.Role} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_dtos_pb.Role, 4));
};


/**
 * @param {?proto.fcp.accounting.v1.Role|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.LoginResponse} returns this
*/
proto.fcp.accounting.v1.auth_private.LoginResponse.prototype.setRole = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.LoginResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.LoginResponse.prototype.clearRole = function() {
  return this.setRole(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.LoginResponse.prototype.hasRole = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional bool need_password_change = 3;
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.LoginResponse.prototype.getNeedPasswordChange = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.accounting.v1.auth_private.LoginResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.LoginResponse.prototype.setNeedPasswordChange = function(value) {
  return jspb.Message.setProto3BooleanField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.LogoutRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.LogoutRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.LogoutRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.LogoutRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    sessionToken: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.LogoutRequest}
 */
proto.fcp.accounting.v1.auth_private.LogoutRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.LogoutRequest;
  return proto.fcp.accounting.v1.auth_private.LogoutRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.LogoutRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.LogoutRequest}
 */
proto.fcp.accounting.v1.auth_private.LogoutRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setSessionToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.LogoutRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.LogoutRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.LogoutRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.LogoutRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSessionToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string session_token = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.LogoutRequest.prototype.getSessionToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.LogoutRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.LogoutRequest.prototype.setSessionToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ValidateSessionRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ValidateSessionRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    sessionToken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    serviceName: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateSessionRequest}
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ValidateSessionRequest;
  return proto.fcp.accounting.v1.auth_private.ValidateSessionRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ValidateSessionRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateSessionRequest}
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setSessionToken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setServiceName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ValidateSessionRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ValidateSessionRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSessionToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getServiceName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string session_token = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionRequest.prototype.getSessionToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateSessionRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionRequest.prototype.setSessionToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string service_name = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionRequest.prototype.getServiceName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateSessionRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionRequest.prototype.setServiceName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ValidateSessionResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    userId: jspb.Message.getFieldWithDefault(msg, 1, ""),
    sessionId: jspb.Message.getFieldWithDefault(msg, 2, ""),
    status: jspb.Message.getFieldWithDefault(msg, 3, 0),
    role: (f = msg.getRole()) && v1_auth_common_dtos_pb.Role.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateSessionResponse}
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ValidateSessionResponse;
  return proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ValidateSessionResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateSessionResponse}
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setSessionId(value);
      break;
    case 3:
      var value = /** @type {!proto.fcp.accounting.v1.Status} */ (reader.readEnum());
      msg.setStatus(value);
      break;
    case 4:
      var value = new v1_auth_common_dtos_pb.Role;
      reader.readMessage(value,v1_auth_common_dtos_pb.Role.deserializeBinaryFromReader);
      msg.setRole(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ValidateSessionResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getSessionId();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getStatus();
  if (f !== 0.0) {
    writer.writeEnum(
      3,
      f
    );
  }
  f = message.getRole();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      v1_auth_common_dtos_pb.Role.serializeBinaryToWriter
    );
  }
};


/**
 * optional string user_id = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateSessionResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string session_id = 2;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.prototype.getSessionId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateSessionResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.prototype.setSessionId = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional fcp.accounting.v1.Status status = 3;
 * @return {!proto.fcp.accounting.v1.Status}
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.prototype.getStatus = function() {
  return /** @type {!proto.fcp.accounting.v1.Status} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {!proto.fcp.accounting.v1.Status} value
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateSessionResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.prototype.setStatus = function(value) {
  return jspb.Message.setProto3EnumField(this, 3, value);
};


/**
 * optional fcp.accounting.v1.Role role = 4;
 * @return {?proto.fcp.accounting.v1.Role}
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.prototype.getRole = function() {
  return /** @type{?proto.fcp.accounting.v1.Role} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_dtos_pb.Role, 4));
};


/**
 * @param {?proto.fcp.accounting.v1.Role|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateSessionResponse} returns this
*/
proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.prototype.setRole = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateSessionResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.prototype.clearRole = function() {
  return this.setRole(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.ValidateSessionResponse.prototype.hasRole = function() {
  return jspb.Message.getField(this, 4) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    sessionToken: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest}
 */
proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest;
  return proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest}
 */
proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setSessionToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSessionToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string session_token = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest.prototype.getSessionToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ValidateUserSessionRequest.prototype.setSessionToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    userId: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse}
 */
proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse;
  return proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse}
 */
proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string user_id = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.ValidateUserSessionResponse.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    sessionToken: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest}
 */
proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest;
  return proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest}
 */
proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setSessionToken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSessionToken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string session_token = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest.prototype.getSessionToken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ValidateWsSessionRequest.prototype.setSessionToken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    userId: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse}
 */
proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse;
  return proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse}
 */
proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUserId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUserId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string user_id = 1;
 * @return {string}
 */
proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse.prototype.getUserId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.ValidateWsSessionResponse.prototype.setUserId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ResetSessionRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ResetSessionRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ResetSessionRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ResetSessionRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    sessionId: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ResetSessionRequest}
 */
proto.fcp.accounting.v1.auth_private.ResetSessionRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ResetSessionRequest;
  return proto.fcp.accounting.v1.auth_private.ResetSessionRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ResetSessionRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ResetSessionRequest}
 */
proto.fcp.accounting.v1.auth_private.ResetSessionRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setSessionId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ResetSessionRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ResetSessionRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ResetSessionRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ResetSessionRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSessionId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 session_id = 1;
 * @return {number}
 */
proto.fcp.accounting.v1.auth_private.ResetSessionRequest.prototype.getSessionId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.accounting.v1.auth_private.ResetSessionRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ResetSessionRequest.prototype.setSessionId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ListSessionsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ListSessionsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ListSessionsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListSessionsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    pagination: (f = msg.getPagination()) && v1_auth_common_common_pb.PaginationRequest.toObject(includeInstance, f),
    filter: (f = msg.getFilter()) && proto.fcp.accounting.v1.auth_private.SessionFilter.toObject(includeInstance, f),
    sort: (f = msg.getSort()) && v1_auth_common_common_pb.Sort.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ListSessionsRequest}
 */
proto.fcp.accounting.v1.auth_private.ListSessionsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ListSessionsRequest;
  return proto.fcp.accounting.v1.auth_private.ListSessionsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ListSessionsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ListSessionsRequest}
 */
proto.fcp.accounting.v1.auth_private.ListSessionsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_auth_common_common_pb.PaginationRequest;
      reader.readMessage(value,v1_auth_common_common_pb.PaginationRequest.deserializeBinaryFromReader);
      msg.setPagination(value);
      break;
    case 2:
      var value = new proto.fcp.accounting.v1.auth_private.SessionFilter;
      reader.readMessage(value,proto.fcp.accounting.v1.auth_private.SessionFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    case 3:
      var value = new v1_auth_common_common_pb.Sort;
      reader.readMessage(value,v1_auth_common_common_pb.Sort.deserializeBinaryFromReader);
      msg.setSort(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ListSessionsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ListSessionsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ListSessionsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListSessionsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPagination();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_auth_common_common_pb.PaginationRequest.serializeBinaryToWriter
    );
  }
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.fcp.accounting.v1.auth_private.SessionFilter.serializeBinaryToWriter
    );
  }
  f = message.getSort();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      v1_auth_common_common_pb.Sort.serializeBinaryToWriter
    );
  }
};


/**
 * optional fcp.accounting.v1.PaginationRequest pagination = 1;
 * @return {?proto.fcp.accounting.v1.PaginationRequest}
 */
proto.fcp.accounting.v1.auth_private.ListSessionsRequest.prototype.getPagination = function() {
  return /** @type{?proto.fcp.accounting.v1.PaginationRequest} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_common_pb.PaginationRequest, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.PaginationRequest|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListSessionsRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.ListSessionsRequest.prototype.setPagination = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.ListSessionsRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ListSessionsRequest.prototype.clearPagination = function() {
  return this.setPagination(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.ListSessionsRequest.prototype.hasPagination = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional SessionFilter filter = 2;
 * @return {?proto.fcp.accounting.v1.auth_private.SessionFilter}
 */
proto.fcp.accounting.v1.auth_private.ListSessionsRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.accounting.v1.auth_private.SessionFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.accounting.v1.auth_private.SessionFilter, 2));
};


/**
 * @param {?proto.fcp.accounting.v1.auth_private.SessionFilter|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListSessionsRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.ListSessionsRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.ListSessionsRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ListSessionsRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.ListSessionsRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional fcp.accounting.v1.Sort sort = 3;
 * @return {?proto.fcp.accounting.v1.Sort}
 */
proto.fcp.accounting.v1.auth_private.ListSessionsRequest.prototype.getSort = function() {
  return /** @type{?proto.fcp.accounting.v1.Sort} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_common_pb.Sort, 3));
};


/**
 * @param {?proto.fcp.accounting.v1.Sort|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListSessionsRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.ListSessionsRequest.prototype.setSort = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.ListSessionsRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ListSessionsRequest.prototype.clearSort = function() {
  return this.setSort(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.ListSessionsRequest.prototype.hasSort = function() {
  return jspb.Message.getField(this, 3) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.SessionFilter.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.SessionFilter} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.toObject = function(includeInstance, msg) {
  var f, obj = {
    sessionId: (f = msg.getSessionId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    token: (f = msg.getToken()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    userId: (f = msg.getUserId()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    serviceName: (f = msg.getServiceName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    createdTime: (f = msg.getCreatedTime()) && v1_auth_common_common_pb.TimeRange.toObject(includeInstance, f),
    expiredTime: (f = msg.getExpiredTime()) && v1_auth_common_common_pb.TimeRange.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.SessionFilter}
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.SessionFilter;
  return proto.fcp.accounting.v1.auth_private.SessionFilter.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.SessionFilter} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.SessionFilter}
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setSessionId(value);
      break;
    case 2:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setToken(value);
      break;
    case 3:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setUserId(value);
      break;
    case 4:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setServiceName(value);
      break;
    case 5:
      var value = new v1_auth_common_common_pb.TimeRange;
      reader.readMessage(value,v1_auth_common_common_pb.TimeRange.deserializeBinaryFromReader);
      msg.setCreatedTime(value);
      break;
    case 6:
      var value = new v1_auth_common_common_pb.TimeRange;
      reader.readMessage(value,v1_auth_common_common_pb.TimeRange.deserializeBinaryFromReader);
      msg.setExpiredTime(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.SessionFilter.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.SessionFilter} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSessionId();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getToken();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getUserId();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getServiceName();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getCreatedTime();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      v1_auth_common_common_pb.TimeRange.serializeBinaryToWriter
    );
  }
  f = message.getExpiredTime();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      v1_auth_common_common_pb.TimeRange.serializeBinaryToWriter
    );
  }
};


/**
 * optional google.protobuf.Int64Value session_id = 1;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.getSessionId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 1));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.SessionFilter} returns this
*/
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.setSessionId = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.SessionFilter} returns this
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.clearSessionId = function() {
  return this.setSessionId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.hasSessionId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional google.protobuf.StringValue token = 2;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.getToken = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 2));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.SessionFilter} returns this
*/
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.setToken = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.SessionFilter} returns this
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.clearToken = function() {
  return this.setToken(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.hasToken = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional google.protobuf.StringValue user_id = 3;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.getUserId = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 3));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.SessionFilter} returns this
*/
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.setUserId = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.SessionFilter} returns this
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.clearUserId = function() {
  return this.setUserId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.hasUserId = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional google.protobuf.StringValue service_name = 4;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.getServiceName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 4));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.SessionFilter} returns this
*/
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.setServiceName = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.SessionFilter} returns this
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.clearServiceName = function() {
  return this.setServiceName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.hasServiceName = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional fcp.accounting.v1.TimeRange created_time = 5;
 * @return {?proto.fcp.accounting.v1.TimeRange}
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.getCreatedTime = function() {
  return /** @type{?proto.fcp.accounting.v1.TimeRange} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_common_pb.TimeRange, 5));
};


/**
 * @param {?proto.fcp.accounting.v1.TimeRange|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.SessionFilter} returns this
*/
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.setCreatedTime = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.SessionFilter} returns this
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.clearCreatedTime = function() {
  return this.setCreatedTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.hasCreatedTime = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional fcp.accounting.v1.TimeRange expired_time = 6;
 * @return {?proto.fcp.accounting.v1.TimeRange}
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.getExpiredTime = function() {
  return /** @type{?proto.fcp.accounting.v1.TimeRange} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_common_pb.TimeRange, 6));
};


/**
 * @param {?proto.fcp.accounting.v1.TimeRange|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.SessionFilter} returns this
*/
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.setExpiredTime = function(value) {
  return jspb.Message.setWrapperField(this, 6, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.SessionFilter} returns this
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.clearExpiredTime = function() {
  return this.setExpiredTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.SessionFilter.prototype.hasExpiredTime = function() {
  return jspb.Message.getField(this, 6) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.accounting.v1.auth_private.ListSessionsResponse.repeatedFields_ = [2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ListSessionsResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ListSessionsResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ListSessionsResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListSessionsResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    pagination: (f = msg.getPagination()) && v1_auth_common_common_pb.PaginationResponse.toObject(includeInstance, f),
    sessionsList: jspb.Message.toObjectList(msg.getSessionsList(),
    v1_auth_common_dtos_pb.Session.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ListSessionsResponse}
 */
proto.fcp.accounting.v1.auth_private.ListSessionsResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ListSessionsResponse;
  return proto.fcp.accounting.v1.auth_private.ListSessionsResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ListSessionsResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ListSessionsResponse}
 */
proto.fcp.accounting.v1.auth_private.ListSessionsResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_auth_common_common_pb.PaginationResponse;
      reader.readMessage(value,v1_auth_common_common_pb.PaginationResponse.deserializeBinaryFromReader);
      msg.setPagination(value);
      break;
    case 2:
      var value = new v1_auth_common_dtos_pb.Session;
      reader.readMessage(value,v1_auth_common_dtos_pb.Session.deserializeBinaryFromReader);
      msg.addSessions(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ListSessionsResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ListSessionsResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ListSessionsResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListSessionsResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPagination();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_auth_common_common_pb.PaginationResponse.serializeBinaryToWriter
    );
  }
  f = message.getSessionsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      v1_auth_common_dtos_pb.Session.serializeBinaryToWriter
    );
  }
};


/**
 * optional fcp.accounting.v1.PaginationResponse pagination = 1;
 * @return {?proto.fcp.accounting.v1.PaginationResponse}
 */
proto.fcp.accounting.v1.auth_private.ListSessionsResponse.prototype.getPagination = function() {
  return /** @type{?proto.fcp.accounting.v1.PaginationResponse} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_common_pb.PaginationResponse, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.PaginationResponse|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListSessionsResponse} returns this
*/
proto.fcp.accounting.v1.auth_private.ListSessionsResponse.prototype.setPagination = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.ListSessionsResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.ListSessionsResponse.prototype.clearPagination = function() {
  return this.setPagination(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.ListSessionsResponse.prototype.hasPagination = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * repeated fcp.accounting.v1.Session sessions = 2;
 * @return {!Array<!proto.fcp.accounting.v1.Session>}
 */
proto.fcp.accounting.v1.auth_private.ListSessionsResponse.prototype.getSessionsList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.Session>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_auth_common_dtos_pb.Session, 2));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.Session>} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListSessionsResponse} returns this
*/
proto.fcp.accounting.v1.auth_private.ListSessionsResponse.prototype.setSessionsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.fcp.accounting.v1.Session=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.Session}
 */
proto.fcp.accounting.v1.auth_private.ListSessionsResponse.prototype.addSessions = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.fcp.accounting.v1.Session, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.auth_private.ListSessionsResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.ListSessionsResponse.prototype.clearSessionsList = function() {
  return this.setSessionsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    pagination: (f = msg.getPagination()) && v1_auth_common_common_pb.PaginationRequest.toObject(includeInstance, f),
    sort: (f = msg.getSort()) && v1_auth_common_common_pb.Sort.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest}
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest;
  return proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest}
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_auth_common_common_pb.PaginationRequest;
      reader.readMessage(value,v1_auth_common_common_pb.PaginationRequest.deserializeBinaryFromReader);
      msg.setPagination(value);
      break;
    case 2:
      var value = new v1_auth_common_common_pb.Sort;
      reader.readMessage(value,v1_auth_common_common_pb.Sort.deserializeBinaryFromReader);
      msg.setSort(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPagination();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_auth_common_common_pb.PaginationRequest.serializeBinaryToWriter
    );
  }
  f = message.getSort();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      v1_auth_common_common_pb.Sort.serializeBinaryToWriter
    );
  }
};


/**
 * optional fcp.accounting.v1.PaginationRequest pagination = 1;
 * @return {?proto.fcp.accounting.v1.PaginationRequest}
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest.prototype.getPagination = function() {
  return /** @type{?proto.fcp.accounting.v1.PaginationRequest} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_common_pb.PaginationRequest, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.PaginationRequest|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest.prototype.setPagination = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest.prototype.clearPagination = function() {
  return this.setPagination(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest.prototype.hasPagination = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional fcp.accounting.v1.Sort sort = 2;
 * @return {?proto.fcp.accounting.v1.Sort}
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest.prototype.getSort = function() {
  return /** @type{?proto.fcp.accounting.v1.Sort} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_common_pb.Sort, 2));
};


/**
 * @param {?proto.fcp.accounting.v1.Sort|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest} returns this
*/
proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest.prototype.setSort = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest} returns this
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest.prototype.clearSort = function() {
  return this.setSort(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersRequest.prototype.hasSort = function() {
  return jspb.Message.getField(this, 2) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.repeatedFields_ = [2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    pagination: (f = msg.getPagination()) && v1_auth_common_common_pb.PaginationResponse.toObject(includeInstance, f),
    referralUsersList: jspb.Message.toObjectList(msg.getReferralUsersList(),
    v1_auth_common_dtos_pb.UserReferralInfo.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse}
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse;
  return proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse}
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new v1_auth_common_common_pb.PaginationResponse;
      reader.readMessage(value,v1_auth_common_common_pb.PaginationResponse.deserializeBinaryFromReader);
      msg.setPagination(value);
      break;
    case 2:
      var value = new v1_auth_common_dtos_pb.UserReferralInfo;
      reader.readMessage(value,v1_auth_common_dtos_pb.UserReferralInfo.deserializeBinaryFromReader);
      msg.addReferralUsers(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPagination();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      v1_auth_common_common_pb.PaginationResponse.serializeBinaryToWriter
    );
  }
  f = message.getReferralUsersList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      v1_auth_common_dtos_pb.UserReferralInfo.serializeBinaryToWriter
    );
  }
};


/**
 * optional fcp.accounting.v1.PaginationResponse pagination = 1;
 * @return {?proto.fcp.accounting.v1.PaginationResponse}
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.prototype.getPagination = function() {
  return /** @type{?proto.fcp.accounting.v1.PaginationResponse} */ (
    jspb.Message.getWrapperField(this, v1_auth_common_common_pb.PaginationResponse, 1));
};


/**
 * @param {?proto.fcp.accounting.v1.PaginationResponse|undefined} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse} returns this
*/
proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.prototype.setPagination = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.prototype.clearPagination = function() {
  return this.setPagination(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.prototype.hasPagination = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * repeated fcp.accounting.v1.UserReferralInfo referral_users = 2;
 * @return {!Array<!proto.fcp.accounting.v1.UserReferralInfo>}
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.prototype.getReferralUsersList = function() {
  return /** @type{!Array<!proto.fcp.accounting.v1.UserReferralInfo>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_auth_common_dtos_pb.UserReferralInfo, 2));
};


/**
 * @param {!Array<!proto.fcp.accounting.v1.UserReferralInfo>} value
 * @return {!proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse} returns this
*/
proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.prototype.setReferralUsersList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.fcp.accounting.v1.UserReferralInfo=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.accounting.v1.UserReferralInfo}
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.prototype.addReferralUsers = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.fcp.accounting.v1.UserReferralInfo, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse} returns this
 */
proto.fcp.accounting.v1.auth_private.ListReferralUsersResponse.prototype.clearReferralUsersList = function() {
  return this.setReferralUsersList([]);
};


goog.object.extend(exports, proto.fcp.accounting.v1.auth_private);
