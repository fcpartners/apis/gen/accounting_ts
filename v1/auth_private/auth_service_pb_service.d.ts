// package: fcp.accounting.v1.auth_private
// file: v1/auth_private/auth_service.proto

import * as v1_auth_private_auth_service_pb from "../../v1/auth_private/auth_service_pb";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import * as v1_auth_common_dtos_pb from "../../v1/auth_common/dtos_pb";
import {grpc} from "@improbable-eng/grpc-web";

type UserManagementServiceSendCode = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.SendCodeRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type UserManagementServiceCheckCode = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.CheckCodeRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.CheckCodeResponse;
};

type UserManagementServiceCheckUserByLogin = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.CheckUserByLoginRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.CheckUserByLoginResponse;
};

type UserManagementServiceGetUser = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.GetUserRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.User;
};

type UserManagementServiceListUsers = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.ListUsersRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.ListUsersResponse;
};

type UserManagementServiceCreateUser = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.CreateUserRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.User;
};

type UserManagementServiceUpdateUser = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.UpdateUserRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.User;
};

type UserManagementServiceCheckPassword = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.CheckPasswordRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.CheckPasswordResponse;
};

type UserManagementServiceChangePassword = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.ChangePasswordRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type UserManagementServiceLockUser = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.LockUserRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type UserManagementServiceUnlockUser = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.UnlockUserRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type UserManagementServiceInitPasswordReset = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.InitPasswordResetRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type UserManagementServiceValidatePasswordResetToken = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.ValidatePasswordResetTokenRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.ValidatePasswordResetTokenResponse;
};

type UserManagementServiceResetPassword = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.ResetPasswordRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type UserManagementServiceGetUserResetToken = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.GetUserResetTokenRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.GetUserResetTokenResponse;
};

type UserManagementServiceGetUsersByIds = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.GetUsersByIdsRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.GetUsersByIdsResponse;
};

type UserManagementServiceGetUsersByPatternForMessage = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.GetUsersByPatternForMessageRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.GetUsersByPatternForMessageResponse;
};

type UserManagementServiceListReferralUsers = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.ListReferralUsersRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.ListReferralUsersResponse;
};

type UserManagementServiceListContactPerson = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.ListContactPersonRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.ListContactPersonResponse;
};

type UserManagementServiceSaveContactPerson = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.SaveContactPersonRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.SaveContactPersonResponse;
};

type UserManagementServiceDeleteContactPerson = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.DeleteContactPersonRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.DeleteContactPersonResponse;
};

type UserManagementServiceListLocation = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.ListLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.ListLocationResponse;
};

type UserManagementServiceSaveLocation = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.SaveLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.SaveLocationResponse;
};

type UserManagementServiceDeleteLocation = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.DeleteLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.DeleteLocationResponse;
};

type UserManagementServiceListDocument = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.ListDocumentRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.ListDocumentResponse;
};

type UserManagementServiceSaveDocument = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.SaveDocumentRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.SaveDocumentResponse;
};

type UserManagementServiceDeleteDocument = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.DeleteDocumentRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.DeleteDocumentResponse;
};

type UserManagementServiceGetCounterparty = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.GetCounterpartyRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.GetCounterpartyResponse;
};

type UserManagementServiceListCounterparty = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.ListCounterpartyRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.ListCounterpartyResponse;
};

type UserManagementServiceSaveCounterparty = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.SaveCounterpartyRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.SaveCounterpartyResponse;
};

type UserManagementServiceDeleteCounterparty = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.DeleteCounterpartyRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.DeleteCounterpartyResponse;
};

type UserManagementServiceGetCounterpartyLocation = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.GetCounterpartyLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.GetCounterpartyLocationResponse;
};

type UserManagementServiceListCounterpartyLocation = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.ListCounterpartyLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.ListCounterpartyLocationResponse;
};

type UserManagementServiceSaveCounterpartyLocation = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.SaveCounterpartyLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.SaveCounterpartyLocationResponse;
};

type UserManagementServiceDeleteCounterpartyLocation = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.DeleteCounterpartyLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.DeleteCounterpartyLocationResponse;
};

type UserManagementServiceSendPushNotification = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.SendPushRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.SendPushResponse;
};

type UserManagementServiceSendPushNotificationToRole = {
  readonly methodName: string;
  readonly service: typeof UserManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.SendPushRequestToRole;
  readonly responseType: typeof v1_auth_private_auth_service_pb.SendPushResponse;
};

export class UserManagementService {
  static readonly serviceName: string;
  static readonly SendCode: UserManagementServiceSendCode;
  static readonly CheckCode: UserManagementServiceCheckCode;
  static readonly CheckUserByLogin: UserManagementServiceCheckUserByLogin;
  static readonly GetUser: UserManagementServiceGetUser;
  static readonly ListUsers: UserManagementServiceListUsers;
  static readonly CreateUser: UserManagementServiceCreateUser;
  static readonly UpdateUser: UserManagementServiceUpdateUser;
  static readonly CheckPassword: UserManagementServiceCheckPassword;
  static readonly ChangePassword: UserManagementServiceChangePassword;
  static readonly LockUser: UserManagementServiceLockUser;
  static readonly UnlockUser: UserManagementServiceUnlockUser;
  static readonly InitPasswordReset: UserManagementServiceInitPasswordReset;
  static readonly ValidatePasswordResetToken: UserManagementServiceValidatePasswordResetToken;
  static readonly ResetPassword: UserManagementServiceResetPassword;
  static readonly GetUserResetToken: UserManagementServiceGetUserResetToken;
  static readonly GetUsersByIds: UserManagementServiceGetUsersByIds;
  static readonly GetUsersByPatternForMessage: UserManagementServiceGetUsersByPatternForMessage;
  static readonly ListReferralUsers: UserManagementServiceListReferralUsers;
  static readonly ListContactPerson: UserManagementServiceListContactPerson;
  static readonly SaveContactPerson: UserManagementServiceSaveContactPerson;
  static readonly DeleteContactPerson: UserManagementServiceDeleteContactPerson;
  static readonly ListLocation: UserManagementServiceListLocation;
  static readonly SaveLocation: UserManagementServiceSaveLocation;
  static readonly DeleteLocation: UserManagementServiceDeleteLocation;
  static readonly ListDocument: UserManagementServiceListDocument;
  static readonly SaveDocument: UserManagementServiceSaveDocument;
  static readonly DeleteDocument: UserManagementServiceDeleteDocument;
  static readonly GetCounterparty: UserManagementServiceGetCounterparty;
  static readonly ListCounterparty: UserManagementServiceListCounterparty;
  static readonly SaveCounterparty: UserManagementServiceSaveCounterparty;
  static readonly DeleteCounterparty: UserManagementServiceDeleteCounterparty;
  static readonly GetCounterpartyLocation: UserManagementServiceGetCounterpartyLocation;
  static readonly ListCounterpartyLocation: UserManagementServiceListCounterpartyLocation;
  static readonly SaveCounterpartyLocation: UserManagementServiceSaveCounterpartyLocation;
  static readonly DeleteCounterpartyLocation: UserManagementServiceDeleteCounterpartyLocation;
  static readonly SendPushNotification: UserManagementServiceSendPushNotification;
  static readonly SendPushNotificationToRole: UserManagementServiceSendPushNotificationToRole;
}

type RoleManagementServiceGetRole = {
  readonly methodName: string;
  readonly service: typeof RoleManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.GetRoleRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.GetRoleResponse;
};

type RoleManagementServiceCreateRole = {
  readonly methodName: string;
  readonly service: typeof RoleManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.CreateRoleRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.CreateRoleResponse;
};

type RoleManagementServiceListRoles = {
  readonly methodName: string;
  readonly service: typeof RoleManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.ListRolesRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.ListRolesResponse;
};

type RoleManagementServiceUpdateRole = {
  readonly methodName: string;
  readonly service: typeof RoleManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.UpdateRoleRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.UpdateRoleResponse;
};

type RoleManagementServiceGetPermission = {
  readonly methodName: string;
  readonly service: typeof RoleManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.GetPermissionRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.GetPermissionResponse;
};

type RoleManagementServiceCreatePermission = {
  readonly methodName: string;
  readonly service: typeof RoleManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.CreatePermissionRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.CreatePermissionResponse;
};

type RoleManagementServiceUpdatePermission = {
  readonly methodName: string;
  readonly service: typeof RoleManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.UpdatePermissionRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.UpdatePermissionResponse;
};

type RoleManagementServiceDeletePermission = {
  readonly methodName: string;
  readonly service: typeof RoleManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.DeletePermissionRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.DeletePermissionResponse;
};

type RoleManagementServiceListPermissions = {
  readonly methodName: string;
  readonly service: typeof RoleManagementService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.ListPermissionsRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.ListPermissionsResponse;
};

export class RoleManagementService {
  static readonly serviceName: string;
  static readonly GetRole: RoleManagementServiceGetRole;
  static readonly CreateRole: RoleManagementServiceCreateRole;
  static readonly ListRoles: RoleManagementServiceListRoles;
  static readonly UpdateRole: RoleManagementServiceUpdateRole;
  static readonly GetPermission: RoleManagementServiceGetPermission;
  static readonly CreatePermission: RoleManagementServiceCreatePermission;
  static readonly UpdatePermission: RoleManagementServiceUpdatePermission;
  static readonly DeletePermission: RoleManagementServiceDeletePermission;
  static readonly ListPermissions: RoleManagementServiceListPermissions;
}

type AuthSessionServiceLogin = {
  readonly methodName: string;
  readonly service: typeof AuthSessionService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.LoginRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.LoginResponse;
};

type AuthSessionServiceLogout = {
  readonly methodName: string;
  readonly service: typeof AuthSessionService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.LogoutRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type AuthSessionServiceValidateSession = {
  readonly methodName: string;
  readonly service: typeof AuthSessionService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.ValidateSessionRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.ValidateSessionResponse;
};

type AuthSessionServiceValidateUserSession = {
  readonly methodName: string;
  readonly service: typeof AuthSessionService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.ValidateUserSessionRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.ValidateUserSessionResponse;
};

type AuthSessionServiceValidateWsSession = {
  readonly methodName: string;
  readonly service: typeof AuthSessionService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.ValidateWsSessionRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.ValidateWsSessionResponse;
};

type AuthSessionServiceResetSession = {
  readonly methodName: string;
  readonly service: typeof AuthSessionService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.ResetSessionRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type AuthSessionServiceListSessions = {
  readonly methodName: string;
  readonly service: typeof AuthSessionService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_private_auth_service_pb.ListSessionsRequest;
  readonly responseType: typeof v1_auth_private_auth_service_pb.ListSessionsResponse;
};

export class AuthSessionService {
  static readonly serviceName: string;
  static readonly Login: AuthSessionServiceLogin;
  static readonly Logout: AuthSessionServiceLogout;
  static readonly ValidateSession: AuthSessionServiceValidateSession;
  static readonly ValidateUserSession: AuthSessionServiceValidateUserSession;
  static readonly ValidateWsSession: AuthSessionServiceValidateWsSession;
  static readonly ResetSession: AuthSessionServiceResetSession;
  static readonly ListSessions: AuthSessionServiceListSessions;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class UserManagementServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  sendCode(
    requestMessage: v1_auth_private_auth_service_pb.SendCodeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  sendCode(
    requestMessage: v1_auth_private_auth_service_pb.SendCodeRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  checkCode(
    requestMessage: v1_auth_private_auth_service_pb.CheckCodeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.CheckCodeResponse|null) => void
  ): UnaryResponse;
  checkCode(
    requestMessage: v1_auth_private_auth_service_pb.CheckCodeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.CheckCodeResponse|null) => void
  ): UnaryResponse;
  checkUserByLogin(
    requestMessage: v1_auth_private_auth_service_pb.CheckUserByLoginRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.CheckUserByLoginResponse|null) => void
  ): UnaryResponse;
  checkUserByLogin(
    requestMessage: v1_auth_private_auth_service_pb.CheckUserByLoginRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.CheckUserByLoginResponse|null) => void
  ): UnaryResponse;
  getUser(
    requestMessage: v1_auth_private_auth_service_pb.GetUserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.User|null) => void
  ): UnaryResponse;
  getUser(
    requestMessage: v1_auth_private_auth_service_pb.GetUserRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.User|null) => void
  ): UnaryResponse;
  listUsers(
    requestMessage: v1_auth_private_auth_service_pb.ListUsersRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.ListUsersResponse|null) => void
  ): UnaryResponse;
  listUsers(
    requestMessage: v1_auth_private_auth_service_pb.ListUsersRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.ListUsersResponse|null) => void
  ): UnaryResponse;
  createUser(
    requestMessage: v1_auth_private_auth_service_pb.CreateUserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.User|null) => void
  ): UnaryResponse;
  createUser(
    requestMessage: v1_auth_private_auth_service_pb.CreateUserRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.User|null) => void
  ): UnaryResponse;
  updateUser(
    requestMessage: v1_auth_private_auth_service_pb.UpdateUserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.User|null) => void
  ): UnaryResponse;
  updateUser(
    requestMessage: v1_auth_private_auth_service_pb.UpdateUserRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.User|null) => void
  ): UnaryResponse;
  checkPassword(
    requestMessage: v1_auth_private_auth_service_pb.CheckPasswordRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.CheckPasswordResponse|null) => void
  ): UnaryResponse;
  checkPassword(
    requestMessage: v1_auth_private_auth_service_pb.CheckPasswordRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.CheckPasswordResponse|null) => void
  ): UnaryResponse;
  changePassword(
    requestMessage: v1_auth_private_auth_service_pb.ChangePasswordRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  changePassword(
    requestMessage: v1_auth_private_auth_service_pb.ChangePasswordRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  lockUser(
    requestMessage: v1_auth_private_auth_service_pb.LockUserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  lockUser(
    requestMessage: v1_auth_private_auth_service_pb.LockUserRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  unlockUser(
    requestMessage: v1_auth_private_auth_service_pb.UnlockUserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  unlockUser(
    requestMessage: v1_auth_private_auth_service_pb.UnlockUserRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  initPasswordReset(
    requestMessage: v1_auth_private_auth_service_pb.InitPasswordResetRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  initPasswordReset(
    requestMessage: v1_auth_private_auth_service_pb.InitPasswordResetRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  validatePasswordResetToken(
    requestMessage: v1_auth_private_auth_service_pb.ValidatePasswordResetTokenRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.ValidatePasswordResetTokenResponse|null) => void
  ): UnaryResponse;
  validatePasswordResetToken(
    requestMessage: v1_auth_private_auth_service_pb.ValidatePasswordResetTokenRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.ValidatePasswordResetTokenResponse|null) => void
  ): UnaryResponse;
  resetPassword(
    requestMessage: v1_auth_private_auth_service_pb.ResetPasswordRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  resetPassword(
    requestMessage: v1_auth_private_auth_service_pb.ResetPasswordRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  getUserResetToken(
    requestMessage: v1_auth_private_auth_service_pb.GetUserResetTokenRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.GetUserResetTokenResponse|null) => void
  ): UnaryResponse;
  getUserResetToken(
    requestMessage: v1_auth_private_auth_service_pb.GetUserResetTokenRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.GetUserResetTokenResponse|null) => void
  ): UnaryResponse;
  getUsersByIds(
    requestMessage: v1_auth_private_auth_service_pb.GetUsersByIdsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.GetUsersByIdsResponse|null) => void
  ): UnaryResponse;
  getUsersByIds(
    requestMessage: v1_auth_private_auth_service_pb.GetUsersByIdsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.GetUsersByIdsResponse|null) => void
  ): UnaryResponse;
  getUsersByPatternForMessage(
    requestMessage: v1_auth_private_auth_service_pb.GetUsersByPatternForMessageRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.GetUsersByPatternForMessageResponse|null) => void
  ): UnaryResponse;
  getUsersByPatternForMessage(
    requestMessage: v1_auth_private_auth_service_pb.GetUsersByPatternForMessageRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.GetUsersByPatternForMessageResponse|null) => void
  ): UnaryResponse;
  listReferralUsers(
    requestMessage: v1_auth_private_auth_service_pb.ListReferralUsersRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.ListReferralUsersResponse|null) => void
  ): UnaryResponse;
  listReferralUsers(
    requestMessage: v1_auth_private_auth_service_pb.ListReferralUsersRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.ListReferralUsersResponse|null) => void
  ): UnaryResponse;
  listContactPerson(
    requestMessage: v1_auth_common_dtos_pb.ListContactPersonRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListContactPersonResponse|null) => void
  ): UnaryResponse;
  listContactPerson(
    requestMessage: v1_auth_common_dtos_pb.ListContactPersonRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListContactPersonResponse|null) => void
  ): UnaryResponse;
  saveContactPerson(
    requestMessage: v1_auth_common_dtos_pb.SaveContactPersonRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveContactPersonResponse|null) => void
  ): UnaryResponse;
  saveContactPerson(
    requestMessage: v1_auth_common_dtos_pb.SaveContactPersonRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveContactPersonResponse|null) => void
  ): UnaryResponse;
  deleteContactPerson(
    requestMessage: v1_auth_common_dtos_pb.DeleteContactPersonRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteContactPersonResponse|null) => void
  ): UnaryResponse;
  deleteContactPerson(
    requestMessage: v1_auth_common_dtos_pb.DeleteContactPersonRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteContactPersonResponse|null) => void
  ): UnaryResponse;
  listLocation(
    requestMessage: v1_auth_common_dtos_pb.ListLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListLocationResponse|null) => void
  ): UnaryResponse;
  listLocation(
    requestMessage: v1_auth_common_dtos_pb.ListLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListLocationResponse|null) => void
  ): UnaryResponse;
  saveLocation(
    requestMessage: v1_auth_common_dtos_pb.SaveLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveLocationResponse|null) => void
  ): UnaryResponse;
  saveLocation(
    requestMessage: v1_auth_common_dtos_pb.SaveLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveLocationResponse|null) => void
  ): UnaryResponse;
  deleteLocation(
    requestMessage: v1_auth_common_dtos_pb.DeleteLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteLocationResponse|null) => void
  ): UnaryResponse;
  deleteLocation(
    requestMessage: v1_auth_common_dtos_pb.DeleteLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteLocationResponse|null) => void
  ): UnaryResponse;
  listDocument(
    requestMessage: v1_auth_common_dtos_pb.ListDocumentRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListDocumentResponse|null) => void
  ): UnaryResponse;
  listDocument(
    requestMessage: v1_auth_common_dtos_pb.ListDocumentRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListDocumentResponse|null) => void
  ): UnaryResponse;
  saveDocument(
    requestMessage: v1_auth_common_dtos_pb.SaveDocumentRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveDocumentResponse|null) => void
  ): UnaryResponse;
  saveDocument(
    requestMessage: v1_auth_common_dtos_pb.SaveDocumentRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveDocumentResponse|null) => void
  ): UnaryResponse;
  deleteDocument(
    requestMessage: v1_auth_common_dtos_pb.DeleteDocumentRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteDocumentResponse|null) => void
  ): UnaryResponse;
  deleteDocument(
    requestMessage: v1_auth_common_dtos_pb.DeleteDocumentRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteDocumentResponse|null) => void
  ): UnaryResponse;
  getCounterparty(
    requestMessage: v1_auth_common_dtos_pb.GetCounterpartyRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.GetCounterpartyResponse|null) => void
  ): UnaryResponse;
  getCounterparty(
    requestMessage: v1_auth_common_dtos_pb.GetCounterpartyRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.GetCounterpartyResponse|null) => void
  ): UnaryResponse;
  listCounterparty(
    requestMessage: v1_auth_common_dtos_pb.ListCounterpartyRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListCounterpartyResponse|null) => void
  ): UnaryResponse;
  listCounterparty(
    requestMessage: v1_auth_common_dtos_pb.ListCounterpartyRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListCounterpartyResponse|null) => void
  ): UnaryResponse;
  saveCounterparty(
    requestMessage: v1_auth_common_dtos_pb.SaveCounterpartyRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveCounterpartyResponse|null) => void
  ): UnaryResponse;
  saveCounterparty(
    requestMessage: v1_auth_common_dtos_pb.SaveCounterpartyRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveCounterpartyResponse|null) => void
  ): UnaryResponse;
  deleteCounterparty(
    requestMessage: v1_auth_common_dtos_pb.DeleteCounterpartyRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteCounterpartyResponse|null) => void
  ): UnaryResponse;
  deleteCounterparty(
    requestMessage: v1_auth_common_dtos_pb.DeleteCounterpartyRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteCounterpartyResponse|null) => void
  ): UnaryResponse;
  getCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.GetCounterpartyLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.GetCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  getCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.GetCounterpartyLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.GetCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  listCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.ListCounterpartyLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  listCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.ListCounterpartyLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  saveCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.SaveCounterpartyLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  saveCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.SaveCounterpartyLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  deleteCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.DeleteCounterpartyLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  deleteCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.DeleteCounterpartyLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  sendPushNotification(
    requestMessage: v1_auth_private_auth_service_pb.SendPushRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.SendPushResponse|null) => void
  ): UnaryResponse;
  sendPushNotification(
    requestMessage: v1_auth_private_auth_service_pb.SendPushRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.SendPushResponse|null) => void
  ): UnaryResponse;
  sendPushNotificationToRole(
    requestMessage: v1_auth_private_auth_service_pb.SendPushRequestToRole,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.SendPushResponse|null) => void
  ): UnaryResponse;
  sendPushNotificationToRole(
    requestMessage: v1_auth_private_auth_service_pb.SendPushRequestToRole,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.SendPushResponse|null) => void
  ): UnaryResponse;
}

export class RoleManagementServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  getRole(
    requestMessage: v1_auth_private_auth_service_pb.GetRoleRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.GetRoleResponse|null) => void
  ): UnaryResponse;
  getRole(
    requestMessage: v1_auth_private_auth_service_pb.GetRoleRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.GetRoleResponse|null) => void
  ): UnaryResponse;
  createRole(
    requestMessage: v1_auth_private_auth_service_pb.CreateRoleRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.CreateRoleResponse|null) => void
  ): UnaryResponse;
  createRole(
    requestMessage: v1_auth_private_auth_service_pb.CreateRoleRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.CreateRoleResponse|null) => void
  ): UnaryResponse;
  listRoles(
    requestMessage: v1_auth_private_auth_service_pb.ListRolesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.ListRolesResponse|null) => void
  ): UnaryResponse;
  listRoles(
    requestMessage: v1_auth_private_auth_service_pb.ListRolesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.ListRolesResponse|null) => void
  ): UnaryResponse;
  updateRole(
    requestMessage: v1_auth_private_auth_service_pb.UpdateRoleRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.UpdateRoleResponse|null) => void
  ): UnaryResponse;
  updateRole(
    requestMessage: v1_auth_private_auth_service_pb.UpdateRoleRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.UpdateRoleResponse|null) => void
  ): UnaryResponse;
  getPermission(
    requestMessage: v1_auth_private_auth_service_pb.GetPermissionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.GetPermissionResponse|null) => void
  ): UnaryResponse;
  getPermission(
    requestMessage: v1_auth_private_auth_service_pb.GetPermissionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.GetPermissionResponse|null) => void
  ): UnaryResponse;
  createPermission(
    requestMessage: v1_auth_private_auth_service_pb.CreatePermissionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.CreatePermissionResponse|null) => void
  ): UnaryResponse;
  createPermission(
    requestMessage: v1_auth_private_auth_service_pb.CreatePermissionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.CreatePermissionResponse|null) => void
  ): UnaryResponse;
  updatePermission(
    requestMessage: v1_auth_private_auth_service_pb.UpdatePermissionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.UpdatePermissionResponse|null) => void
  ): UnaryResponse;
  updatePermission(
    requestMessage: v1_auth_private_auth_service_pb.UpdatePermissionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.UpdatePermissionResponse|null) => void
  ): UnaryResponse;
  deletePermission(
    requestMessage: v1_auth_private_auth_service_pb.DeletePermissionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.DeletePermissionResponse|null) => void
  ): UnaryResponse;
  deletePermission(
    requestMessage: v1_auth_private_auth_service_pb.DeletePermissionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.DeletePermissionResponse|null) => void
  ): UnaryResponse;
  listPermissions(
    requestMessage: v1_auth_private_auth_service_pb.ListPermissionsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.ListPermissionsResponse|null) => void
  ): UnaryResponse;
  listPermissions(
    requestMessage: v1_auth_private_auth_service_pb.ListPermissionsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.ListPermissionsResponse|null) => void
  ): UnaryResponse;
}

export class AuthSessionServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  login(
    requestMessage: v1_auth_private_auth_service_pb.LoginRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.LoginResponse|null) => void
  ): UnaryResponse;
  login(
    requestMessage: v1_auth_private_auth_service_pb.LoginRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.LoginResponse|null) => void
  ): UnaryResponse;
  logout(
    requestMessage: v1_auth_private_auth_service_pb.LogoutRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  logout(
    requestMessage: v1_auth_private_auth_service_pb.LogoutRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  validateSession(
    requestMessage: v1_auth_private_auth_service_pb.ValidateSessionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.ValidateSessionResponse|null) => void
  ): UnaryResponse;
  validateSession(
    requestMessage: v1_auth_private_auth_service_pb.ValidateSessionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.ValidateSessionResponse|null) => void
  ): UnaryResponse;
  validateUserSession(
    requestMessage: v1_auth_private_auth_service_pb.ValidateUserSessionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.ValidateUserSessionResponse|null) => void
  ): UnaryResponse;
  validateUserSession(
    requestMessage: v1_auth_private_auth_service_pb.ValidateUserSessionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.ValidateUserSessionResponse|null) => void
  ): UnaryResponse;
  validateWsSession(
    requestMessage: v1_auth_private_auth_service_pb.ValidateWsSessionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.ValidateWsSessionResponse|null) => void
  ): UnaryResponse;
  validateWsSession(
    requestMessage: v1_auth_private_auth_service_pb.ValidateWsSessionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.ValidateWsSessionResponse|null) => void
  ): UnaryResponse;
  resetSession(
    requestMessage: v1_auth_private_auth_service_pb.ResetSessionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  resetSession(
    requestMessage: v1_auth_private_auth_service_pb.ResetSessionRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  listSessions(
    requestMessage: v1_auth_private_auth_service_pb.ListSessionsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.ListSessionsResponse|null) => void
  ): UnaryResponse;
  listSessions(
    requestMessage: v1_auth_private_auth_service_pb.ListSessionsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_private_auth_service_pb.ListSessionsResponse|null) => void
  ): UnaryResponse;
}

