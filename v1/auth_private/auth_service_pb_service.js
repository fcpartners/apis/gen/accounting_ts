// package: fcp.accounting.v1.auth_private
// file: v1/auth_private/auth_service.proto

var v1_auth_private_auth_service_pb = require("../../v1/auth_private/auth_service_pb");
var google_protobuf_empty_pb = require("google-protobuf/google/protobuf/empty_pb");
var v1_auth_common_dtos_pb = require("../../v1/auth_common/dtos_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var UserManagementService = (function () {
  function UserManagementService() {}
  UserManagementService.serviceName = "fcp.accounting.v1.auth_private.UserManagementService";
  return UserManagementService;
}());

UserManagementService.SendCode = {
  methodName: "SendCode",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.SendCodeRequest,
  responseType: google_protobuf_empty_pb.Empty
};

UserManagementService.CheckCode = {
  methodName: "CheckCode",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.CheckCodeRequest,
  responseType: v1_auth_private_auth_service_pb.CheckCodeResponse
};

UserManagementService.CheckUserByLogin = {
  methodName: "CheckUserByLogin",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.CheckUserByLoginRequest,
  responseType: v1_auth_private_auth_service_pb.CheckUserByLoginResponse
};

UserManagementService.GetUser = {
  methodName: "GetUser",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.GetUserRequest,
  responseType: v1_auth_common_dtos_pb.User
};

UserManagementService.ListUsers = {
  methodName: "ListUsers",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ListUsersRequest,
  responseType: v1_auth_private_auth_service_pb.ListUsersResponse
};

UserManagementService.CreateUser = {
  methodName: "CreateUser",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.CreateUserRequest,
  responseType: v1_auth_common_dtos_pb.User
};

UserManagementService.UpdateUser = {
  methodName: "UpdateUser",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.UpdateUserRequest,
  responseType: v1_auth_common_dtos_pb.User
};

UserManagementService.CheckPassword = {
  methodName: "CheckPassword",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.CheckPasswordRequest,
  responseType: v1_auth_private_auth_service_pb.CheckPasswordResponse
};

UserManagementService.ChangePassword = {
  methodName: "ChangePassword",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ChangePasswordRequest,
  responseType: google_protobuf_empty_pb.Empty
};

UserManagementService.LockUser = {
  methodName: "LockUser",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.LockUserRequest,
  responseType: google_protobuf_empty_pb.Empty
};

UserManagementService.UnlockUser = {
  methodName: "UnlockUser",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.UnlockUserRequest,
  responseType: google_protobuf_empty_pb.Empty
};

UserManagementService.InitPasswordReset = {
  methodName: "InitPasswordReset",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.InitPasswordResetRequest,
  responseType: google_protobuf_empty_pb.Empty
};

UserManagementService.ValidatePasswordResetToken = {
  methodName: "ValidatePasswordResetToken",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ValidatePasswordResetTokenRequest,
  responseType: v1_auth_private_auth_service_pb.ValidatePasswordResetTokenResponse
};

UserManagementService.ResetPassword = {
  methodName: "ResetPassword",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ResetPasswordRequest,
  responseType: google_protobuf_empty_pb.Empty
};

UserManagementService.GetUserResetToken = {
  methodName: "GetUserResetToken",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.GetUserResetTokenRequest,
  responseType: v1_auth_private_auth_service_pb.GetUserResetTokenResponse
};

UserManagementService.GetUsersByIds = {
  methodName: "GetUsersByIds",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.GetUsersByIdsRequest,
  responseType: v1_auth_private_auth_service_pb.GetUsersByIdsResponse
};

UserManagementService.GetUsersByPatternForMessage = {
  methodName: "GetUsersByPatternForMessage",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.GetUsersByPatternForMessageRequest,
  responseType: v1_auth_private_auth_service_pb.GetUsersByPatternForMessageResponse
};

UserManagementService.ListReferralUsers = {
  methodName: "ListReferralUsers",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ListReferralUsersRequest,
  responseType: v1_auth_private_auth_service_pb.ListReferralUsersResponse
};

UserManagementService.ListContactPerson = {
  methodName: "ListContactPerson",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.ListContactPersonRequest,
  responseType: v1_auth_common_dtos_pb.ListContactPersonResponse
};

UserManagementService.SaveContactPerson = {
  methodName: "SaveContactPerson",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.SaveContactPersonRequest,
  responseType: v1_auth_common_dtos_pb.SaveContactPersonResponse
};

UserManagementService.DeleteContactPerson = {
  methodName: "DeleteContactPerson",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.DeleteContactPersonRequest,
  responseType: v1_auth_common_dtos_pb.DeleteContactPersonResponse
};

UserManagementService.ListLocation = {
  methodName: "ListLocation",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.ListLocationRequest,
  responseType: v1_auth_common_dtos_pb.ListLocationResponse
};

UserManagementService.SaveLocation = {
  methodName: "SaveLocation",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.SaveLocationRequest,
  responseType: v1_auth_common_dtos_pb.SaveLocationResponse
};

UserManagementService.DeleteLocation = {
  methodName: "DeleteLocation",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.DeleteLocationRequest,
  responseType: v1_auth_common_dtos_pb.DeleteLocationResponse
};

UserManagementService.ListDocument = {
  methodName: "ListDocument",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.ListDocumentRequest,
  responseType: v1_auth_common_dtos_pb.ListDocumentResponse
};

UserManagementService.SaveDocument = {
  methodName: "SaveDocument",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.SaveDocumentRequest,
  responseType: v1_auth_common_dtos_pb.SaveDocumentResponse
};

UserManagementService.DeleteDocument = {
  methodName: "DeleteDocument",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.DeleteDocumentRequest,
  responseType: v1_auth_common_dtos_pb.DeleteDocumentResponse
};

UserManagementService.GetCounterparty = {
  methodName: "GetCounterparty",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.GetCounterpartyRequest,
  responseType: v1_auth_common_dtos_pb.GetCounterpartyResponse
};

UserManagementService.ListCounterparty = {
  methodName: "ListCounterparty",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.ListCounterpartyRequest,
  responseType: v1_auth_common_dtos_pb.ListCounterpartyResponse
};

UserManagementService.SaveCounterparty = {
  methodName: "SaveCounterparty",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.SaveCounterpartyRequest,
  responseType: v1_auth_common_dtos_pb.SaveCounterpartyResponse
};

UserManagementService.DeleteCounterparty = {
  methodName: "DeleteCounterparty",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.DeleteCounterpartyRequest,
  responseType: v1_auth_common_dtos_pb.DeleteCounterpartyResponse
};

UserManagementService.GetCounterpartyLocation = {
  methodName: "GetCounterpartyLocation",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.GetCounterpartyLocationRequest,
  responseType: v1_auth_common_dtos_pb.GetCounterpartyLocationResponse
};

UserManagementService.ListCounterpartyLocation = {
  methodName: "ListCounterpartyLocation",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.ListCounterpartyLocationRequest,
  responseType: v1_auth_common_dtos_pb.ListCounterpartyLocationResponse
};

UserManagementService.SaveCounterpartyLocation = {
  methodName: "SaveCounterpartyLocation",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.SaveCounterpartyLocationRequest,
  responseType: v1_auth_common_dtos_pb.SaveCounterpartyLocationResponse
};

UserManagementService.DeleteCounterpartyLocation = {
  methodName: "DeleteCounterpartyLocation",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.DeleteCounterpartyLocationRequest,
  responseType: v1_auth_common_dtos_pb.DeleteCounterpartyLocationResponse
};

UserManagementService.SendPushNotification = {
  methodName: "SendPushNotification",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.SendPushRequest,
  responseType: v1_auth_private_auth_service_pb.SendPushResponse
};

UserManagementService.SendPushNotificationToRole = {
  methodName: "SendPushNotificationToRole",
  service: UserManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.SendPushRequestToRole,
  responseType: v1_auth_private_auth_service_pb.SendPushResponse
};

exports.UserManagementService = UserManagementService;

function UserManagementServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

UserManagementServiceClient.prototype.sendCode = function sendCode(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.SendCode, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.checkCode = function checkCode(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.CheckCode, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.checkUserByLogin = function checkUserByLogin(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.CheckUserByLogin, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.getUser = function getUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.GetUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.listUsers = function listUsers(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.ListUsers, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.createUser = function createUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.CreateUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.updateUser = function updateUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.UpdateUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.checkPassword = function checkPassword(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.CheckPassword, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.changePassword = function changePassword(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.ChangePassword, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.lockUser = function lockUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.LockUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.unlockUser = function unlockUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.UnlockUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.initPasswordReset = function initPasswordReset(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.InitPasswordReset, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.validatePasswordResetToken = function validatePasswordResetToken(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.ValidatePasswordResetToken, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.resetPassword = function resetPassword(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.ResetPassword, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.getUserResetToken = function getUserResetToken(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.GetUserResetToken, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.getUsersByIds = function getUsersByIds(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.GetUsersByIds, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.getUsersByPatternForMessage = function getUsersByPatternForMessage(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.GetUsersByPatternForMessage, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.listReferralUsers = function listReferralUsers(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.ListReferralUsers, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.listContactPerson = function listContactPerson(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.ListContactPerson, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.saveContactPerson = function saveContactPerson(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.SaveContactPerson, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.deleteContactPerson = function deleteContactPerson(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.DeleteContactPerson, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.listLocation = function listLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.ListLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.saveLocation = function saveLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.SaveLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.deleteLocation = function deleteLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.DeleteLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.listDocument = function listDocument(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.ListDocument, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.saveDocument = function saveDocument(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.SaveDocument, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.deleteDocument = function deleteDocument(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.DeleteDocument, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.getCounterparty = function getCounterparty(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.GetCounterparty, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.listCounterparty = function listCounterparty(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.ListCounterparty, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.saveCounterparty = function saveCounterparty(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.SaveCounterparty, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.deleteCounterparty = function deleteCounterparty(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.DeleteCounterparty, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.getCounterpartyLocation = function getCounterpartyLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.GetCounterpartyLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.listCounterpartyLocation = function listCounterpartyLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.ListCounterpartyLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.saveCounterpartyLocation = function saveCounterpartyLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.SaveCounterpartyLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.deleteCounterpartyLocation = function deleteCounterpartyLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.DeleteCounterpartyLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.sendPushNotification = function sendPushNotification(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.SendPushNotification, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

UserManagementServiceClient.prototype.sendPushNotificationToRole = function sendPushNotificationToRole(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(UserManagementService.SendPushNotificationToRole, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.UserManagementServiceClient = UserManagementServiceClient;

var RoleManagementService = (function () {
  function RoleManagementService() {}
  RoleManagementService.serviceName = "fcp.accounting.v1.auth_private.RoleManagementService";
  return RoleManagementService;
}());

RoleManagementService.GetRole = {
  methodName: "GetRole",
  service: RoleManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.GetRoleRequest,
  responseType: v1_auth_private_auth_service_pb.GetRoleResponse
};

RoleManagementService.CreateRole = {
  methodName: "CreateRole",
  service: RoleManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.CreateRoleRequest,
  responseType: v1_auth_private_auth_service_pb.CreateRoleResponse
};

RoleManagementService.ListRoles = {
  methodName: "ListRoles",
  service: RoleManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ListRolesRequest,
  responseType: v1_auth_private_auth_service_pb.ListRolesResponse
};

RoleManagementService.UpdateRole = {
  methodName: "UpdateRole",
  service: RoleManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.UpdateRoleRequest,
  responseType: v1_auth_private_auth_service_pb.UpdateRoleResponse
};

RoleManagementService.GetPermission = {
  methodName: "GetPermission",
  service: RoleManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.GetPermissionRequest,
  responseType: v1_auth_private_auth_service_pb.GetPermissionResponse
};

RoleManagementService.CreatePermission = {
  methodName: "CreatePermission",
  service: RoleManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.CreatePermissionRequest,
  responseType: v1_auth_private_auth_service_pb.CreatePermissionResponse
};

RoleManagementService.UpdatePermission = {
  methodName: "UpdatePermission",
  service: RoleManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.UpdatePermissionRequest,
  responseType: v1_auth_private_auth_service_pb.UpdatePermissionResponse
};

RoleManagementService.DeletePermission = {
  methodName: "DeletePermission",
  service: RoleManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.DeletePermissionRequest,
  responseType: v1_auth_private_auth_service_pb.DeletePermissionResponse
};

RoleManagementService.ListPermissions = {
  methodName: "ListPermissions",
  service: RoleManagementService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ListPermissionsRequest,
  responseType: v1_auth_private_auth_service_pb.ListPermissionsResponse
};

exports.RoleManagementService = RoleManagementService;

function RoleManagementServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

RoleManagementServiceClient.prototype.getRole = function getRole(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(RoleManagementService.GetRole, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

RoleManagementServiceClient.prototype.createRole = function createRole(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(RoleManagementService.CreateRole, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

RoleManagementServiceClient.prototype.listRoles = function listRoles(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(RoleManagementService.ListRoles, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

RoleManagementServiceClient.prototype.updateRole = function updateRole(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(RoleManagementService.UpdateRole, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

RoleManagementServiceClient.prototype.getPermission = function getPermission(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(RoleManagementService.GetPermission, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

RoleManagementServiceClient.prototype.createPermission = function createPermission(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(RoleManagementService.CreatePermission, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

RoleManagementServiceClient.prototype.updatePermission = function updatePermission(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(RoleManagementService.UpdatePermission, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

RoleManagementServiceClient.prototype.deletePermission = function deletePermission(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(RoleManagementService.DeletePermission, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

RoleManagementServiceClient.prototype.listPermissions = function listPermissions(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(RoleManagementService.ListPermissions, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.RoleManagementServiceClient = RoleManagementServiceClient;

var AuthSessionService = (function () {
  function AuthSessionService() {}
  AuthSessionService.serviceName = "fcp.accounting.v1.auth_private.AuthSessionService";
  return AuthSessionService;
}());

AuthSessionService.Login = {
  methodName: "Login",
  service: AuthSessionService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.LoginRequest,
  responseType: v1_auth_private_auth_service_pb.LoginResponse
};

AuthSessionService.Logout = {
  methodName: "Logout",
  service: AuthSessionService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.LogoutRequest,
  responseType: google_protobuf_empty_pb.Empty
};

AuthSessionService.ValidateSession = {
  methodName: "ValidateSession",
  service: AuthSessionService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ValidateSessionRequest,
  responseType: v1_auth_private_auth_service_pb.ValidateSessionResponse
};

AuthSessionService.ValidateUserSession = {
  methodName: "ValidateUserSession",
  service: AuthSessionService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ValidateUserSessionRequest,
  responseType: v1_auth_private_auth_service_pb.ValidateUserSessionResponse
};

AuthSessionService.ValidateWsSession = {
  methodName: "ValidateWsSession",
  service: AuthSessionService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ValidateWsSessionRequest,
  responseType: v1_auth_private_auth_service_pb.ValidateWsSessionResponse
};

AuthSessionService.ResetSession = {
  methodName: "ResetSession",
  service: AuthSessionService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ResetSessionRequest,
  responseType: google_protobuf_empty_pb.Empty
};

AuthSessionService.ListSessions = {
  methodName: "ListSessions",
  service: AuthSessionService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_private_auth_service_pb.ListSessionsRequest,
  responseType: v1_auth_private_auth_service_pb.ListSessionsResponse
};

exports.AuthSessionService = AuthSessionService;

function AuthSessionServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

AuthSessionServiceClient.prototype.login = function login(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(AuthSessionService.Login, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

AuthSessionServiceClient.prototype.logout = function logout(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(AuthSessionService.Logout, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

AuthSessionServiceClient.prototype.validateSession = function validateSession(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(AuthSessionService.ValidateSession, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

AuthSessionServiceClient.prototype.validateUserSession = function validateUserSession(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(AuthSessionService.ValidateUserSession, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

AuthSessionServiceClient.prototype.validateWsSession = function validateWsSession(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(AuthSessionService.ValidateWsSession, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

AuthSessionServiceClient.prototype.resetSession = function resetSession(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(AuthSessionService.ResetSession, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

AuthSessionServiceClient.prototype.listSessions = function listSessions(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(AuthSessionService.ListSessions, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.AuthSessionServiceClient = AuthSessionServiceClient;

