// package: fcp.accounting.v1.auth_web
// file: v1/auth_web/auth_service.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import * as v1_auth_common_common_pb from "../../v1/auth_common/common_pb";
import * as v1_auth_common_dtos_pb from "../../v1/auth_common/dtos_pb";

export class GetUserRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetUserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetUserRequest): GetUserRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetUserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetUserRequest;
  static deserializeBinaryFromReader(message: GetUserRequest, reader: jspb.BinaryReader): GetUserRequest;
}

export namespace GetUserRequest {
  export type AsObject = {
    userId: string,
  }
}

export class UpdateUserRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getFirstName(): string;
  setFirstName(value: string): void;

  getLastName(): string;
  setLastName(value: string): void;

  getMiddleName(): string;
  setMiddleName(value: string): void;

  getEmail(): string;
  setEmail(value: string): void;

  getLatitude(): number;
  setLatitude(value: number): void;

  getLongitude(): number;
  setLongitude(value: number): void;

  getAddress(): string;
  setAddress(value: string): void;

  getCountryCode(): string;
  setCountryCode(value: string): void;

  getLanguageCode(): string;
  setLanguageCode(value: string): void;

  getBusinessName(): string;
  setBusinessName(value: string): void;

  getBusinessAddress(): string;
  setBusinessAddress(value: string): void;

  getReferralUrl(): string;
  setReferralUrl(value: string): void;

  hasTaxInfo(): boolean;
  clearTaxInfo(): void;
  getTaxInfo(): v1_auth_common_dtos_pb.TaxInfo | undefined;
  setTaxInfo(value?: v1_auth_common_dtos_pb.TaxInfo): void;

  hasBusinessInfo(): boolean;
  clearBusinessInfo(): void;
  getBusinessInfo(): v1_auth_common_dtos_pb.BusinessInfo | undefined;
  setBusinessInfo(value?: v1_auth_common_dtos_pb.BusinessInfo): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateUserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateUserRequest): UpdateUserRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateUserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateUserRequest;
  static deserializeBinaryFromReader(message: UpdateUserRequest, reader: jspb.BinaryReader): UpdateUserRequest;
}

export namespace UpdateUserRequest {
  export type AsObject = {
    userId: string,
    firstName: string,
    lastName: string,
    middleName: string,
    email: string,
    latitude: number,
    longitude: number,
    address: string,
    countryCode: string,
    languageCode: string,
    businessName: string,
    businessAddress: string,
    referralUrl: string,
    taxInfo?: v1_auth_common_dtos_pb.TaxInfo.AsObject,
    businessInfo?: v1_auth_common_dtos_pb.BusinessInfo.AsObject,
  }
}

export class RegisterEmployeeRequest extends jspb.Message {
  hasUser(): boolean;
  clearUser(): void;
  getUser(): v1_auth_common_dtos_pb.UserBriefInfo | undefined;
  setUser(value?: v1_auth_common_dtos_pb.UserBriefInfo): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegisterEmployeeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RegisterEmployeeRequest): RegisterEmployeeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RegisterEmployeeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegisterEmployeeRequest;
  static deserializeBinaryFromReader(message: RegisterEmployeeRequest, reader: jspb.BinaryReader): RegisterEmployeeRequest;
}

export namespace RegisterEmployeeRequest {
  export type AsObject = {
    user?: v1_auth_common_dtos_pb.UserBriefInfo.AsObject,
  }
}

export class RegisterEmployeeResponse extends jspb.Message {
  hasUser(): boolean;
  clearUser(): void;
  getUser(): v1_auth_common_dtos_pb.UserBriefInfo | undefined;
  setUser(value?: v1_auth_common_dtos_pb.UserBriefInfo): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RegisterEmployeeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: RegisterEmployeeResponse): RegisterEmployeeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RegisterEmployeeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RegisterEmployeeResponse;
  static deserializeBinaryFromReader(message: RegisterEmployeeResponse, reader: jspb.BinaryReader): RegisterEmployeeResponse;
}

export namespace RegisterEmployeeResponse {
  export type AsObject = {
    user?: v1_auth_common_dtos_pb.UserBriefInfo.AsObject,
  }
}

export class UpdateEmployeeRequest extends jspb.Message {
  hasUser(): boolean;
  clearUser(): void;
  getUser(): v1_auth_common_dtos_pb.UserBriefInfo | undefined;
  setUser(value?: v1_auth_common_dtos_pb.UserBriefInfo): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateEmployeeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateEmployeeRequest): UpdateEmployeeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateEmployeeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateEmployeeRequest;
  static deserializeBinaryFromReader(message: UpdateEmployeeRequest, reader: jspb.BinaryReader): UpdateEmployeeRequest;
}

export namespace UpdateEmployeeRequest {
  export type AsObject = {
    user?: v1_auth_common_dtos_pb.UserBriefInfo.AsObject,
  }
}

export class UpdateEmployeeResponse extends jspb.Message {
  hasUser(): boolean;
  clearUser(): void;
  getUser(): v1_auth_common_dtos_pb.UserBriefInfo | undefined;
  setUser(value?: v1_auth_common_dtos_pb.UserBriefInfo): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateEmployeeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateEmployeeResponse): UpdateEmployeeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateEmployeeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateEmployeeResponse;
  static deserializeBinaryFromReader(message: UpdateEmployeeResponse, reader: jspb.BinaryReader): UpdateEmployeeResponse;
}

export namespace UpdateEmployeeResponse {
  export type AsObject = {
    user?: v1_auth_common_dtos_pb.UserBriefInfo.AsObject,
  }
}

export class ListEmployeeRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListEmployeeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListEmployeeRequest): ListEmployeeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListEmployeeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListEmployeeRequest;
  static deserializeBinaryFromReader(message: ListEmployeeRequest, reader: jspb.BinaryReader): ListEmployeeRequest;
}

export namespace ListEmployeeRequest {
  export type AsObject = {
  }
}

export class ListEmployeeResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<v1_auth_common_dtos_pb.UserBriefInfo>;
  setItemsList(value: Array<v1_auth_common_dtos_pb.UserBriefInfo>): void;
  addItems(value?: v1_auth_common_dtos_pb.UserBriefInfo, index?: number): v1_auth_common_dtos_pb.UserBriefInfo;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListEmployeeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListEmployeeResponse): ListEmployeeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListEmployeeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListEmployeeResponse;
  static deserializeBinaryFromReader(message: ListEmployeeResponse, reader: jspb.BinaryReader): ListEmployeeResponse;
}

export namespace ListEmployeeResponse {
  export type AsObject = {
    itemsList: Array<v1_auth_common_dtos_pb.UserBriefInfo.AsObject>,
  }
}

export class LockEmployeeRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getReason(): string;
  setReason(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LockEmployeeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: LockEmployeeRequest): LockEmployeeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LockEmployeeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LockEmployeeRequest;
  static deserializeBinaryFromReader(message: LockEmployeeRequest, reader: jspb.BinaryReader): LockEmployeeRequest;
}

export namespace LockEmployeeRequest {
  export type AsObject = {
    userId: string,
    reason: string,
  }
}

export class LockEmployeeResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LockEmployeeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: LockEmployeeResponse): LockEmployeeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LockEmployeeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LockEmployeeResponse;
  static deserializeBinaryFromReader(message: LockEmployeeResponse, reader: jspb.BinaryReader): LockEmployeeResponse;
}

export namespace LockEmployeeResponse {
  export type AsObject = {
  }
}

export class UnlockEmployeeRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UnlockEmployeeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UnlockEmployeeRequest): UnlockEmployeeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UnlockEmployeeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UnlockEmployeeRequest;
  static deserializeBinaryFromReader(message: UnlockEmployeeRequest, reader: jspb.BinaryReader): UnlockEmployeeRequest;
}

export namespace UnlockEmployeeRequest {
  export type AsObject = {
    userId: string,
  }
}

export class UnlockEmployeeResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UnlockEmployeeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: UnlockEmployeeResponse): UnlockEmployeeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UnlockEmployeeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UnlockEmployeeResponse;
  static deserializeBinaryFromReader(message: UnlockEmployeeResponse, reader: jspb.BinaryReader): UnlockEmployeeResponse;
}

export namespace UnlockEmployeeResponse {
  export type AsObject = {
  }
}

export class FireEmployeeRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getReason(): string;
  setReason(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FireEmployeeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: FireEmployeeRequest): FireEmployeeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FireEmployeeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FireEmployeeRequest;
  static deserializeBinaryFromReader(message: FireEmployeeRequest, reader: jspb.BinaryReader): FireEmployeeRequest;
}

export namespace FireEmployeeRequest {
  export type AsObject = {
    userId: string,
    reason: string,
  }
}

export class FireEmployeeResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FireEmployeeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: FireEmployeeResponse): FireEmployeeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FireEmployeeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FireEmployeeResponse;
  static deserializeBinaryFromReader(message: FireEmployeeResponse, reader: jspb.BinaryReader): FireEmployeeResponse;
}

export namespace FireEmployeeResponse {
  export type AsObject = {
  }
}

export class CheckPasswordRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getPassword(): string;
  setPassword(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CheckPasswordRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CheckPasswordRequest): CheckPasswordRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CheckPasswordRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CheckPasswordRequest;
  static deserializeBinaryFromReader(message: CheckPasswordRequest, reader: jspb.BinaryReader): CheckPasswordRequest;
}

export namespace CheckPasswordRequest {
  export type AsObject = {
    userId: string,
    password: string,
  }
}

export class CheckPasswordResponse extends jspb.Message {
  getValid(): boolean;
  setValid(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CheckPasswordResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CheckPasswordResponse): CheckPasswordResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CheckPasswordResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CheckPasswordResponse;
  static deserializeBinaryFromReader(message: CheckPasswordResponse, reader: jspb.BinaryReader): CheckPasswordResponse;
}

export namespace CheckPasswordResponse {
  export type AsObject = {
    valid: boolean,
  }
}

export class InitPasswordResetRequest extends jspb.Message {
  getLogin(): string;
  setLogin(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InitPasswordResetRequest.AsObject;
  static toObject(includeInstance: boolean, msg: InitPasswordResetRequest): InitPasswordResetRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: InitPasswordResetRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InitPasswordResetRequest;
  static deserializeBinaryFromReader(message: InitPasswordResetRequest, reader: jspb.BinaryReader): InitPasswordResetRequest;
}

export namespace InitPasswordResetRequest {
  export type AsObject = {
    login: string,
  }
}

export class InitPasswordResetResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InitPasswordResetResponse.AsObject;
  static toObject(includeInstance: boolean, msg: InitPasswordResetResponse): InitPasswordResetResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: InitPasswordResetResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InitPasswordResetResponse;
  static deserializeBinaryFromReader(message: InitPasswordResetResponse, reader: jspb.BinaryReader): InitPasswordResetResponse;
}

export namespace InitPasswordResetResponse {
  export type AsObject = {
  }
}

export class ResetPasswordRequest extends jspb.Message {
  getResetToken(): string;
  setResetToken(value: string): void;

  getNewPassword(): string;
  setNewPassword(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResetPasswordRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ResetPasswordRequest): ResetPasswordRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ResetPasswordRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResetPasswordRequest;
  static deserializeBinaryFromReader(message: ResetPasswordRequest, reader: jspb.BinaryReader): ResetPasswordRequest;
}

export namespace ResetPasswordRequest {
  export type AsObject = {
    resetToken: string,
    newPassword: string,
  }
}

export class ResetPasswordResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ResetPasswordResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ResetPasswordResponse): ResetPasswordResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ResetPasswordResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ResetPasswordResponse;
  static deserializeBinaryFromReader(message: ResetPasswordResponse, reader: jspb.BinaryReader): ResetPasswordResponse;
}

export namespace ResetPasswordResponse {
  export type AsObject = {
  }
}

export class ValidatePasswordResetTokenRequest extends jspb.Message {
  getResetToken(): string;
  setResetToken(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValidatePasswordResetTokenRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ValidatePasswordResetTokenRequest): ValidatePasswordResetTokenRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ValidatePasswordResetTokenRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValidatePasswordResetTokenRequest;
  static deserializeBinaryFromReader(message: ValidatePasswordResetTokenRequest, reader: jspb.BinaryReader): ValidatePasswordResetTokenRequest;
}

export namespace ValidatePasswordResetTokenRequest {
  export type AsObject = {
    resetToken: string,
  }
}

export class ValidatePasswordResetTokenResponse extends jspb.Message {
  getValid(): boolean;
  setValid(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValidatePasswordResetTokenResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ValidatePasswordResetTokenResponse): ValidatePasswordResetTokenResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ValidatePasswordResetTokenResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValidatePasswordResetTokenResponse;
  static deserializeBinaryFromReader(message: ValidatePasswordResetTokenResponse, reader: jspb.BinaryReader): ValidatePasswordResetTokenResponse;
}

export namespace ValidatePasswordResetTokenResponse {
  export type AsObject = {
    valid: boolean,
  }
}

export class GetUserResetTokenRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetUserResetTokenRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetUserResetTokenRequest): GetUserResetTokenRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetUserResetTokenRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetUserResetTokenRequest;
  static deserializeBinaryFromReader(message: GetUserResetTokenRequest, reader: jspb.BinaryReader): GetUserResetTokenRequest;
}

export namespace GetUserResetTokenRequest {
  export type AsObject = {
    userId: string,
  }
}

export class GetUserResetTokenResponse extends jspb.Message {
  getResetToken(): string;
  setResetToken(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetUserResetTokenResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetUserResetTokenResponse): GetUserResetTokenResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetUserResetTokenResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetUserResetTokenResponse;
  static deserializeBinaryFromReader(message: GetUserResetTokenResponse, reader: jspb.BinaryReader): GetUserResetTokenResponse;
}

export namespace GetUserResetTokenResponse {
  export type AsObject = {
    resetToken: string,
  }
}

export class LoginRequest extends jspb.Message {
  getLogin(): string;
  setLogin(value: string): void;

  getPassword(): string;
  setPassword(value: string): void;

  getDeviceToken(): string;
  setDeviceToken(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoginRequest.AsObject;
  static toObject(includeInstance: boolean, msg: LoginRequest): LoginRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LoginRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoginRequest;
  static deserializeBinaryFromReader(message: LoginRequest, reader: jspb.BinaryReader): LoginRequest;
}

export namespace LoginRequest {
  export type AsObject = {
    login: string,
    password: string,
    deviceToken: string,
  }
}

export class LoginResponse extends jspb.Message {
  getSessionToken(): string;
  setSessionToken(value: string): void;

  getNeedPasswordChange(): boolean;
  setNeedPasswordChange(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LoginResponse.AsObject;
  static toObject(includeInstance: boolean, msg: LoginResponse): LoginResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LoginResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LoginResponse;
  static deserializeBinaryFromReader(message: LoginResponse, reader: jspb.BinaryReader): LoginResponse;
}

export namespace LoginResponse {
  export type AsObject = {
    sessionToken: string,
    needPasswordChange: boolean,
  }
}

export class LogoutRequest extends jspb.Message {
  getSessionToken(): string;
  setSessionToken(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): LogoutRequest.AsObject;
  static toObject(includeInstance: boolean, msg: LogoutRequest): LogoutRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: LogoutRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): LogoutRequest;
  static deserializeBinaryFromReader(message: LogoutRequest, reader: jspb.BinaryReader): LogoutRequest;
}

export namespace LogoutRequest {
  export type AsObject = {
    sessionToken: string,
  }
}

export class ValidateSessionRequest extends jspb.Message {
  getSessionToken(): string;
  setSessionToken(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValidateSessionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ValidateSessionRequest): ValidateSessionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ValidateSessionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValidateSessionRequest;
  static deserializeBinaryFromReader(message: ValidateSessionRequest, reader: jspb.BinaryReader): ValidateSessionRequest;
}

export namespace ValidateSessionRequest {
  export type AsObject = {
    sessionToken: string,
  }
}

export class ValidateSessionResponse extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ValidateSessionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ValidateSessionResponse): ValidateSessionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ValidateSessionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ValidateSessionResponse;
  static deserializeBinaryFromReader(message: ValidateSessionResponse, reader: jspb.BinaryReader): ValidateSessionResponse;
}

export namespace ValidateSessionResponse {
  export type AsObject = {
    userId: string,
  }
}

export class ChangePasswordRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getOldPassword(): string;
  setOldPassword(value: string): void;

  getNewPassword(): string;
  setNewPassword(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ChangePasswordRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ChangePasswordRequest): ChangePasswordRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ChangePasswordRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ChangePasswordRequest;
  static deserializeBinaryFromReader(message: ChangePasswordRequest, reader: jspb.BinaryReader): ChangePasswordRequest;
}

export namespace ChangePasswordRequest {
  export type AsObject = {
    userId: string,
    oldPassword: string,
    newPassword: string,
  }
}

export class ListUserRequest extends jspb.Message {
  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_auth_common_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_auth_common_common_pb.PaginationRequest): void;

  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): v1_auth_common_dtos_pb.UserFilter | undefined;
  setFilter(value?: v1_auth_common_dtos_pb.UserFilter): void;

  hasSort(): boolean;
  clearSort(): void;
  getSort(): v1_auth_common_common_pb.Sort | undefined;
  setSort(value?: v1_auth_common_common_pb.Sort): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListUserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListUserRequest): ListUserRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListUserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListUserRequest;
  static deserializeBinaryFromReader(message: ListUserRequest, reader: jspb.BinaryReader): ListUserRequest;
}

export namespace ListUserRequest {
  export type AsObject = {
    pagination?: v1_auth_common_common_pb.PaginationRequest.AsObject,
    filter?: v1_auth_common_dtos_pb.UserFilter.AsObject,
    sort?: v1_auth_common_common_pb.Sort.AsObject,
  }
}

export class ListUserResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<v1_auth_common_dtos_pb.UserBriefInfo>;
  setItemsList(value: Array<v1_auth_common_dtos_pb.UserBriefInfo>): void;
  addItems(value?: v1_auth_common_dtos_pb.UserBriefInfo, index?: number): v1_auth_common_dtos_pb.UserBriefInfo;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListUserResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListUserResponse): ListUserResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListUserResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListUserResponse;
  static deserializeBinaryFromReader(message: ListUserResponse, reader: jspb.BinaryReader): ListUserResponse;
}

export namespace ListUserResponse {
  export type AsObject = {
    itemsList: Array<v1_auth_common_dtos_pb.UserBriefInfo.AsObject>,
  }
}

export class UpdateReferralUrlRequest extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  getReferralUrl(): string;
  setReferralUrl(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateReferralUrlRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateReferralUrlRequest): UpdateReferralUrlRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateReferralUrlRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateReferralUrlRequest;
  static deserializeBinaryFromReader(message: UpdateReferralUrlRequest, reader: jspb.BinaryReader): UpdateReferralUrlRequest;
}

export namespace UpdateReferralUrlRequest {
  export type AsObject = {
    userId: string,
    referralUrl: string,
  }
}

export class UpdateReferralUrlResponse extends jspb.Message {
  getUserId(): string;
  setUserId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateReferralUrlResponse.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateReferralUrlResponse): UpdateReferralUrlResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateReferralUrlResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateReferralUrlResponse;
  static deserializeBinaryFromReader(message: UpdateReferralUrlResponse, reader: jspb.BinaryReader): UpdateReferralUrlResponse;
}

export namespace UpdateReferralUrlResponse {
  export type AsObject = {
    userId: string,
  }
}

export class GetReferralUrlRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetReferralUrlRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetReferralUrlRequest): GetReferralUrlRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetReferralUrlRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetReferralUrlRequest;
  static deserializeBinaryFromReader(message: GetReferralUrlRequest, reader: jspb.BinaryReader): GetReferralUrlRequest;
}

export namespace GetReferralUrlRequest {
  export type AsObject = {
  }
}

export class GetReferralUrlResponse extends jspb.Message {
  getReferralUrl(): string;
  setReferralUrl(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetReferralUrlResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetReferralUrlResponse): GetReferralUrlResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetReferralUrlResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetReferralUrlResponse;
  static deserializeBinaryFromReader(message: GetReferralUrlResponse, reader: jspb.BinaryReader): GetReferralUrlResponse;
}

export namespace GetReferralUrlResponse {
  export type AsObject = {
    referralUrl: string,
  }
}

export class ListReferralUsersRequest extends jspb.Message {
  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_auth_common_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_auth_common_common_pb.PaginationRequest): void;

  hasSort(): boolean;
  clearSort(): void;
  getSort(): v1_auth_common_common_pb.Sort | undefined;
  setSort(value?: v1_auth_common_common_pb.Sort): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListReferralUsersRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListReferralUsersRequest): ListReferralUsersRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListReferralUsersRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListReferralUsersRequest;
  static deserializeBinaryFromReader(message: ListReferralUsersRequest, reader: jspb.BinaryReader): ListReferralUsersRequest;
}

export namespace ListReferralUsersRequest {
  export type AsObject = {
    pagination?: v1_auth_common_common_pb.PaginationRequest.AsObject,
    sort?: v1_auth_common_common_pb.Sort.AsObject,
  }
}

export class ListReferralUsersResponse extends jspb.Message {
  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_auth_common_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_auth_common_common_pb.PaginationResponse): void;

  clearReferralUsersList(): void;
  getReferralUsersList(): Array<v1_auth_common_dtos_pb.UserReferralInfo>;
  setReferralUsersList(value: Array<v1_auth_common_dtos_pb.UserReferralInfo>): void;
  addReferralUsers(value?: v1_auth_common_dtos_pb.UserReferralInfo, index?: number): v1_auth_common_dtos_pb.UserReferralInfo;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListReferralUsersResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListReferralUsersResponse): ListReferralUsersResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListReferralUsersResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListReferralUsersResponse;
  static deserializeBinaryFromReader(message: ListReferralUsersResponse, reader: jspb.BinaryReader): ListReferralUsersResponse;
}

export namespace ListReferralUsersResponse {
  export type AsObject = {
    pagination?: v1_auth_common_common_pb.PaginationResponse.AsObject,
    referralUsersList: Array<v1_auth_common_dtos_pb.UserReferralInfo.AsObject>,
  }
}

export class CreateUserRequest extends jspb.Message {
  hasUser(): boolean;
  clearUser(): void;
  getUser(): v1_auth_common_dtos_pb.UserBriefInfo | undefined;
  setUser(value?: v1_auth_common_dtos_pb.UserBriefInfo): void;

  getRawPassword(): string;
  setRawPassword(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateUserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateUserRequest): CreateUserRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateUserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateUserRequest;
  static deserializeBinaryFromReader(message: CreateUserRequest, reader: jspb.BinaryReader): CreateUserRequest;
}

export namespace CreateUserRequest {
  export type AsObject = {
    user?: v1_auth_common_dtos_pb.UserBriefInfo.AsObject,
    rawPassword: string,
  }
}

