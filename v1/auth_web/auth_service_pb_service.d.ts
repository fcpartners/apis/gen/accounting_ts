// package: fcp.accounting.v1.auth_web
// file: v1/auth_web/auth_service.proto

import * as v1_auth_web_auth_service_pb from "../../v1/auth_web/auth_service_pb";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import * as v1_auth_common_dtos_pb from "../../v1/auth_common/dtos_pb";
import {grpc} from "@improbable-eng/grpc-web";

type WebAuthServiceGetUser = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.GetUserRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.UserBriefInfo;
};

type WebAuthServiceCreateUser = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.CreateUserRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.User;
};

type WebAuthServiceUpdateUser = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.UpdateUserRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.UserBriefInfo;
};

type WebAuthServiceRegisterEmployee = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.RegisterEmployeeRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.RegisterEmployeeResponse;
};

type WebAuthServiceUpdateEmployee = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.UpdateEmployeeRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.UpdateEmployeeResponse;
};

type WebAuthServiceListEmployee = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.ListEmployeeRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.ListEmployeeResponse;
};

type WebAuthServiceLockEmployee = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.LockEmployeeRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.LockEmployeeResponse;
};

type WebAuthServiceUnlockEmployee = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.UnlockEmployeeRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.UnlockEmployeeResponse;
};

type WebAuthServiceFireEmployee = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.FireEmployeeRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.FireEmployeeResponse;
};

type WebAuthServiceCheckPassword = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.CheckPasswordRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.CheckPasswordResponse;
};

type WebAuthServiceInitPasswordReset = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.InitPasswordResetRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.InitPasswordResetResponse;
};

type WebAuthServiceValidatePasswordResetToken = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.ValidatePasswordResetTokenRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.ValidatePasswordResetTokenResponse;
};

type WebAuthServiceResetPassword = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.ResetPasswordRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.ResetPasswordResponse;
};

type WebAuthServiceGetUserResetToken = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.GetUserResetTokenRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.GetUserResetTokenResponse;
};

type WebAuthServiceLogin = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.LoginRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.LoginResponse;
};

type WebAuthServiceLogout = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.LogoutRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type WebAuthServiceValidateSession = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.ValidateSessionRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.ValidateSessionResponse;
};

type WebAuthServiceChangePassword = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.ChangePasswordRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type WebAuthServiceUpdateReferralUrl = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.UpdateReferralUrlRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.UpdateReferralUrlResponse;
};

type WebAuthServiceListUser = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.ListUserRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.ListUserResponse;
};

type WebAuthServiceListContactPerson = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.ListContactPersonRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.ListContactPersonResponse;
};

type WebAuthServiceSaveContactPerson = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.SaveContactPersonRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.SaveContactPersonResponse;
};

type WebAuthServiceDeleteContactPerson = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.DeleteContactPersonRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.DeleteContactPersonResponse;
};

type WebAuthServiceListLocation = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.ListLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.ListLocationResponse;
};

type WebAuthServiceSaveLocation = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.SaveLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.SaveLocationResponse;
};

type WebAuthServiceDeleteLocation = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.DeleteLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.DeleteLocationResponse;
};

type WebAuthServiceListDocument = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.ListDocumentRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.ListDocumentResponse;
};

type WebAuthServiceSaveDocument = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.SaveDocumentRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.SaveDocumentResponse;
};

type WebAuthServiceDeleteDocument = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.DeleteDocumentRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.DeleteDocumentResponse;
};

type WebAuthServiceGetDefaultLocation = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.GetDefaultLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.GetDefaultLocationResponse;
};

type WebAuthServiceGetReferralUrl = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.GetReferralUrlRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.GetReferralUrlResponse;
};

type WebAuthServiceListReferralUsers = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_web_auth_service_pb.ListReferralUsersRequest;
  readonly responseType: typeof v1_auth_web_auth_service_pb.ListReferralUsersResponse;
};

type WebAuthServiceGetCounterparty = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.GetCounterpartyRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.GetCounterpartyResponse;
};

type WebAuthServiceListCounterparty = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.ListCounterpartyRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.ListCounterpartyResponse;
};

type WebAuthServiceSaveCounterparty = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.SaveCounterpartyRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.SaveCounterpartyResponse;
};

type WebAuthServiceDeleteCounterparty = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.DeleteCounterpartyRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.DeleteCounterpartyResponse;
};

type WebAuthServiceGetCounterpartyLocation = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.GetCounterpartyLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.GetCounterpartyLocationResponse;
};

type WebAuthServiceListCounterpartyLocation = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.ListCounterpartyLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.ListCounterpartyLocationResponse;
};

type WebAuthServiceSaveCounterpartyLocation = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.SaveCounterpartyLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.SaveCounterpartyLocationResponse;
};

type WebAuthServiceDeleteCounterpartyLocation = {
  readonly methodName: string;
  readonly service: typeof WebAuthService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_auth_common_dtos_pb.DeleteCounterpartyLocationRequest;
  readonly responseType: typeof v1_auth_common_dtos_pb.DeleteCounterpartyLocationResponse;
};

export class WebAuthService {
  static readonly serviceName: string;
  static readonly GetUser: WebAuthServiceGetUser;
  static readonly CreateUser: WebAuthServiceCreateUser;
  static readonly UpdateUser: WebAuthServiceUpdateUser;
  static readonly RegisterEmployee: WebAuthServiceRegisterEmployee;
  static readonly UpdateEmployee: WebAuthServiceUpdateEmployee;
  static readonly ListEmployee: WebAuthServiceListEmployee;
  static readonly LockEmployee: WebAuthServiceLockEmployee;
  static readonly UnlockEmployee: WebAuthServiceUnlockEmployee;
  static readonly FireEmployee: WebAuthServiceFireEmployee;
  static readonly CheckPassword: WebAuthServiceCheckPassword;
  static readonly InitPasswordReset: WebAuthServiceInitPasswordReset;
  static readonly ValidatePasswordResetToken: WebAuthServiceValidatePasswordResetToken;
  static readonly ResetPassword: WebAuthServiceResetPassword;
  static readonly GetUserResetToken: WebAuthServiceGetUserResetToken;
  static readonly Login: WebAuthServiceLogin;
  static readonly Logout: WebAuthServiceLogout;
  static readonly ValidateSession: WebAuthServiceValidateSession;
  static readonly ChangePassword: WebAuthServiceChangePassword;
  static readonly UpdateReferralUrl: WebAuthServiceUpdateReferralUrl;
  static readonly ListUser: WebAuthServiceListUser;
  static readonly ListContactPerson: WebAuthServiceListContactPerson;
  static readonly SaveContactPerson: WebAuthServiceSaveContactPerson;
  static readonly DeleteContactPerson: WebAuthServiceDeleteContactPerson;
  static readonly ListLocation: WebAuthServiceListLocation;
  static readonly SaveLocation: WebAuthServiceSaveLocation;
  static readonly DeleteLocation: WebAuthServiceDeleteLocation;
  static readonly ListDocument: WebAuthServiceListDocument;
  static readonly SaveDocument: WebAuthServiceSaveDocument;
  static readonly DeleteDocument: WebAuthServiceDeleteDocument;
  static readonly GetDefaultLocation: WebAuthServiceGetDefaultLocation;
  static readonly GetReferralUrl: WebAuthServiceGetReferralUrl;
  static readonly ListReferralUsers: WebAuthServiceListReferralUsers;
  static readonly GetCounterparty: WebAuthServiceGetCounterparty;
  static readonly ListCounterparty: WebAuthServiceListCounterparty;
  static readonly SaveCounterparty: WebAuthServiceSaveCounterparty;
  static readonly DeleteCounterparty: WebAuthServiceDeleteCounterparty;
  static readonly GetCounterpartyLocation: WebAuthServiceGetCounterpartyLocation;
  static readonly ListCounterpartyLocation: WebAuthServiceListCounterpartyLocation;
  static readonly SaveCounterpartyLocation: WebAuthServiceSaveCounterpartyLocation;
  static readonly DeleteCounterpartyLocation: WebAuthServiceDeleteCounterpartyLocation;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class WebAuthServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  getUser(
    requestMessage: v1_auth_web_auth_service_pb.GetUserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.UserBriefInfo|null) => void
  ): UnaryResponse;
  getUser(
    requestMessage: v1_auth_web_auth_service_pb.GetUserRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.UserBriefInfo|null) => void
  ): UnaryResponse;
  createUser(
    requestMessage: v1_auth_web_auth_service_pb.CreateUserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.User|null) => void
  ): UnaryResponse;
  createUser(
    requestMessage: v1_auth_web_auth_service_pb.CreateUserRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.User|null) => void
  ): UnaryResponse;
  updateUser(
    requestMessage: v1_auth_web_auth_service_pb.UpdateUserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.UserBriefInfo|null) => void
  ): UnaryResponse;
  updateUser(
    requestMessage: v1_auth_web_auth_service_pb.UpdateUserRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.UserBriefInfo|null) => void
  ): UnaryResponse;
  registerEmployee(
    requestMessage: v1_auth_web_auth_service_pb.RegisterEmployeeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.RegisterEmployeeResponse|null) => void
  ): UnaryResponse;
  registerEmployee(
    requestMessage: v1_auth_web_auth_service_pb.RegisterEmployeeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.RegisterEmployeeResponse|null) => void
  ): UnaryResponse;
  updateEmployee(
    requestMessage: v1_auth_web_auth_service_pb.UpdateEmployeeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.UpdateEmployeeResponse|null) => void
  ): UnaryResponse;
  updateEmployee(
    requestMessage: v1_auth_web_auth_service_pb.UpdateEmployeeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.UpdateEmployeeResponse|null) => void
  ): UnaryResponse;
  listEmployee(
    requestMessage: v1_auth_web_auth_service_pb.ListEmployeeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.ListEmployeeResponse|null) => void
  ): UnaryResponse;
  listEmployee(
    requestMessage: v1_auth_web_auth_service_pb.ListEmployeeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.ListEmployeeResponse|null) => void
  ): UnaryResponse;
  lockEmployee(
    requestMessage: v1_auth_web_auth_service_pb.LockEmployeeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.LockEmployeeResponse|null) => void
  ): UnaryResponse;
  lockEmployee(
    requestMessage: v1_auth_web_auth_service_pb.LockEmployeeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.LockEmployeeResponse|null) => void
  ): UnaryResponse;
  unlockEmployee(
    requestMessage: v1_auth_web_auth_service_pb.UnlockEmployeeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.UnlockEmployeeResponse|null) => void
  ): UnaryResponse;
  unlockEmployee(
    requestMessage: v1_auth_web_auth_service_pb.UnlockEmployeeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.UnlockEmployeeResponse|null) => void
  ): UnaryResponse;
  fireEmployee(
    requestMessage: v1_auth_web_auth_service_pb.FireEmployeeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.FireEmployeeResponse|null) => void
  ): UnaryResponse;
  fireEmployee(
    requestMessage: v1_auth_web_auth_service_pb.FireEmployeeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.FireEmployeeResponse|null) => void
  ): UnaryResponse;
  checkPassword(
    requestMessage: v1_auth_web_auth_service_pb.CheckPasswordRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.CheckPasswordResponse|null) => void
  ): UnaryResponse;
  checkPassword(
    requestMessage: v1_auth_web_auth_service_pb.CheckPasswordRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.CheckPasswordResponse|null) => void
  ): UnaryResponse;
  initPasswordReset(
    requestMessage: v1_auth_web_auth_service_pb.InitPasswordResetRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.InitPasswordResetResponse|null) => void
  ): UnaryResponse;
  initPasswordReset(
    requestMessage: v1_auth_web_auth_service_pb.InitPasswordResetRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.InitPasswordResetResponse|null) => void
  ): UnaryResponse;
  validatePasswordResetToken(
    requestMessage: v1_auth_web_auth_service_pb.ValidatePasswordResetTokenRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.ValidatePasswordResetTokenResponse|null) => void
  ): UnaryResponse;
  validatePasswordResetToken(
    requestMessage: v1_auth_web_auth_service_pb.ValidatePasswordResetTokenRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.ValidatePasswordResetTokenResponse|null) => void
  ): UnaryResponse;
  resetPassword(
    requestMessage: v1_auth_web_auth_service_pb.ResetPasswordRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.ResetPasswordResponse|null) => void
  ): UnaryResponse;
  resetPassword(
    requestMessage: v1_auth_web_auth_service_pb.ResetPasswordRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.ResetPasswordResponse|null) => void
  ): UnaryResponse;
  getUserResetToken(
    requestMessage: v1_auth_web_auth_service_pb.GetUserResetTokenRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.GetUserResetTokenResponse|null) => void
  ): UnaryResponse;
  getUserResetToken(
    requestMessage: v1_auth_web_auth_service_pb.GetUserResetTokenRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.GetUserResetTokenResponse|null) => void
  ): UnaryResponse;
  login(
    requestMessage: v1_auth_web_auth_service_pb.LoginRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.LoginResponse|null) => void
  ): UnaryResponse;
  login(
    requestMessage: v1_auth_web_auth_service_pb.LoginRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.LoginResponse|null) => void
  ): UnaryResponse;
  logout(
    requestMessage: v1_auth_web_auth_service_pb.LogoutRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  logout(
    requestMessage: v1_auth_web_auth_service_pb.LogoutRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  validateSession(
    requestMessage: v1_auth_web_auth_service_pb.ValidateSessionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.ValidateSessionResponse|null) => void
  ): UnaryResponse;
  validateSession(
    requestMessage: v1_auth_web_auth_service_pb.ValidateSessionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.ValidateSessionResponse|null) => void
  ): UnaryResponse;
  changePassword(
    requestMessage: v1_auth_web_auth_service_pb.ChangePasswordRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  changePassword(
    requestMessage: v1_auth_web_auth_service_pb.ChangePasswordRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  updateReferralUrl(
    requestMessage: v1_auth_web_auth_service_pb.UpdateReferralUrlRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.UpdateReferralUrlResponse|null) => void
  ): UnaryResponse;
  updateReferralUrl(
    requestMessage: v1_auth_web_auth_service_pb.UpdateReferralUrlRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.UpdateReferralUrlResponse|null) => void
  ): UnaryResponse;
  listUser(
    requestMessage: v1_auth_web_auth_service_pb.ListUserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.ListUserResponse|null) => void
  ): UnaryResponse;
  listUser(
    requestMessage: v1_auth_web_auth_service_pb.ListUserRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.ListUserResponse|null) => void
  ): UnaryResponse;
  listContactPerson(
    requestMessage: v1_auth_common_dtos_pb.ListContactPersonRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListContactPersonResponse|null) => void
  ): UnaryResponse;
  listContactPerson(
    requestMessage: v1_auth_common_dtos_pb.ListContactPersonRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListContactPersonResponse|null) => void
  ): UnaryResponse;
  saveContactPerson(
    requestMessage: v1_auth_common_dtos_pb.SaveContactPersonRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveContactPersonResponse|null) => void
  ): UnaryResponse;
  saveContactPerson(
    requestMessage: v1_auth_common_dtos_pb.SaveContactPersonRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveContactPersonResponse|null) => void
  ): UnaryResponse;
  deleteContactPerson(
    requestMessage: v1_auth_common_dtos_pb.DeleteContactPersonRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteContactPersonResponse|null) => void
  ): UnaryResponse;
  deleteContactPerson(
    requestMessage: v1_auth_common_dtos_pb.DeleteContactPersonRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteContactPersonResponse|null) => void
  ): UnaryResponse;
  listLocation(
    requestMessage: v1_auth_common_dtos_pb.ListLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListLocationResponse|null) => void
  ): UnaryResponse;
  listLocation(
    requestMessage: v1_auth_common_dtos_pb.ListLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListLocationResponse|null) => void
  ): UnaryResponse;
  saveLocation(
    requestMessage: v1_auth_common_dtos_pb.SaveLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveLocationResponse|null) => void
  ): UnaryResponse;
  saveLocation(
    requestMessage: v1_auth_common_dtos_pb.SaveLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveLocationResponse|null) => void
  ): UnaryResponse;
  deleteLocation(
    requestMessage: v1_auth_common_dtos_pb.DeleteLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteLocationResponse|null) => void
  ): UnaryResponse;
  deleteLocation(
    requestMessage: v1_auth_common_dtos_pb.DeleteLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteLocationResponse|null) => void
  ): UnaryResponse;
  listDocument(
    requestMessage: v1_auth_common_dtos_pb.ListDocumentRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListDocumentResponse|null) => void
  ): UnaryResponse;
  listDocument(
    requestMessage: v1_auth_common_dtos_pb.ListDocumentRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListDocumentResponse|null) => void
  ): UnaryResponse;
  saveDocument(
    requestMessage: v1_auth_common_dtos_pb.SaveDocumentRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveDocumentResponse|null) => void
  ): UnaryResponse;
  saveDocument(
    requestMessage: v1_auth_common_dtos_pb.SaveDocumentRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveDocumentResponse|null) => void
  ): UnaryResponse;
  deleteDocument(
    requestMessage: v1_auth_common_dtos_pb.DeleteDocumentRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteDocumentResponse|null) => void
  ): UnaryResponse;
  deleteDocument(
    requestMessage: v1_auth_common_dtos_pb.DeleteDocumentRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteDocumentResponse|null) => void
  ): UnaryResponse;
  getDefaultLocation(
    requestMessage: v1_auth_common_dtos_pb.GetDefaultLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.GetDefaultLocationResponse|null) => void
  ): UnaryResponse;
  getDefaultLocation(
    requestMessage: v1_auth_common_dtos_pb.GetDefaultLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.GetDefaultLocationResponse|null) => void
  ): UnaryResponse;
  getReferralUrl(
    requestMessage: v1_auth_web_auth_service_pb.GetReferralUrlRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.GetReferralUrlResponse|null) => void
  ): UnaryResponse;
  getReferralUrl(
    requestMessage: v1_auth_web_auth_service_pb.GetReferralUrlRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.GetReferralUrlResponse|null) => void
  ): UnaryResponse;
  listReferralUsers(
    requestMessage: v1_auth_web_auth_service_pb.ListReferralUsersRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.ListReferralUsersResponse|null) => void
  ): UnaryResponse;
  listReferralUsers(
    requestMessage: v1_auth_web_auth_service_pb.ListReferralUsersRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_web_auth_service_pb.ListReferralUsersResponse|null) => void
  ): UnaryResponse;
  getCounterparty(
    requestMessage: v1_auth_common_dtos_pb.GetCounterpartyRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.GetCounterpartyResponse|null) => void
  ): UnaryResponse;
  getCounterparty(
    requestMessage: v1_auth_common_dtos_pb.GetCounterpartyRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.GetCounterpartyResponse|null) => void
  ): UnaryResponse;
  listCounterparty(
    requestMessage: v1_auth_common_dtos_pb.ListCounterpartyRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListCounterpartyResponse|null) => void
  ): UnaryResponse;
  listCounterparty(
    requestMessage: v1_auth_common_dtos_pb.ListCounterpartyRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListCounterpartyResponse|null) => void
  ): UnaryResponse;
  saveCounterparty(
    requestMessage: v1_auth_common_dtos_pb.SaveCounterpartyRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveCounterpartyResponse|null) => void
  ): UnaryResponse;
  saveCounterparty(
    requestMessage: v1_auth_common_dtos_pb.SaveCounterpartyRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveCounterpartyResponse|null) => void
  ): UnaryResponse;
  deleteCounterparty(
    requestMessage: v1_auth_common_dtos_pb.DeleteCounterpartyRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteCounterpartyResponse|null) => void
  ): UnaryResponse;
  deleteCounterparty(
    requestMessage: v1_auth_common_dtos_pb.DeleteCounterpartyRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteCounterpartyResponse|null) => void
  ): UnaryResponse;
  getCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.GetCounterpartyLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.GetCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  getCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.GetCounterpartyLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.GetCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  listCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.ListCounterpartyLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  listCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.ListCounterpartyLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.ListCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  saveCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.SaveCounterpartyLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  saveCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.SaveCounterpartyLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.SaveCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  deleteCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.DeleteCounterpartyLocationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
  deleteCounterpartyLocation(
    requestMessage: v1_auth_common_dtos_pb.DeleteCounterpartyLocationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_auth_common_dtos_pb.DeleteCounterpartyLocationResponse|null) => void
  ): UnaryResponse;
}

