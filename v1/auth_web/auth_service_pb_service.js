// package: fcp.accounting.v1.auth_web
// file: v1/auth_web/auth_service.proto

var v1_auth_web_auth_service_pb = require("../../v1/auth_web/auth_service_pb");
var google_protobuf_empty_pb = require("google-protobuf/google/protobuf/empty_pb");
var v1_auth_common_dtos_pb = require("../../v1/auth_common/dtos_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var WebAuthService = (function () {
  function WebAuthService() {}
  WebAuthService.serviceName = "fcp.accounting.v1.auth_web.WebAuthService";
  return WebAuthService;
}());

WebAuthService.GetUser = {
  methodName: "GetUser",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.GetUserRequest,
  responseType: v1_auth_common_dtos_pb.UserBriefInfo
};

WebAuthService.CreateUser = {
  methodName: "CreateUser",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.CreateUserRequest,
  responseType: v1_auth_common_dtos_pb.User
};

WebAuthService.UpdateUser = {
  methodName: "UpdateUser",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.UpdateUserRequest,
  responseType: v1_auth_common_dtos_pb.UserBriefInfo
};

WebAuthService.RegisterEmployee = {
  methodName: "RegisterEmployee",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.RegisterEmployeeRequest,
  responseType: v1_auth_web_auth_service_pb.RegisterEmployeeResponse
};

WebAuthService.UpdateEmployee = {
  methodName: "UpdateEmployee",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.UpdateEmployeeRequest,
  responseType: v1_auth_web_auth_service_pb.UpdateEmployeeResponse
};

WebAuthService.ListEmployee = {
  methodName: "ListEmployee",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.ListEmployeeRequest,
  responseType: v1_auth_web_auth_service_pb.ListEmployeeResponse
};

WebAuthService.LockEmployee = {
  methodName: "LockEmployee",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.LockEmployeeRequest,
  responseType: v1_auth_web_auth_service_pb.LockEmployeeResponse
};

WebAuthService.UnlockEmployee = {
  methodName: "UnlockEmployee",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.UnlockEmployeeRequest,
  responseType: v1_auth_web_auth_service_pb.UnlockEmployeeResponse
};

WebAuthService.FireEmployee = {
  methodName: "FireEmployee",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.FireEmployeeRequest,
  responseType: v1_auth_web_auth_service_pb.FireEmployeeResponse
};

WebAuthService.CheckPassword = {
  methodName: "CheckPassword",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.CheckPasswordRequest,
  responseType: v1_auth_web_auth_service_pb.CheckPasswordResponse
};

WebAuthService.InitPasswordReset = {
  methodName: "InitPasswordReset",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.InitPasswordResetRequest,
  responseType: v1_auth_web_auth_service_pb.InitPasswordResetResponse
};

WebAuthService.ValidatePasswordResetToken = {
  methodName: "ValidatePasswordResetToken",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.ValidatePasswordResetTokenRequest,
  responseType: v1_auth_web_auth_service_pb.ValidatePasswordResetTokenResponse
};

WebAuthService.ResetPassword = {
  methodName: "ResetPassword",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.ResetPasswordRequest,
  responseType: v1_auth_web_auth_service_pb.ResetPasswordResponse
};

WebAuthService.GetUserResetToken = {
  methodName: "GetUserResetToken",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.GetUserResetTokenRequest,
  responseType: v1_auth_web_auth_service_pb.GetUserResetTokenResponse
};

WebAuthService.Login = {
  methodName: "Login",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.LoginRequest,
  responseType: v1_auth_web_auth_service_pb.LoginResponse
};

WebAuthService.Logout = {
  methodName: "Logout",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.LogoutRequest,
  responseType: google_protobuf_empty_pb.Empty
};

WebAuthService.ValidateSession = {
  methodName: "ValidateSession",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.ValidateSessionRequest,
  responseType: v1_auth_web_auth_service_pb.ValidateSessionResponse
};

WebAuthService.ChangePassword = {
  methodName: "ChangePassword",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.ChangePasswordRequest,
  responseType: google_protobuf_empty_pb.Empty
};

WebAuthService.UpdateReferralUrl = {
  methodName: "UpdateReferralUrl",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.UpdateReferralUrlRequest,
  responseType: v1_auth_web_auth_service_pb.UpdateReferralUrlResponse
};

WebAuthService.ListUser = {
  methodName: "ListUser",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.ListUserRequest,
  responseType: v1_auth_web_auth_service_pb.ListUserResponse
};

WebAuthService.ListContactPerson = {
  methodName: "ListContactPerson",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.ListContactPersonRequest,
  responseType: v1_auth_common_dtos_pb.ListContactPersonResponse
};

WebAuthService.SaveContactPerson = {
  methodName: "SaveContactPerson",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.SaveContactPersonRequest,
  responseType: v1_auth_common_dtos_pb.SaveContactPersonResponse
};

WebAuthService.DeleteContactPerson = {
  methodName: "DeleteContactPerson",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.DeleteContactPersonRequest,
  responseType: v1_auth_common_dtos_pb.DeleteContactPersonResponse
};

WebAuthService.ListLocation = {
  methodName: "ListLocation",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.ListLocationRequest,
  responseType: v1_auth_common_dtos_pb.ListLocationResponse
};

WebAuthService.SaveLocation = {
  methodName: "SaveLocation",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.SaveLocationRequest,
  responseType: v1_auth_common_dtos_pb.SaveLocationResponse
};

WebAuthService.DeleteLocation = {
  methodName: "DeleteLocation",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.DeleteLocationRequest,
  responseType: v1_auth_common_dtos_pb.DeleteLocationResponse
};

WebAuthService.ListDocument = {
  methodName: "ListDocument",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.ListDocumentRequest,
  responseType: v1_auth_common_dtos_pb.ListDocumentResponse
};

WebAuthService.SaveDocument = {
  methodName: "SaveDocument",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.SaveDocumentRequest,
  responseType: v1_auth_common_dtos_pb.SaveDocumentResponse
};

WebAuthService.DeleteDocument = {
  methodName: "DeleteDocument",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.DeleteDocumentRequest,
  responseType: v1_auth_common_dtos_pb.DeleteDocumentResponse
};

WebAuthService.GetDefaultLocation = {
  methodName: "GetDefaultLocation",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.GetDefaultLocationRequest,
  responseType: v1_auth_common_dtos_pb.GetDefaultLocationResponse
};

WebAuthService.GetReferralUrl = {
  methodName: "GetReferralUrl",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.GetReferralUrlRequest,
  responseType: v1_auth_web_auth_service_pb.GetReferralUrlResponse
};

WebAuthService.ListReferralUsers = {
  methodName: "ListReferralUsers",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_web_auth_service_pb.ListReferralUsersRequest,
  responseType: v1_auth_web_auth_service_pb.ListReferralUsersResponse
};

WebAuthService.GetCounterparty = {
  methodName: "GetCounterparty",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.GetCounterpartyRequest,
  responseType: v1_auth_common_dtos_pb.GetCounterpartyResponse
};

WebAuthService.ListCounterparty = {
  methodName: "ListCounterparty",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.ListCounterpartyRequest,
  responseType: v1_auth_common_dtos_pb.ListCounterpartyResponse
};

WebAuthService.SaveCounterparty = {
  methodName: "SaveCounterparty",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.SaveCounterpartyRequest,
  responseType: v1_auth_common_dtos_pb.SaveCounterpartyResponse
};

WebAuthService.DeleteCounterparty = {
  methodName: "DeleteCounterparty",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.DeleteCounterpartyRequest,
  responseType: v1_auth_common_dtos_pb.DeleteCounterpartyResponse
};

WebAuthService.GetCounterpartyLocation = {
  methodName: "GetCounterpartyLocation",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.GetCounterpartyLocationRequest,
  responseType: v1_auth_common_dtos_pb.GetCounterpartyLocationResponse
};

WebAuthService.ListCounterpartyLocation = {
  methodName: "ListCounterpartyLocation",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.ListCounterpartyLocationRequest,
  responseType: v1_auth_common_dtos_pb.ListCounterpartyLocationResponse
};

WebAuthService.SaveCounterpartyLocation = {
  methodName: "SaveCounterpartyLocation",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.SaveCounterpartyLocationRequest,
  responseType: v1_auth_common_dtos_pb.SaveCounterpartyLocationResponse
};

WebAuthService.DeleteCounterpartyLocation = {
  methodName: "DeleteCounterpartyLocation",
  service: WebAuthService,
  requestStream: false,
  responseStream: false,
  requestType: v1_auth_common_dtos_pb.DeleteCounterpartyLocationRequest,
  responseType: v1_auth_common_dtos_pb.DeleteCounterpartyLocationResponse
};

exports.WebAuthService = WebAuthService;

function WebAuthServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

WebAuthServiceClient.prototype.getUser = function getUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.GetUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.createUser = function createUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.CreateUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.updateUser = function updateUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.UpdateUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.registerEmployee = function registerEmployee(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.RegisterEmployee, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.updateEmployee = function updateEmployee(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.UpdateEmployee, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.listEmployee = function listEmployee(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.ListEmployee, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.lockEmployee = function lockEmployee(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.LockEmployee, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.unlockEmployee = function unlockEmployee(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.UnlockEmployee, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.fireEmployee = function fireEmployee(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.FireEmployee, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.checkPassword = function checkPassword(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.CheckPassword, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.initPasswordReset = function initPasswordReset(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.InitPasswordReset, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.validatePasswordResetToken = function validatePasswordResetToken(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.ValidatePasswordResetToken, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.resetPassword = function resetPassword(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.ResetPassword, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.getUserResetToken = function getUserResetToken(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.GetUserResetToken, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.login = function login(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.Login, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.logout = function logout(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.Logout, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.validateSession = function validateSession(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.ValidateSession, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.changePassword = function changePassword(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.ChangePassword, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.updateReferralUrl = function updateReferralUrl(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.UpdateReferralUrl, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.listUser = function listUser(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.ListUser, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.listContactPerson = function listContactPerson(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.ListContactPerson, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.saveContactPerson = function saveContactPerson(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.SaveContactPerson, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.deleteContactPerson = function deleteContactPerson(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.DeleteContactPerson, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.listLocation = function listLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.ListLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.saveLocation = function saveLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.SaveLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.deleteLocation = function deleteLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.DeleteLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.listDocument = function listDocument(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.ListDocument, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.saveDocument = function saveDocument(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.SaveDocument, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.deleteDocument = function deleteDocument(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.DeleteDocument, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.getDefaultLocation = function getDefaultLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.GetDefaultLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.getReferralUrl = function getReferralUrl(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.GetReferralUrl, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.listReferralUsers = function listReferralUsers(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.ListReferralUsers, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.getCounterparty = function getCounterparty(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.GetCounterparty, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.listCounterparty = function listCounterparty(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.ListCounterparty, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.saveCounterparty = function saveCounterparty(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.SaveCounterparty, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.deleteCounterparty = function deleteCounterparty(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.DeleteCounterparty, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.getCounterpartyLocation = function getCounterpartyLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.GetCounterpartyLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.listCounterpartyLocation = function listCounterpartyLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.ListCounterpartyLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.saveCounterpartyLocation = function saveCounterpartyLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.SaveCounterpartyLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

WebAuthServiceClient.prototype.deleteCounterpartyLocation = function deleteCounterpartyLocation(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(WebAuthService.DeleteCounterpartyLocation, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.WebAuthServiceClient = WebAuthServiceClient;

